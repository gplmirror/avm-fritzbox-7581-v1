#include <sys/file.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#include "logging.h"

#ifdef LIBVORBIS_STDOUT_TRACES
static int	log_level =	LOGLEVEL;
static FILE *libvorbis_debug_stream = NULL;
#endif

/*---------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------*/
void libvorbis_open_stdout (void)
{
#ifdef LIBVORBIS_STDOUT_TRACES
  if (!libvorbis_debug_stream) {
    libvorbis_debug_stream = fopen ("/dev/console", "w");
  }
#endif
}

/*----------------------------------------------------------------------------*\
\*----------------------------------------------------------------------------*/
void libvorbis_writef (int level, const char * s, ...) {	/* libs printf */
#ifdef LIBVORBIS_STDOUT_TRACES
	if (!level || (level >= log_level)) {
		va_list	args;

		va_start (args, s);
		if (libvorbis_debug_stream) {
			vfprintf (libvorbis_debug_stream, s, args);
		}
		va_end (args);
	}
#endif
}
