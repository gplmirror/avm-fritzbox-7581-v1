# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Werner Baumann <werner.baumann@onlinehome.de>
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: dav-linuxfs@lists.sf.net\n"
"POT-Creation-Date: 2009-01-02 12:22+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/cache.c:578
msgid ""
"connection timed out two times;\n"
"trying one last time"
msgstr ""

#: src/cache.c:583
#, c-format
msgid "Last try succeeded.\n"
msgstr ""

#: src/cache.c:586
msgid ""
"server temporarily unreachable;\n"
"mounting anyway"
msgstr ""

#: src/cache.c:589
#, c-format
msgid ""
"Mounting failed.\n"
"%s"
msgstr ""

#: src/cache.c:627
#, c-format
msgid "can't replace %s with %s"
msgstr ""

#: src/cache.c:631
#, c-format
msgid "error writing new index file %s"
msgstr ""

#: src/cache.c:635
#, c-format
msgid "can't create new index file for %s"
msgstr ""

#: src/cache.c:2429 src/cache.c:2463
#, c-format
msgid "can't create cache file %s"
msgstr ""

#: src/cache.c:2488
#, c-format
msgid "error writing directory %s"
msgstr ""

#: src/cache.c:2633 src/mount_davfs.c:564 src/mount_davfs.c:566
#: src/mount_davfs.c:738 src/mount_davfs.c:740 src/mount_davfs.c:746
#: src/mount_davfs.c:786 src/mount_davfs.c:788 src/mount_davfs.c:805
#: src/mount_davfs.c:807 src/mount_davfs.c:1699 src/mount_davfs.c:1701
msgid "can't read user data base"
msgstr ""

#: src/cache.c:2644
#, c-format
msgid "can't open cache directory %s"
msgstr ""

#: src/cache.c:2659
#, c-format
msgid "can't create cache directory %s"
msgstr ""

#: src/cache.c:2666
#, c-format
msgid "can't access cache directory %s"
msgstr ""

#: src/cache.c:2669
#, c-format
msgid "wrong owner of cache directory %s"
msgstr ""

#: src/cache.c:2673
#, c-format
msgid "wrong permissions set for cache directory %s"
msgstr ""

#: src/cache.c:2703
msgid "found orphaned file in cache:"
msgstr ""

#: src/cache.c:2774
#, c-format
msgid "error parsing %s"
msgstr ""

#: src/cache.c:2775
#, c-format
msgid "  at line %i"
msgstr ""

#: src/cache.c:2820
#, c-format
msgid "open files exceed max cache size by %llu MiBytes"
msgstr ""

#: src/dav_coda2.c:161 src/dav_coda3.c:145 src/dav_fuse5.c:127
#: src/dav_fuse7.c:150
msgid "can't allocate message buffer"
msgstr ""

#: src/kernel_interface.c:95
msgid "trying fuse kernel file system"
msgstr ""

#: src/kernel_interface.c:103
msgid "fuse device opened successfully"
msgstr ""

#: src/kernel_interface.c:115
msgid "trying coda kernel file system"
msgstr ""

#: src/kernel_interface.c:121
msgid "coda device opened successfully"
msgstr ""

#: src/kernel_interface.c:129
#, c-format
msgid "unknown kernel file system %s"
msgstr ""

#: src/kernel_interface.c:176
msgid "no free coda device to mount"
msgstr ""

#: src/kernel_interface.c:185
#, c-format
msgid "CODA_KERNEL_VERSION %u can not be used on 64 bit systems"
msgstr ""

#: src/kernel_interface.c:195
#, c-format
msgid "CODA_KERNEL_VERSION %u not supported"
msgstr ""

#: src/kernel_interface.c:234
msgid "can't open fuse device"
msgstr ""

#: src/kernel_interface.c:279
msgid "can't mount using fuse kernel file system"
msgstr ""

#: src/mount_davfs.c:213
msgid "program is not setuid root"
msgstr ""

#: src/mount_davfs.c:215 src/mount_davfs.c:416 src/mount_davfs.c:420
msgid "can't change effective user id"
msgstr ""

#: src/mount_davfs.c:281
msgid "can't start daemon process"
msgstr ""

#: src/mount_davfs.c:304
msgid "can't release root privileges"
msgstr ""

#: src/mount_davfs.c:324
msgid "failed to release tty properly"
msgstr ""

#: src/mount_davfs.c:335
#, c-format
msgid "can't write pid file %s"
msgstr ""

#: src/mount_davfs.c:353
#, c-format
msgid "unmounting %s"
msgstr ""

#: src/mount_davfs.c:355
msgid "unmounting failed"
msgstr ""

#: src/mount_davfs.c:404 src/mount_davfs.c:467 src/mount_davfs.c:550
#: src/mount_davfs.c:810
#, c-format
msgid "group %s does not exist"
msgstr ""

#: src/mount_davfs.c:408
msgid "can't change group id"
msgstr ""

#: src/mount_davfs.c:413
#, c-format
msgid "user %s does not exist"
msgstr ""

#: src/mount_davfs.c:453 src/mount_davfs.c:535 src/mount_davfs.c:570
#, c-format
msgid "can't create directory %s"
msgstr ""

#: src/mount_davfs.c:457 src/mount_davfs.c:539 src/mount_davfs.c:574
#: src/mount_davfs.c:579 src/mount_davfs.c:588
#, c-format
msgid "can't access directory %s"
msgstr ""

#: src/mount_davfs.c:463 src/mount_davfs.c:545
#, c-format
msgid "can't change mode of directory %s"
msgstr ""

#: src/mount_davfs.c:472 src/mount_davfs.c:555
#, c-format
msgid "can't change group of directory %s"
msgstr ""

#: src/mount_davfs.c:583 src/mount_davfs.c:791
msgid "can't read group data base"
msgstr ""

#: src/mount_davfs.c:606
#, c-format
msgid "can't open file %s"
msgstr ""

#: src/mount_davfs.c:611
#, c-format
msgid "%s is already mounted on %s"
msgstr ""

#: src/mount_davfs.c:634
#, c-format
msgid ""
"found PID file %s.\n"
"Either %s is used by another process,\n"
"or another mount process ended irregular"
msgstr ""

#: src/mount_davfs.c:657
#, c-format
msgid "no entry for %s found in %s"
msgstr ""

#: src/mount_davfs.c:663
#, c-format
msgid "different URL in %s"
msgstr ""

#: src/mount_davfs.c:671
#, c-format
msgid "different file system type in %s"
msgstr ""

#: src/mount_davfs.c:674
#, c-format
msgid "different config file in %s"
msgstr ""

#: src/mount_davfs.c:676
#, c-format
msgid "option `user' not set in %s"
msgstr ""

#: src/mount_davfs.c:678
#, c-format
msgid "different mount options in %s"
msgstr ""

#: src/mount_davfs.c:681
#, c-format
msgid "different uid in %s"
msgstr ""

#: src/mount_davfs.c:683
#, c-format
msgid "different gid in %s"
msgstr ""

#: src/mount_davfs.c:685
#, c-format
msgid "different dir_mode in %s"
msgstr ""

#: src/mount_davfs.c:687
#, c-format
msgid "different file_mode in %s"
msgstr ""

#: src/mount_davfs.c:715
#, c-format
msgid "can't evaluate path of mount point %s"
msgstr ""

#: src/mount_davfs.c:720
#, c-format
msgid "can't get home directory for uid %i"
msgstr ""

#: src/mount_davfs.c:722
msgid "A relative mount point must lie within your home directory"
msgstr ""

#: src/mount_davfs.c:730
#, c-format
msgid "invalid mount point %s"
msgstr ""

#: src/mount_davfs.c:733
#, c-format
msgid "mount point %s does not exist"
msgstr ""

#: src/mount_davfs.c:751
#, c-format
msgid ""
"%s is the home directory of user %s.\n"
"You can't mount into another users home directory"
msgstr ""

#: src/mount_davfs.c:779
msgid "you can't set file owner different from your uid"
msgstr ""

#: src/mount_davfs.c:797
msgid "you must be member of the group of the file system"
msgstr ""

#: src/mount_davfs.c:817
#, c-format
msgid "user %s must be member of group %s"
msgstr ""

#: src/mount_davfs.c:840
#, c-format
msgid "can't mount %s on %s"
msgstr ""

#: src/mount_davfs.c:842
#, c-format
msgid "kernel does not know file system %s"
msgstr ""

#: src/mount_davfs.c:844
msgid "mount point is busy"
msgstr ""

#: src/mount_davfs.c:905 src/umount_davfs.c:80
#, c-format
msgid ""
"This is free software; see the source for copying conditions.  There is NO\n"
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"
msgstr ""

#: src/mount_davfs.c:921 src/umount_davfs.c:105
msgid "unknown error parsing arguments"
msgstr ""

#: src/mount_davfs.c:930 src/umount_davfs.c:111
msgid "missing argument"
msgstr ""

#: src/mount_davfs.c:943 src/umount_davfs.c:113
msgid "too many arguments"
msgstr ""

#: src/mount_davfs.c:949
msgid "no mountpoint specified"
msgstr ""

#: src/mount_davfs.c:954
msgid "no WebDAV-server specified"
msgstr ""

#: src/mount_davfs.c:957
msgid "invalid URL"
msgstr ""

#: src/mount_davfs.c:1000
msgid "can't determine home directory"
msgstr ""

#: src/mount_davfs.c:1078 src/webdav.c:378
#, c-format
msgid "can't read client certificate %s"
msgstr ""

#: src/mount_davfs.c:1083
#, c-format
msgid "client certificate file %s has wrong owner"
msgstr ""

#: src/mount_davfs.c:1089
#, c-format
msgid "client certificate file %s has wrong permissions"
msgstr ""

#: src/mount_davfs.c:1132
#, c-format
msgid ""
"Please enter the username to authenticate with proxy\n"
"%s or hit enter for none.\n"
msgstr ""

#: src/mount_davfs.c:1134 src/mount_davfs.c:1150
msgid "Username:"
msgstr ""

#: src/mount_davfs.c:1138
#, c-format
msgid ""
"Please enter the password to authenticate user %s with proxy\n"
"%s or hit enter for none.\n"
msgstr ""

#: src/mount_davfs.c:1140 src/mount_davfs.c:1156 src/webdav.c:385
msgid "Password: "
msgstr ""

#: src/mount_davfs.c:1148
#, c-format
msgid ""
"Please enter the username to authenticate with server\n"
"%s or hit enter for none.\n"
msgstr ""

#: src/mount_davfs.c:1154
#, c-format
msgid ""
"Please enter the password to authenticate user %s with server\n"
"%s or hit enter for none.\n"
msgstr ""

#: src/mount_davfs.c:1203
#, c-format
msgid "pid %i, got signal %i"
msgstr ""

#: src/mount_davfs.c:1235
msgid ""
"Warning: can't read user data base. Mounting anyway, but there is no entry "
"in mtab."
msgstr ""

#: src/mount_davfs.c:1252
msgid ""
"Warning: can't write entry into mtab, but will mount the file system anyway"
msgstr ""

#: src/mount_davfs.c:1278
#, c-format
msgid "option %s has invalid argument;it must be a decimal number"
msgstr ""

#: src/mount_davfs.c:1281
#, c-format
msgid "option %s has invalid argument;it must be an octal number"
msgstr ""

#: src/mount_davfs.c:1284
#, c-format
msgid "option %s has invalid argument;it must be a number"
msgstr ""

#: src/mount_davfs.c:1555
#, c-format
msgid "option %s requires argument"
msgstr ""

#: src/mount_davfs.c:1661
#, c-format
msgid "Unknown option %s.\n"
msgstr ""

#: src/mount_davfs.c:2118 src/mount_davfs.c:2340 src/mount_davfs.c:2352
#, c-format
msgid "opening %s failed"
msgstr ""

#: src/mount_davfs.c:2139 src/mount_davfs.c:2267 src/mount_davfs.c:2368
#: src/mount_davfs.c:2447
msgid "malformed line"
msgstr ""

#: src/mount_davfs.c:2248 src/mount_davfs.c:2261
msgid "unknown option"
msgstr ""

#: src/mount_davfs.c:2344
#, c-format
msgid "file %s has wrong owner"
msgstr ""

#: src/mount_davfs.c:2347
#, c-format
msgid "file %s has wrong permissions"
msgstr ""

#: src/mount_davfs.c:2506
#, c-format
msgid ""
"Usage:\n"
"    %s -V,--version   : print version string\n"
"    %s -h,--help      : print this message\n"
"\n"
msgstr ""

#: src/mount_davfs.c:2510
#, c-format
msgid ""
"To mount a WebDAV-resource don't call %s directly, but use\n"
"`mount' instead.\n"
msgstr ""

#: src/mount_davfs.c:2512
#, c-format
msgid ""
"    mount <mountpoint>  : or\n"
"    mount <server-url>  : mount the WebDAV-resource as specified in\n"
"                          /etc/fstab.\n"
msgstr ""

#: src/mount_davfs.c:2515
#, c-format
msgid ""
"    mount -t davfs <server-url> <mountpoint> [-o options]\n"
"                        : mount the WebDAV-resource <server-url>\n"
"                          on mountpoint <mountpoint>. Only root\n"
"                          is allowed to do this. options is a\n"
"                          comma separated list of options.\n"
"\n"
msgstr ""

#: src/mount_davfs.c:2520
#, c-format
msgid ""
"Recognised options:\n"
"    conf=        : absolute path of user configuration file\n"
"    uid=         : owner of the filesystem (username or numeric id)\n"
"    gid=         : group of the filesystem (group name or numeric id)\n"
"    file_mode=   : default file mode (octal)\n"
"    dir_mode=    : default directory mode (octal)\n"
msgstr ""

#: src/mount_davfs.c:2526
#, c-format
msgid ""
"    ro           : mount read-only\n"
"    rw           : mount read-write\n"
"    [no]exec     : (don't) allow execution of binaries\n"
"    [no]suid     : (don't) allow suid and sgid bits to take effect\n"
"    [no]_netdev  : (no) network connection needed\n"
msgstr ""

#: src/umount_davfs.c:86
#, c-format
msgid ""
"Usage:\n"
"    u%s -V,--version  : print version string\n"
"    u%s -h,--help     : print this message\n"
"\n"
msgstr ""

#: src/umount_davfs.c:90
#, c-format
msgid ""
"To umount a WebDAV-resource don't call u%s directly, but use\n"
"`umount' instead.\n"
msgstr ""

#: src/umount_davfs.c:93
#, c-format
msgid ""
"    umount <mountpoint> : umount the WebDAV-resource as specified in\n"
"                          /etc/fstab.\n"
msgstr ""

#: src/umount_davfs.c:119
msgid "can't determine mount point"
msgstr ""

#: src/umount_davfs.c:139
#, c-format
msgid ""
"\n"
"  can't read PID from file %s;\n"
"  trying to unmount anyway;\n"
"  please wait for %s to terminate"
msgstr ""

#: src/umount_davfs.c:151
#, c-format
msgid ""
"\n"
"  can't read process list;\n"
"  trying to unmount anyway;\n"
"  please wait for %s to terminate"
msgstr ""

#: src/umount_davfs.c:167
#, c-format
msgid ""
"\n"
"  can't find %s-process with pid %s;\n"
"  trying to unmount anyway.\n"
"  you propably have to remove %s manually"
msgstr ""

#: src/umount_davfs.c:178
#, c-format
msgid "%s: waiting while %s (pid %s) synchronizes the cache ."
msgstr ""

#: src/umount_davfs.c:191
#, c-format
msgid "an error occured while waiting; please wait for %s to terminate"
msgstr ""

#: src/webdav.c:311
msgid "socket library initialization failed"
msgstr ""

#: src/webdav.c:324
msgid "can't open stream to log neon-messages"
msgstr ""

#: src/webdav.c:357
msgid "neon library does not support TLS/SSL"
msgstr ""

#: src/webdav.c:365
#, c-format
msgid "can't read server certificate %s"
msgstr ""

#: src/webdav.c:383
#, c-format
msgid ""
"Please enter the password to decrypt client\n"
"certificate %s.\n"
msgstr ""

#: src/webdav.c:397
#, c-format
msgid "can't decrypt client certificate %s"
msgstr ""

#: src/webdav.c:450 src/webdav.c:453
msgid "mounting failed; the server does not support WebDAV"
msgstr ""

#: src/webdav.c:461
msgid "warning: the server does not support locks"
msgstr ""

#: src/webdav.c:1501
msgid "authentication failure:"
msgstr ""

#: src/webdav.c:1539
#, c-format
msgid "%i can't open cache file"
msgstr ""

#: src/webdav.c:1547
#, c-format
msgid "%i error writing to cache file"
msgstr ""

#: src/webdav.c:1852 src/webdav.c:1855
msgid "error processing server certificate"
msgstr ""

#: src/webdav.c:1862 src/webdav.c:1898
msgid "the server certificate is not yet valid"
msgstr ""

#: src/webdav.c:1864 src/webdav.c:1901
msgid "the server certificate has expired"
msgstr ""

#: src/webdav.c:1866 src/webdav.c:1904
msgid "the server certificate does not match the server name"
msgstr ""

#: src/webdav.c:1868 src/webdav.c:1907
msgid "the server certificate is not trusted"
msgstr ""

#: src/webdav.c:1870 src/webdav.c:1910
msgid "unknown certificate error"
msgstr ""

#: src/webdav.c:1871
#, c-format
msgid "  issuer:      %s"
msgstr ""

#: src/webdav.c:1873
#, c-format
msgid "  subject:     %s"
msgstr ""

#: src/webdav.c:1875
#, c-format
msgid "  identity:    %s"
msgstr ""

#: src/webdav.c:1877
#, c-format
msgid "  fingerprint: %s"
msgstr ""

#: src/webdav.c:1880
#, c-format
msgid ""
"You only should accept this certificate, if you can\n"
"verify the fingerprint! The server might be faked\n"
"or there might be a man-in-the-middle-attack.\n"
msgstr ""

#: src/webdav.c:1883
#, c-format
msgid "Accept certificate for this session? [y,N] "
msgstr ""

#: src/webdav.c:1911
#, c-format
msgid "  issuer: %s"
msgstr ""

#: src/webdav.c:1912
#, c-format
msgid "  subject: %s"
msgstr ""

#: src/webdav.c:1913
#, c-format
msgid "  identity: %s"
msgstr ""

#: src/webdav.c:1916
msgid "  accepted by user"
msgstr ""
