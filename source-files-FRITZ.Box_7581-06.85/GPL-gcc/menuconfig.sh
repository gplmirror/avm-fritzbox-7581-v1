#! /bin/bash

PWD=`pwd`

Source=${PWD}/menuconfig.menu
Dest=${PWD}/menuconfig.res
MenuName=MenueAuswahl
PerlFile=Generate/AutoHtmlScripte/MenueSourcen/Menu_Starter.Pl
rm -f $Dest
eval `resize`
( cd .. ; perl -x $PerlFile $Source $Dest $MenuName -X${COLUMNS} -Y${LINES} -Fix )

Result=`cat $Dest`
case $Result in
    LineId@buildroot)
        make $1
        sh $0 $*
        ;;
    LineId@uclibc)
        make $2
        sh $0 $*
        ;;
    LineId@busybox)
        make $3
        sh $0 $*
        ;;
    *)
        ;;
esac

