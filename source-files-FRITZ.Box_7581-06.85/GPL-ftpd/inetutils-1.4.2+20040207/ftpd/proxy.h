#ifdef PUMA6_ARM

// Proxy for the ftp control connection to another ftp server (the ATOM in this case)

int do_proxy(int client_fd, struct sockaddr_storage *server_addr, struct sockaddr_storage *client_addr);

#endif // PUMA6_ARM
