export PROJECTDEFS  = -DAR7 -DOSIP_RETRANSMIT_2XX


export PROJECTCFLAGS = -Os -fomit-frame-pointer

export PROJECTCXXFLAGS = $(PROJECTCFLAGS) \
                         -fno-rtti -fno-exceptions -fno-unwind-tables \
		         -fno-use-cxa-atexit  -fno-vtable-gc

export SED=/bin/sed
export EGREP=/bin/egrep

EWNWROOTFS=uclibc

ifeq ($(EWNWROOTFS),uclibc)
TARGET=mipsel-uclibc-linux
TARGETFS=$(HOME)/filesystem
TARGETPATH=$(HOME)/mipsel-uclibc-linux/bin
#TARGETPATH=/home/mipsel/mipsel-uclibc-linux/bin
CROSSTARGET=mipsel-glibc-linux
endif

HOSTCC=gcc

export PATH := $(TARGETPATH):$(PATH)

export LC_CTYPE=C
