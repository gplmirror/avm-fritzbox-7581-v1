#
# This file replaces the entityInfoArray in eid.c
# (See also eid_bcm_mgmt.txt and cms_eid.h)
#
# Fields not set here are initialized to zero or NULL.
# All field values must be on a single line
#

BEGIN
  eid         = EID_SMD
  name        = smd
  flags = EIF_MESSAGING_CAPABLE|EIF_DESKTOP_LINUX_CAPABLE|EIF_SET_CGROUPS
  cpuGroupName = normal/default
END

BEGIN
  eid         = EID_SSK
  name        = ssk
  flags = EIF_LAUNCH_IN_STAGE_1|EIF_MESSAGING_CAPABLE|EIF_MSG_TERMINATE|EIF_DESKTOP_LINUX_CAPABLE|EIF_SET_CGROUPS|EIF_SET_SCHED
  schedPolicy = SCHED_RR
  schedPriority = BCM_RTPRIO_DATA
  cpuGroupName = rt/data
END

# AVM: no dhcpd for fritz!os
# BEGIN
#   eid         = EID_DHCPD
#   name        = dhcpd
#   flags       = EIF_MESSAGING_CAPABLE
# END

# AVM: no dhcpc for fritz!os
# BEGIN
#   eid         = EID_DHCPC
#   name        = dhcpc
#   runArgs     = -f
#   flags       = EIF_MESSAGING_CAPABLE|EIF_MULTIPLE_INSTANCES
# END

# AVM: no pppd for fritz!os
# BEGIN
#   eid         = EID_PPP
#   name        = pppd
#   runArgs     = -c
#   flags       = EIF_MESSAGING_CAPABLE|EIF_MULTIPLE_INSTANCES
# END

# AVM: no bftpd for fritz!os
# BEGIN
#   eid         = EID_FTPD         /* this is the ftp server */
#   name        = bftpd
#   flags       = EIF_SERVER|EIF_SERVER_TCP|EIF_MESSAGING_CAPABLE|EIF_IPV6
#   backLog     = 1
#   port        = FTPD_PORT
# END

# AVM: no ftp for fritz!os
# BEGIN
#   eid         = EID_FTP         /* this is the ftp client */
#   name        = ftp
#   flags       = EIF_MESSAGING_CAPABLE|EIF_DESKTOP_LINUX_CAPABLE
# END

# AVM: no tftpd for fritz!os
# BEGIN
#   eid         = EID_TFTPD        /* this is the tftp server */
#   name        = tftpd
#   flags = EIF_SERVER|EIF_MESSAGING_CAPABLE|EIF_DESKTOP_LINUX_CAPABLE|EIF_IPV6
#   port        = TFTPD_PORT
# END

# AVM: no tftp with connection to broadcom CMS for fritz!os
BEGIN
  eid         = EID_TFTP         /* this is the tftp client */
  name        = tftp
  flags       = EIF_MESSAGING_CAPABLE|EIF_DESKTOP_LINUX_CAPABLE
END

BEGIN
  eid         = EID_DNSPROBE
  name        = dnsprobe
END

# AVM: no dnsproxy for fritz!os
# BEGIN
#   eid         = EID_DNSPROXY
#   name        = dnsproxy
#   flags       = EIF_MESSAGING_CAPABLE|EIF_AUTO_RELAUNCH
# END

BEGIN
  eid         = EID_SYSLOGD
  name        = syslogd
  runArgs     = -n
END

BEGIN
  eid         = EID_KLOGD
  name        = klogd
  runArgs     = -n
END

# AVM: no ddnsd for fritz!os
# BEGIN
#   eid         = EID_DDNSD
#   name        = ddnsd
#   flags       = EIF_MESSAGING_CAPABLE
# END

# AVM: no zebra for fritz!os
# BEGIN
#   eid         = EID_ZEBRA
#   name        = zebra
#   flags       = EIF_MESSAGING_CAPABLE
# END

# AVM: no ripd for fritz!os
# BEGIN
#   eid         = EID_RIPD
#   name        = ripd
#   flags       = EIF_MESSAGING_CAPABLE
# END

# AVM: no sntp for fritz!os
# BEGIN
#   eid         = EID_SNTP
#   name        = sntp
#   flags       = EIF_MESSAGING_CAPABLE|EIF_MSG_TERMINATE
# END

# AVM: no urlfilterd for fritz!os
# BEGIN
#   eid         = EID_URLFILTERD
#   name        = urlfilterd
#   flags       = EIF_MESSAGING_CAPABLE
# END

BEGIN
  eid         = EID_MCPD
  name        = mcpd
  flags       = EIF_MESSAGING_CAPABLE|EIF_MSG_TERMINATE
END

BEGIN
  eid         = EID_MCPCTL
  name        = mcpctl
  flags       = EIF_MESSAGING_CAPABLE|EIF_MULTIPLE_INSTANCES
END

BEGIN
  eid         = EID_WLMNGR
  name        = WLMNGR-DAEMON
  path        = /bin/wlmngr
  altPath     = /bin/wlmngr2
  flags       = EIF_MDM|EIF_MESSAGING_CAPABLE|EIF_LAUNCH_ON_BOOT|EIF_USE_ALTPATH_FOR_TR181
END

BEGIN
  eid         = EID_WLNVRAM
  name        = WLNVRAM
  path        = /bin/nvram
  flags       = EIF_MESSAGING_CAPABLE
END

BEGIN
  eid         = EID_WLEVENT
  name        = WLEVENT
  path        = /bin/wlevt
  flags       = EIF_MESSAGING_CAPABLE
END

BEGIN
  eid         = EID_WLWPS
  name        = WLWPS
  path        = /bin/wps_monitor
  flags       = EIF_MESSAGING_CAPABLE
END

BEGIN
  eid         = EID_WLWAPID
  name        = WLWAPID
  path        = /bin/wapid
  flags       = EIF_MESSAGING_CAPABLE
END

# AVM: no dhcp6c for fritz!os
# BEGIN
#   eid         = EID_DHCP6C
#   name        = dhcp6c
#   flags       = EIF_MESSAGING_CAPABLE|EIF_MULTIPLE_INSTANCES
# END

# AVM: no dhcp6s for fritz!os
# BEGIN
#   eid         = EID_DHCP6S
#   name        = dhcp6s
#   flags       = EIF_MESSAGING_CAPABLE
# END

# AVM: no radvd for fritz!os
#   eid         = EID_RADVD
#   name        = radvd
#   flags       = EIF_MESSAGING_CAPABLE
# END

# AVM: no rastatus6 for fritz!os
# BEGIN
#   eid         = EID_RASTATUS6
#   name        = rastatus6
#   flags       = EIF_MESSAGING_CAPABLE
# END

BEGIN
  eid         = EID_IPPD
  name        = ippd
  flags       = EIF_MESSAGING_CAPABLE
END

BEGIN
  eid         = EID_DSLDIAGD
  name        = dsldiagd
# AVM: no startup on boot, but available for manual startup via console
# flags       = EIF_LAUNCH_ON_BOOT
END

BEGIN
  eid         = EID_VECTORINGD
  name        = vectoringd
  flags       = EIF_MDM|EIF_MESSAGING_CAPABLE|EIF_LAUNCH_ON_BOOT
END

# AVM: no soapserver for fritz!os
# BEGIN
#   eid         = EID_SOAPSERVER
#   name        = soapserver
#   runArgs     = -f
#   flags       = EIF_LAUNCH_ON_BOOT
# END

BEGIN
  eid         = EID_UNITTEST
  name        = ut
END

BEGIN
  eid         = EID_MISC
  name        = misc
END

# AVM: no ping from broadcom for fritz!os
# BEGIN
#   eid         = EID_PING
#   name        = ping
#   flags       = EIF_MESSAGING_CAPABLE|EIF_MULTIPLE_INSTANCES
# END

# AVM: no traceroute from broadcom for fritz!os
# BEGIN
#   eid         = EID_TRACERT
#   name        = traceroute
#   flags       = EIF_MESSAGING_CAPABLE|EIF_MULTIPLE_INSTANCES
# END

BEGIN
  eid         = EID_HOTPLUG
  name        = hotplug
  flags       = EIF_MESSAGING_CAPABLE|EIF_MULTIPLE_INSTANCES
END

# AVM: no smbd from broadcom for fritz!os
# BEGIN
#   eid         = EID_SAMBA
#   name        = smbd
#   runArgs     = -D
#   flags        = EIF_SET_SCHED|EIF_SET_CGROUPS
#   cpuGroupName = normal/default
# END

BEGIN
  eid         = EID_DMSD
  name        = bcmmserver
  flags       = EIF_MESSAGING_CAPABLE
END

BEGIN
  eid         = EID_CMFD
  name        = cmfd
  flags       = EIF_LAUNCH_ON_BOOT|EIF_MESSAGING_CAPABLE
END

# AVM: no l2tpd for fritz!os
# BEGIN
#   eid         = EID_L2TPD
#   name        = l2tpd
#   path        = /bin/openl2tpd
# END

# AVM: no pptp for fritz!os
# BEGIN
#   eid         = EID_PPTPD
#   name        = pptp
# END

# AVM: no mocad for fritz!os
# BEGIN
#   eid         = EID_MOCAD
#   name        = mocad
#   flags       = EIF_MESSAGING_CAPABLE|EIF_MULTIPLE_INSTANCES
# END

BEGIN
  eid         = EID_RNGD
  name        = rngd
  flags       = EIF_LAUNCH_ON_BOOT
END

BEGIN
  eid         = EID_PLC_NVM
  name        = plcnvm
  flags       = EIF_LAUNCH_ON_BOOT|EIF_MDM
END

BEGIN
  eid         = EID_PLC_BOOT
  name        = plcboot
  flags       = EIF_LAUNCH_ON_BOOT
END

BEGIN
  eid         = EID_PLC_L2UPGRADE
  name        = plc_l2upgrade
  flags       = EIF_LAUNCH_ON_BOOT
END

BEGIN
  eid         = EID_PWRCTL
  name        = pwrctl
  flags       = EIF_MESSAGING_CAPABLE
END

BEGIN
  eid         = EID_BMUCTL
  name        = bmuctl
  flags       = EIF_MESSAGING_CAPABLE
END

BEGIN
  eid         = EID_BMUD
  name        = bmud
  flags       = EIF_MESSAGING_CAPABLE
END

BEGIN
  eid         = EID_EPON_APP
  name        = eponapp
  flags       = EIF_MDM|EIF_MESSAGING_CAPABLE
END

BEGIN
  eid         = EID_CELLULAR_APP
  name        = cellularapp
  flags       = EIF_MDM|EIF_LAUNCH_ON_BOOT|EIF_MESSAGING_CAPABLE
END

BEGIN
  eid         = EID_EPON_OAM_PORT_LOOP_DETECT
  name        = portLoopDetect
  flags       = EIF_MESSAGING_CAPABLE
END

BEGIN
  eid         = EID_TMSCTL
  name        = tmsctl
  flags       = EIF_MULTIPLE_INSTANCES
END

# AVM: no nfcd for fritz!os
# BEGIN
#   eid         = EID_NFCD
#   name        = nfcd
#   flags       = EIF_MDM|EIF_LAUNCH_ON_BOOT|EIF_MESSAGING_CAPABLE
# END

# AVM: no xmppc for fritz!os
# BEGIN
#   eid         = EID_XMPPC
#   name        = xmppc
#   flags       = EIF_MDM|EIF_LAUNCH_ON_BOOT|EIF_MESSAGING_CAPABLE
# END

# AVM: no websockd for fritz!os
# BEGIN
#   eid         = EID_WEBSOCKD
#   accessBit   = NDA_ACCESS_HTTPD /* 0x0004 */
#   name        = websockd
#   flags       = EIF_MDM|EIF_LAUNCH_ON_BOOT|EIF_MESSAGING_CAPABLE
# END

BEGIN
  eid         = EID_WANCONF
  name        = wanconf
  flags       = EIF_MESSAGING_CAPABLE
END
