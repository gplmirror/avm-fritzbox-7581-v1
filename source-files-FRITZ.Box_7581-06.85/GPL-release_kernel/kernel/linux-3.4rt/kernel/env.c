#include <stddef.h>
#include <linux/export.h>
#include <linux/env.h>
#include <linux/etherdevice.h>

/* prom_getenv in arch/arm/kernel/setup.c and  arch/x86/platform/ce2600/puma6_avm.c */

/*------------------------------------------------------------------------------------------*\
 * similar to prom_getenv(), except that it can only be used for
 * MAC address variables. It converts the MAC address into binary format and
 * returns the original string value.
\*------------------------------------------------------------------------------------------*/
char *prom_getmac(char *macenv, unsigned char *mac_out)
{
	int i;
	char *val;

	val = prom_getenv(macenv);
	if (val) {
		unsigned int m[6];
		int ret;

		ret = sscanf(val, "%x:%x:%x:%x:%x:%x",
		             &m[0], &m[1], &m[2], &m[3], &m[4], &m[5]);
		if (ret < ETH_ALEN) {
			return NULL;
		}
		else {
			for (i = 0; i < ETH_ALEN; i++)
				mac_out[i] = (unsigned char) (m[i] & 0xff);
		}
	}

	return val;
}

EXPORT_SYMBOL(prom_getmac);
