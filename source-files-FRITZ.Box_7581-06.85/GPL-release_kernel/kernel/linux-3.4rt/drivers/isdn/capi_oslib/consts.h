#ifndef _consts_h_
#define _consts_h_

#include <linux/version.h>

/*-------------------------------------------------------------------------------------*\
\*-------------------------------------------------------------------------------------*/
#define MAX_CAPI_MESSAGE_SIZE               2048U
#define MAX_CONTROLLERS                     10
/*--- #define NEED_PAGE_LOCK ---*/

#if defined(CONFIG_EVA_1GB)
#define CAPI_OSLIB_USE_LOCAL_BUFFERS        /* copy_from_user und copy_to_user fuer messages verwenden */
#endif

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if LINUX_VERSION_CODE <= KERNEL_VERSION(2, 6, 19)
/*--- hier gibt es leider keine Realtime-Workqueues: Kernelthread mit hoher Prio besser als Workqueue ! ---*/
#define USE_THREAD
#else
/*--- Realtime-Workqueues sind deutlich performanter als Kernelthreads! ---*/
#if defined(CONFIG_SMP) 
#define USE_WORKQUEUES
#else
#define USE_TASKLETS
#endif
#endif


#endif /*--- #ifndef _consts_h_ ---*/
#include <linux/pcmlink_ul.h>
