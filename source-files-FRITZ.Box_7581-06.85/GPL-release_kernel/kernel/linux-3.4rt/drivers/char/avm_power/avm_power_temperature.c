/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2013 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 *
\*------------------------------------------------------------------------------------------*/
#include <linux/version.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/avm_power.h>
#include "avm_power.h"

static void  *temperature_eventhandle;

#if defined(CONFIG_AR10) || defined(CONFIG_VR9)
extern int ifx_ts_get_temp(int *p_temp);
/*--------------------------------------------------------------------------------*\
 * CPU-Temperatur verwenden
\*--------------------------------------------------------------------------------*/
static inline int arch_get_temperature(void) {
    int temperature, rest;
    if(ifx_ts_get_temp(&temperature)){
        return 0;
    }
    rest = temperature % 10;
    temperature /= 10;
    if(rest >= 5)  {
        /*--- aufrunden ---*/
        rest = 1;
    } else {
        /*--- unter Null Grad wird der Chip sowieso nicht arbeiten ... ---*/
        rest = 0;
    }
    /*--- printk(KERN_INFO"[avm_power]temp %d\n", temperature + rest); ---*/
    return temperature + rest; 
}
#elif defined(CONFIG_X86)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline int arch_get_temperature(void) {
    char buf[128];
    int temperature;
    if(avm_power_read_from_file("/sys/devices/platform/coretemp.0/temp1_input", buf, sizeof(buf))) {
        sscanf(buf, "%d", &temperature);
        temperature /= 1000;
    }
    return temperature;
}
#elif defined(CONFIG_SOC_GRX500) 
#include <asm/mach_avm.h>
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline int arch_get_temperature(void) {
    int rest, chan;
    int max_temperature = -40;

    for(chan = 0; chan < 2; chan++) {
        int temperature = grx_get_chip_temperature(chan);
        if(max_temperature < temperature) {
            max_temperature = temperature;
        }
    }
    rest = max_temperature % 3;
    max_temperature /= 4;
    if(rest >= 2)  {
        /*--- aufrunden ---*/
        rest = 1;
    } else {
        /*--- unter Null Grad wird der Chip sowieso nicht arbeiten ... ---*/
        rest = 0;
    }
    /*--- printk(KERN_INFO"[avm_power]temp %d\n", max_temperature + rest); ---*/
    return max_temperature + rest;
}
#elif defined(CONFIG_BCM963138)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline int arch_get_temperature(void) {
    char buf[128];
    int temperature;
    if(avm_power_read_from_file("/sys/power/bpcm/select0", buf, sizeof(buf))) {
        sscanf(buf, "%d", &temperature);
        temperature /= 1000;
    }
    return temperature;
}
#else
/*--------------------------------------------------------------------------------*\
 * Temperatursensor nicht unterstuetzt
 * bei Puma6 liefert Remote-Device ueber powerdevice_temperature die Temperatur
\*--------------------------------------------------------------------------------*/
static inline int arch_get_temperature(void) {
	return pm_ressourceinfo.deviceinfo[powerdevice_temperature].power_rate;
}
#endif/*--- #if defined(CONFIG_AR10) || defined(CONFIG_VR9) ---*/

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int avm_power_temperature(void) {
    return arch_get_temperature();
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void avmevent_temperature_notify(void *context __attribute__((unused)), enum _avm_event_id id) {
#if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE) 
    struct _avm_event_temperature *event;
    int handled;

	if(id != avm_event_id_temperature){
        printk(KERN_WARNING "[%s]unknown event: %d\n", __func__, id);
        return;
    }
	event = (struct _avm_event_temperature *)kmalloc(sizeof(struct _avm_event_temperature), GFP_ATOMIC);
    if(event == NULL) {
        printk(KERN_WARNING "[%s]can't alloc event: %d\n", __func__, id);
        return;
    }
	event->event_header.id = id;
	event->temperature     = avm_power_temperature();

    if(avm_power_disp_loadrate & 8) {
        printk(KERN_ERR "[%s]temperature event: %d\n", __func__, event->temperature);
    }
	handled = avm_event_source_trigger(temperature_eventhandle, id, sizeof(struct _avm_event_temperature), event);
    if(handled == 0) {
        printk(KERN_WARNING "[%s]event: %d not handled\n", __func__, id);
    }
#endif/*--- #if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE)  ---*/
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int avm_power_temperature_init(void){
#if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE) 
	struct _avm_event_id_mask id_mask;
    temperature_eventhandle =  avm_event_source_register( "temperature",
												   avm_event_build_id_mask(&id_mask, 1, avm_event_id_temperature),
                                                   avmevent_temperature_notify,
                                                   NULL
                                                   );
    if(temperature_eventhandle == NULL) {
        printk(KERN_ERR"[%s] avm event temperature register failed !\n", __func__);
        return -1;
	}
#endif/*--- #if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE)  ---*/
    PowerManagmentRessourceInfo(powerdevice_temperature, 0);
    return 0;
}
#ifdef CONFIG_AVM_POWER_MODULE
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void avm_power_temperature_exit(void){
#if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE) 
    if(temperature_eventhandle) {
      avm_event_source_release(temperature_eventhandle);
      temperature_eventhandle = NULL;
    }
#endif/*--- #if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE)  ---*/
}
#endif/*--- #ifdef CONFIG_AVM_POWER_MODULE ---*/
