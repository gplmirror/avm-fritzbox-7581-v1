#include <linux/version.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/file.h>
#include <linux/fs.h>
#include <linux/avm_power.h>
#include <asm/uaccess.h>
#include "avm_power.h"

/*--- #define DBG_TRC(args...)   printk(KERN_INFO args) ---*/
#define DBG_TRC(args...)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
struct _speedstep {
    atomic_t speed_state;      
    void *handle;
} speedstep;
#define GOVERNOR_KICK      "/var/web_activity"
#define GOVERNOR_KICK_VAL  "1"

#if defined(CONFIG_SOC_GRX500)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void write_to_file (char *filename, char *str, int len) {
    struct file *fp;
    mm_segment_t oldfs;

    DBG_TRC("%s: write %s to %s\n", __func__,  str, filename);
    fp = filp_open(filename, O_WRONLY, 00);
    if(IS_ERR(fp) || (fp->f_op == 0)) {
        /*--- printk(KERN_ERR " failed: open : %s\n", filename); ---*/
        return;
    }
    /* Schreibzugriff auf File(system) erlaubt? */
    if (fp->f_op->write == NULL) {
        printk(KERN_ERR "[avm_power]speedstep failed: write %s\n", filename);
        filp_close(fp, NULL);
        return;
    }
    oldfs = get_fs();
    set_fs(KERNEL_DS);
    fp->f_pos = 0; /* Von Anfang an schreiben*/
    fp->f_op->write(fp, str, len, &fp->f_pos);
    set_fs(oldfs);
    filp_close(fp, NULL); /* Close the file */
}
#endif/*--- #if defined(CONFIG_SOC_GRX500) ---*/

/*--------------------------------------------------------------------------------*\
 * beliebiger Kontext
 * state: 0 do nothing 
 *        1 Kick
\*--------------------------------------------------------------------------------*/
static int avm_power_speedstep_Callback(int state){
    DBG_TRC("%s: state=0x%x\n", __func__,  state);
    atomic_set(&speedstep.speed_state, state);
    pm_ressourceinfo.Changes++;
    wake_up_interruptible(&pm_ressourceinfo.wait_queue);
    return 0;
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int avm_power_speedstep_init(void) {
    atomic_set(&speedstep.speed_state, 0);
    speedstep.handle = PowerManagmentRegister("speedstep", avm_power_speedstep_Callback);
    return speedstep.handle ? 0 : -1;
}
#ifdef CONFIG_AVM_POWER_MODULE
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void avm_power_speedstep_exit(void) {
    if(speedstep.handle) {
        PowerManagmentRelease(speedstep.handle);
        speedstep.handle = NULL;
    }
}
#endif/*--- #ifdef CONFIG_AVM_POWER_MODULE ---*/
/*--------------------------------------------------------------------------------*\
 * im Kontext des Threads
\*--------------------------------------------------------------------------------*/
void avm_power_speedstep_mode(void) {
    unsigned int state;  
    state =  atomic_xchg(&speedstep.speed_state, 0);
    if(state) {
#if defined(CONFIG_SOC_GRX500)
        write_to_file(GOVERNOR_KICK, GOVERNOR_KICK_VAL, sizeof(GOVERNOR_KICK_VAL) - 1);
#endif/*--- #if defined(CONFIG_SOC_GRX500) ---*/
    }
}
