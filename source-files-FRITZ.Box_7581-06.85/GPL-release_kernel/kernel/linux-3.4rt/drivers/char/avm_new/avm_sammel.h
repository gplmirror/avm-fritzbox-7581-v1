/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2006 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/
#ifndef _AVM_SAMMEL_H_
#define _AVM_SAMMEL_H_

#if defined(CONFIG_AVM_IPI_YIELD)

#include <asm/yield_context.h>
#define __BUILD_AVM_CONTEXT_FUNC(func) yield_##func

#elif defined(CONFIG_AVM_FASTIRQ)

#include <mach/avm_fiq_os.h>
#define __BUILD_AVM_CONTEXT_FUNC(func) firq_##func

#elif defined(CONFIG_SMP) && defined(CONFIG_MIPS)

#include <asm/yield_context.h>
/*--- shit Kompatibilitaets-hack (AR10 - kein CONFIG_AVM_IPI_YIELD - yield_spinlock().. muss aber existieren) ---*/
#define yield_wake_up_interruptible(a) if(yield_is_linux_context()) wake_up_interruptible(a)
#define yield_wake_up(a) if(yield_is_linux_context()) wake_up(a)
#define __BUILD_AVM_CONTEXT_FUNC(func) yield_##func

#else

#define __BUILD_AVM_CONTEXT_FUNC(func) func
#define is_linux_context()        1

#endif

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*--- #define AVM_LED_DEBUG ---*/
/*--- #define AVM_WATCHDOG_DEBUG ---*/
#define AVM_EVENT_DEBUG

void show_avm_memory_statistic(int complete);

#endif /*--- #ifmdef _AVM_SAMMEL_H_ ---*/

