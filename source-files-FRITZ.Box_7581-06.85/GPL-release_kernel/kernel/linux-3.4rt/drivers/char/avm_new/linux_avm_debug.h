#ifndef _linux_avm_debug_h_
#define _linux_avm_debug_h_
/*--------------------------------------------------------------------------------*\
 * die /dev/debug-Schnittstelle
\*--------------------------------------------------------------------------------*/
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/version.h>
#include <stdarg.h>

/*--------------------------------------------------------------------------------*\
 * alle Daten koennen mit cat /dev/debug abgeholt werden
\*--------------------------------------------------------------------------------*/
asmlinkage void avm_DebugPrintf(const char *format, ...);
void avm_DebugvPrintf(const char *format, va_list args);
extern asmlinkage int printk_avm(const char *fmt, ...);
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)
extern int vprintk_avm(const char *format, va_list args);
#endif

#if defined(CONFIG_AVM_DEBUG)
extern void avm_debug_disable_avm_printk(void);
extern void avm_debug_enable_avm_printk(void);
#else/*--- #if defined(CONFIG_AVM_DEBUG) ---*/
#define  avm_debug_disable_avm_printk()
#define  avm_debug_enable_avm_printk()
#endif/*--- #else ---*//*--- #if defined(CONFIG_AVM_DEBUG) ---*/

/*--------------------------------------------------------------------------------*\
 * console-output from printk to debug-device
 * ret: 0  consumed
 *      1: Ausgabe ueber Standardkonsole
\*--------------------------------------------------------------------------------*/
int avm_debug_console_write(const char *text, int len);

/*--------------------------------------------------------------------------------*\
 * fuer printk-Spezialfaelle:
 * printk_linux():
 * FORCE_PRINTK_LINUX_FACILITIES_VALUE verodert mit facilitie-Value in vprintk_emit()
 * avm_DebugPrintf():
 * FORCE_PRINTK_AVM_FACILITIES_VALUE verodert mit facilitie-Value in vprintk_emit()        
\*--------------------------------------------------------------------------------*/
#define FORCE_PRINTK_LINUX_FACILITIES_VALUE         0x100
#define FORCE_PRINTK_AVM_FACILITIES_VALUE           0x200

/*-------------------------------------------------------------------------------------*\
 * Debug-Funktion am Treiber anmelden
 * prefix: der Inputdaten werden nach diesem Prefix durchsucht, und bei Match 
 * wird die CallbackFkt aufgerufen
 * um also den string 'blabla=haha' zum Treiber angemeldet mit prefix 'unicate_' zu transportieren
 * ist einfach ein "echo unicate_blabla=haha >/dev/debug" auf der Konsole auszufuehren
 * ret: handle (fuer UnRegister)
\*-------------------------------------------------------------------------------------*/
void *avm_DebugCallRegister(char *prefix, void (*CallBackDebug)(char *string, void *refdata), void *refdata);

/*--------------------------------------------------------------------------------*\
 * Debug-Funktion am Treiber abmelden
\*--------------------------------------------------------------------------------*/
void avm_DebugCallUnRegister(void *handle);

/*--------------------------------------------------------------------------------*\
 * signal 0 ...31 
 * signal: 0 supportdata per pushmail
\*--------------------------------------------------------------------------------*/
void avm_DebugSignal(unsigned int signal);
asmlinkage void avm_DebugSignalLog(unsigned int signal, char *fmt, ...);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct file;
struct inode;

typedef ssize_t (*avm_debug_write_t)(struct file *, const char *, size_t , loff_t *);
typedef ssize_t (*avm_debug_read_t)(struct file *, char *, size_t , loff_t *);
typedef int (*avm_debug_open_t)(struct inode *, struct file *);
typedef int (*avm_debug_close_t)(struct inode *, struct file *);
typedef long (*avm_debug_ioctl_t)(struct file *, unsigned, unsigned long);

int avm_debug_register_minor(int minor, 
                             avm_debug_open_t,
                             avm_debug_close_t,
                             avm_debug_write_t,
                             avm_debug_read_t,
                             avm_debug_ioctl_t);

int avm_debug_release_minor(int);

/*------------------------------------------------------------------------------------------*\
 * Genutzte Minor Nummern
\*------------------------------------------------------------------------------------------*/
#define NLR_AUDIO_MINOR            1

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define AVM_DEBUG_MAX_MINOR        1
#define AVM_DEBUG_MINOR_COUNT (AVM_DEBUG_MAX_MINOR+1)

#endif /*--- #ifndef _linux_avm_debug_h_ ---*/
