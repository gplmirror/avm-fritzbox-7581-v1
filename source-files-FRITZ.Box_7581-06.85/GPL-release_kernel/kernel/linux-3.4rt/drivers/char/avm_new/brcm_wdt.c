/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2015 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_AVM_WATCHDOG)

#include <mach/avm_fiq_groups.h>
#define CLIENT_FIQ_PRIO FIQ_PRIO_WATCHDOG

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/smp.h>
#include <linux/init.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/io.h>
#include <linux/interrupt.h>
#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/reboot.h>
#include <linux/sched.h>
#include <asm/cacheflush.h>
#include <linux/nmi.h>
#include <asm/irq_regs.h>
#include <linux/kmsg_dump.h>

#include <asm/mach_avm.h>

#ifdef CONFIG_AVM_FASTIRQ
#include "63138_intr.h"
#include <mach/avm_gic.h>
#include <mach/avm_gic_fiq.h>
#include <mach/avm_fiq.h>
#include <mach/avm_fiq_os.h>
#endif

void bcm_init_watchdog(void);
void bcm_configure_watchdog( unsigned int enabled, unsigned int timer, unsigned int userMode, unsigned int userThreshold );
void bcm_acknowledge_watchdog( void );
int bcm_suspend_watchdog( void );

static struct timer_list WDTimer[NR_CPUS];

static unsigned int            wdt_initialized;
static unsigned int            act_wdt_cpu;
static atomic_t                wdt_busy;

static RAW_NOTIFIER_HEAD(nmi_chain);
/**-----------------------------------------------------------------------------------------*\
 * der (pseudo-)nmi-handler ueber fastirq
\**-----------------------------------------------------------------------------------------*/
int register_nmi_notifier(struct notifier_block *nb) {
	return raw_notifier_chain_register(&nmi_chain, nb);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void wd_timer_function(unsigned long context __maybe_unused) {
   bcm_acknowledge_watchdog();
   atomic_set(&wdt_busy, 0);
}

/*------------------------------------------------------------------------------------------*\
 * \brief: jede CPU per roundrobin triggern
\*------------------------------------------------------------------------------------------*/
unsigned int cancelled;
void ar7wdt_hw_trigger(void) {
    unsigned int cpu;

    if(wdt_initialized == 0){
        return;
    }
    if(atomic_read(&wdt_busy)) {
        return;
    }
    //printk(KERN_ERR "[%s]Trigger cpu=%u\n", __func__, act_wdt_cpu);
    for(cpu = act_wdt_cpu; cpu < NR_CPUS; cpu++) {
        if(!cpu_online(cpu)) {
            continue;
        }
        atomic_set(&wdt_busy, cpu + 1);
        if(cpu == smp_processor_id()) {
            if(!cancelled)
               wd_timer_function(0);
            break;
        }
        WDTimer[cpu].expires = jiffies + 1;
        del_timer(&WDTimer[cpu]);
        add_timer_on(&WDTimer[cpu], cpu);
        break;
    }
    if(++cpu >= NR_CPUS) cpu = 0;
    act_wdt_cpu = cpu;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static spinlock_t wd_lock;
static spinlock_t wd_sync;
extern unsigned int bcm_gpio_get_data(unsigned int gpio_num);
extern void bcm_gpio_set_data(unsigned int gpio_num, unsigned int data);

void bcm_intclear_watchdog(void);

extern void avm_mask_all_fiqs_down(unsigned int fiq_prio, unsigned long *restore_PMR, unsigned long flags);
extern void avm_unmask_all_fiqs_up(unsigned long restore_PMR, unsigned long flags);

irqreturn_t wdt_isr(int irq __attribute__((unused)), void *arg __attribute__((unused))) {
    struct pt_regs regs, *pregs;

    unsigned long restore_PMR;

    unsigned int limit;
    unsigned int tstart;

    if(!firq_spin_trylock(&wd_lock))
    {
       //local_fiq_disable();
       //printk( KERN_ERR "FIQ watchdog handling on CPU#%d cancelled, already running on other cpu! New mask: %d\n",
       //        smp_processor_id(), (get_ICDIPTR(AVM_IRQ_WD, IS_ATOMIC) & ~(1 << smp_processor_id())) );

       /* disable target routing */
       set_ICDIPTR(AVM_IRQ_WD, get_ICDIPTR(AVM_IRQ_WD, IS_ATOMIC) & ~(1 << smp_processor_id()), IS_ATOMIC);
       /* mask interrupt to this cpu */
       avm_mask_all_fiqs_down(get_ICDIPR(AVM_IRQ_WD, IS_ATOMIC), &restore_PMR, 0);
       /* late ack */
       (void)get_ICCIAR(IS_ATOMIC);
       /* clear the source */
       bcm_intclear_watchdog();
       /* clear any pending */
       set_ICDICPR(AVM_IRQ_WD, 1, IS_ATOMIC);
       /* finalize interrupt */
       set_ICCEOIR(AVM_IRQ_WD, 0);

       local_fiq_enable();

       //printk( KERN_ERR "FIQ watchdog handling on CPU#%d waiting for backtrace trigger ...\n", smp_processor_id() );

       /* Sync ... */
       firq_spin_unlock(&wd_sync);

       /* Festhalten, IPI-FIRQ-Trigger wird hier reinschlagen ... */
       while(1) {
       }

       return IRQ_HANDLED;
    }
    else
    {
       firq_spin_lock(&wd_sync);

       printk( KERN_ERR "FIQ watchdog handling on CPU#%d\n", smp_processor_id() );

       limit  = avm_get_cyclefreq() * 5; /* sec */
       tstart = avm_get_cycles();
       while(!firq_spin_trylock(&wd_sync)) {
          if((avm_get_cycles() - tstart) > limit){
             printk( KERN_ERR "It seems, other CPU is caught badly while keeping WATCHDOG FIQ masked ...\n");
             printk( KERN_ERR "Sync trigger should come through though, if not, FIQs are erroneously switched off ...\n");
             break;
          }
       }
       //firq_spin_unlock(&wd_sync);

       firq_spin_unlock(&wd_lock);

       if(firq_is_linux_context()) {
          pregs = get_irq_regs();
       } else {
          //avm_tick_jiffies_update(); //--- auch im FASTIRQ-Fall jiffies korrigieren ---
          pregs = get_fiq_regs();
       }
       prepare_register_for_trap(&regs, &pregs);
       avm_set_reset_status(RS_NMIWATCHDOG);
       raw_notifier_call_chain(&nmi_chain, 0, pregs);

       console_verbose();
       bust_spinlocks(1);

       flush_cache_all();
       die("HardwareWatchDog - NMI taken" , pregs, 0);

       // Zur Sicherheit, wir kehren nicht zurueck, Reset wird durch WD gezogen ...
       while(1) {
       }

       return IRQ_HANDLED;
    }
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void register_wdt_irq(int irq) {
	int ret;

    ret  = avm_request_fiq_on(3, irq, wdt_isr, FIQ_CPUMASK, "Watchdog", 0);
    avm_gic_fiq_setup(irq, 3, AVM_IRQ_WD, 1, 0);
    if(!ret) {
        printk(KERN_ERR "[%s] Watchdog as fastirq(%u) on all cpus registered\n", __func__, irq);
        return;
    }
    printk(KERN_ERR "[%s] ERROR request_irq(irq(%d)) ret:%d\n", __func__, irq, ret);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ar7wdt_hw_init(void) {
   int cpu;

   printk(KERN_ERR "[ar7wdt_hw_init]: Setting up watchdog for 64s ...\n");

   for(cpu = 0; cpu < NR_CPUS; cpu++) {
        init_timer(&WDTimer[cpu]);
        WDTimer[cpu].data     = (unsigned long)&WDTimer[cpu];
        WDTimer[cpu].function = wd_timer_function;
    }
   bcm_init_watchdog();
   firq_spin_lock_init(&wd_lock);
   firq_spin_lock_init(&wd_sync);
   register_wdt_irq(AVM_IRQ_WD);

   bcm_configure_watchdog(1, 0x4000000, 0, 0);

   wdt_initialized = 1;
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void ar7wdt_hw_deinit(void) {
   printk(KERN_ERR "[ar7wdt_hw_deinit]: Switching off watchdog ...\n");
   bcm_acknowledge_watchdog();
   mdelay(100);
   bcm_configure_watchdog(0, 0x4000000, 0, 0);
   mdelay(100);
   bcm_configure_watchdog(1, 0x4000000, 0, 0);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ar7wdt_hw_reboot(void) {
   printk(KERN_ERR "[ar7wdt_hw_reboot]: triggered ...\n");
   bcm_acknowledge_watchdog();
   mdelay(100);
   bcm_configure_watchdog(1, 0x100000, 0, 0);
   //panic("ar7wdt_hw_reboot: Watchdog expired\n");
}


#endif/*--- #if defined(CONFIG_AVM_WATCHDOG) ---*/
