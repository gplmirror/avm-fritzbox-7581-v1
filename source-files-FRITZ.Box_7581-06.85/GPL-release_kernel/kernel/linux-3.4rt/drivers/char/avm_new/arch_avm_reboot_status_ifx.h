#ifndef __arch_avm_reboot_status_ifx_h__
#define __arch_avm_reboot_status_ifx_h__

extern unsigned int avm_nmi_taken;

#define UPDATE_REBOOT_STATUS_TEXT       "(c) AVM 2013, Reboot Status is: Firmware-Update" \
                                        "(c) AVM 2013, Reboot Status is: Firmware-Update" \
                                        "(c) AVM 2013, Reboot Status is: Firmware-Update"
#define SOFTWATCHDOG_REBOOT_STATUS_TEXT "(c) AVM 2013, Reboot Status is: Software-Watchdog" \
                                        "(c) AVM 2013, Reboot Status is: Software-Watchdog" \
                                        "(c) AVM 2013, Reboot Status is: Software-Watchdog"

#define NMI_REBOOT_STATUS_TEXT          "(c) AVM 2013, Reboot Status is: Software-NMI-Watchdog" \
                                        "(c) AVM 2013, Reboot Status is: Software-NMI-Watchdog" \
                                        "(c) AVM 2013, Reboot Status is: Software-NMI-Watchdog"
#define POWERON_REBOOT_STATUS_TEXT      "(c) AVM 2013, Reboot Status is: Power-On-Reboot" \
                                        "(c) AVM 2013, Reboot Status is: Power-On-Reboot" \
                                        "(c) AVM 2013, Reboot Status is: Power-On-Reboot"
#define TEMP_REBOOT_STATUS_TEXT         "(c) AVM 2013, Reboot Status is: Temperature-Reboot" \
                                        "(c) AVM 2013, Reboot Status is: Temperature-Reboot" \
                                        "(c) AVM 2013, Reboot Status is: Temperature-Reboot"

#define SOFT_REBOOT_STATUS_TEXT_PANIC   "(c) AVM 2013, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2013, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2013, Reboot Status is: Software-Reboot\0(PANIC)"
#define SOFT_REBOOT_STATUS_TEXT_OOM     "(c) AVM 2013, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2013, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2013, Reboot Status is: Software-Reboot\0(OOM)"
#define SOFT_REBOOT_STATUS_TEXT_OOPS    "(c) AVM 2013, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2013, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2013, Reboot Status is: Software-Reboot\0(OOPS)"
/*--- Achtung! Untermenge von obigen Eintraegen: ---*/
#define SOFT_REBOOT_STATUS_TEXT         "(c) AVM 2013, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2013, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2013, Reboot Status is: Software-Reboot"

#include <asm/mips_mt.h>
#include <linux/avm_kernel_config.h>
#include <asm/reboot.h>

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static char *arch_get_mailbox(void){
    char *mailbox = (char *)(0xA1000000 - 512);
    return mailbox;
}

/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static int arch_ifx_die_notifier(struct notifier_block *self __maybe_unused, unsigned long cmd __maybe_unused, 
                                 void *ptr __maybe_unused) {
    if(cmd == DIE_OOPS) {
        struct die_args *args = (struct die_args *)ptr;
        struct pt_regs *regs = args->regs;
        static int die_counter;

        oops_enter();

        console_verbose();
        bust_spinlocks(1);
#ifdef CONFIG_MIPS_MT_SMTC
        mips_mt_regdump(0);
#endif /* CONFIG_MIPS_MT_SMTC */

        printk("%s[#%d]:\n", args->str, ++die_counter);
        show_registers(regs);
        add_taint(TAINT_DIE, LOCKDEP_NOW_UNRELIABLE);
        trigger_all_cpu_backtrace();

        oops_exit();
        panic("Fatal exception %s", in_interrupt() ? "in interrupt" : "");
    }
    return NOTIFY_OK;
}
#define arch_die_notifier arch_ifx_die_notifier
#endif /*--- #ifndef __arch_avm_reboot_status_ifx_h__ ---*/
