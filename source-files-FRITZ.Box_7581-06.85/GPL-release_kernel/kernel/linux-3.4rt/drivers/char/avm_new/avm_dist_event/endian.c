#include <linux/kernel.h>
#include <linux/string.h>
#include "avm_event_gen_enum_range.h"
#include "avm_event_gen_types.h"
#include "avm_event_gen_endian_external_defines.h"
#include "avm_event_gen_endian.h"

#include "avm_event_endian.h"


/*------------------------------------------------------------------------------------------*\
 * Defines
\*------------------------------------------------------------------------------------------*/
#define DO_CHECK_AHA_MESSAGE
#if defined(__LITTLE_ENDIAN)
    #define MACHINE_IS_LITTLE_ENDIAN
#else /*--- #if __BYTE_ORDER == __LITTLE_ENDIAN ---*/
#endif /*--- #else ---*/ /*--- #if __BYTE_ORDER == __LITTLE_ENDIAN ---*/

#define __unused__      __attribute__ ((unused))

unsigned int global_level;
unsigned int dump_structure = 1;
void print_one_entry(struct _endian_convert *C, unsigned int);

/*------------------------------------------------------------------------------------------*\
 * Prototypes
\*------------------------------------------------------------------------------------------*/
static uint64_t one_entry_swap_from_machine(unsigned char *in, unsigned char *out, unsigned int size, struct _endian_convert *C __unused__) __unused__;
static uint64_t one_entry_swap_to_machine(unsigned char *in, unsigned char *out, unsigned int size, struct _endian_convert *C __unused__); __unused__
#ifdef DO_CHECK_AHA_MESSAGE
static uint64_t one_entry_no_swap_just_check(unsigned char *in, unsigned char *out, unsigned int size, struct _endian_convert *C __unused__); __unused__

unsigned int endian_no_debug = 1;

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void print_one_entry(struct _endian_convert *C, unsigned int value) {
    unsigned int i;
    static char *last_struct = NULL;
    static unsigned int last_level = 0;
    static char buffer[1024];

    if(likely(endian_no_debug))
        return;

    if(last_level > global_level) {
        buffer[0] = '\0';
        for(i = 0 ; i < global_level ; i++)
            strcat(buffer, "        ");
        printk(KERN_ERR "[E]: %s}\n", buffer);
    }

    if((last_level < global_level) || ((last_level == 0) && (global_level == 0))) {
        if(last_struct != C->struct_name) {
            last_struct = C->struct_name;

            buffer[0] = '\0';
            for(i = 0 ; i < global_level ; i++)
                strcat(buffer, "        ");
            sprintf(buffer + strlen(buffer), "%s {", C->struct_name);
            printk(KERN_ERR "[E]: %s\n", buffer);
        }
    }
    last_level = global_level;

    buffer[0] = '\0';
    for(i = 0 ; i < global_level + 1 ; i++)
        strcat(buffer, "        ");
    sprintf(buffer + strlen(buffer), "%s %s = %u(%#x)", C->type_name, C->name, value, value);
    sprintf(buffer + strlen(buffer), " {offset %u}", C->offset);
    if(C->enum_debug_function) {
        sprintf(buffer + strlen(buffer), " [%s]", (*C->enum_debug_function)(value));
    }
    printk(KERN_ERR "[E]: %s\n", buffer);
}

/*--------------------------------------------------------------------------------*\
 * just resolve unaligned access from compiler
\*--------------------------------------------------------------------------------*/
union __p_access {
	uint8_t  bval;
	uint16_t sval __attribute__((packed));
	uint32_t dval __attribute__((packed));
	uint64_t qval __attribute__((packed));
};
/*------------------------------------------------------------------------------------------*\
 * Private methods
\*------------------------------------------------------------------------------------------*/
static uint64_t one_entry_no_swap_just_check(unsigned char *in, unsigned char *out, unsigned int size, struct _endian_convert *C __unused__) {
	union __p_access *pin, *pout;
    uint64_t value = 0;

    if(!size)
        return 0;

	pin  = (union __p_access *)in; 
	pout = (union __p_access *)out; 

    // normale Wandlung
    switch(size) {
        case 1:
            value = pout->bval = pin->bval;
            break;
        case 2:
            value = pout->sval = pin->sval;
            break;
        case 4:
            value = pout->dval = pin->dval;
            break;
        case 8:
            value = pout->qval = pin->qval;
            break;

        default:
            // maybe a char array -> just copy
            memcpy(out, in, size);
            break;
    }

    if(dump_structure)
        print_one_entry(C, value);

    return value;
}
#endif /*--- #ifdef DO_CHECK_AHA_MESSAGE ---*/

/*
 * Convert one single value.
 */
static uint64_t one_entry_swap_from_machine(unsigned char *in, unsigned char *out, unsigned int size, struct _endian_convert *C __unused__) {
	union __p_access *pin, *pout;
    uint64_t value = 0;

    if(!size)
        return 0;

	pin  = (union __p_access *)in; 
	pout = (union __p_access *)out; 

    // normale Wandlung
    switch(size) {
        case 1:
            value = pout->bval = pin->bval;
            break;
        case 2:
            pout->sval = __bswap_16(pin->sval);
            value = pin->sval;
            break;
        case 4:
            pout->dval = __bswap_32(pin->dval);
            value = pin->dval;
            break;
        case 8:
            pout->qval = __bswap_64(pin->qval);
            value = pin->qval;
            break;

        default:
            // maybe a char array -> just copy
            memcpy(out, in, size);
            break;
    }

    if(dump_structure)
        print_one_entry(C, value);

    return value;
}

/*
 * Convert one single value.
 *
 * @size: Groesse des Feldes (wenn hoechstes Bit gesetzt ist -> Bitfeld)
 */
static uint64_t one_entry_swap_to_machine(unsigned char *in, unsigned char *out, unsigned int size, struct _endian_convert *C __unused__) {
	union __p_access *pin, *pout;
    uint64_t value = 0;

    if(!size)
        return 0;

	pin  = (union __p_access *)in; 
	pout = (union __p_access *)out; 

    // normale Wandlung
    switch(size) {
        case 1:
            value = pout->bval = pin->bval;
            break;
        case 2:
            pout->sval = __bswap_16(pin->sval);
            value = pout->sval;
            break;
        case 4:
            pout->dval = __bswap_32(pin->dval);
            value = pout->dval;
            break;
        case 8:
            pout->qval = __bswap_64(pin->qval);
            value = pout->qval;
            break;

        default:
            // maybe a char array -> just copy
            memcpy(out, in, size);
            break;
    }
    if(dump_structure)
        print_one_entry(C, value);

    return value;
}

struct convert_version_info {
    unsigned int in_version;
    unsigned int out_version;
};

/*
 * Convert the given 'in' structure into big/little endian.
 *
 * @func: converter function
 */

typedef struct sublen {
    int err;
    unsigned int converted_bytes;
    int inCorr;
    int outCorr;
}sublength_t;
sublength_t errReturn = { .err = -1, .converted_bytes = 0, .inCorr = 0, .outCorr = 0 };

typedef struct sublenFieldDescr {
    struct _endian_convert *C;

    unsigned char *in;
    unsigned char *out;
    unsigned int length;
}sublenFieldDescr_t;

static sublength_t
convert_endian(int id,
                          uint64_t (*func)(unsigned char *in, unsigned char *out, unsigned int size, struct _endian_convert *C),
                          unsigned int select_value,
                          int bytes_to_convert,
                          struct _endian_convert *C,
                          unsigned char *in,
                          unsigned char *out,
                          struct convert_version_info version_info,
                          unsigned int level) {
    //-----
    sublength_t new_length = {.err=0, .converted_bytes=0, .inCorr=0, .outCorr=0};
    unsigned char bitfield_count = 0;
    unsigned int current_value = 0;

    int array_element_size = 0;
    int array_element_anzahl = 0;
    unsigned int tmp;
    char *entry_name = C->struct_name;
    unsigned int entry_len = bytes_to_convert;
    sublenFieldDescr_t this_layer_totallength_C = {.C=NULL, .out=NULL, .length=0}; // Feld auf dieser Ebene in das eine Sub-Länge eingetragen werden soll
    sublenFieldDescr_t this_layer_sublength_C = {.C=NULL, .out=NULL, .length=0}; // Feld auf dieser Ebene in das eine Sub-Länge eingetragen werden soll

    global_level = level;

    this_layer_totallength_C.length = bytes_to_convert;
    while(bytes_to_convert) {
        sublength_t sub_length = {.err=0, .converted_bytes=0, .inCorr=0, .outCorr=0};

        // falls die out-Version kleiner ist, kann das Feld ignoriert werden -> Wert wird verworfen
        if(C->ab_version && (C->ab_version > version_info.out_version)) {
            //printk(KERN_ERR "%d:%02u C(%p size:% 4d offs:% 4d v:%u.%u %28s) ptr(!! IGNORE !! i:%p o:%p) corr->(i:% 4d o:% 4d) v(i:%u.%u o:%u.%u)\n",
            //        id,
            //        level,
            //        C,
            //        C->size,
            //        C->offset,
            //        AVM_DIST_EVENT_VERSION__GET_MAJOR(C->ab_version),
            //        AVM_DIST_EVENT_VERSION__GET_MINOR(C->ab_version),
            //        C->name,
            //        in,
            //        out,
            //        new_length.inCorr,
            //        new_length.outCorr,
            //        AVM_DIST_EVENT_VERSION__GET_MAJOR(version_info.in_version),
            //        AVM_DIST_EVENT_VERSION__GET_MINOR(version_info.in_version),
            //        AVM_DIST_EVENT_VERSION__GET_MAJOR(version_info.out_version),
            //        AVM_DIST_EVENT_VERSION__GET_MINOR(version_info.out_version)
            //        );

            // check if there are unconverted bitfields
            if(bitfield_count) {
                unsigned int last_length = C->offset - (C - 1)->offset;

                if(bitfield_count) {
                    func(in + (C - 1)->offset + new_length.inCorr, out + (C - 1)->offset + new_length.outCorr, bitfield_count / 8, C);
                    bitfield_count  = 0;
                }

                bytes_to_convert -= last_length;
                new_length.converted_bytes += last_length;
            }

            bytes_to_convert -= C->size;
            new_length.converted_bytes += C->size;
            new_length.outCorr -= C->size;

            if((C->flags & ENDIAN_CONVERT_LAST)) {
                break;
            }
            C++;
            continue;
        }

        // Feld konvertieren
        if(C->flags & ENDIAN_CONVERT_IGNORE) {
            // Feld ignorieren
        } else {
            if(C->bits) {
                bitfield_count += C->bits;

                // do not convert, but get value if its a select type
                current_value = func(in + C->offset + new_length.inCorr, (unsigned char*)&tmp, 4, C);
                current_value = (current_value >> (32 - C->bits-C->bit_offset)) & ((1 << C->bits) - 1);

                if(bitfield_count >= 32) {
                    func(in + C->offset + new_length.inCorr, out + C->offset + new_length.outCorr, 4, C);
                    bitfield_count -= 32;

                    new_length.converted_bytes += 4;
                    bytes_to_convert -= 4;
                }
            } else {
                // zuerst ein noch ausstehendes Bitfeld konvertieren?
                if(bitfield_count) {
                    unsigned int last_length = C->offset - (C - 1)->offset;

                    if(bitfield_count) {
                        func(in + (C - 1)->offset + new_length.inCorr, out + (C - 1)->offset + new_length.outCorr, bitfield_count / 8, C);
                        bitfield_count  = 0;
                    }

                    bytes_to_convert -= last_length;
                    new_length.converted_bytes += last_length;
                }
                if((C->flags & ENDIAN_CONVERT_ARRAY_USE) == 0) {

                    // Feld existiert nicht im in? -> Default-Wert kopieren
                    if(C->ab_version && (C->ab_version > version_info.in_version)) {
                        current_value = C->default_value;
                        new_length.inCorr -= C->size;

                        memcpy(out + C->offset + new_length.outCorr, &current_value, C->size);
                    } else {
                        current_value = func(in + C->offset + new_length.inCorr, out + C->offset + new_length.outCorr, C->size, C);
                    }

                    bytes_to_convert  -= C->size;
                    new_length.converted_bytes += C->size;
                }
            }

            if(C->flags & ENDIAN_CONVERT_SUB_LENGTH) {
                this_layer_sublength_C.C = C;
                this_layer_sublength_C.in = in + C->offset + new_length.inCorr;
                this_layer_sublength_C.out = out + C->offset + new_length.outCorr;
            }
            if(C->flags & ENDIAN_CONVERT_TOTAL_LENGTH) {
                this_layer_totallength_C.C = C;
                this_layer_totallength_C.in = in + C->offset + new_length.inCorr;
                this_layer_totallength_C.out = out + C->offset + new_length.outCorr;
            }
        }

        if(bytes_to_convert < 0) {
            printk(KERN_ERR "[%s] fatal error: bytes_to_convert (%d) < 0 \n", __FUNCTION__, bytes_to_convert);
            return errReturn;
        }

        //printk(KERN_ERR "%d:%02u C(%p size:% 4d offs:% 4d v:%u.%u %28s) ptr(v:0x%08x i:%p o:%p) corr->(i:% 4d o:% 4d) v(i:%u.%u o:%u.%u)\n",
        //        id,
        //        level,
        //        C,
        //        C->size,
        //        C->offset,
        //        AVM_DIST_EVENT_VERSION__GET_MAJOR(C->ab_version),
        //        AVM_DIST_EVENT_VERSION__GET_MINOR(C->ab_version),
        //        C->name,
        //        current_value,
        //        in,
        //        out,
        //        new_length.inCorr,
        //        new_length.outCorr,
        //        AVM_DIST_EVENT_VERSION__GET_MAJOR(version_info.in_version),
        //        AVM_DIST_EVENT_VERSION__GET_MINOR(version_info.in_version),
        //        AVM_DIST_EVENT_VERSION__GET_MAJOR(version_info.out_version),
        //        AVM_DIST_EVENT_VERSION__GET_MINOR(version_info.out_version)
        //        );

        // Select-Wert?
        if(C->flags & ENDIAN_CONVERT_SELECT) {
            if(C->enum_check_function && C->enum_check_function(current_value)) {
                printk(KERN_ERR "[%s] fatal error: select value out of range (%d)\n", __FUNCTION__, current_value);
                return errReturn;
            }
            select_value = current_value;
        }

        // Array-Behandlung
        if(C->flags & ENDIAN_CONVERT_ARRAY_ELEMENT_SIZE) {
            array_element_size = current_value;
        }
        if(C->flags & ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL) {
            array_element_anzahl = current_value;
        }

        // Ab in tiefere Ebenen -> substrukturen auswerten
        {
            struct _endian_convert *sub = NULL;

            if(C->flags & ENDIAN_CONVERT_SELECT_ENTRY) {
                sub = &C->substruct[select_value];
            } else if(C->substruct && !(C->flags & ENDIAN_CONVERT_ARRAY_USE)) {
                sub = C->substruct;
            }

            if(sub) {
                sub_length = convert_endian(id,
                                            func,
                                            select_value,
                                            bytes_to_convert,
                                            sub,
                                            in + C->offset + new_length.inCorr,
                                            out + C->offset + new_length.outCorr,
                                            version_info,
                                            level+1);
                global_level = level;

                if(sub_length.err < 0) {  /*--- fataler message fehler ---*/
                    printk(KERN_ERR "[%s] fatal error: process substruct failed.\n\tat field: '%s'\n\tin struct: '%s'\n\tglobal_len: %d (before process)\n",
                                    __FUNCTION__, C->name, C->struct_name, bytes_to_convert);

                    return sub_length;
                }
                if((int)sub_length.converted_bytes > bytes_to_convert) {
                    printk(KERN_ERR "[%s] fatal error: sub_length (%d) > bytes_to_convert (%d) \n", __FUNCTION__, sub_length.converted_bytes, bytes_to_convert);
                    return errReturn;
                }

                bytes_to_convert -= sub_length.converted_bytes;
                new_length.converted_bytes += sub_length.converted_bytes;
                new_length.inCorr += sub_length.inCorr;
                new_length.outCorr += sub_length.outCorr;
                this_layer_sublength_C.length += sub_length.converted_bytes + sub_length.outCorr;
                this_layer_totallength_C.length += sub_length.outCorr;
            }

            // Array-Spielereien
            if(C->flags & ENDIAN_CONVERT_ARRAY_USE) {
                /*--- printk(KERN_ERR "[%s] ENDIAN_CONVERT_ARRAY_USE: element size %d element_anzahl %d\n", __FUNCTION__, array_element_size, array_element_anzahl); ---*/

                if(array_element_size == 0) {
                    array_element_size = C->size; // ist wohl null...
                }
                /*--- printk(KERN_ERR "[%s] array_element_size=%d\n", __func__, array_element_size); ---*/

                while(array_element_anzahl) {
                    /*--- printk(KERN_ERR "[%s:%d] array_element_anzahl=%d\n", __func__, __LINE__, array_element_anzahl); ---*/
                    if(C->substruct) {
                        sub_length = convert_endian(id, func, select_value, bytes_to_convert, C->substruct, in + C->offset + new_length.inCorr, out + C->offset + new_length.outCorr, version_info, level+1);
                        global_level = level;

                        array_element_size = sub_length.converted_bytes;
                    } else {
                        func(in + C->offset + new_length.inCorr, out + C->offset + new_length.outCorr, array_element_size, C);
                    }
                    /*--- printk(KERN_ERR "[%s:%d] array_element_size=%d, bytes_to_convert=%d, length=%d\n", __func__, __LINE__, array_element_size, bytes_to_convert, length); ---*/

                    in += array_element_size;
                    out += array_element_size;

                    if(bytes_to_convert < array_element_size) {
                        printk(KERN_ERR "[%s] fatal error: array length (rest: %d) > bytes_to_convert (%d) \n", __FUNCTION__, array_element_anzahl, bytes_to_convert);
                        return errReturn;
                    }
                    array_element_anzahl--;
                    bytes_to_convert           -= array_element_size;
                    new_length.converted_bytes += array_element_size;
                    new_length.inCorr += sub_length.inCorr;
                    new_length.outCorr += sub_length.outCorr;
                    this_layer_sublength_C.length += array_element_size;
                }
                if((C->flags & ENDIAN_CONVERT_LAST)) {
                    break;
                }
                C++;
            } else if(C->flags & ENDIAN_CONVERT_REPEAT) {
                in += (sub_length.converted_bytes + C->size);
                out += (sub_length.converted_bytes + C->size);
                this_layer_sublength_C.length += array_element_size;

                if(array_element_anzahl) {
                    /*--- printk(KERN_ERR "[%s] ENDIAN_CONVERT_REPEAT: element_anzahl %d\n", __FUNCTION__, array_element_anzahl); ---*/
                    /*--- array element anzahl kann genutzt werden um die Array anzahl zu begrenzen */
                    array_element_anzahl--;

                    if(array_element_anzahl == 0) {
                        if((C->flags & ENDIAN_CONVERT_LAST)) {
                            break;
                        }
                        C++;
                    }
                }
            } else {
                if((C->flags & ENDIAN_CONVERT_LAST)) {
                    break;
                }
                C++;
            }
        }
    }
    if(!(C->flags & ENDIAN_CONVERT_LAST)) {
        printk(KERN_ERR "[%s] fatal error: sudden end of message, \n"
                        "\tat field: '%s' \n"
                        "\tin struct: '%s'\n"
                        "\tin entry struct: '%s'\n"
                        "\tglobal_len: %d\n"
                        "\tentry_len: %d\n"
                        "\tlevel: %u\n",
                __FUNCTION__, C->name, C->struct_name, entry_name, bytes_to_convert, entry_len, level);
        return errReturn;
    }

    if(this_layer_sublength_C.C) {
        C = this_layer_sublength_C.C;

        //printk(KERN_ERR "%d: length=%d, *out=0x%x\n", __LINE__,
        //        this_layer_sublength_C.length,
        //        *(unsigned int*)this_layer_sublength_C.out
        //        );

        if(C->bits) {
            // out is machine format?
            if (func == one_entry_swap_from_machine) {
                #ifdef MACHINE_IS_LITTLE_ENDIAN
                    *(unsigned int*)this_layer_sublength_C.out =   (*(unsigned int*)this_layer_sublength_C.out & ~(((1 << C->bits)-1) << C->bit_offset))    // Löschen des alten Wertes
                                                                 | ((this_layer_sublength_C.length & ((1 << C->bits)-1)) << C->bit_offset);                 // Setzen des neuen Wertes
                #else /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
                    *(unsigned int*)this_layer_sublength_C.out =   (*(unsigned int*)this_layer_sublength_C.out & ~(((1 << C->bits)-1) << (C->bit_offset - C->bits)))    // Löschen des alten Wertes
                                                                 | ((this_layer_sublength_C.length & ((1 << C->bits)-1)) << (C->bit_offset - C->bits));     // Setzen des neuen Wertes
                #endif /*--- #else ---*/ /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
            } else if (func == one_entry_swap_to_machine) {
                #ifdef MACHINE_IS_LITTLE_ENDIAN
                    *(unsigned int*)this_layer_sublength_C.out =   (*(unsigned int*)this_layer_sublength_C.out & ~(((1 << C->bits)-1) << C->bit_offset))    // Löschen des alten Wertes
                                                                 | ((this_layer_sublength_C.length & ((1 << C->bits)-1)) << C->bit_offset);                 // Setzen des neuen Wertes
                #else /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
                    *(unsigned int*)this_layer_sublength_C.out =   (*(unsigned int*)this_layer_sublength_C.out & ~(((1 << C->bits)-1) << (C->bit_offset - C->bits)))    // Löschen des alten Wertes
                                                                 | ((this_layer_sublength_C.length & ((1 << C->bits)-1)) << (C->bit_offset - C->bits));     // Setzen des neuen Wertes
                #endif /*--- #else ---*/ /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
            } else {
                *(unsigned int*)this_layer_sublength_C.out =   (*(unsigned int*)this_layer_sublength_C.out & ~(((1 << C->bits)-1) << (C->bit_offset - C->bits)))        // Löschen des alten Wertes
                                                             | ((this_layer_sublength_C.length & ((1 << C->bits)-1)) << (C->bit_offset - C->bits));         // Setzen des neuen Wertes
            }
        } else {
            // out is machine format?
            if (func == one_entry_swap_from_machine) {
                #ifdef MACHINE_IS_LITTLE_ENDIAN
                    func((((unsigned char*)&this_layer_sublength_C.length)), this_layer_sublength_C.out, C->size, C);
                #else /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
                    func((((unsigned char*)&this_layer_sublength_C.length) + (4-C->size)), this_layer_sublength_C.out, C->size, C);
                #endif /*--- #else ---*/ /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
            } else if (func == one_entry_swap_to_machine) {
                #ifdef MACHINE_IS_LITTLE_ENDIAN
                    one_entry_no_swap_just_check((((unsigned char*)&this_layer_sublength_C.length)), this_layer_sublength_C.out, C->size, C);
                #else /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
                    one_entry_no_swap_just_check((((unsigned char*)&this_layer_sublength_C.length) + (4-C->size)), this_layer_sublength_C.out, C->size, C);
                #endif /*--- #else ---*/ /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
            } else {
                one_entry_no_swap_just_check((((unsigned char*)&this_layer_sublength_C.length) + (4-C->size)), this_layer_sublength_C.out, C->size, C);
            }
        }
        //printk(KERN_ERR "%d: *out=0x%x\n", __LINE__,
        //        *(unsigned int*)this_layer_sublength_C.out
        //        );
    }

    if(this_layer_totallength_C.C) {
        C = this_layer_totallength_C.C;

        //printk(KERN_ERR "%d: length=%d, *out=0x%x\n", __LINE__,
        //        this_layer_totallength_C.length,
        //        *(unsigned int*)this_layer_totallength_C.out
        //        );
        if(C->bits) {
            // out is machine format?
            if (func == one_entry_swap_from_machine) {
                #ifdef MACHINE_IS_LITTLE_ENDIAN
                    *(unsigned int*)this_layer_totallength_C.out =   (*(unsigned int*)this_layer_totallength_C.out & ~(((1 << C->bits)-1) << C->bit_offset))    // Löschen des alten Wertes
                                                                   | __bswap_32(this_layer_totallength_C.length & ((1 << C->bits)-1));                          // Setzen des neuen Wertes
                #else /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
                    *(unsigned int*)this_layer_totallength_C.out =   (*(unsigned int*)this_layer_totallength_C.out & ~(((1 << C->bits)-1) << (C->bit_offset - C->bits)))    // Löschen des alten Wertes
                                                                   | ((this_layer_totallength_C.length & ((1 << C->bits)-1)) << (C->bit_offset - C->bits));     // Setzen des neuen Wertes
                #endif /*--- #else ---*/ /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
            } else if (func == one_entry_swap_to_machine) {
                #ifdef MACHINE_IS_LITTLE_ENDIAN
                    *(unsigned int*)this_layer_totallength_C.out =   (*(unsigned int*)this_layer_totallength_C.out & ~(((1 << C->bits)-1)))                     // Löschen des alten Wertes
                                                                   | (this_layer_totallength_C.length & ((1 << C->bits)-1));                                    // Setzen des neuen Wertes
                #else /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
                    *(unsigned int*)this_layer_totallength_C.out =   (*(unsigned int*)this_layer_totallength_C.out & ~(((1 << C->bits)-1) << (C->bit_offset - C->bits)))    // Löschen des alten Wertes
                                                                   | ((this_layer_totallength_C.length & ((1 << C->bits)-1)) << (C->bit_offset - C->bits));     // Setzen des neuen Wertes
                #endif /*--- #else ---*/ /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
            } else {
                *(unsigned int*)this_layer_totallength_C.out =   (*(unsigned int*)this_layer_totallength_C.out & ~(((1 << C->bits)-1) << (C->bit_offset - C->bits)))        // Löschen des alten Wertes
                                                               | ((this_layer_totallength_C.length & ((1 << C->bits)-1)) << (C->bit_offset - C->bits));         // Setzen des neuen Wertes
            }
        } else {
            // out is machine format?
            if (func == one_entry_swap_from_machine) {
                #ifdef MACHINE_IS_LITTLE_ENDIAN
                    func((((unsigned char*)&this_layer_totallength_C.length)), this_layer_totallength_C.out, C->size, C);
                #else /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
                    func((((unsigned char*)&this_layer_totallength_C.length) + (4-C->size)), this_layer_totallength_C.out, C->size, C);
                #endif /*--- #else ---*/ /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
            } else if (func == one_entry_swap_to_machine) {
                #ifdef MACHINE_IS_LITTLE_ENDIAN
                    one_entry_no_swap_just_check((((unsigned char*)&this_layer_totallength_C.length)), this_layer_totallength_C.out, C->size, C);
                #else /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
                    one_entry_no_swap_just_check((((unsigned char*)&this_layer_totallength_C.length) + (4-C->size)), this_layer_totallength_C.out, C->size, C);
                #endif /*--- #else ---*/ /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
            } else {
                one_entry_no_swap_just_check((((unsigned char*)&this_layer_totallength_C.length) + (4-C->size)), this_layer_totallength_C.out, C->size, C);
            }
        }
        //printk(KERN_ERR "%d: *out=0x%x\n", __LINE__,
        //        *(unsigned int*)this_layer_totallength_C.out
        //        );
    }

    return new_length;
}

/*------------------------------------------------------------------------------------------*\
 * Public
\*------------------------------------------------------------------------------------------*/
int id = 0;
int convert_fromBigEndian_toMachine(unsigned int bytes_to_convert __unused__,
                                    struct _endian_convert *C __unused__,
                                    unsigned char *in __unused__,
                                    unsigned char *out __unused__,
                                    unsigned int version_from) {
    sublength_t ret = {.err=0, .converted_bytes=0, .inCorr=0, .outCorr=0};
    struct convert_version_info version_info = { .in_version = version_from, .out_version = AVM_DIST_EVENT_VERSION };

    #ifdef MACHINE_IS_LITTLE_ENDIAN
        ret = convert_endian(id++, one_entry_swap_to_machine, 0, bytes_to_convert, C, in, out, version_info, 0);
    #else /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
        #ifdef DO_CHECK_AHA_MESSAGE
            ret = convert_endian(id++, one_entry_no_swap_just_check, 0, bytes_to_convert, C, in, out, version_info, 0);
        #else /*--- #ifdef DO_CHECK_AHA_MESSAGE ---*/
            if(in != out)
                memcpy(out, in, bytes_to_convert);
        #endif /*--- #else ---*/ /*--- #ifdef DO_CHECK_AHA_MESSAGE ---*/
    #endif /*--- #else ---*/ /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/

    return ret.err ? ret.err : (int)ret.converted_bytes + ret.outCorr;
}

int convert_fromLittleEndian_toMachine(unsigned int bytes_to_convert __unused__,
                                       struct _endian_convert *C __unused__,
                                       unsigned char *in __unused__,
                                       unsigned char *out __unused__,
                                       unsigned int version_from) {
    sublength_t ret = {.err=0, .converted_bytes=0, .inCorr=0, .outCorr=0};
    struct convert_version_info version_info = { .in_version = version_from, .out_version = AVM_DIST_EVENT_VERSION };

    #ifdef MACHINE_IS_LITTLE_ENDIAN
        #ifdef DO_CHECK_AHA_MESSAGE
            ret = convert_endian(id++, one_entry_no_swap_just_check, 0, bytes_to_convert, C, in, out, version_info, 0);
        #else /*--- #ifdef DO_CHECK_AHA_MESSAGE ---*/
            if(in != out)
                memcpy(out, in, bytes_to_convert);
        #endif /*--- #else ---*/ /*--- #ifdef DO_CHECK_AHA_MESSAGE ---*/
    #else /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
        ret = convert_endian(id++, one_entry_swap_to_machine, 0, bytes_to_convert, C, in, out, version_info, 0);
    #endif /*--- #else ---*/ /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/

    return ret.err ? ret.err : (int)ret.converted_bytes + ret.outCorr;
}

int convert_fromMachine_toBigEndian(unsigned int bytes_to_convert __unused__,
                                    struct _endian_convert *C __unused__,
                                    unsigned char *in __unused__,
                                    unsigned char *out __unused__,
                                    unsigned int version_to) {
    sublength_t ret = {.err=0, .converted_bytes=0, .inCorr=0, .outCorr=0};
    struct convert_version_info version_info = { .in_version = AVM_DIST_EVENT_VERSION, .out_version = version_to };

    #ifdef MACHINE_IS_LITTLE_ENDIAN
        ret = convert_endian(id++, one_entry_swap_from_machine, 0, bytes_to_convert, C, in, out, version_info, 0);
    #else /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
        #ifdef DO_CHECK_AHA_MESSAGE
            ret = convert_endian(id++, one_entry_no_swap_just_check, 0, bytes_to_convert, C, in, out, version_info, 0);
        #else /*--- #ifdef DO_CHECK_AHA_MESSAGE ---*/
            if(in != out)
                memcpy(out, in, bytes_to_convert);
        #endif /*--- #else ---*/ /*--- #ifdef DO_CHECK_AHA_MESSAGE ---*/
    #endif /*--- #else ---*/ /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/

    return ret.err ? ret.err : (int)ret.converted_bytes + ret.outCorr;
}

int convert_fromMachine_toLittleEndian(unsigned int bytes_to_convert __unused__,
                                       struct _endian_convert *C __unused__,
                                       unsigned char *in __unused__,
                                       unsigned char *out __unused__,
                                       unsigned int version_to) {
    sublength_t ret = {.err=0, .converted_bytes=0, .inCorr=0, .outCorr=0};
    struct convert_version_info version_info = { .in_version = AVM_DIST_EVENT_VERSION, .out_version = version_to };

    #ifdef MACHINE_IS_LITTLE_ENDIAN
        #ifdef DO_CHECK_AHA_MESSAGE
            ret = convert_endian(id++, one_entry_no_swap_just_check, 0, bytes_to_convert, C, in, out, version_info, 0);
        #else /*--- #ifdef DO_CHECK_AHA_MESSAGE ---*/
            if(in != out)
                memcpy(out, in, bytes_to_convert);
        #endif /*--- #else ---*/ /*--- #ifdef DO_CHECK_AHA_MESSAGE ---*/
    #else /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/
        ret = convert_endian(id++, one_entry_swap_from_machine, 0, bytes_to_convert, C, in, out, version_info, 0);
    #endif /*--- #else ---*/ /*--- #ifdef MACHINE_IS_LITTLE_ENDIAN ---*/

    return ret.err ? ret.err : (int)ret.converted_bytes + ret.outCorr;
}

int convert_fromMachine_toMachine(unsigned int bytes_to_convert __unused__,
                                  struct _endian_convert *C __unused__,
                                  unsigned char *in __unused__,
                                  unsigned char *out __unused__,
                                  unsigned int version_from) {
    sublength_t ret = {.err=0, .converted_bytes=0, .inCorr=0, .outCorr=0};
    struct convert_version_info version_info = { .in_version = version_from, .out_version = AVM_DIST_EVENT_VERSION };

    ret = convert_endian(id++, one_entry_no_swap_just_check, 0, bytes_to_convert, C, in, out, version_info, 0);

    return ret.err ? ret.err : (int)ret.converted_bytes + ret.outCorr;
}
