/*--------------------------------------------------------------------------------*\
 * Ueber echo "<func>(arg0, .... arg4) [cpu<x>] [simulate]" >/proc/avm/call
 * eine beliebige Kernelfunktion ausfuehren -nur MIPS und Entwicklerversion
\*--------------------------------------------------------------------------------*/
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/kallsyms.h>
#include <linux/simple_proc.h>

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0) && defined(CONFIG_MIPS)
#include <linux/avm_kernel_config.h>
#endif/*--- #if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0) ---*/

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
struct _call_list {
    unsigned int  args[5];
    unsigned int  argcount;
    unsigned int  argstring_mask;
    unsigned int  simulate;
    unsigned int  asmlink;
    unsigned long func;
    char *tmpbuf;
};
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static char *print_callback_string(char *txt, unsigned int txt_len, struct _call_list *call) {
    char *p = txt;
    unsigned int idx = 0, len;

    len = snprintf(p, txt_len, "%pS(", (void *)call->func);
    if(txt_len >= len) txt_len -= len, p+= len;

    for(idx = 0; idx < call->argcount; idx++) {
        if(call->argstring_mask & (1 << idx)) {
            len = snprintf(p, txt_len, "%s\"%s\"", idx ? "," : "", (char *)call->args[idx]);
        } else {
            len = snprintf(p, txt_len, "%s0x%x", idx ? "," : "", call->args[idx]);
        }
        if(txt_len >= len) txt_len -= len, p+= len;
    }
    snprintf(p, txt_len, ")");
    return txt;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void call_function(struct _call_list *call) {
    unsigned int ret = 0;
    char txt[256];
    if(call->simulate) {
        printk(KERN_ERR"Call%s:%s\n", call->asmlink ? "(asmlinkage)" : "", print_callback_string(txt, sizeof(txt), call));
        kfree(call);
        return;
    }
    if(call->asmlink) {
        switch(call->argcount) {
            case 0: ret = ((asmlinkage unsigned int (*)(void))call->func)(); break;
            case 1: ret = ((asmlinkage unsigned int (*)(unsigned int))call->func)(call->args[0]); break;
            case 2: ret = ((asmlinkage unsigned int (*)(unsigned int, unsigned int))call->func)(call->args[0], call->args[1]); break;
            case 3: ret = ((asmlinkage unsigned int (*)(unsigned int, unsigned int, unsigned int))call->func)(call->args[0], call->args[1], call->args[2]); break;
            case 4: ret = ((asmlinkage unsigned int (*)(unsigned int, unsigned int, unsigned int, unsigned int))call->func)(call->args[0], call->args[1], call->args[2], call->args[3]); break;
            case 5: ret = ((asmlinkage unsigned int (*)(unsigned int, unsigned int, unsigned int, unsigned int, unsigned int))call->func)(call->args[0], call->args[1], call->args[2], call->args[3], call->args[4]); break;
        }
    } else {
        switch(call->argcount) {
            case 0: ret = ((unsigned int (*)(void))call->func)(); break;
            case 1: ret = ((unsigned int (*)(unsigned int))call->func)(call->args[0]); break;
            case 2: ret = ((unsigned int (*)(unsigned int, unsigned int))call->func)(call->args[0], call->args[1]); break;
            case 3: ret = ((unsigned int (*)(unsigned int, unsigned int, unsigned int))call->func)(call->args[0], call->args[1], call->args[2]); break;
            case 4: ret = ((unsigned int (*)(unsigned int, unsigned int, unsigned int, unsigned int))call->func)(call->args[0], call->args[1], call->args[2], call->args[3]); break;
            case 5: ret = ((unsigned int (*)(unsigned int, unsigned int, unsigned int, unsigned int, unsigned int))call->func)(call->args[0], call->args[1], call->args[2], call->args[3], call->args[4]); break;
        }
    }
    printk(KERN_ERR"\n----> Called%s:%s ret=0x%08x\n", call->asmlink ? "(asmlinkage)" : "", print_callback_string(txt, sizeof(txt), call), ret);
    kfree(call);
}
#if defined(CONFIG_SMP)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void start_func_on_cpu(void *param) {
    call_function((struct _call_list *)param);
}
#endif/*--- #if defined(CONFIG_SMP) ---*/

#define SKIP_SPACES(txt) while(*txt && ((*txt == ' ') || (*txt == '\t'))) txt++
#define SKIP_UNTIL_SPACES_OR_SIGN(txt, sign) while(*txt && ((*txt != ' ') && (*txt != '\t') && (*txt != sign))) txt++
#define SKIP_UNTIL_SEPERATOR(txt) while(*txt && ((*txt != ' ') && (*txt != ',') && (*txt != '\t') && (*txt != ')'))) txt++
#define SKIP_UNTIL(txt, sign) while(*txt && (*txt != sign)) txt++

/*--------------------------------------------------------------------------------*\
 * format: (arg1, arg2, arg3 ...)
 * arg<x>  0x<val>   -> hex
 *          val      -> dezimal
 *          'string' -> string 
 *          "string" -> string 
 * 
 * ret: NULL Fehler, sonst pointer hinter ')'
 * Beispiel:
 * echo 'strstr("test", "s" ) ' >/proc/avm/call
\*--------------------------------------------------------------------------------*/
static char *scan_arguments(char *txt, struct _call_list *call) {
    unsigned int idx = 0;
    SKIP_SPACES(txt);
    SKIP_UNTIL(txt,'(');
    if(*txt == 0) {
        printk(KERN_ERR"invalid arguments - missing '('\n");
        return NULL;
    }
    txt++;
    for(;;) {
        SKIP_SPACES(txt);
        if(txt[0] == 0) {
            printk(KERN_ERR"%s:missing ')'\n", __func__);
            return NULL;
        }
        if(txt[0] == ')') {
            txt++;
            break;
        }
        if (idx >= ARRAY_SIZE(call->args)){
            printk(KERN_ERR"%s:too much arguments\n", __func__);
            return NULL;
        }
        if(txt[0] == '0' && txt[1] == 'x') {
            sscanf(txt, "0x%x", &call->args[idx]);
        } else if(txt[0] == '\'' || txt[0] == '"') {
            unsigned int len;
            unsigned char endsign = txt[0];
            char *end;
            txt++;
            end = txt;
            SKIP_UNTIL(end, endsign);
            if(*end != endsign) {
                printk(KERN_ERR"%s:invalid arguments - missing %c\n", __func__, endsign);
                return NULL;
            }
            len  = end - txt;
            memcpy(call->tmpbuf, txt, len);
            call->args[idx] = (unsigned int)call->tmpbuf;
            call->argstring_mask |= 1 << idx;
            call->tmpbuf += len + 1;
            txt = end + 1;
        } else {
            sscanf(txt, "%d", &call->args[idx]);
        }
        idx++;
        SKIP_UNTIL_SEPERATOR(txt);
        if(*txt == ',') txt++;
    } 
    call->argcount = idx;
    return txt;
}
/*--------------------------------------------------------------------------------*\
 * fuehrt eine Funktion aus: 
\*--------------------------------------------------------------------------------*/
static int lproc_wd_call(char *buffer, void *priv __attribute__((unused))) {
    struct _call_list *pcall;
    char namebuf[KSYM_NAME_LEN];
    char *p1, *p = buffer;
    unsigned int cpu, this_cpu;
    
    pcall = kzalloc(sizeof(struct _call_list) + strlen(buffer) + 1 + ARRAY_SIZE(pcall->args), GFP_KERNEL);
    if(pcall == NULL) {
        return 0;
    }
    pcall->tmpbuf = (char *)(pcall + 1);
    SKIP_SPACES(p);
    /*--- extrahiere Funktionsname/Funktionspointer: ---*/
    p1 = p;
    SKIP_UNTIL_SPACES_OR_SIGN(p1, '(');
    if(*p1) {
        size_t len = min((size_t)(p1 - p), sizeof(namebuf) - 1);
        memcpy(namebuf, p,  len);
        namebuf[len] = 0;
        pcall->func = (unsigned long)kallsyms_lookup_name(namebuf);
    }
    if(pcall->func == 0) {
        sscanf(p,"%lx", &pcall->func);
    }
    if(!func_ptr_is_kernel_text((void *)pcall->func)) {
        printk(KERN_ERR"invalid func-addr use: <funcname/addr>(arg0, .. arg4) [cpu<x>] [simulate] [asmlinkage] ('arg' for string)\n");
        kfree(pcall);
        return 0;
    }
    /*--- extrahiere Argumente: ---*/
    if((p = scan_arguments(p, pcall)) == NULL) {
        kfree(pcall);
        return 0;
    }
    /*--- nur simulieren (geparste Argumente anzeigen) ? ---*/
    if(strstr(p, "sim")) {
        pcall->simulate = 1;
    }
    if(strstr(p, "asm")) {
        pcall->asmlink = 1;
    }
    /*---ausfuehren auf CPU<x> ? ---*/
    this_cpu = get_cpu();
    if((p = strstr(p, "cpu"))) {
        p += sizeof("cpu") - 1;
        cpu = p[0] - '0';
    } else {
        cpu = this_cpu;
    }
#if defined(CONFIG_SMP)
    if(cpu_online(cpu) && (cpu != this_cpu)) {
        smp_call_function_single(cpu, start_func_on_cpu, (void *)pcall, 0);
    } else 
#endif /*--- #if defined(CONFIG_SMP) ---*/
        call_function(pcall);
    put_cpu();
    return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int __init avm_proc_call_init(void) {
    int add_proc = 0;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0) && defined(CONFIG_MIPS)
    if(avm_kernel_version_info) {
        int len;
        len = strlen(avm_kernel_version_info->svnversion);
        /*--- printk("%s: '%s' '%s'\n", __func__, avm_kernel_version_info->firmwarestring, avm_kernel_version_info->svnversion); ---*/
        if(len && avm_kernel_version_info->svnversion[len-1] == 'M'){
            add_proc = 1;
        }
    }
#endif
    if(add_proc) {
        add_simple_proc_file( "avm/call", lproc_wd_call, NULL, NULL);
    }
    return 0;
}
late_initcall(avm_proc_call_init);
