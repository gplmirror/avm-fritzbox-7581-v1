#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <asm/prom.h>
#include <board.h>
#include "pushbutton.h"
#include <linux/avm_hw_config.h>
#include <mach/avm_hw_config_hw224.h>
#include <mach/avm_hw_config_hw228.h>


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config_table avm_hw_config_tables[] = {
    {  .hwrev = 224, .table = avm_hardware_config_hw224 },   /*--- FRITZ!Box 7581 ---*/
    {  .hwrev = 228, .table = avm_hardware_config_hw228 }    /*--- FRITZ!Box 7582 ---*/
};
EXPORT_SYMBOL(avm_hw_config_tables);



/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config *avm_current_hw_config = NULL;
EXPORT_SYMBOL(avm_current_hw_config);

struct _avm_hw_config *avm_get_hw_config_table(void)
{
    static struct _avm_hw_config *current_config_table = NULL;
    unsigned int hwrev, hwsubrev, i;
    char *s;

    if(current_config_table) {
        return current_config_table;
    }

    s = prom_getenv("HWRevision");
    if (s) {
        hwrev = simple_strtoul(s, NULL, 10);
    } else {
        hwrev = 0;
    } 
    s = prom_getenv("HWSubRevision");
    if (s) {
        hwsubrev = simple_strtoul(s, NULL, 10);
    } else {
        hwsubrev = 1;
    }    
    if(hwrev == 0) {
        printk("[%s] error: no HWRevision detected in environment variables\n", __FUNCTION__);
        BUG_ON(1);
        return NULL;
    }
    for(i = 0; i < sizeof(avm_hw_config_tables)/sizeof(struct _avm_hw_config_table); i++) {
        if(avm_hw_config_tables[i].hwrev == hwrev) {
            if((avm_hw_config_tables[i].hwsubrev == 0) || (avm_hw_config_tables[i].hwsubrev == hwsubrev)) {
                current_config_table = avm_hw_config_tables[i].table;
                return current_config_table;
            }
        }
    }
    printk("[%s] error: No hardware configuration defined for HWRevision %d\n", __FUNCTION__, hwrev);
    BUG_ON(1);
    return NULL;
}
EXPORT_SYMBOL(avm_get_hw_config_table);

