/*------------------------------------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------------------------------------*/

#ifndef _AVM_GPIO_H_
#define _AVM_GPIO_H_


#define GPIO_OK                 (0)
#define GPIO_FAIL               (-1)

enum _hw_gpio_function
{
    FUNCTIONAL_PIN = 0,
    GPIO_PIN = 1
};

enum _hw_gpio_direction
{
    GPIO_INPUT_PIN = 0,
    GPIO_OUTPUT_PIN = 1
};

enum _hw_gpio_polarity {
    GPIO_ACTIVE_HIGH = 0,
    GPIO_ACTIVE_LOW  = 1
};

enum _hw_gpio_sensitivity {
	GPIO_LEVEL_SENSITIVE       = 0,
	GPIO_EDGE_SENSITIVE        = 1,
	GPIO_BOTH_EDGES_SENSITIVE  = 2
};

typedef enum _hw_gpio_function  GPIO_MODE;
typedef enum _hw_gpio_direction GPIO_DIR;
typedef enum _hw_gpio_polarity  GPIO_POLAR;

int brcm_gpio_init(void);
int brcm_gpio_ctrl(unsigned int gpio_pin, enum _hw_gpio_function pin_mode, enum _hw_gpio_direction pin_dir);
int brcm_button_request_callback(unsigned int mask, enum _hw_gpio_polarity mode, 
                                 enum _hw_gpio_sensitivity edge, int (*handle_func)(unsigned int));
void brcm_button_disable_callback(int index);
int brcm_gpio_out_bit(unsigned int gpio_pin, int value);
int brcm_gpio_out_bit_no_sched(unsigned int gpio_pin, int value);
int brcm_gpio_in_bit(unsigned int gpio_pin);
void brcm_gpio_set_bitmask(uint64_t mask, uint64_t value);


#endif/*--- #ifndef _AVM_GPIO_H_ ---*/

