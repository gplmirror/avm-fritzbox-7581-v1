/*------------------------------------------------------------------------------------------*\
 *
 * Hardware Config für FRITZ!Box 7569
 *
\*------------------------------------------------------------------------------------------*/


struct _avm_hw_config avm_hardware_config_hw224[] = {


    /****************************************************************************************\
     *
     * GPIO Config
     *
    \****************************************************************************************/


    /*------------------------------------------------------------------------------------------*\
     * LEDs / Taster
    \*------------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_led_power",
        .value  = BP_GPIO_21_AH,
        .param  = avm_hw_param_gpio_out_active_low,
    },
    {
        .name   = "gpio_avm_led_info_red",
        .value  = BP_GPIO_7_AH,
        .param  = avm_hw_param_gpio_out_active_low,
    },
    {
        .name   = "gpio_avm_led_internet",
        .value  = BP_GPIO_26_AH,
        .param  = avm_hw_param_gpio_out_active_low,
    },
    {
        .name   = "gpio_avm_led_wan",
        .value  = BP_GPIO_15_AH,
        .param  = avm_hw_param_gpio_out_active_low,
    },
    {
        .name   = "gpio_avm_led_wlan",
        .value  = BP_GPIO_5_AH,
        .param  = avm_hw_param_gpio_out_active_low,
    },
    {
        .name   = "gpio_avm_led_info",
        .value  = BP_GPIO_24_AH,
        .param  = avm_hw_param_gpio_out_active_low,
    },


    {
        .name   = "gpio_avm_button_dect",
        .value  =  PB_BUTTON_0,
        .param  = avm_hw_param_gpio_in_active_low,
    },
    {
        .name   = "gpio_avm_button_wps",
        .value  =  PB_BUTTON_1,
        .param  = avm_hw_param_gpio_in_active_low,
    },
    {
        .name   = "gpio_avm_button_wlan",
        .value  =  PB_BUTTON_2,
        .param  = avm_hw_param_gpio_in_active_low,
    },

    /*--------------------------------------------------------------------------------------*\
     * PCM-BUS
    \*--------------------------------------------------------------------------------------*/
    { 
        .name   = "gpio_avm_pcmlink_fsc",              
        .value  = BP_GPIO_59_AH,
        .param  = avm_hw_param_gpio_out_active_high,
    },
    {
        .name   = "gpio_avm_pcmlink_do",
        .value  = BP_GPIO_57_AH,
        .param  = avm_hw_param_gpio_out_active_high,
    },
    {
        .name   = "gpio_avm_pcmlink_di",
        .value  = BP_GPIO_56_AH,
        .param  = avm_hw_param_gpio_in_active_high,
    },
    {
        .name   = "gpio_avm_pcmlink_dcl",
        .value  = BP_GPIO_58_AH,
        .param  = avm_hw_param_gpio_out_active_high,
    },
    /*--------------------------------------------------------------------------------------*\
     * Slic-Reset
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_pcmlink_slicreset",
        .value  = BP_GPIO_4_AL,
        .param  = avm_hw_param_gpio_out_active_low,
    },


    {   .name   = NULL }
};
EXPORT_SYMBOL(avm_hardware_config_hw224);


