#if defined(CONFIG_BCM_KF_ARM_BCM963XX)

#include <linux/module.h>
#include <linux/kernel.h>
#include <asm/prom.h>
#include <linux/mtd/mtd.h>
#include <linux/sched.h>
#include <linux/mtd/partitions.h>
#include <linux/vmalloc.h>
#include <linux/printk.h>
#include <linux/zlib.h>

//#define DEBUG_WLAN_DECT_CONFIG
#if defined(DEBUG_WLAN_DECT_CONFIG)
#define DBG_WLAN_DECT(arg...) printk(KERN_ERR arg)
#else
#define DBG_WLAN_DECT(arg...)
#endif

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static unsigned int wlan_dect_config[AVM_MAX_CONFIG_ENTRIES];
extern struct mtd_info *urlader_mtd;

void set_wlan_dect_config_address(unsigned int value) {
    int i = 0;

    DBG_WLAN_DECT("[%s] wlan_dect_config\n", __FUNCTION__);
    while (i < AVM_MAX_CONFIG_ENTRIES) {
        if (!wlan_dect_config[i]) {
            wlan_dect_config[i] = value;
            DBG_WLAN_DECT("[%s] wlan_dect_config[%d] 0x%x\n", __FUNCTION__, i, wlan_dect_config[i]);
            break;
        }
        i++;
    }
}

/*------------------------------------------------------------------------------------------*\
 * config->Len ist schon auf CPU-Byteorder korrigiert
\*------------------------------------------------------------------------------------------*/
static int wlan_dect_read_config(struct wlan_dect_config *config, int offset, unsigned char *buffer, unsigned int bufferlen) {

    unsigned int readlen, status;
    unsigned int configlen = config->Len + sizeof(struct wlan_dect_config);     /*--- den Header mitlesen ---*/
    unsigned char *tmpbuffer;
    z_stream stream;

    memset(&stream, 0, sizeof(stream));

    tmpbuffer = vmalloc(configlen + sizeof(unsigned int));       /*--- wir brauchen einen Buffer zum umkopieren ---*/
    if (!tmpbuffer) {
        printk(KERN_ERR "[%s] ERROR: no mem %d\n", __FUNCTION__, configlen);
        return -1;
    }

    mtd_read(urlader_mtd, offset & ~1, configlen + sizeof(unsigned int), &readlen, tmpbuffer);

    if (readlen != (configlen + sizeof(unsigned int))) {
        printk(KERN_ERR"[%s] ERROR: read Data\n", __FUNCTION__);
        return -5;
    }

    if ((config->Type == WLAN_ZIP) || (config->Type == WLAN2_ZIP)) {
        struct wlan_dect_config *pconfig;

        stream.workspace = vmalloc(zlib_deflate_workspacesize(MAX_WBITS, MAX_MEM_LEVEL));
        if ( ! stream.workspace) {
            printk(KERN_ERR "[%s] no space for workspace\n", __func__);
            return -7;
        }

        stream.data_type = Z_BINARY;
        stream.total_in  = 0;

        zlib_inflateInit(&stream);

        memcpy(buffer, tmpbuffer, sizeof(struct wlan_dect_config));

        stream.next_in  = tmpbuffer + sizeof(struct wlan_dect_config);
        stream.avail_in = config->Len;

        stream.next_out  = buffer + sizeof(struct wlan_dect_config);
        stream.avail_out = bufferlen - sizeof(struct wlan_dect_config);

        status = zlib_inflate(&stream, Z_SYNC_FLUSH);
        switch (status) {
            case Z_STREAM_END:
                zlib_inflateEnd(&stream);
            case Z_OK:
                break;
            default:
                printk(KERN_ERR "[%s] ERROR: zlib_inflate Type %d %s %d\n", __func__, config->Type, stream.msg, status);
        }
        vfree(stream.workspace);

        pconfig = (struct wlan_dect_config *)buffer;
        pconfig->Len = stream.total_out;
        if (config->Type == WLAN_ZIP)
            config->Type = pconfig->Type = WLAN;
        else
            config->Type = pconfig->Type = WLAN2;
    } else {
        memcpy(buffer, &tmpbuffer[offset & 1], config->Len + sizeof(struct wlan_dect_config));

    }
    vfree(tmpbuffer);

#if defined(DEBUG_WLAN_DECT_CONFIG)
    {
        int x;
        printk(KERN_ERR "0x");
        for (x=0;x<config->Len;x++)
            printk("%02x ", buffer[x]);
        printk("\n");
    }
#endif

    return 0;

}

/*------------------------------------------------------------------------------------------*\
 * die dect_wlan_config kann an einer ungeraden Adresse beginnen
\*------------------------------------------------------------------------------------------*/
int get_wlan_dect_config(enum wlan_dect_type Type, unsigned char *buffer, unsigned int len) {

    int i;
    unsigned int readlen = 0;
    struct wlan_dect_config config;
    int offset;
    unsigned char tmpbuffer[2 * sizeof(struct wlan_dect_config)];

    DBG_WLAN_DECT("[%s] Type %d buffer 0x%p len %d\n", __FUNCTION__, Type, buffer , len);

    for (i=0;i<AVM_MAX_CONFIG_ENTRIES;i++) {
        DBG_WLAN_DECT("[%s] wlan_dect_config[%d] 0x%x\n", __FUNCTION__, i , wlan_dect_config[i]);
        if (wlan_dect_config[i]) {    /*--- Eintrag vorhanden und nicht leer ---*/
            offset = wlan_dect_config[i];
            mtd_read(urlader_mtd, offset & ~1, 2 * sizeof(struct wlan_dect_config), &readlen, tmpbuffer);
            DBG_WLAN_DECT("[%s] offset 0x%x readlen %d\n", __FUNCTION__, offset, readlen);

            if (readlen != 2 * sizeof(struct wlan_dect_config)) {
                DBG_WLAN_DECT("[%s] ERROR: read wlan_dect_config\n", __FUNCTION__);
                return -1;
            }
            memcpy(&config, &tmpbuffer[offset & 1], sizeof(struct wlan_dect_config));

#if defined(__LITTLE_ENDIAN)
            {
                /*--- der Header wird immer in BIG_ENDIAN gespeichert ---*/
                unsigned short tmp = be16_to_cpu(config.Len);
                config.Len = tmp;
            }
#endif

            DBG_WLAN_DECT("[%s] Version 0x%x Type %d Len 0x%x\n", __FUNCTION__, config.Version, config.Type, config.Len);
            switch (config.Version) {
	            case 1:
                case 2:
                    if ((Type == WLAN) || (Type == WLAN2)) {
                        if ((config.Type == WLAN_ZIP) || (config.Type == WLAN2_ZIP)) {
                            int status = wlan_dect_read_config(&config, offset, buffer, len);
                            if (status < 0) {
                                DBG_WLAN_DECT("[%s]ERROR: read ZIP Data\n", __func__);
                                return -6;
                            }
                            DBG_WLAN_DECT("{%s} Type %d config.Type %d\n", __func__, Type, config.Type);
                            if (Type == config.Type)
                                return 0;
                        }
                    }
                    if (Type != config.Type) {
                        DBG_WLAN_DECT("[%s/%d] config.Type(%d) != Type(%d)\n", __FUNCTION__, __LINE__, config.Type, Type);
                        break;                      /*--- nächster Konfigeintrag ---*/
                    }
                    if ( ! (len >= config.Len + sizeof(struct wlan_dect_config))) {
                        DBG_WLAN_DECT("[%s/%d] config.Type(%d) != Type(%d)\n", __FUNCTION__, __LINE__, config.Type, Type);
                        return -2;                  /*--- buffer zu klein ---*/
                    }
                    DBG_WLAN_DECT("[%s] read ", __FUNCTION__);
                    switch (config.Type) {
                        case WLAN:
                        case WLAN2:
                            DBG_WLAN_DECT("WLAN\n");
                            break;
                        case DECT:
                            DBG_WLAN_DECT("DECT\n");
                            break;
                        case DOCSIS:
                            DBG_WLAN_DECT("DOCSIS\n");
                            break;
                        case DSL:
                            DBG_WLAN_DECT("DSL\n");
                            break;
                        case ZERTIFIKATE:
                            DBG_WLAN_DECT("ZERTIFIKATE\n");
                            break;
                        default:
                            DBG_WLAN_DECT("Type unknown\n");
                            return -3;
                    }
                    DBG_WLAN_DECT("[%s] uralder_mtd='%s', offset=%d, len=%d \n", __FUNCTION__, urlader_mtd->name, offset,config.Len + sizeof(struct wlan_dect_config));

                    if (wlan_dect_read_config(&config, offset, buffer, len) < 0) {
                        DBG_WLAN_DECT("ERROR: read Data\n");
                        return -5;
                    }
                    return 0;
                default:
                    DBG_WLAN_DECT("[%s] unknown Version %x\n", __FUNCTION__, config.Version);
                    return -3;
            }
        }
    }
    return -1;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int search_wlan_dect_config(enum wlan_dect_type Type, struct wlan_dect_config *config) {

    int i;
    unsigned int readlen = 0;
    int offset;
    unsigned char tmpbuffer[2 * sizeof(struct wlan_dect_config)];

    if (!config) {
        printk( KERN_ERR "[%s] ERROR: no configbuffer\n", __FUNCTION__);
        return -1;
    }

    for (i=0;i<AVM_MAX_CONFIG_ENTRIES;i++) {
        DBG_WLAN_DECT("[%s] wlan_dect_config[%d] 0x%x\n", __FUNCTION__, i , wlan_dect_config[i]);
        if (wlan_dect_config[i]) {    /*--- Eintrag vorhanden und nicht leer ---*/
            offset = wlan_dect_config[i];
            mtd_read(urlader_mtd, offset & ~1, 2 * sizeof(struct wlan_dect_config), &readlen, tmpbuffer);
            if (readlen != 2 * sizeof(struct wlan_dect_config)) {
                DBG_WLAN_DECT("[%s] ERROR: read wlan_dect_config\n", __FUNCTION__);
                return -2;
            }

            memcpy(config, &tmpbuffer[offset & 1], sizeof(struct wlan_dect_config));

#if defined(__LITTLE_ENDIAN)
            {
                /*--- der Header wird immer in BIG_ENDIAN gespeichert ---*/
                unsigned short len = be16_to_cpu(config->Len);
                config->Len = len;
            }
#endif

            switch (config->Version) {
	            case 1:
                case 2:
                    DBG_WLAN_DECT("[%s] Type %d Len 0x%x\n", __FUNCTION__, config->Type, config->Len);
                    if (Type != config->Type) {
                        break;                      /*--- nächster Konfigeintrag ---*/
                    }
                    return 1;
                default:
                    printk( KERN_ERR "[%s] ERROR: unknown ConfigVersion 0x%x\n", __FUNCTION__, config->Version);
                    break;
            }
        }
    }

    /*--- nix gefunden ---*/
    memset(config, 0, sizeof(struct wlan_dect_config));
    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int test_wlan_dect_config(char *buffer, size_t *bufferlen) {

    struct wlan_dect_config config;
    enum wlan_dect_type count = WLAN;
    int tmp = 0, len, error = 0;

    len = *bufferlen;
    *bufferlen = 0;
    buffer[0] = 0;  /*--- damit strcat auch funktioniert ---*/

    while (count < MAX_TYPE) {
        if (search_wlan_dect_config(count, &config)) {
            switch (config.Version) {
                case 1:
                case 2:
                    switch (config.Type) {
                        case WLAN:
                        case WLAN_ZIP:
                            strcat(buffer, "WLAN\n");
                            tmp = strlen("WLAN\n");
                            break;
                        case WLAN2:
                        case WLAN2_ZIP:
                            strcat(buffer, "WLAN2\n");
                            tmp = strlen("WLAN2\n");
                            break;
                        case DECT:
                            strcat(buffer, "DECT\n");
                            tmp = strlen("DECT\n");
                            break;
                        case DOCSIS:
                            strcat(buffer, "DOCSIS\n");
                            tmp = strlen("DOCSIS\n");
                            break;
                        case ZERTIFIKATE:
                            strcat(buffer, "ZERTIFIKATE\n");
                            tmp = strlen("ZERTIFIKATE\n");
                            break;
                        default:
                            printk( KERN_ERR "[%s] ERROR: unknown ConfigVersion 0x%x\n", __FUNCTION__, config.Version);
                            error = -1;
                    }
                    break;
                default:
                    printk( KERN_ERR "[%s] ERROR: unknown ConfigVersion 0x%x\n", __FUNCTION__, config.Version);
                    error = -1;
            }
            if (len > tmp) {
                len -= tmp;
                *bufferlen += tmp;
            } else {
                DBG_WLAN_DECT( KERN_ERR "[%s] ERROR: Buffer\n", __FUNCTION__);
                error = -1;
            }
        }
        count++;
    }

    return error;

}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <linux/fs.h>
#include <linux/file.h>
#include <asm/io.h>
#include <asm/fcntl.h>
#include <asm/errno.h>
#include <asm/ioctl.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/mman.h>

int copy_wlan_dect_config2user(char *buffer, size_t bufferlen) {

    struct wlan_dect_config config;
    char *ConfigStrings[10] = { "WLAN", "DECT", "WLAN2", "ZERTIFIKATE", "DOCSIS", "DSL" , "PROLIFIC", "WLAN_ZIP", "WLAN2_ZIP", "UNKNOWN"};
    enum wlan_dect_type Type;
    char *p, *vbuffer, *map_buffer;
    struct file *fp;
    int    configlen, written;

    if (!bufferlen)
        return -1;

    if (buffer[bufferlen-1] == '\n') {      /*--- \n entfernen ---*/
        buffer[bufferlen-1] = 0; 
        bufferlen--;
    }

    for (Type = WLAN; Type < MAX_TYPE; Type++) {
        p = strstr(buffer, ConfigStrings[Type]);
        if (p) {
            if ((Type == WLAN) && (buffer[4] == '2'))   /*--- WLAN & WLAN2 unterscheiden ---*/
                continue;
            p += strlen(ConfigStrings[Type]);
            break;
        }
    }

    if (!p) {
        printk(KERN_ERR "ERROR: Type unknown\n");
        return -1;
    }

    while (*p && (*p == ' ') && (p < &buffer[bufferlen]))   /*--- die spaces im Pfadnamen löschen ---*/
       p++; 

    if (!search_wlan_dect_config(Type, &config)) {
        printk(KERN_ERR "ERROR: no Config found\n");
        return -1;  /*--- keine Config gefunden ---*/
    }

    configlen = config.Len + sizeof(struct wlan_dect_config);     /*--- wir müssen den Header mitlesen ---*/

    fp = filp_open(p, O_CREAT, FMODE_READ|FMODE_WRITE);  /*--- open read/write ---*/
    if(IS_ERR(fp)) {
        printk("ERROR: Could not open file %s\n", p);
        return -1;
    }

    map_buffer = (unsigned char *)do_mmap(0, 0, configlen, PROT_READ|PROT_WRITE, MAP_SHARED, 0);
    if (IS_ERR(buffer)) {
        printk("ERROR: no mem 0x%p\n", map_buffer);
        return -1;
    }

    vbuffer = (char *)vmalloc(configlen);       /*--- wir brauchen einen Buffer zum umkopieren ---*/
    if (!vbuffer) {
        printk("ERROR: no mem\n");
        return -1;
    }
    if (!get_wlan_dect_config(Type, vbuffer, configlen)) {
        memcpy(map_buffer, &vbuffer[sizeof(struct wlan_dect_config)], config.Len);   /*--- umkopieren & den Header verwerfen ---*/
        written = fp->f_op->write(fp, map_buffer, config.Len, &fp->f_pos);      /*--- die Datei schreiben ---*/

        do_munmap(current->mm, (unsigned long)map_buffer, configlen);    /*--- den buffer wieder frei geben ---*/
        vfree(vbuffer);
        if (written != config.Len) {
            printk("ERROR: write Config\n");
            return -1;
        }
    } else {
        do_munmap(current->mm, (unsigned long)map_buffer, configlen);    /*--- den buffer wieder frei geben ---*/
        vfree(vbuffer);
        printk("ERROR: read Config\n");
        return -1;
    }

    return 0;

}
#if 0 //defined(CONFIG_EARLY_PRINTK)
#include <asm/mach_avm.h>
#include <mach/hw_uart.h>
#define PROM_UART_BASE   AVALANCHE_UART2_REGS_BASE
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int promuart_baud(int baudrate) {
    int baud;
    int uartclk	= PAL_sysClkcGetFreq(PAL_SYS_CLKC_UART2); /* UART2 use the same Clkc 1x */
    baud        = ((uartclk / baudrate) + 8) >> 4;
    return baud;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void promuart_init(unsigned int baudrate) {
    unsigned int baud;
    struct _hw_uart *U = (struct _hw_uart *)PROM_UART_BASE;

    PAL_sysResetCtrl(AVALANCHE_UART1_RESET, OUT_OF_RESET);
    baud = promuart_baud(baudrate);
    U->iir_fcr.Register =  0;       /*--- fifo disabled, no dma, .. ---*/ 
    U->ie.Register      =  0;
    U->lc.Bits.ws       = 3;   /*--- 8 Bit ---*/
    U->lc.Bits.dlab = 1;
    U->data.tx.data = baud & 0xff;
    U->ie.Register  = baud >> 8;
    U->lc.Bits.dlab = 0;

    U->iir_fcr.Bits_fcr.rxrst   =  1;
    U->iir_fcr.Bits_fcr.txrst   =  1;
    U->iir_fcr.Bits_fcr.dmam    =  0;
    U->iir_fcr.Bits_fcr.rxtrg   =  3;
    U->iir_fcr.Bits_fcr.fen     =  1;       /*--- fifo enabled ---*/ 
    U->ie.Bits.erbi             =  1;       /*--- rx-irq enable ---*/ 
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void inline prom_putc(char c) {
    struct _hw_uart *U = (struct _hw_uart *)PROM_UART_BASE;
     while((U->ls.Bits.temt) == 0) 
                ;
     U->data.tx.data = c;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int prom_printf(const char * fmt, ...) {
    static int init;
    static char buf[1024];
    va_list args;
    int l;
    char *p, *buf_end;

    if(init == 0) {
        promuart_init(115200);
        prom_putc('\r');
        prom_putc('\n');
        init = 1;
    }
    va_start(args, fmt);
    l = vsnprintf(buf, sizeof(buf), fmt, args);
    va_end(args);
    buf_end = buf + l;

    for ( p = buf; p < buf_end; p++ ) {
        if ( *p == '\n') {
            prom_putc('\r');
        }
        prom_putc(*p);
    }
    return l;
}
EXPORT_SYMBOL(prom_printf);
#endif/*--- #if defined(CONFIG_EARLY_PRINTK) ---*/

EXPORT_SYMBOL(copy_wlan_dect_config2user);
EXPORT_SYMBOL(test_wlan_dect_config);
EXPORT_SYMBOL(set_wlan_dect_config_address);
EXPORT_SYMBOL(get_wlan_dect_config);

#endif /* CONFIG_BCM_KF_ARM_BCM963XX */
