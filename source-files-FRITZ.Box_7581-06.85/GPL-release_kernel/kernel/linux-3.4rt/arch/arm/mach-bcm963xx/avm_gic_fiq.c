  /*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2016 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/

#include <mach/avm_fiq_groups.h>
#define CLIENT_FIQ_PRIO FIQ_PRIO_WATCHDOG

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/spinlock.h>
#include <linux/io.h>
#include <linux/delay.h>

#include <mach/avm_fiq_os.h>
#include <mach/avm_gic.h>
#include <mach/avm_gic_fiq.h>

/*--- #define DBG_TRC(args...) printk(KERN_INFO args) ---*/
#define DBG_TRC(args...)

// Externer GIC-Lock
extern raw_spinlock_t irq_controller_lock;

extern void __iomem *get_dist_base( unsigned int gic_nr );
extern void __iomem *get_cpu_base( unsigned int gic_nr );

void __iomem *DIST_BASE;
void __iomem *CPU_BASE;
unsigned int MAX_INTERRUPTS;


/*typedef enum gic_reg_atomic_e
{
   IS_NOT_ATOMIC,
   IS_ATOMIC
} gic_reg_atomic_t;*/


#define _ENABLE_SET_      1
#define _ENABLE_CLEAR_    1
#define _PENDING_SET_     1
#define _PENDING_CLEAR_   1
#define _TARGET_BITS_     8
#define _PRIORITY_BITS_   8
#define _CONFIG_BITS_     2
#define _SECURE_BITS_     1

#define groups_per_word(bits_per_group)   (32 / bits_per_group)
#define reg_offset(pinNr, bits_per_group) ((pinNr / (groups_per_word(bits_per_group))) << 2)
#define group(pinNr, bits_per_group)      (pinNr & ((groups_per_word(bits_per_group)) - 1))
#define to_shift(pinNr, bits_per_group)   ((group(pinNr, bits_per_group)) * bits_per_group)
#define group_mask(bits_per_group)        ((1 << bits_per_group) - 1)

#define read_gic_pin(pinNr, regbase, bits_to_use, atomic)                                                       \
   ( (readl_relaxed(regbase + reg_offset(pinNr, bits_to_use)) &                                                 \
           ((group_mask(bits_to_use)) << (to_shift(pinNr, bits_to_use)))) >> (to_shift(pinNr, bits_to_use)) )   \

#define write_en_gic_pin(pinNr, regbase, bits_to_use, val, is_atomic)                                           \
     (writel_relaxed( ((val & (group_mask(bits_to_use))) << (to_shift(pinNr, bits_to_use))),                    \
              (regbase + reg_offset(pinNr, bits_to_use)) ));                                                    \
     dmb()                                                                                                      \

#define write_gic_pin(pinNr, regbase, bits_to_use, val, is_atomic)                                              \
   {                                                                                                            \
      unsigned long flags = 0;                                                                                  \
      if(!is_atomic) {                                                                                          \
         firq_raw_spin_lock_irqsave(&irq_controller_lock, flags);                                               \
      }                                                                                                         \
                                                                                                                \
      writel_relaxed( (readl_relaxed(regbase + reg_offset(pinNr, bits_to_use)) &                                \
                   ~((group_mask(bits_to_use)) << (to_shift(pinNr, bits_to_use)))) |                            \
                    ((val & (group_mask(bits_to_use))) << (to_shift(pinNr, bits_to_use))),                      \
              (regbase + reg_offset(pinNr, bits_to_use)) );                                                     \
      dmb();                                                                                                    \
                                                                                                                \
      if(!is_atomic) {                                                                                          \
         firq_raw_spin_unlock_irqrestore(&irq_controller_lock, flags);                                          \
      }                                                                                                         \
   }                                                                                                            \

/*------------------------------------------------------------------------------------------*/

/* Registerzugriffe */

// ENABLE CLEAR
unsigned int get_ICDICER(unsigned int pinNr, gic_reg_atomic_t is_atomic)
{
   return( read_gic_pin(pinNr, DIST_BASE + OFFSET_ICDICER, _ENABLE_CLEAR_, is_atomic) );
}

void set_ICDICER(unsigned int pinNr, unsigned int val, gic_reg_atomic_t is_atomic)
{
   write_en_gic_pin(pinNr, DIST_BASE + OFFSET_ICDICER, _ENABLE_CLEAR_, val, is_atomic);
   return;
}

// ENABLE SET
unsigned int get_ICDISER(unsigned int pinNr, gic_reg_atomic_t is_atomic)
{
   return( read_gic_pin(pinNr, DIST_BASE + OFFSET_ICDISER, _ENABLE_SET_, is_atomic) );
}

void set_ICDISER(unsigned int pinNr, unsigned int val, gic_reg_atomic_t is_atomic)
{
   write_en_gic_pin(pinNr, DIST_BASE + OFFSET_ICDISER, _ENABLE_SET_, val, is_atomic);
   return;
}

// PENDING CLEAR
unsigned int get_ICDICPR(unsigned int pinNr, gic_reg_atomic_t is_atomic)
{
   return( read_gic_pin(pinNr, DIST_BASE + OFFSET_ICDICPR, _PENDING_CLEAR_, is_atomic) );
}

void set_ICDICPR(unsigned int pinNr, unsigned int val, gic_reg_atomic_t is_atomic)
{
   write_en_gic_pin(pinNr, DIST_BASE + OFFSET_ICDICPR, _PENDING_CLEAR_, val, is_atomic);
   return;
}

// PENDING SET
unsigned int get_ICDISPR(unsigned int pinNr, gic_reg_atomic_t is_atomic)
{
   return( read_gic_pin(pinNr, DIST_BASE + OFFSET_ICDISPR, _PENDING_SET_, is_atomic) );
}

void set_ICDISPR(unsigned int pinNr, unsigned int val, gic_reg_atomic_t is_atomic)
{
   write_en_gic_pin(pinNr, DIST_BASE + OFFSET_ICDISPR, _PENDING_SET_, val, is_atomic);
   return;
}

// TARGET
unsigned int get_ICDIPTR(unsigned int pinNr, gic_reg_atomic_t is_atomic)
{
   return( read_gic_pin(pinNr, DIST_BASE + OFFSET_ICDIPTR, _TARGET_BITS_, is_atomic) );
}

void set_ICDIPTR(unsigned int pinNr, unsigned int val, gic_reg_atomic_t is_atomic)
{
   write_gic_pin(pinNr, DIST_BASE + OFFSET_ICDIPTR, _TARGET_BITS_, val, is_atomic);
   return;
}

// PRIORITY
unsigned int get_ICDIPR(unsigned int pinNr, gic_reg_atomic_t is_atomic)
{
   return( read_gic_pin(pinNr, DIST_BASE + OFFSET_ICDIPR, _PRIORITY_BITS_, is_atomic) );
}

void set_ICDIPR(unsigned int pinNr, unsigned int val, gic_reg_atomic_t is_atomic)
{
   write_gic_pin(pinNr, DIST_BASE + OFFSET_ICDIPR, _PRIORITY_BITS_, val, is_atomic);
   return;
}

// CONFIG
unsigned int get_ICDICR(unsigned int pinNr, gic_reg_atomic_t is_atomic)
{
   return( read_gic_pin(pinNr, DIST_BASE + OFFSET_ICDICR, _CONFIG_BITS_, is_atomic) );
}

void set_ICDICR(unsigned int pinNr, unsigned int val, gic_reg_atomic_t is_atomic)
{
   write_gic_pin(pinNr, DIST_BASE + OFFSET_ICDICR, _CONFIG_BITS_, val, is_atomic);
   return;
}

// SECURITY
unsigned int get_ICDISR(unsigned int pinNr, gic_reg_atomic_t is_atomic)
{
   return( read_gic_pin(pinNr, DIST_BASE + OFFSET_ICDISR, _SECURE_BITS_, is_atomic) );
}

void set_ICDISR(unsigned int pinNr, unsigned int val, gic_reg_atomic_t is_atomic)
{
   write_gic_pin(pinNr, DIST_BASE + OFFSET_ICDISR, _SECURE_BITS_, val, is_atomic);
   return;
}

/* CPU-INTERFACE */

// ICCPMR
unsigned int get_ICCPMR(gic_reg_atomic_t is_atomic)
{
    if(CPU_BASE) {
       unsigned int ret = readl_relaxed(CPU_BASE + OFFSET_ICCPMR);
       dmb();
       return( ret );
    }

    return 0xF0;
}

// ICCPMR
void set_ICCPMR(unsigned int val, gic_reg_atomic_t is_atomic)
{
    if(CPU_BASE) {
       writel_relaxed(val, CPU_BASE + OFFSET_ICCPMR);
       dmb();
       return;
    }

    return;
}

// HIGHEST PENDING PRIORITY
unsigned int get_ICCHPIR(gic_reg_atomic_t is_atomic)
{
    if(CPU_BASE) {
       unsigned int ret = readl_relaxed(CPU_BASE + OFFSET_ICCHPIR);
       dmb();
       return( ret );
    }
    return 0;
}

// Acknowledge
unsigned int get_ICCIAR(gic_reg_atomic_t is_atomic)
{
    if(CPU_BASE) {
       unsigned int ret = readl(CPU_BASE + OFFSET_ICCIAR);
       dmb();
       return( ret );
    }
    return 0;
}

// EOI
void set_ICCEOIR(unsigned int val, gic_reg_atomic_t is_atomic)
{
    if(CPU_BASE) {
       writel_relaxed(val, CPU_BASE + OFFSET_ICCEOIR);
       dmb();
       return;
    }
    return;
}

//CPU CTRL
void set_ICCICR(unsigned int val)
{
    if(CPU_BASE) {
       writel(val, CPU_BASE);
    }
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void avm_gic_fiq_enable(unsigned int pinNr, unsigned int cpu)
{
   DBG_TRC("%s: irq=%u cpu=%u\n", __func__, pinNr, cpu);
   set_ICDISER(pinNr, 1, IS_NOT_ATOMIC);
   //udelay(5);
   return;
}

void avm_gic_fiq_disable(unsigned int pinNr, unsigned int cpu)
{
   DBG_TRC("%s: irq=%u cpu=%u\n", __func__, pinNr, cpu);
   set_ICDICER(pinNr, 1, IS_NOT_ATOMIC);
   //udelay(5);
   return;
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int avm_gic_fiq_is(unsigned int pinNr){
    unsigned int is_fiq = 0;
    if(DIST_BASE) {
        /*--- unsigned long flags; ---*/
        /*--- raw_spin_lock_irqsave(&irq_controller_lock, flags); ---*/
        is_fiq = get_ICDISER(pinNr, IS_ATOMIC) && !get_ICDISR(pinNr, IS_ATOMIC);
        /*--- raw_spin_unlock_irqrestore(&irq_controller_lock, flags); ---*/
    }
    return is_fiq;
}

void avm_gic_fiq_setup(unsigned int pinNr, unsigned int cpumask, unsigned int prio, unsigned int config, unsigned int mode)
{
   unsigned long flags;

   raw_spin_lock_irqsave(&irq_controller_lock, flags);
   DBG_TRC("%s: irq=%u cpumask=%u prio=%u config=%u mode=%u\n", __func__, pinNr, cpumask, prio, config, mode);

   set_ICDICER(pinNr, 1, IS_ATOMIC);
   udelay(5);

   set_ICDIPTR(pinNr, cpumask, IS_ATOMIC);
   set_ICDIPR(pinNr, prio, IS_ATOMIC);
   set_ICDICR(pinNr, config, IS_ATOMIC);
   set_ICDISR(pinNr, mode, IS_ATOMIC);

   udelay(5);
   set_ICDISER(pinNr, 1, IS_ATOMIC);

   raw_spin_unlock_irqrestore(&irq_controller_lock, flags);

   return;
}

void avm_gic_fiq_raise_irq(unsigned int pinNr)
{
   DBG_TRC("%s: irq=%u\n", __func__, pinNr);
   set_ICDISPR(pinNr, 1, IS_NOT_ATOMIC);
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void avm_gic_clearpending_irq(unsigned int pinNr) {
    set_ICDICPR(pinNr, 1, IS_ATOMIC);
}

unsigned int avm_gic_fiq_get_ICCHPIR(void)
{
   return get_ICCHPIR(IS_NOT_ATOMIC);
}

unsigned int avm_gic_fiq_nr_ints(void)
{
   return MAX_INTERRUPTS;
}

void __iomem *avm_gic_fiq_dist_base(void)
{
   return DIST_BASE;
}

void __iomem *avm_gic_fiq_cpu_base(void)
{
   return CPU_BASE;
}

void avm_gic_fiq_init(void)
{
   unsigned int gic_irqs;

   DIST_BASE = get_dist_base(0);
   CPU_BASE = get_cpu_base(0);

   gic_irqs = readl(DIST_BASE + OFFSET_ICDICTR) & 0x1f;
   MAX_INTERRUPTS = (gic_irqs + 1) * 32;
}

