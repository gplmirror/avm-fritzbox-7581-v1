  /*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2016 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/

#include <linux/linkage.h>
#include <asm/assembler.h>
#include <asm/thread_info.h>

#include <asm/asm-offsets.h>
#include <mach/avm_gic.h>


/* ===   AVM SECURE MONITOR FIQ VECTOR  === */

	.arch_extension sec

	.text
	.global avm_secmon_fiqhandler_begin
	.global avm_secmon_fiqhandler_end

	.balign 32
avm_secmon_fiqhandler_begin:
	.long	0x00000000
	.long	0x00000000
1:
	.long	0x00000000
	.long	0x00000000
2:
	.long	0x00000000
	.long	0x00000000
	.long	0x00000000
#if 0
ENTRY(avm_secmon_fiqhandler)
	
	movw	sp, #:lower16:2b
	movt	sp, #:upper16:2b

	stmfd	sp!, {r0-r1}

	mrs	r0, cpsr
	str	r0, [sp, #-4]

	ldr	r0, =0xFCFE8114
	ldr	r1, [r0]
	bic	r1, r1, #0x200000
	str	r1, [r0]

	/* Secure Config Register holen */
	mrc	p15, 0, r1, c1, c1, 0
	bic	r1, r1, #4
	/* Secure Config Register mit geloeschtem FIQ-Monitor zurueckschreiben */
	mcr	p15, 0, r1, c1, c1, 0

	ldmfd	sp!, {r0-r1}

	/* Rückkehr aus dem SECURE MONITOR */
	movs	pc, lr

ENDPROC(avm_secmon_fiqhandler)
#endif

ENTRY(avm_secmon_fiqhandler)

	/* MPIDR (Multiprocessor Identification/Affinity Register) lesen */
	mrc	p15, 0, sp, c0, c0, 5
	and	sp, sp, #3		@ R0: CPU-Nr

        cmp	sp, #0
	movweq	sp, #:lower16:1b
	movteq	sp, #:upper16:1b
	movwne	sp, #:lower16:2b
	movtne	sp, #:upper16:2b

	stmfd	sp, {r0-r1}

	/* Uebergabeparameter fuer den FIQ */
	mrs	r0, spsr

	mov	r1, r0
	bic	r1, r1, #0xFF
	orr	r1, r1, #0xD1
	msr	spsr_c, r1
	
	mov	r1, lr

	movw	lr, #:lower16:4f
	movt	lr, #:upper16:4f

	/* Rückkehr aus dem SECURE MONITOR zur Secmon-FIQ Transition */
	movs	pc, lr

ENDPROC(avm_secmon_fiqhandler)

	/* Immediate Literale fuer den SECURE MONITOR-Handler */
	.align
/*3:	.long	0xc05bef5c		@ Pointer auf den Stack-Bereich*/
avm_secmon_fiqhandler_end:

/* ===   AVM SECURE MONITOR FIQ API  === */

ENTRY(avm_secmon_fiqhandler_prepare)

	stmfd	sp!, {r1}

	/* Sichern und Setzen von MVBAR */
	mrc	p15, 0, r0, c12, c0, 1
	ldr	r1, =avm_secmon_fiqhandler_begin
	mcr	p15, 0, r1, c12, c0, 1

	/* Secure Config Register holen */
	mrc	p15, 0, r1, c1, c1, 0
	orr	r1, r1, #4
	/* Secure Config Register mit FIQ-Monitor zurueckschreiben */
	mcr	p15, 0, r1, c1, c1, 0

	ldmfd	sp!, {r1}

	bx	lr
ENDPROC(avm_secmon_fiqhandler_prepare)

ENTRY(avm_secmon_fiqhandler_cleanup)

	stmfd	sp!, {r1}

	/* Secure Config Register holen */
	mrc	p15, 0, r1, c1, c1, 0
	bic	r1, r1, #4
	/* Secure Config Register mit geloeschtem FIQ-Monitor zurueckschreiben */
	mcr	p15, 0, r1, c1, c1, 0

	/* Wiederherstellen von MVBAR */
	mcr	p15, 0, r0, c12, c0, 1

	ldmfd	sp!, {r1}

	bx	lr
ENDPROC(avm_secmon_fiqhandler_cleanup)


/* ===   AVM FIQ   === */

	.data
	.global avm_fiq_stacks
	.align
avm_fiq_stacks:
	.long	0

	.text
	.global avm_fiq_handler_begin
	.global avm_fiq_handler_end

/* Secmon-FIQ Transition, ab hier im FIQ-Modus
    - R0: SPSR, R0_orig in avm_secmon_fiqhandler_begin
    - R1: LR,   LR_orig in avm_secmon_fiqhandler_begin + 4
*/
4:	str	lr, [sp, #-20]			@ LR in pt_regs Struktur sichern

	msr	spsr_cxsf, r0
	mov	lr, r1
	/*add	lr, lr, #4			@ im LR wird der PC des unterbrochenen Kontextes erwartet
						@ dieser ist bei FIQ-Einsprung mit Offset +4 weiter ...*/
#if 0
	/* LED an */
	ldr	r0, =0xFCFE8114
	ldr	r1, [r0]
	bic	r1, r1, #0x200000
	str	r1, [r0]

	/* !!! DEBUG !!! */
	ldr	r0, 5f
	ldr	r0, [r0]			@ Stack-Array
	ldr	r0, [r0]			@ Stack CPU0

	mov	r1, #THREAD_SIZE
	sub	r1, #0x01
	bic	r0, r0, r1
	add	r0, r0, #0x1000

	ldr	r1, [r0], #4
	cmp	r1, #0
	movne	r0, r1

	mrs	r1, spsr
	str	r1, [r0], #4
	str	lr, [r0], #4
	ldr	r1, [sp, #-20]
	str	r1, [r0], #4
	str	sp, [r0], #4
	mov	r1, r0
	bic	r0, r0, #0xF00
	bic	r0, r0, #0xFF
	str	r1, [r0]
	/* !!!  END  !!! */

	/* Secure Config Register holen */
	mrc	p15, 0, r1, c1, c1, 0
	bic	r1, r1, #4
	/* Secure Config Register mit geloeschtem FIQ-Monitor zurueckschreiben */
	mcr	p15, 0, r1, c1, c1, 0
#endif

	ldr	r0, =avm_secmon_fiqhandler_begin
	/* MPIDR (Multiprocessor Identification/Affinity Register) lesen */
	mrc	p15, 0, r1, c0, c0, 5
	and	r1, r1, #3		@ R0: CPU-Nr
	mov	r1, r1, lsl #3		@ Stack-Offset (CPU-Nr * 8)
	add	r0, r0, r1

	ldr	r1, [r0, #4]
	ldr	r0, [r0]

	//subs	pc, lr, #4

/* Initial vorbereitetes Register:
 *  - SP : Stackpointer
 */
ENTRY(avm_fiq_handler)
avm_fiq_handler_begin:

	/* Kompletten Kontext in struct pt_regs sichern, wir wollen rekursiv/reentrant sein ... */
	sub	lr, lr, #4
	str	lr, [sp, #-4]!			@ PC zur Rueckkehr
						@ Ab hier PT_REGS Struktur:
	str	r0, [sp, #-4]!			@ R0_orig
	mrs	r0, spsr
	str	r0, [sp, #-4]			@ CPSR

//      and	r0, r0, #0x1F
//	cmp	r0, #0x11
//	subne	lr, lr, #4

	mov	r0, sp
	sub	r0, r0, #4
	str	lr, [r0, #-4]!			@ PC
	sub	r0, r0, #4			@ LR
	str	sp, [r0, #-4]!			@ SP
	stmdb	r0!, {r1-r12}			@ R1-R12
	mov	sp, r0
	str	r0, [sp, #-4]!			@ R0

	/* Register vorbereiten:
	 *  - R12: GIC CPU-IF Basis
	 *  - R11: FIQ C-Handler
	 *  - R10: User-Ref
	 */
	mov	r8, #THREAD_SIZE
	sub	r8, #0x01
	mov	r9, sp
	bic	r9, r9, r8
	add	r9, r9, r8
	sub	r9, r9, #0x17

	ldr	r10, [r9], #4
	ldr	r11, [r9], #4
	ldr	r12, [r9]	

	/* Hoechste anliegende Prioritaet lesen */
	ldr	r9, [r12, #OFFSET_ICCHPIR]

	mov	r8, #0x400
	sub	r8, r8, #1
	and	r8, r9, r8			@ untere 10 bit mit 0x3FF herausfiltern

	/* Watchdog */
	cmp	r8, #0x30

	/* Ack für den Interrupt nur im nicht-Watchdog Fall; Interrupt-Nr jetzt in R9 */
	moveq	r9, r8
	ldrne	r9, [r12, #OFFSET_ICCIAR]


        /* --- Kern der FIQ-Routine ---

	/* Register
	 *  - R12: GIC CPU-IF Basis
	 *  - R11: FIQ C-Handler
	 *  - R10: User-Ref
	 *  - R9 : Interrupt-Nr
	 *  - R8 : hoechste anliegende Interrupt-Prio, maskiert
	 */

	/* FIQ private Register auf Stack sichern */
	stmfd	sp!, {r8-r12}

	/* Argumente für den Handler holen,
	 *  - R0: pt_regs
	 *  - R1: Interrupt-Nr
	 *  - R2: Nutzer-Ref
	 * C-Handler wird ueber R11 angesprungen
	 */

	add	r0, sp, #0x14			@ pt_regs
	mov	r1, r9				@ Interrupt-Nr
	mov	r2, r10				@ User-Ref 

	/* Handler rufen */
	blx r11

	/* Sicherstellen, dass alle
	 *  - Speicherzugriffe
	 *  - Cache-,
	 *  - Branch predictor- und
	 *  - TLB-Verwaltungs-Operationen
	 *  beendet werden, bevor weitere Opcodes
	 *  zur Ausfuehrung kommen.
	 */
	dmb

	/* FIQ's private Register wieder herstellen */
	ldmfd   sp!, {r8-r12}

        /* ---------------------------- */


	/* Watchdog */
	cmp	r9, #0x30			@ Watchdog? Dann spaetes Ack, denn wir kommen nur hierher,
						@ wenn auf der anderen CPU das Handling bereits begonnen hat.
	//ldreq	r9, [r12, #OFFSET_ICCIAR]

	/* EOI */
    	strne	r9, [r12, #OFFSET_ICCEOIR]


	/* SPSR fuer Ruecksprung zum vorherigen Modus wieder herstellen */
	ldr	r0, [sp, #64]
	msr	spsr_cxsf, r0

	/* Ruecksprung vom FIQ */
	mov	r0, sp
	add	r0, r0, #4			@ Stack auf pt_regs's R1 stellen
	ldmia	r0!, {r1-r12}			@ R1 - R12 wieder herstellen
	ldr	sp, [r0], #4
	ldr	lr, [r0], #4
	ldmia   sp!, {r0,pc}^			@ Zum Schluss R0 und PC holen, mit Modewechsel ueber SPSR ...

ENDPROC(avm_fiq_handler)

	/* Immediate Literale fuer den FIQ-Handler */
	.align
5:  .long   avm_fiq_stacks			@ Pointer auf den Stack-Bereich
avm_fiq_handler_end:

/* === AVM FIQ END === */


