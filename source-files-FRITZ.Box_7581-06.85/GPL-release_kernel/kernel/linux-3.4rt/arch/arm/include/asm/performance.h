#ifndef _arch_arm_include_asm_performance_h_
#define _arch_arm_include_asm_performance_h_


#define PMCR            "p15, 0, %0, c9, c12, 0"
#define PMCNTENSET      "p15, 0, %0, c9, c12, 1"
#define PMCNTENCLR      "p15, 0, %0, c9, c12, 2"
#define PMSOVSR         "p15, 0, %0, c9, c12, 3"
#define PMSWINC         "p15, 0, %0, c9, c12, 4"
#define PMSELR          "p15, 0, %0, c9, c12, 5"
#define PMCCNTR         "p15, 0, %0, c9, c13, 0"
#define PMXEVTYPER      "p15, 0, %0, c9, c13, 1"
#define PMXEVCNTR       "p15, 0, %0, c9, c13, 2"
#define PMINTENSET      "p15, 0, %0, c9, c14, 1"
#define PMINTENCLR      "p15, 0, %0, c9, c14, 2"

#define PM_EVENT_SW_INCR                0x00 
#define PM_EVENT_L1I_CACHE_REFILL       0x01 
#define PM_EVENT_L1I_TLB_REFILL         0x02 
#define PM_EVENT_L1D_CACHE_REFILL       0x03 
#define PM_EVENT_L1D_CACHE              0x04 
#define PM_EVENT_L1D_TLB_REFILL         0x05
#define PM_EVENT_LD_RETIRED             0x06 
#define PM_EVENT_ST_RETIRED             0x07  
#define PM_EVENT_EXC_TAKEN              0x09 
#define PM_EVENT_EXC_RETURN             0x0A 
#define PM_EVENT_CID_WRITE_RETIRED      0x0B 
#define PM_EVENT_PC_WRITE_RETIRED       0x0C 
#define PM_EVENT_BR_IMMED_RETIRED       0x0D 
#define PM_EVENT_UNALIGNED_LDST_RETIRED 0x0F 
#define PM_EVENT_BR_MIS_PRED            0x10 
#define PM_EVENT_CPU_CYCLES             0x11 
#define PM_EVENT_BR_PRED                0x12 
/*** Cortex-A9 spezifische Events ***/
#define PM_EVENT_COH_LINEFILL_MISS               0x50
#define PM_EVENT_COH_LINEFILL_HIT                0x51
#define PM_EVENT_L1I_DEP_STALL                   0x60
#define PM_EVENT_L1D_DEP_STALL                   0x61
#define PM_EVENT_MAIN_TLB_STALL                  0x62
#define PM_EVENT_STREX_PASS_PREC                 0x63
#define PM_EVENT_STREX_FAIL_PREC                 0x64
#define PM_EVENT_L1D_EVICTION                    0x65
#define PM_EVENT_ISSUE_STAGE_STALL               0x66
#define PM_EVENT_ISSUE_STAGE_EMPTY               0x67
#define PM_EVENT_INST_RETIRED                    0x68
#define PM_EVENT_EXT_LINEFILL_REQ                0x69
#define PM_EVENT_PREFETCH_REFILL                 0x6A
#define PM_EVENT_PREFETCH_HIT                    0x6B
#define PM_EVENT_RETURN_EXEC_PRED_VAL            0x6E
#define PM_EVENT_MAIN_EXEC_UNIT_INST             0x70
#define PM_EVENT_SECOND_EXEC_UNIT_INST           0x71
#define PM_EVENT_LDST_SPEC                       0x72
#define PM_EVENT_FP_EXEC_INST                    0x73
#define PM_EVENT_NEON_EXEC_INST                  0x74
#define PM_EVENT_PLD_STALL                       0x80
#define PM_EVENT_EXT_MEM_WRITE_STALL             0x81
#define PM_EVENT_MAIN_TLB_INST_STALL             0x82
#define PM_EVENT_MAIN_TLB_DATA_STALL             0x83
#define PM_EVENT_MICRO_TLB_INST_STALL            0x84
#define PM_EVENT_MICRO_TLB_DATA_STALL            0x85
#define PM_EVENT_DMB_STALL_SPEC                  0x86
#define PM_EVENT_INTEGER_CLOCK_EN                0x8A
#define PM_EVENT_DATA_ENGINE_CLOCK_EN            0x8B
#define PM_NEON_SIMD_CLOCK_EN                    0x8C
#define PM_EVENT_TLB_INST_ALLOC                  0x8D
#define PM_EVENT_TLB_DATA_ALLOC                  0x8E
#define PM_EVENT_ISB_PREC                        0x90
#define PM_EVENT_DSB_PREC                        0x91
#define PM_EVENT_DMB_SPEC                        0x92
#define PM_EVENT_EXT_IRQ                         0x93
#define PM_EVENT_PLE_CACHE_REQ_COMPL             0xA0
#define PM_EVENT_PLE_CACHE_REQ_SKIP              0xA1
#define PM_EVENT_PLE_FIFO_FLUSH                  0xA2
#define PM_EVENT_PLE_REQ_COMPL                   0xA3
#define PM_EVENT_PLE_FIFO_OVERFL                 0xA4
#define PM_EVENT_PLE_REQ_PROG                    0xA5
#define PM_EVENT_LAST                            PM_EVENT_PLE_REQ_PROG + 1

union __performance_monitor_control {
    struct _performance_monitor_control {
        unsigned int EnableBit : 1;
        unsigned int CounterReset : 1; /* 2 */
        unsigned int CycleCounterReset : 1; /* 2 */
        unsigned int CycleCounterDivider : 1; /* 3 */
        unsigned int ExportEnable : 1; /* 4 */
        unsigned int DisableCycleCounter : 1; /* 5 */
        unsigned int reserved : 5 ; /* 10:6 */
        unsigned int NumCounters : 5;   /* 15:11 */
        unsigned int IDCode : 8;   /* 23:16 */
        unsigned int ImpCode : 8;   /* 31:24 */
    } Bits;
    volatile unsigned int Register;
};


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static inline void write_p15_performance_monitor_control(unsigned int value) {
    __asm__ __volatile__ ("mcr " PMCR : : "r" (value));
    return;
}

static inline void write_p15_cycle_counter(unsigned int value) {
    __asm__ __volatile__ ("mcr " PMCCNTR : : "r" (value));
    return;
}

static inline void write_p15_performance_counter(unsigned int counter, unsigned int value) {
    __asm__ __volatile__ ("mcr " PMSELR : : "r" (counter));
    __asm__ __volatile__ ("mcr " PMXEVCNTR : : "r" (value));
    return;
}
static inline void write_p15_performance_event_type_with_cnt_reset(unsigned int counter, unsigned int value) {
    __asm__ __volatile__ ("mcr " PMSELR : : "r" (counter));
    __asm__ __volatile__ ("mcr " PMXEVTYPER : : "r" (value));
    value = 0;
    __asm__ __volatile__ ("mcr " PMXEVCNTR : : "r" (value));
    return;
}
static inline void p15_reset_performance_counter(unsigned int counter) {
    int value = 0;
    __asm__ __volatile__ ("mcr " PMSELR : : "r" (counter));
    __asm__ __volatile__ ("mcr " PMXEVCNTR : : "r" (value));
}
static inline void write_p15_performance_event_type(unsigned int counter, unsigned int value) {
    __asm__ __volatile__ ("mcr " PMSELR : : "r" (counter));
    __asm__ __volatile__ ("mcr " PMXEVTYPER : : "r" (value));
    return;
}
static inline unsigned int write_p15_performance_count_enable(unsigned int value) {
    __asm__ __volatile__ ("mcr " PMCNTENSET : : "r" (value));
    return value;
}


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static inline unsigned int read_p15_performance_monitor_control(void) {
    unsigned int value;
    __asm__ __volatile__ ("mrc " PMCR : "=r" (value));
    return value;
}

static inline unsigned int read_p15_cycle_counter(void) {
    unsigned int value;
    __asm__ __volatile__ ("mrc " PMCCNTR : "=r" (value));
    return value;
}

static inline unsigned int read_p15_performance_counter_with_reset(unsigned int counter) {
    unsigned int value;
    unsigned int resetval = 0;
    __asm__ __volatile__ ("mcr " PMSELR : : "r" (counter));
    __asm__ __volatile__ ("mrc " PMXEVCNTR : "=r" (value));
    __asm__ __volatile__ ("mcr " PMXEVCNTR : : "r" (resetval));
    return value;
}
static inline unsigned int read_p15_performance_counter(unsigned int counter) {
    unsigned int value;
    __asm__ __volatile__ ("mcr " PMSELR : : "r" (counter));
    __asm__ __volatile__ ("mrc " PMXEVCNTR : "=r" (value));
    return value;
}
static inline unsigned int read_p15_performance_event_type(unsigned int counter) {
    unsigned int value;
    __asm__ __volatile__ ("mcr " PMSELR : : "r" (counter));
    __asm__ __volatile__ ("mrc " PMXEVTYPER : "=r" (value));
    return value;
}
static inline unsigned int read_p15_performance_count_enable(void) {
    unsigned int value;
    __asm__ __volatile__ ("mrc " PMCNTENSET : "=r" (value));
    return value;
}

#endif /*--- #ifndef _arch_arm_include_asm_performance_h_ ---*/
