#ifndef _mach_avm_h_
#define _mach_avm_h_

#include <linux/avm_reboot_status.h>

/*------------------------------------------------------------------------------------------*\
 * 240-254 char	LOCAL/EXPERIMENTAL USE
 * 240-254 block	LOCAL/EXPERIMENTAL USE
 *         Allocated for local/experimental use.  For devices not
 *         assigned official numbers, these ranges should be
 *         used in order to avoid conflicting with future assignments.
\*------------------------------------------------------------------------------------------*/
#define AVM_DECT_IO_MAJOR	    227
#define AVM_USERMAN_MAJOR	    228
#define KDSLD_USERMAN_MAJOR	    229
#define AVM_TIATM_MAJOR         230
#define TFFS_MAJOR              262
#define AVM_EVENT_MAJOR         261
#define AVM_DEBUG_MAJOR         263
#define WATCHDOG_MAJOR          260
#define KDSLD_MAJOR             243
#define KDSLDPTRACE_MAJOR       244
#define UBIK_MAJOR              245
#define DEBUG_TRACE_MAJOR       246
#define AVM_LED_MAJOR           264
#define AVM_I2C_MAJOR           248
#define YAFFS                   249
#define AVM_AUDIO_MAJOR         250
#define AVM_NEW_LED_MAJOR       251
#define AVM_POWER_MAJOR         252
#define AVM_VINAX_MAJOR		    253
#define AVM_HSK_MAJOR		    254
#define AVM_NET_TRACE_MAJOR     255
#define AVM_ATH_EEPROM              239

/*------------------------------------------------------------------------------------------*\
 * zusaetzlich folgende:
 *
 * 207 char	Compaq ProLiant health feature indicate
 * 220 char	Myricom Myrinet "GM" board
 * 224 char	A2232 serial card
 * 225 char	A2232 serial card (alternate devices)
 * 227 char	IBM 3270 terminal Unix tty access
 * 228 char	IBM 3270 terminal block-mode access
 * 229 char	IBM iSeries virtual console
 * 230 char	IBM iSeries virtual tape
 *
\*------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define MAX_ENV_ENTRY               256
#define FLASH_ENV_ENTRY_SIZE        64


/*------------------------------------------------------------------------------------------*\
 * OHIO OHIO OHIO OHIO OHIO OHIO OHIO OHIO OHIO OHIO OHIO OHIO OHIO OHIO OHIO OHIO OHIO 
\*------------------------------------------------------------------------------------------*/
#ifdef CONFIG_MIPS_OHIO

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ohio_pre_init(void);
#define avm_pre_init ohio_pre_init

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
enum _avm_clock_id {
    avm_clock_id_non        = 0x00,
    avm_clock_id_cpu        = 0x01,
    avm_clock_id_system     = 0x02,
    avm_clock_id_usb        = 0x04,
    avm_clock_id_dsp        = 0x08,
    avm_clock_id_vbus       = 0x10,
    avm_clock_id_peripheral = 0x20
};

#include <asm/mips-boards/ohio_clk.h>

#define avm_clk_get_pll_factor      ohio_clk_get_pll_factor
#define avm_get_clock               ohio_get_clock
#define avm_set_clock               ohio_set_clock
#define avm_get_clock_notify        ohio_get_clock_notify
#define avm_set_system_clock_notify ohio_set_system_clock_notify

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
char *avm_urlader_env_get_variable(int idx);
char *avm_urlader_env_get_value_by_id(unsigned int id);
char *avm_urlader_env_get_value(char *var);
int avm_urlader_env_set_variable(char *var, char *val);
int avm_urlader_env_unset_variable(char *var);
int avm_urlader_env_defrag(void);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mach-ohio/hw_gpio.h>
#include <asm/mips-boards/ohio_gpio.h>

#define avm_gpio_init        ohio_gpio_init
#define avm_gpio_ctrl        ohio_gpio_ctrl
#define avm_gpio_out_bit     ohio_gpio_out_bit
#define avm_gpio_in_bit      ohio_gpio_in_bit
#define avm_gpio_in_value    ohio_gpio_in_value
#define avm_gpio_set_bitmask ohio_gpio_set_bitmask


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mips-boards/ohio_power.h>

#define avm_power_down_init               ohio_power_down_init
#define avm_put_device_into_power_down    ohio_put_device_into_power_down
#define avm_take_device_out_of_power_down ohio_take_device_out_of_power_down
#define avm_register_power_down_gpio      ohio_register_power_down_gpio
#define avm_release_power_down_gpio       ohio_release_power_down_gpio
#define avm_power_down_status             ohio_power_down_status

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mach-ohio/hw_reset.h>
#include <asm/mips-boards/ohio_reset.h>

#define avm_reset_init                      ohio_reset_init
#define avm_take_device_out_of_reset        ohio_take_device_out_of_reset
#define avm_put_device_into_reset           ohio_put_device_into_reset
#define avm_reset_device                    ohio_reset_device
#define avm_reset_status                    ohio_reset_status
#define avm_register_reset_gpio		        ohio_register_reset_gpio
#define avm_release_reset_gpio		        ohio_release_reset_gpio

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mips-boards/ohio_vlynq.h>

#define avm_vlynq_init                      ohio_vlynq_init
#define avm_vlynq_init_link                 ohio_vlynq_init_link
#define avm_vlynq_alloc_context             ohio_vlynq_alloc_context
#define avm_vlynq_free_context              ohio_vlynq_free_context

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mips-boards/ohioint.h>

#define avm_int_init                        ohioint_init
#define avm_int_set_type                    ohioint_set_type
#define avm_int_ctrl_irq_pacing_setup       ohioint_ctrl_irq_pacing_setup
#define avm_int_ctrl_irq_pacing_register    ohioint_ctrl_irq_pacing_register
#define avm_int_ctrl_irq_pacing_unregister  ohioint_ctrl_irq_pacing_unregister
#define avm_int_ctrl_irq_pacing_set         ohioint_ctrl_irq_pacing_set
#define avm_nsec_timer                      ohio_nsec_timer

#endif /*--- #ifdef CONFIG_MIPS_OHIO ---*/

/*------------------------------------------------------------------------------------------*\
 * AR7 AR7 AR7 AR7 AR7 AR7 AR7 AR7 AR7 AR7 AR7 AR7 AR7 AR7 AR7 AR7 AR7 
\*------------------------------------------------------------------------------------------*/
#ifdef CONFIG_MIPS_AR7

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ar7_pre_init(void);
#define avm_pre_init ar7_pre_init

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
enum _avm_clock_id {
    avm_clock_id_non        = 0x00,
    avm_clock_id_cpu        = 0x01,
    avm_clock_id_system     = 0x02,
    avm_clock_id_usb        = 0x04,
    avm_clock_id_dsp        = 0x08,
    avm_clock_id_vbus       = 0x10,
    avm_clock_id_peripheral = 0x20
};

#include <asm/mips-boards/ar7_clk.h>

#define avm_clk_get_pll_factor      ar7_clk_get_pll_factor
#define avm_get_clock               ar7_get_clock
#define avm_set_clock               ar7_set_clock
#define avm_get_clock_notify        ar7_get_clock_notify
#define avm_set_system_clock_notify ar7_set_system_clock_notify

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define MAX_ENV_ENTRY               256
#define FLASH_ENV_ENTRY_SIZE        64

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
char *avm_urlader_env_get_variable(int idx);
char *avm_urlader_env_get_value_by_id(unsigned int id);
char *avm_urlader_env_get_value(char *var);
int avm_urlader_env_set_variable(char *var, char *val);
int avm_urlader_env_unset_variable(char *var);
int avm_urlader_env_defrag(void);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mach-ar7/hw_gpio.h>
#include <asm/mips-boards/ar7_gpio.h>

#define avm_gpio_init        ar7_gpio_init
#define avm_gpio_ctrl        ar7_gpio_ctrl
#define avm_gpio_out_bit     ar7_gpio_out_bit
#define avm_gpio_in_bit      ar7_gpio_in_bit
#define avm_gpio_in_value    ar7_gpio_in_value
#define avm_gpio_set_bitmask ar7_gpio_set_bitmask


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mips-boards/ar7_power.h>

#define avm_power_down_init               ar7_power_down_init
#define avm_put_device_into_power_down    ar7_put_device_into_power_down
#define avm_take_device_out_of_power_down ar7_take_device_out_of_power_down
#define avm_register_power_down_gpio      ar7_register_power_down_gpio
#define avm_release_power_down_gpio       ar7_release_power_down_gpio
#define avm_power_down_status             ar7_power_down_status

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mach-ar7/hw_reset.h>
#include <asm/mips-boards/ar7_reset.h>

#define avm_reset_init                      ar7_reset_init
#define avm_take_device_out_of_reset        ar7_take_device_out_of_reset
#define avm_put_device_into_reset           ar7_put_device_into_reset
#define avm_reset_device                    ar7_reset_device
#define avm_reset_status                    ar7_reset_status
#define avm_register_reset_gpio		        ar7_register_reset_gpio
#define avm_release_reset_gpio		        ar7_release_reset_gpio

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mips-boards/ar7_vlynq.h>

#define avm_vlynq_init                      ar7_vlynq_init
#define avm_vlynq_init_link                 ar7_vlynq_init_link
#define avm_vlynq_alloc_context             ar7_vlynq_alloc_context
#define avm_vlynq_free_context              ar7_vlynq_free_context

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mips-boards/ar7int.h>

#define avm_int_init                        ar7int_init
#define avm_int_set_type                    ar7int_set_type
#define avm_int_ctrl_irq_pacing_setup       ar7int_ctrl_irq_pacing_setup
#define avm_int_ctrl_irq_pacing_register    ar7int_ctrl_irq_pacing_register
#define avm_int_ctrl_irq_pacing_unregister  ar7int_ctrl_irq_pacing_unregister
#define avm_int_ctrl_irq_pacing_set         ar7int_ctrl_irq_pacing_set
#define avm_nsec_timer                      ar7_nsec_timer

#endif /*--- #ifdef CONFIG_MIPS_AR7 ---*/

/*------------------------------------------------------------------------------------------*\
 * UR8 UR8 UR8 UR8 UR8 UR8 UR8 UR8 UR8 UR8 UR8 UR8 UR8 UR8 UR8 UR8 UR8 UR8 UR8 UR8 UR8 UR8 
\*------------------------------------------------------------------------------------------*/
#ifdef CONFIG_MIPS_UR8

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ur8_pre_init(void);
#define avm_pre_init ur8_pre_init

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
enum _avm_clock_id {
    avm_clock_id_non        = 0x00,
    avm_clock_id_cpu        = 0x01,
    avm_clock_id_system     = 0x02,
    avm_clock_id_usb        = 0x04,
    avm_clock_id_dsp        = 0x08,
    avm_clock_id_vbus       = 0x10,
    avm_clock_id_peripheral = 0x20,
    avm_clock_id_c55x       = 0x40,
    avm_clock_id_ephy       = 0x80,
    avm_clock_id_pci        = 0x100,
    avm_clock_id_tdm        = 0x200
};

#include <asm/mips-boards/ur8_clk.h>

#define avm_clk_get_pll_factor      ur8_clk_get_pll_factor
#define avm_get_clock               ur8_get_clock
#define avm_set_clock               ur8_set_clock
#define avm_get_clock_notify        ur8_get_clock_notify
#define avm_set_system_clock_notify ur8_set_system_clock_notify

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define MAX_ENV_ENTRY               256
#define FLASH_ENV_ENTRY_SIZE        64

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
char *avm_urlader_env_get_variable(int idx);
char *avm_urlader_env_get_value_by_id(unsigned int id);
char *avm_urlader_env_get_value(char *var);
int avm_urlader_env_set_variable(char *var, char *val);
int avm_urlader_env_unset_variable(char *var);
int avm_urlader_env_defrag(void);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mach-ur8/hw_gpio.h>
#include <asm/mips-boards/ur8_gpio.h>

#define avm_gpio_init        ur8_gpio_init
#define avm_gpio_ctrl        ur8_gpio_ctrl
#define avm_gpio_out_bit     ur8_gpio_out_bit
#define avm_gpio_in_bit      ur8_gpio_in_bit
#define avm_gpio_in_value    ur8_gpio_in_value
#define avm_gpio_set_bitmask ur8_gpio_set_bitmask


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mips-boards/ur8_power.h>

#define avm_power_down_init               ur8_power_down_init
#define avm_put_device_into_power_down    ur8_put_device_into_power_down
#define avm_take_device_out_of_power_down ur8_take_device_out_of_power_down
#define avm_register_power_down_gpio      ur8_register_power_down_gpio
#define avm_release_power_down_gpio       ur8_release_power_down_gpio
#define avm_power_down_status             ur8_power_down_status

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mach-ur8/hw_reset.h>
#include <asm/mips-boards/ur8_reset.h>

#define avm_reset_init                      ur8_reset_init
#define avm_take_device_out_of_reset        ur8_take_device_out_of_reset
#define avm_put_device_into_reset           ur8_put_device_into_reset
#define avm_reset_device                    ur8_reset_device
#define avm_reset_status                    ur8_reset_status
#define avm_register_reset_gpio		        ur8_register_reset_gpio
#define avm_release_reset_gpio		        ur8_release_reset_gpio

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mips-boards/ur8_vlynq.h>

#define avm_vlynq_init                      ur8_vlynq_init
#define avm_vlynq_init_link                 ur8_vlynq_init_link
#define avm_vlynq_alloc_context             ur8_vlynq_alloc_context
#define avm_vlynq_free_context              ur8_vlynq_free_context

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mips-boards/ur8int.h>

#define avm_int_init                        ur8int_init
#define avm_int_set_type                    ur8int_set_type
#define avm_int_ctrl_irq_pacing_setup       ur8int_ctrl_irq_pacing_setup
#define avm_int_ctrl_irq_pacing_register    ur8int_ctrl_irq_pacing_register
#define avm_int_ctrl_irq_pacing_unregister  ur8int_ctrl_irq_pacing_unregister
#define avm_int_ctrl_irq_pacing_set         ur8int_ctrl_irq_pacing_set
#define avm_nsec_timer                      ur8_nsec_timer

#endif /*--- #ifdef CONFIG_MIPS_OHIO ---*/

/*------------------------------------------------------------------------------------------*\
 * DAVINCI DAVINCI DAVINCI DAVINCI DAVINCI DAVINCI DAVINCI DAVINCI DAVINCI DAVINCI DAVINCI
\*------------------------------------------------------------------------------------------*/
#ifdef CONFIG_ARCH_DAVINCI

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void davinci_pre_init(void);
#define avm_pre_init davinci_pre_init

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
enum _avm_clock_id {
    avm_clock_id_non        = 0x00,
    avm_clock_id_cpu        = 0x01,
    avm_clock_id_system     = 0x02,
    avm_clock_id_usb        = 0x04,
    avm_clock_id_dsp        = 0x08,
    avm_clock_id_vbus       = 0x10,
    avm_clock_id_peripheral = 0x20,
    avm_clock_id_c55x       = 0x40,
    avm_clock_id_ephy       = 0x80,
    avm_clock_id_pci        = 0x100,
    avm_clock_id_tdm        = 0x200
};

/*--- #include <asm/mach-davinci/davinci_clk.h> ---*/

#define avm_clk_get_pll_factor      davinci_clk_get_pll_factor
#define avm_get_clock               davinci_get_clock
#define avm_set_clock               davinci_set_clock
#define avm_get_clock_notify        davinci_get_clock_notify
#define avm_set_system_clock_notify davinci_set_system_clock_notify

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define MAX_ENV_ENTRY               256
#define FLASH_ENV_ENTRY_SIZE        64

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
char *avm_urlader_env_get_variable(int idx);
char *avm_urlader_env_get_value_by_id(unsigned int id);
char *avm_urlader_env_get_value(char *var);
int avm_urlader_env_set_variable(char *var, char *val);
int avm_urlader_env_unset_variable(char *var);
int avm_urlader_env_defrag(void);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/io.h>
#include <asm/arch/irqs.h>
#include <asm/arch/hardware.h>
#include <asm/arch/gpio.h>
/*--- #include <asm/mach-davinci/davinci_gpio.h> ---*/

#define avm_gpio_init        davinci_gpio_init
#define avm_gpio_ctrl        davinci_gpio_ctrl
#define avm_gpio_out_bit     davinci_gpio_out_bit
#define avm_gpio_in_bit      davinci_gpio_in_bit
#define avm_gpio_in_value    davinci_gpio_in_value
#define avm_gpio_set_bitmask davinci_gpio_set_bitmask


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*--- #include <asm/mach-davinci/davinci_power.h> ---*/

#define avm_power_down_init               davinci_power_down_init
#define avm_put_device_into_power_down    davinci_put_device_into_power_down
#define avm_take_device_out_of_power_down davinci_take_device_out_of_power_down
#define avm_register_power_down_gpio      davinci_register_power_down_gpio
#define avm_release_power_down_gpio       davinci_release_power_down_gpio
#define avm_power_down_status             davinci_power_down_status

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*--- #include <asm/mach-davinci/hw_reset.h> ---*/
/*--- #include <asm/mach/davinci_reset.h> ---*/

#define avm_reset_init                      davinci_reset_init
#define avm_take_device_out_of_reset        davinci_take_device_out_of_reset
#define avm_put_device_into_reset           davinci_put_device_into_reset
#define avm_reset_device                    davinci_reset_device
#define avm_reset_status                    davinci_reset_status
#define avm_register_reset_gpio		        davinci_register_reset_gpio
#define avm_release_reset_gpio		        davinci_release_reset_gpio

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*--- #include <asm/mach/davinci_vlynq.h> ---*/

#define avm_vlynq_init                      davinci_vlynq_init
#define avm_vlynq_init_link                 davinci_vlynq_init_link
#define avm_vlynq_alloc_context             davinci_vlynq_alloc_context
#define avm_vlynq_free_context              davinci_vlynq_free_context

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/

#define avm_int_init                        davinciint_init
#define avm_int_set_type                    davinciint_set_type
#define avm_int_ctrl_irq_pacing_setup       davinciint_ctrl_irq_pacing_setup
#define avm_int_ctrl_irq_pacing_register    davinciint_ctrl_irq_pacing_register
#define avm_int_ctrl_irq_pacing_unregister  davinciint_ctrl_irq_pacing_unregister
#define avm_int_ctrl_irq_pacing_set         davinciint_ctrl_irq_pacing_set
#define avm_nsec_timer                      davinci_nsec_timer

#endif /*--- #ifdef CONFIG_ARCH_DAVINCI ---*/

#if defined(CONFIG_ARCH_PUMA5) || defined(CONFIG_MACH_PUMA6)
/*--------------------------------------------------------------------------------*\
 * PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  
\*--------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
enum _avm_clock_id {
    avm_clock_id_non        = 0x00,
    avm_clock_id_cpu        = 0x01,
    avm_clock_id_system     = 0x02, /*--- DDR and fast-peripherals ---*/
    avm_clock_id_usb        = 0x04,
    avm_clock_id_docsis     = 0x08,
    avm_clock_id_gmii       = 0x10,
    avm_clock_id_vlynq      = 0x20, /*--- vlynq and slow peripherals ---*/
    avm_clock_id_vbus       = 0x40, /*--- vlynq and slow peripherals ---*/
    avm_clock_id_sflash     = 0x80,
    avm_clock_id_tdm        = 0x100
};

extern unsigned int puma_get_clock(enum _avm_clock_id clock_id);
extern unsigned int puma_set_clock(enum _avm_clock_id clock_id, unsigned int clk);

#define avm_get_clock               puma_get_clock
#define avm_set_clock               puma_set_clock
#if defined(CONFIG_ARCH_PUMA5)
#include <arch-avalanche/puma5/puma5.h>
#elif defined(CONFIG_MACH_PUMA6)
#include <arch-avalanche/puma6/puma6.h>
#endif/*--- #elif defined(CONFIG_MACH_PUMA6) ---*/

#include <mach/hw_gpio.h>
/*--- #include <asm/gpio.h> ---*/

#define avm_gpio_init               puma_gpio_init
#define avm_gpio_ctrl               puma_gpio_ctrl
#define avm_gpio_out_bit            puma_gpio_out_bit
#define avm_gpio_out_bit_no_sched   puma_gpio_out_bit_no_sched
#define avm_gpio_in_bit             puma_gpio_in_bit

#endif/*--- #ifdef CONFIG_ARCH_PUMA5 ---*/


#ifdef CONFIG_ARM

#ifdef CONFIG_MACH_BCM963138

#include <linux/init.h>
#include <plat/bcm63xx_timer.h>
#include <asm/io.h>

/* Der Timer hat nur 30Bit Aufloesung. Die obersten beiden Bits sind immer 0.
 * Da der Profiler im Sammeltreiber nicht damit klar kommt werden die Werte des
 * Timers um 2 Bit nach oben geshiftet. */
#define avm_get_cycles()             (bcm63xx_read_timer_count2() << 2)
#define avm_get_cyclefreq()          (50000000 << 2)
#define avm_cycles_cpuclock_depend() (0)

#include <mach/avm_gpio.h>

#define avm_gpio_init               brcm_gpio_init
#define avm_gpio_ctrl               brcm_gpio_ctrl
#define avm_gpio_request_irq        brcm_button_request_callback
#define avm_gpio_disable_irq        brcm_button_disable_callback
#define avm_gpio_out_bit            brcm_gpio_out_bit
#define avm_gpio_out_bit_no_sched   brcm_gpio_out_bit_no_sched
#define avm_gpio_in_bit             brcm_gpio_in_bit
#define avm_gpio_set_bitmask        brcm_gpio_set_bitmask

void dma_cache_inv(unsigned long start_virt_addr, size_t len);
void dma_cache_wback_inv(unsigned long page_address, ssize_t size);

/*--------------------------------------------------------------------------------*\
 * kernelmem_module.c
\*--------------------------------------------------------------------------------*/
enum _module_alloc_type_ {
    module_alloc_type_init,
    module_alloc_type_core,
    module_alloc_type_page,
    module_alloc_type_unknown
};
struct resource;
void __init module_alloc_bootmem_init(struct resource *res, unsigned long start_addr);
unsigned long module_alloc_size_list_alloc(unsigned long size, char *name, enum _module_alloc_type_ type);
int module_alloc_size_list_free(unsigned long addr);
char *module_alloc_find_module_name(char *buff, char *end, unsigned long addr);

void *module_alloc(unsigned long size, char *name, enum _module_alloc_type_ type __attribute__ ((unused)));
#endif /*--- #ifdef CONFIG_MACH_BCM963138 ---*/

char *avm_urlader_env_get_variable(int idx);
char *avm_urlader_env_get_value_by_id(unsigned int id);
char *avm_urlader_env_get_value(char *var);
int avm_urlader_env_set_variable(char *var, char *val);
int avm_urlader_env_unset_variable(char *var);
int avm_urlader_env_defrag(void);

#endif /*--- #ifdef CONFIG_ARM ---*/


#endif /*--- #ifndef _mach_avm_h_ ---*/
