/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2016 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/

#ifndef __avm_enh_h__
#define __avm_enh_h__

#define __get_last_unaligned_info
/**--------------------------------------------------------------------------------**\
 * \\brief: 
 * Liefere Unaligned-Daten 
 * user: 0 Kernel 1 Userland
 * ret: Zeiger auf last_comm des Userprozesses
 *
 * -> wird fuer system-load in avm_power benoetigt
\**--------------------------------------------------------------------------------**/
const char *get_last_unaligned_info(unsigned long *ai_count, unsigned long *last_pc, int user);

void print_code_range(struct seq_file *seq, const char *prefix, unsigned long addr, unsigned int thumb);

/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
void ai_add_to_scorelist(void *pc, int user);
/**--------------------------------------------------------------------------------**\
 * ret: summe der unaligneds
\**--------------------------------------------------------------------------------**/
unsigned long ai_show_scorelist(struct seq_file *seq, int user);
#endif/*--- #ifndef __avm_enh_h__ ---*/
