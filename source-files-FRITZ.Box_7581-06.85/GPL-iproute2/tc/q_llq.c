/*
 * q_llq.c		LLQ.
 *
 *		This program is free software; you can redistribute it and/or
 *		modify it under the terms of the GNU General Public License
 *		as published by the Free Software Foundation; either version
 *		2 of the License, or (at your option) any later version.
 *
 * Authors:	Carsten Paeth, <c.paeth@avm.de>
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

#include "utils.h"
#include "tc_util.h"

static void explain_class(void)
{
	fprintf(stderr, "Usage: ... llq prio NUMBER [ weight PERCENT ]\n");
}

static void explain(void)
{
	fprintf(stderr, "Usage: ... llq [ minq BYTES ] [ maxq BYTES ] [ default N ]\n");
}

static void explain1(char *arg)
{
	fprintf(stderr, "Illegal \"%s\"\n", arg);
}

#define usage() return(-1)

static int llq_parse_opt(struct qdisc_util *qu, int argc, char **argv, struct nlmsghdr *n)
{
	struct tc_llq_qopt qopt;
	unsigned minq = 0;
	unsigned maxq = 0;
	__u32 defaultclass;

	memset(&qopt, 0, sizeof(qopt));

	while (argc > 0) {
		if (matches(*argv, "minq") == 0) {
			NEXT_ARG();
			if (get_size(&minq, *argv)) {
				explain1("minq");
				return -1;
			}
		} else if (matches(*argv, "maxq") == 0) {
			NEXT_ARG();
			if (get_size(&maxq, *argv)) {
				explain1("maxq");
				return -1;
			}
		} else if (matches(*argv, "default") == 0) {
		    NEXT_ARG();
		    if (get_u32(&defaultclass, *argv, 16)) {
			explain1("default"); return -1;
		    }
		} else if (matches(*argv, "help") == 0) {
			explain();
			return -1;
		} else {
			fprintf(stderr, "What is \"%s\"?\n", *argv);
			explain();
			return -1;
		}
		argc--; argv++;
	}

	qopt.minq = minq ? minq : 10;
	qopt.maxq = maxq;
	qopt.defaultclass = defaultclass;

	addattr_l(n, 1024, TCA_OPTIONS, &qopt, sizeof(qopt));

	return 0;
}

static int llq_parse_class_opt(struct qdisc_util *qu, int argc, char **argv, struct nlmsghdr *n)
{
	struct tc_llq_copt copt;
	struct rtattr *tail;
    __u32 prio = 0;
    __u32 weight = 0;

	while (argc > 0) {
		if (strcmp(*argv, "prio") == 0) {
			NEXT_ARG();
			if (get_u32(&prio, *argv, 0)) {
				explain1("prio");
				return -1;
			}
			if (prio > 255) {
				fprintf(stderr, "\"prio\" must be number in the range 1...%d\n", 255);
				return -1;
			}
		} else if (strcmp(*argv, "weight") == 0) {
			NEXT_ARG();
			if (get_u32(&weight, *argv, 0)) {
				explain1("weight");
				return -1;
			}
			if (weight > 100) {
				fprintf(stderr, "\"weight\" must be number in the range 1...%d\n", 100);
				return -1;
			}
		} else if (strcmp(*argv, "help") == 0) {
			explain_class();
			return -1;
		} else {
			fprintf(stderr, "What is \"%s\"?\n", *argv);
			explain_class();
			return -1;
		}
		argc--; argv++;
	}

	copt.priority = prio;
	copt.weight = weight ? weight : 1;

	tail = NLMSG_TAIL(n);
	addattr_l(n,1024,TCA_OPTIONS,NULL,0);
	addattr_l(n,1024,TCA_LLQ_OPTIONS,&copt,sizeof(copt));
	tail->rta_len = (void *) NLMSG_TAIL(n) - (void *) tail;

	return 0;
}


static int llq_print_opt(struct qdisc_util *qu, FILE *f, struct rtattr *opt)
{
	struct tc_llq_qopt *qopt;

	if (opt == NULL)
		return 0;

	if (RTA_PAYLOAD(opt) < sizeof(struct tc_llq_qopt))
		return -1;
	qopt = RTA_DATA(opt);

    fprintf(f, "minq %u maxq %u default %u",
				(unsigned)qopt->minq,
				(unsigned)qopt->maxq,
				(unsigned)TC_H_MIN(qopt->defaultclass));
	return 0;
}

static int llq_print_class_opt(struct qdisc_util *qu, FILE *f, struct rtattr *opt)
{
	struct tc_llq_cinfo *copt;

	if (opt == NULL)
		return 0;

	if (RTA_PAYLOAD(opt)  < sizeof(*copt))
		return -1;
	copt = RTA_DATA(opt);

    fprintf(f, "prio %u weight %u (quantum %ld, deficit %ld)",
				(unsigned)copt->priority,
				(unsigned)copt->weight,
				copt->quantum, copt->deficit);
	return 0;
}

static int llq_print_xstats(struct qdisc_util *qu, FILE *f, struct rtattr *xstats)
{
	struct gnet_stats_basic *st;

	if (xstats == NULL)
		return 0;

	if (RTA_PAYLOAD(xstats) < sizeof(*st))
		return -1;

	st = RTA_DATA(xstats);
	fprintf(f, "  %llu bytes %u pkt", 
			   (unsigned long long)st->bytes, st->packets);
	return 0;
}

struct qdisc_util llq_qdisc_util = {
	.id		= "llq",
	.parse_qopt	= llq_parse_opt,
	.print_qopt	= llq_print_opt,
	.print_xstats = llq_print_xstats,
	.parse_copt	= llq_parse_class_opt,
	.print_copt	= llq_print_class_opt,
};

