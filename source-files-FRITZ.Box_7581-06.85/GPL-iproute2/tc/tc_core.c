/*
 * tc_core.c		TC core library.
 *
 *		This program is free software; you can redistribute it and/or
 *		modify it under the terms of the GNU General Public License
 *		as published by the Free Software Foundation; either version
 *		2 of the License, or (at your option) any later version.
 *
 * Authors:	Alexey Kuznetsov, <kuznet@ms2.inr.ac.ru>
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <fcntl.h>
#include <math.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

#include "tc_core.h"

static __u32 t2us=1;
static __u32 us2t=1;
static double tick_in_usec = 1;
static unsigned tab_size;

long tc_core_usec2tick(long usec)
{
	return usec*tick_in_usec;
}

long tc_core_tick2usec(long tick)
{
	return tick/tick_in_usec;
}

unsigned tc_calc_xmittime(unsigned rate, unsigned size)
{
	return tc_core_usec2tick(1000000*((double)size/rate));
}

/*
   rtab[pkt_len>>cell_log] = pkt_xmit_time
 */

int tc_calc_rtable(unsigned bps, __u32 *rtab, int cell_log, unsigned cell_overhead, unsigned packet_overhead, unsigned mtu,
		   unsigned mpu)
{
	int i;
	unsigned overhead = (mpu >> 8) & 0xFF;
	unsigned cell_size = 0x1;
	unsigned cell_log_as_potence = 0;
	mpu = mpu & 0xFF;
	

	printf("\ntc_calc_rtable initial: bps=%u, cell_log=%d, cell_overhead=%d, packet_overhead=%d, mtu=%d, mpu=%d, overhead=%d", 
	       bps, cell_log, cell_overhead, packet_overhead, mtu, mpu, overhead);

	if (overhead && packet_overhead) {
		printf("\nignore overhead in mpu (mpu&0xff00=%d), take packet_overhead (%d)!!!\n", overhead, packet_overhead);
		overhead = packet_overhead;
	}
	if (!overhead) 
		overhead = packet_overhead;
		

	if (cell_log < 0) {
		cell_log_as_potence = 0;
		while ((mtu>>cell_log_as_potence) > 255)
			cell_log_as_potence++;
	} else {
		/* test for 2^n for cell_log (1,2,4,8,..) */
		int i;		
		for (i=0; i<32; i++) {
			if ((1<<i) == cell_log) {
				cell_log_as_potence = i;
				break;
			}
		}
		if (i >= 32) { /* cell_log is not 2^n -> search for the best 2^n */
			int last_celllog = 0;
			for (i=0; i<32; i++) {
				if((cell_log%(1<<i)) == 0) {
					last_celllog = i;
					cell_log_as_potence = i;
				} else {
					cell_log_as_potence = last_celllog;
					break;
				}
			}
		}
		
		
	}
	cell_size = cell_size << cell_log_as_potence;
	if (cell_log < 0)
		cell_log = cell_size;
        tab_size = (mtu/cell_size) + 1;//(int)(mtu / 48) + 1; // hier ev. die +1 bei Vielfachen von 48 weglassen
        if (cell_overhead && cell_log) {
        	double bps_ch = ((double)bps)*((double)(cell_log)/(double)(cell_log+cell_overhead));
	        //bps = (int)(((double)bps)*48/53); // ca 10%
		printf("\ntc_calc_rtable change bps(%d->%d)", (int)bps, (int)bps_ch);
	        bps = (int)bps_ch; 
        }

	printf("\ntc_calc_rtable calcul.: bps=%u, cell_log=%d, cell_overhead=%d, packet_overhead=%d, mtu=%d, mpu=%d", 
	       bps, cell_log, cell_overhead, packet_overhead, mtu, mpu);
	for (i=0; i<tab_size; i++) {
		unsigned sz = (i<<cell_log_as_potence);
		if (overhead)
			sz += overhead;
		if (sz < mpu)
			sz = mpu;
		//sz = ((sz / 48) * 48) + 48;
		sz = ((sz / cell_log) * cell_log) + cell_log;
		rtab[i] = tc_core_usec2tick(1000000*((double)sz/bps));
	}
	printf("\ntc_calc_rtable: cell_log_as_potence=%d\n", cell_log_as_potence);
	return cell_log_as_potence;
}

int tc_core_init()
{
	FILE *fp = fopen("/proc/net/psched", "r");

	if (fp == NULL)
		return -1;

	if (fscanf(fp, "%08x%08x", &t2us, &us2t) != 2) {
		fclose(fp);
		return -1;
	}
	fclose(fp);
	tick_in_usec = (double)t2us/us2t;
	return 0;
}
