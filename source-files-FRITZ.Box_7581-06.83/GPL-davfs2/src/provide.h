/*  provide.h: provide status infos.
    Copyright (C) 2009 AVM

    This file is part of davfs2.

    davfs2 is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    davfs2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with davfs2; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */

#ifndef DAV_PROVIDE_H
#define DAV_PROVIDE_H

/* Provided info types. */ 
#define DIRTY_FILES         1
#define RUNNING_UPLOADS     2
#define RUNNING_DOWNLOADS   3
#define FINISHED_UPLOADS    4
#define FAILED_UPLOADS      5
#define FINISHED_DOWNLOADS  6
#define FAILED_DOWNLOADS    7
#define AVG_UPLOAD_SPEED    8
#define AVG_DOWNLOAD_SPEED  9
#define SIZE_UPLOADS        10
#define SIZE_DOWNLOADS      11
#define UPLOAD_FILES        12
#define DOWNLOAD_FILES      13
#define CONNECTION_STATE    14
#define CACHE_SPEED         15
#define CACHE_STORAGE_AVAIL 16
#define CACHE_STORAGE_USED  17
#define SUM_FAILED_UPLOADS  18

/* Quota infos, retrieved from connected webdav servers.
 * Some values are only provided by servers of UI. */
typedef struct {
    __uint64_t storage_available;            /* Available bytes. */
    __uint64_t storage_used;                 /* Used bytes. */
	__uint32_t storage_filecount;            /* Number files in online storage */

    __uint64_t upload_avail;                 /* traffic quotas, special infos provided by UI */
    __uint64_t upload_used;
    __uint64_t download_avail;
    __uint64_t download_used;
    __uint64_t traffic_avail;
    __uint64_t traffic_used;

	__uint64_t max_filesize;
	__uint32_t max_filecount;
	__uint32_t max_filesperfolder;
	__uint32_t max_filenamelength;

    time_t update_time;                     /* timestamp of the last PROPFIND/USERINFO call */
    int update_ret;                         /* return value of the last PROPFIND/USERINFO call */
} quota_context;


enum {
    TRANSFER_UPLOAD = 1,
    TRANSFER_DOWNLOAD
};

/* Create shared memory to provide status infos to other processes.
 * The infos includes the count of dirty file, average download speed ..
 * debug      : Debug-Flag.
 * return value : 0 on success; -1 if an error occurred. */
int
provide_init(int debug);

/* Destroy shared memory if the client will be stopped.*/
void
provide_destroy(void);

/* Set new values of the storage and traffic quota, provided by the connected webdav server.
 * value  : quota infos, determined with PROPFIND or USERINFO. */
void
provide_set_quota(quota_context value);

/* Get values of the storage and traffic quota, stored in shared mem.
 * value  : pointer to allocated quota_context struct */
void
provide_get_quota(quota_context *quotas);

/* Set new uint values.
 * type     : The type of the value, e.g. "count of dirty files".
 * value    : The new value for the shared memory.
 * op       : Integer-Operation for the given and stored value, e.g. "+". */
void
provide_set_uint(int type, unsigned int value, const char op);

/* Set new uint64 values.
 * type     : The type of the value, e.g. "count of dirty files".
 * value    : The new value for the shared memory.
 * op       : Integer-Operation for the given and stored value, e.g. "+". */
void
provide_set_uint64(int type, __uint64_t value, const char op);

/* Set new string values for uploading/downloading files.
 * type     : The type of the values, e.g. "name of currently uploading files ".
 * file1    : Filename for the shared memory.
 * file2    : Filename for the shared memory. */
void
provide_set_files(int type, const char* file1, const char* file2);

void
provide_set_string(int type, const char* value);

void
provide_set_transfer_speed(int type, __uint64_t bytes, __uint64_t duration);

/* Print all current infos of the shared memory. */
void
provide_print(void);

/* Update average upload and download speed of the last five minutes. */
void
provide_update_avg_speed(void);

/* Clear filecounter, including finished and failed downloads, filenames and
 * up/download speeds, if a new transfer session is started with FTP or Samba. */
void
provide_clear_transfervalues(void);

void
provide_update_alivetime(void);

/**
 * Read values from USB configuration.
 *
 * @param debug_val enable/disable debug output
 * @param [in,out] args configuration values
 *
 * @retval 0 on success
 * @retval -1 on failure
 */
int provide_get_usb_cfg_infos(int debug_val, dav_args *args);

#endif /* DAV_PROVIDE_H */
