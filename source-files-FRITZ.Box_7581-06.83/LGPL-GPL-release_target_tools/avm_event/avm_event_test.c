#include <stdio.h>
#include <stdlib.h>
#include <linux/avm_event.h>
#include "avm_event.h"
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>



/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
    int handle = 0;
    int fd = 0;
    int max_fd;
    int i;
    int alive = 0;
    int zuende = 0;
    struct termios t;

    fd_set readfds;
    fd_set writefds;
    fd_set exceptfds;
    struct timeval timeout;

    for(i = 1 ; i < argc ; i++) {
        if(argv[i][0] == '-') {
            switch(argv[i][1]) {
                case '-':
                    i = argc;
                    break;
                case 'h':
                    printf("%s: -h (help) -a (alive)\n", argv[0]);
                    return 0;
                case 'a':
                    alive = 1;
                    break;
            }
        }
    }


    max_fd = 0; /*--- stdin ---*/

    FD_ZERO(&readfds);
    FD_ZERO(&writefds);
    FD_ZERO(&exceptfds);

    timeout.tv_sec  = 1000;
    timeout.tv_usec = 1000;

    tcgetattr(0, &t);
    t.c_lflag &= ~ICANON;
    tcsetattr(0, TCSANOW, &t);
    setbuf(stdin, NULL);

    FD_SET(0, &readfds);

    printf("[%s]:", argv[0]);

    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
    for( ; zuende == 0; ) {
        int ret;
        FD_SET(0, &readfds);
        max_fd = 0;
        if(fd) {
            FD_SET(fd, &readfds);
            max_fd = fd;
        }
        /*----------------------------------------------------------------------------------*\
        \*----------------------------------------------------------------------------------*/
        i = select(max_fd + 1, &readfds, &writefds, &exceptfds, &timeout);

        /*----------------------------------------------------------------------------------*\
        \*----------------------------------------------------------------------------------*/
        if(FD_ISSET(0, &readfds)) {
            char ch;
            ret = read(0, &ch, sizeof(ch));
            if(ret <= 0)
                continue;
            printf(" %c\n", ch);
            switch(ch) {
                case 'h':
                    printf("help: [q]uit [r]egister [R]elease\n");
                    break;
                case 'r':
                    handle = avm_event_register(argv[0], 0xFF);
                    fd = avm_event_get_fd(handle);
                    FD_SET(fd , &readfds);
                    max_fd = fd;
                    break;
                case 'R':
                    if(handle) {
                        avm_event_release(handle);
                        FD_CLR(fd , &readfds);
                        handle = 0;
                        fd     = 0;
                        max_fd = 0; /*--- stdin ---*/
                    }
                    break;
                case 'q':
                    zuende = 1;
                    break;
            }
            printf("[%s]:", argv[0]);
        }

        /*----------------------------------------------------------------------------------*\
        \*----------------------------------------------------------------------------------*/
        if(handle && FD_ISSET(fd, &readfds)) {
            char Buffer[80];
            ret = read(fd, Buffer, sizeof(Buffer));
            printf("AVM_EVENT(%u): read '%s'\n", ret, Buffer);
        }
    }
    if(handle)
        avm_event_release(handle);
    return 0;
}
