/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <linux/tffs.h>
#include <string.h>
#include <stdio.h>


#define MIN(a, b)       ((a) > (b) ? (b) : (a))
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int htoi(char *p) {
    unsigned int value = 0;
	if(!p || !*p) return value;
    if(*p == '0' && (p[1] == 'x' || p[1] == 'X')) {
        if(!p || !*p) return value; if(*p == '0') p++;
        if(!p || !*p) return value; if(*p == 'x') p++;
        for( ;; ) {
            if(!p || !*p) return value;
            value <<= 4;
            if(*p >= '0' && *p <= '9')
                value |= *p - '0';
            else if(*p >= 'a' && *p <= 'f')
                value |= *p - 'a' + 10;
            else if(*p >= 'A' && *p <= 'F')
                value |= *p - 'A' + 10;
            else
                break;
            p++;
        }
        return value;
    }
    return atoi(p);
}

char dummy[1024];

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
    int size = 0;
    int count;
    int fd;
    char *outputfile = NULL;
    char *device = NULL;

    for(count = 1 ; count < argc ; count++) {
        if(argv[count][0] == '-') {
            switch(argv[count][1]) {
                case 'h': 
                    puts("reinit_jffs2 [-c <mtd char device> -a][-s <size>][-o <outputfile>]");
                    puts("\t-h\tthis help\n");
                    puts("\t-a\tauto size mode\n");
                    puts("\t-s\tsize of mtd block\n");
                    puts("\t-o\toutput file\n");
                    exit(1);
                case 'a':
                    size = -1;
                    break;
                case 's':
                    size = htoi(argv[++count]);
                    break;
                case 'o':
                    outputfile = argv[++count];
                    break;
                case 'c':
                    device = argv[++count];
                    break;
            }
        }
    }

    if(size == -1) {
        unsigned int read_length;
        fd = open(device, O_RDONLY);
        if(fd < 0)
            return -1;
        size = 0;

        do {
            read_length = read(fd, dummy, sizeof(dummy));
            size += read_length;
        } while(read_length);

        close(fd);
    }

    if(outputfile == NULL) {
        fd = 1;
    } else {
        fd = open(outputfile, O_CREAT | O_WRONLY | O_TRUNC, 0666);
        if(fd < 0)
            return -1;
    }

    memset(dummy, 0xFF, sizeof(dummy));

    while(size) {
        size -= write(fd, dummy, MIN(size, (int)sizeof(dummy)));
    }

    close(fd);
    return 0;
}


