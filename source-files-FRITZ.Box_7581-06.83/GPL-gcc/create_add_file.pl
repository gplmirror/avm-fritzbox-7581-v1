#! /usr/bin/perl -w

use strict;
use warnings;

my %kategorie;
my @input_files;
my $ARCH = uc($ENV{FRITZ_BOX_ARCHITECTURE});
my $FILE_ARCH = "unknown";
my $TOOL_ARCH = "unknown";

if(defined($ARCH)) {
    if($ARCH eq "MIPSEL") {
        $FILE_ARCH = "MIPS";
        $TOOL_ARCH = "mipsel";
    }
    if($ARCH eq "MIPS") {
        $FILE_ARCH = "MIPS";
        $TOOL_ARCH = "mips";
    }
    if($ARCH eq "ARM") {
        $FILE_ARCH = "ARM";
        $TOOL_ARCH = "arm";
    }
    if($ARCH eq "ARMLE") {
        $FILE_ARCH = "ARM";
        $TOOL_ARCH = "armle";
    }
    if($ARCH eq "ARMEL") {
        $FILE_ARCH = "ARM";
        $TOOL_ARCH = "armel";
    }
    if($ARCH eq "ARMEB") {
        $FILE_ARCH = "ARM";
        $TOOL_ARCH = "armeb";
    }
    if($ARCH eq "ARMBE") {
        $FILE_ARCH = "ARM";
        $TOOL_ARCH = "armbe";
    }
    if($ARCH eq "X86") {
        $FILE_ARCH = "Intel 80386";
        $TOOL_ARCH = "i686";
    }
}


##########################################################################################
#
##########################################################################################
$kategorie{BASIS} = {
    "order" => "1",
    "group" => "basis",
    "exclude_dirs" => [
        "rootfs/usr/bin/ncurses5-config",
        "rootfs/sbin/chcpu",
        "rootfs/sbin/ldconfig",
        "rootfs/sbin/findfs",
        "rootfs/sbin/fsfreeze",
        "rootfs/sbin/fstrim",
        "rootfs/sbin/blockdev",
        "rootfs/sbin/fdisk",
        "rootfs/sbin/cfdisk",
        "rootfs/sbin/losetup",
        "rootfs/sbin/sfdisk",
        "rootfs/sbin/wipefs",
        "rootfs/sbin/swaplabel",
        "rootfs/sbin/hwclock",
        "rootfs/sbin/ctrlaltdel",
        "rootfs/sbin/fsck.minix",
        "rootfs/sbin/mkfs.bfs",
        "rootfs/sbin/mkfs.minix",
        "rootfs/sbin/mkfs",
        "rootfs/usr/sbin/mkfs.ext4dev",
        "rootfs/usr/sbin/mkfs.ext4",
        "rootfs/usr/sbin/mkfs.ext3",
        "rootfs/usr/sbin/mkfs.ext2",
        "rootfs/usr/sbin/mke2fs",
        "rootfs/usr/bin/col",
        "rootfs/usr/bin/colcrt",
        "rootfs/usr/bin/column",
        "rootfs/usr/bin/cytune",
        "rootfs/usr/bin/tailf",
        "rootfs/usr/bin/getopt",
        "rootfs/usr/bin/ldd",
        "rootfs/usr/bin/strace",
        "rootfs/lib/libthread_db*.so*",
        "rootfs/lib/libthread_db-*.so",
        "rootfs/bin/findmnt",
        "rootfs/bin/more",
        "rootfs/bin/lsblk",
        "rootfs/bin/lsof",
        "rootfs/bin/mountpoint",
        "rootfs/bin/wdctl",
        "rootfs/sbin/swapon",
        "rootfs/sbin/swapoff",
        "rootfs/sbin/swaplabel",
        "rootfs/sbin/mkswap",
        "rootfs/lib/libmount.so.1",
        "rootfs/lib/libmount.so.1.1.0",
        ],
    "include_dirs" => [
        "rootfs/bin",
        "rootfs/sbin",
        "rootfs/lib",
        "rootfs/dev/log",
        "rootfs/lib/ld-uClibc.*.so",
        "rootfs/lib/ld-uClibc.so.0",
        "rootfs/usr/bin/[",
        "rootfs/usr/bin/[[",
        "rootfs/usr/bin/arping",
        "rootfs/usr/bin/awk",
        "rootfs/usr/bin/basename",
        "rootfs/usr/bin/bunzip2",
        "rootfs/usr/bin/bzcat",
        "rootfs/usr/bin/bzip2",
        "rootfs/usr/bin/cc",
        "rootfs/usr/bin/cmp",
        "rootfs/usr/bin/cut",
        "rootfs/usr/bin/dirname",
        "rootfs/usr/bin/du",
        "rootfs/usr/bin/env",
        "rootfs/usr/bin/ether-wake",
        "rootfs/usr/bin/expr",
        "rootfs/usr/bin/find",
        "rootfs/usr/bin/free",
        "rootfs/usr/bin/ftpget",
        "rootfs/usr/bin/ftpput",
        "rootfs/usr/bin/id",
        "rootfs/usr/bin/killall",
        "rootfs/usr/bin/killall5",
        "rootfs/usr/bin/less",
        "rootfs/usr/bin/logname",
        "rootfs/usr/bin/md5sum",
        "rootfs/usr/bin/mkfifo",
        "rootfs/usr/bin/nc",
        "rootfs/usr/bin/nohup",
        "rootfs/usr/bin/nslookup",
        "rootfs/usr/bin/passwd",
        "rootfs/usr/bin/printf",
        "rootfs/usr/bin/readlink",
        "rootfs/usr/bin/realpath",
        "rootfs/usr/bin/renice",
        "rootfs/usr/bin/reset",
        "rootfs/usr/bin/seq",
        "rootfs/usr/bin/sort",
        "rootfs/usr/bin/strace",
        "rootfs/usr/bin/tail",
        "rootfs/usr/bin/tee",
        "rootfs/usr/bin/test",
        "rootfs/usr/bin/tftp",
        "rootfs/usr/bin/time",
        "rootfs/usr/bin/top",
        "rootfs/usr/bin/tr",
        "rootfs/usr/bin/traceroute",
        "rootfs/usr/bin/tty",
        "rootfs/usr/bin/uniq",
        "rootfs/usr/bin/unzip",
        "rootfs/usr/bin/uptime",
        "rootfs/usr/bin/wc",
        "rootfs/usr/bin/wget",
        "rootfs/usr/bin/which",
        "rootfs/usr/bin/xargs",
        "rootfs/usr/lib/libc.so",
        "rootfs/usr/lib/libcrypt.so",
        "rootfs/usr/lib/libdl.so",
        "rootfs/usr/lib/libgcc_s.so",
        "rootfs/usr/lib/libm.so",
        "rootfs/lib/libpthread*so*",
        "rootfs/usr/lib/librt.so",
        "rootfs/usr/sbin/brctl",
        "rootfs/usr/sbin/chroot",
        "rootfs/usr/sbin/inetd",
        "rootfs/usr/sbin/telnetd",
        "rootfs/usr/lib/libintl.so",
        "rootfs/usr/bin/ldd",
        "rootfs/usr/bin/taskset",
        "rootfs/usr/bin/sendarp",
        "rootfs/bin/base64",
    ]        
};

$kategorie{DEV_GCC} = {
    "order" => "10",
    "group" => "capi",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/lib/libgettextpo.so",
        "rootfs/usr/lib/libmudflapth.so.0",
        "rootfs/usr/lib/libgettextpo.so.0",
        "rootfs/usr/lib/libpthread_pic.a",
        "rootfs/usr/lib/libm_pic.a",
        "rootfs/usr/lib/libthread_db_pic.a",
        "rootfs/usr/share/terminfo/v/vt200",
        "rootfs/usr/lib/terminfo",
        "rootfs/usr/include/ncurses.h",
        "rootfs/usr/lib/libcurses.a",
        "rootfs/usr/lib/libgomp.so",
        "rootfs/usr/lib/libmudflapth.so",
        "rootfs/usr/lib/libmpfr.so",
        "rootfs/usr/lib/libmpfr.so.1",
        "rootfs/usr/lib/libgettextsrc.so",
        "rootfs/usr/lib/libmudflap.so.0",
        "rootfs/usr/lib/libgmp.so",
        "rootfs/usr/lib/libgmp.so.3",
        "rootfs/usr/lib/libmudflap.so",
        "rootfs/usr/lib/libgettextlib.so",
        "rootfs/usr/lib/libgomp.so.1",
        "rootfs/usr/lib/librt_pic.a",
        "rootfs/usr/lib/libcrypt_pic.a",
        "rootfs/usr/lib/libdl_pic.a",
        "rootfs/usr/lib/libc_pic.a",
        "rootfs/usr/lib/libtermcap.a",
        "rootfs/usr/share/locale",
        "rootfs/usr/mips-unknown-linux-uclibc",
        "rootfs/usr/lib/libmudflapth.so.0.0.0",
        "rootfs/usr/lib/libgettextpo.a",
        "rootfs/usr/lib/crt1.o",
        "rootfs/usr/lib/libthread_db.a",
        "rootfs/usr/lib/uclibc_nonshared.a",
        "rootfs/usr/lib/libstdc[+]*.a",
        "rootfs/usr/lib/gcc",
        "rootfs/usr/lib/libmudflapth.a",
        "rootfs/usr/lib/libm.a",
        "rootfs/usr/lib/libform.a",
        "rootfs/usr/lib/libgettextpo.la",
        "rootfs/usr/lib/libgettextsrc.a",
        "rootfs/usr/lib/libbfd.a",
        "rootfs/usr/lib/libmpfr.a",
        "rootfs/usr/lib/libgomp.a",
        "rootfs/usr/lib/libintl.la",
        "rootfs/usr/lib/libmpfr.la",
        "rootfs/usr/lib/crti.o",
        "rootfs/usr/lib/libncurses.a",
        "rootfs/usr/lib/libgomp.spec",
        "rootfs/usr/lib/libfl.a",
        "rootfs/usr/lib/libmenu.a",
        "rootfs/usr/lib/libltdl.a",
        "rootfs/usr/lib/libsupc[+]*.a",
        "rootfs/usr/lib/libgmp.la",
        "rootfs/usr/lib/liby.a",
        "rootfs/usr/lib/libpanel.a",
        "rootfs/usr/lib/libiberty.a",
        "rootfs/usr/lib/Scrt1.o",
        "rootfs/usr/lib/libmudflap.a",
        "rootfs/usr/lib/libopcodes.a",
        "rootfs/usr/lib/libfl_pic.a",
        "rootfs/usr/lib/libgettextlib.a",
        "rootfs/usr/lib/libintl.a",
        "rootfs/usr/lib/libc.a",
        "rootfs/usr/lib/crtn.o",
        "rootfs/usr/lib/libltdl.la",
        "rootfs/usr/lib/libdl.a",
        "rootfs/usr/lib/ldscripts",
        "rootfs/usr/lib/libmudflap.so.0.0.0",
        "rootfs/usr/lib/libgettextlib-0.16.1.so",
        "rootfs/usr/lib/libgettextsrc-0.16.1.so",
        "rootfs/usr/lib/libmpfr.so.1.2.0",
        "rootfs/usr/lib/libgettextpo.so.0.3.0",
        "rootfs/usr/lib/libgomp.so.1.0.0",
        "rootfs/usr/lib/libgettextlib.la",
        "rootfs/usr/lib/librt.a",
        "rootfs/usr/lib/libcrypt.a",
        "rootfs/usr/lib/libgmp.a",
        "rootfs/usr/lib/libgettextsrc.la",
        "rootfs/usr/lib/libgmp.so.3.4.4",
        "rootfs/usr/bin/addr2line",
        "rootfs/usr/bin/readelf",
        "rootfs/usr/bin/mips-unknown-linux-uclibc-c[+]*",
        "rootfs/usr/bin/cpp",
        "rootfs/usr/bin/gawk",
        "rootfs/usr/bin/gprof",
        "rootfs/usr/bin/gcc",
        "rootfs/usr/lib/libpthread.a",
        "rootfs/usr/bin/size",
        "rootfs/usr/bin/as",
        "rootfs/usr/bin/gcov",
        "rootfs/usr/bin/ar",
        "rootfs/usr/bin/ld",
        "rootfs/usr/bin/mips-unknown-linux-uclibc-g.*",
        "rootfs/usr/bin/objcopy",
        "rootfs/usr/bin/ranlib",
        "rootfs/usr/bin/gccbug",
        "rootfs/usr/bin/iconv",
        "rootfs/usr/bin/c[+]*",
        "rootfs/usr/bin/g[+]*",
        "rootfs/usr/bin/c.*filt",
        "rootfs/usr/bin/strings",
        "rootfs/usr/bin/objdump",
        "rootfs/usr/bin/strip",
        "rootfs/usr/bin/mips-unknown-linux-uclibc-gcc-.*",
        "rootfs/usr/bin/nm",
        "rootfs/usr/libexec",
        "rootfs/usr/share/locale",
        "rootfs/usr/include",
        "rootfs/etc/ld.so.cache",
        "rootfs/sbin/ldconfig"
        ]
};

$kategorie{UBI} = {
    "order" => "99",
    "group" => "ubi",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/ubidetach",
        "rootfs/usr/sbin/ubirmvol",
        "rootfs/usr/sbin/ubiupdatevol",
        "rootfs/usr/sbin/ubirsvol",
        "rootfs/usr/sbin/ubimkvol",
        "rootfs/usr/sbin/ubiattach"
        ]
};

$kategorie{DROP} = {
    "order" => "99",
    "group" => "drop",
    "exclude_dirs" => [
        "filesystem/rootfs/tmp/ldconfig"
        ],
    "include_dirs" => [
        ]
};


$kategorie{CXX} = {
    "order" => "9",
    "group" => "cxx",
    "exclude_dirs" => [
        "rootfs/usr/lib/libstdc.*so.*-gdb.py",   ### lib stdc++
        ],
    "include_dirs" => [
        "rootfs/usr/lib/libstdc.*so.*",   ### lib stdc++
        ]
};

$kategorie{STRACE} = {
    "order" => "9",
    "group" => "strace",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/bin/strace"
        ]
};

$kategorie{GDB} = {
    "order" => "9",
    "group" => "gdb",
    "exclude_dirs" => [
        "rootfs/usr/bin/ncurses5-config"
        ],
    "include_dirs" => [
        "rootfs/usr/lib/libncurses.so",
        "rootfs/usr/bin/gdbserver",
        "rootfs/usr/bin/gdb",
        "rootfs/lib/libthread_db-*.so",
        "rootfs/lib/libthread_db*.so*",
        ]
};

$kategorie{USB_HOST} = {
    "order" => "3",
    "group" => "usb_host",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/blkid",
        "rootfs/usr/sbin/fsck",
        "rootfs/usr/lib/libuuid.so",
        "rootfs/usr/lib/libblkid.so",
        ]
};

$kategorie{EXT4} = {
    "order" => "3",
    "group" => "ext4",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/fsck.ext4",
        "rootfs/usr/sbin/fsck.ext4dev"
        ]
};

$kategorie{EXT3} = {
    "order" => "3",
    "group" => "ext3",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/fsck.ext3"
    ]
};

$kategorie{MKFS} = {
    "order" => "3",
    "group" => "mkfs",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/mkfs",
        "rootfs/usr/sbin/mkfs.ext4dev",
        "rootfs/usr/sbin/mkfs.ext4",
        "rootfs/usr/sbin/mkfs.ext3",
        "rootfs/usr/sbin/mkfs.ext2",
        "rootfs/usr/sbin/mke2fs"
    ]
};

$kategorie{EXT2} = {
    "order" => "3",
    "group" => "ext2",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/e2fsck",
        "rootfs/usr/lib/libe2p.so",
        "rootfs/usr/sbin/fsck.ext2",
        "rootfs/etc/mke2fs.conf",
        "rootfs/usr/lib/libext2fs.so",
        "rootfs/usr/lib/libcom_err.so"
    ]
};

$kategorie{UBI} = {
    "order" => "3",
    "group" => "ubi",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/ubiattach",
        "rootfs/usr/sbin/ubiblock",
        "rootfs/usr/sbin/ubicrc32",
        "rootfs/usr/sbin/ubidetach",
        "rootfs/usr/sbin/ubiformat",
        "rootfs/usr/sbin/ubimkvol",
        "rootfs/usr/sbin/ubinfo",
        "rootfs/usr/sbin/ubinize",
        "rootfs/usr/sbin/ubirename",
        "rootfs/usr/sbin/ubirmvol",
        "rootfs/usr/sbin/ubirsvol",
        "rootfs/usr/sbin/ubiupdatevol",
        "rootfs/usr/sbin/flash_erase",
    ]
};

$kategorie{NAND} = {
    "order" => "3",
    "group" => "nand",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/nanddump",
        ]
};

$kategorie{EXT_EXTRA} = {
    "order" => "3",
    "group" => "ext_extra",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        ]
};

$kategorie{NAND} = {
    "order" => "3",
    "group" => "nand",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/sbin/nanddump",
    ]
};

$kategorie{DEV_TOOLS} = {
    "order" => "10",
    "group" => "dev_tools",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/usr/bin/gettext",
        "rootfs/usr/lib/awk/grcat",
        "rootfs/usr/lib/awk/pwcat",
        "rootfs/usr/lib/libltdl.so",
        "rootfs/usr/bin/patch",
        "rootfs/usr/bin/make",
        "rootfs/usr/bin/bison",
        "rootfs/usr/bin/pgawk",
        "rootfs/usr/bin/libtoolize",
        "rootfs/usr/bin/libtool",
        "rootfs/usr/bin/pgawk-3.1.5",
        "rootfs/usr/bin/yacc",
        "rootfs/usr/bin/flex",
        "rootfs/usr/bin/igawk",
        "rootfs/usr/share/aclocal",
        "rootfs/usr/share/bison",
        "rootfs/usr/share/libtool",
        "rootfs/usr/share/awk"
        ]
};

$kategorie{USER_ROOT} = {
    "order" => "10",
    "group" => "user_root",
    "exclude_dirs" => [
        ],
    "include_dirs" => [
        "rootfs/root/.bash_profile",
        "rootfs/root/.bash_history",
        "rootfs/root/.bash_logout",
        "rootfs/root/.bashrc"
        ]
};

my %dirs;
my %files;

##########################################################################################
#
##########################################################################################
sub add_file_dir {
    my ( $tag, $file ) = @_;

    my $dir = $file;
    $dir =~ s/(.*)\/.*/$1/g;
    my $hash_dir = $dir;
    $hash_dir =~ s/\//_/g;

    if (not defined($dirs{$hash_dir})) {
        $dirs{$hash_dir} = [ ];
    };
    foreach my $i (keys %dirs) {
#        print "## hash=" . $i . "\n";
        foreach my $ii (@{${dirs}{$i}}) {
            if (( $ii->{dir} eq $dir ) and ( $ii->{tag} eq $tag )) {
                return;
            }
        }
    }
    push @{$dirs{$hash_dir}}, ( { "tag" => $tag, "dir" => $dir } );
#     print "## dir=" . $dir . "\n";
}

##########################################################################################
#
##########################################################################################
sub add_file_file {
    my ( $tag, $file ) = @_;

    my $hash_file = $file;
    $hash_file =~ s/\//_/g;

    if (not defined($files{$hash_file})) {
        $files{$hash_file} = [ ];
    };
    foreach my $i (keys %files) {
#        print "## hash=" . $i . "\n";
        foreach my $ii (@{${files}{$i}}) {
            if (( $ii->{file} eq $file) and ( $ii->{tag} eq $tag )) {
                return;
            }
        }
    }
    push @{$files{$hash_file}}, ( { "tag" => $tag, "file" => $file } );
#    print "## file=" . $file . "\n";
}


##########################################################################################
#
##########################################################################################
sub find_kategorie {
    my ( $file ) = @_;

#    print STDERR "########## FILE " . $file . "\n";
    foreach my $i (sort { ${kategorie}{$a}->{order} <=> ${kategorie}{$b}->{order} } keys %{kategorie}) {
#        print STDERR "Check Kategorie: " . $i . "\n";
        foreach my $inc_dir (@{$kategorie{$i}{include_dirs}}) {
#            print STDERR "Check Include: " . $inc_dir . ": ";
            my $match = "\$file =~ \"" . $inc_dir . "\"";
            if ( eval $match ) {
#                print STDERR "Ja\n";
                my $include = "ja";
                if($#{$kategorie{$i}{exclude_dirs}} >= 0) {
                    foreach my $excl_dir (@{$kategorie{$i}{exclude_dirs}}) {
#                        print STDERR "\tCheck Exclude: " . $excl_dir . ": ";
                        $match = "\$file =~ \"" . $excl_dir . "\"";
                        if ( eval $match ) {
#                            print STDERR "Ja\n";
                            $include = "nein";
                            last;
#                        } else {
#                            print STDERR "Nein\n";
                        }
                    }
                }
                if($include eq "ja") {
                    add_file_dir($i, $file);
                    add_file_file($i, $file);
                    return;
                }
#            } else {
#                print STDERR "Nein\n";
            }
        }
    }
#    if($file =~ /rootfs/) {
#        print STDERR "[find_kategorie]: " . $file . " konnte keine Kategorie zugeordnet werden.\n";
#    }
}

##########################################################################################
#
##########################################################################################
sub read_files {
    my ( $kernel_dir ) = @_;
    my $handle;
    my $count = 0;

#    print "## open \$handle, \"find \"" . $kernel_dir . "\" |\"\n";
    open $handle, "find " . $kernel_dir . 
        " |" or die "FEHLER: Konnte keine Kernel Treiber finden";

    while(<$handle>) {
        $count++;
        if($count > 25) {
            $count = 0;
            print STDERR ".";
        }
        $_ =~ s/\n//g;
        push @input_files, ( $_ );
    }
    print STDERR "\n";
    close $handle;
}

##########################################################################################
#
##########################################################################################
sub process_files {
    my $count = 0;
    for my $file ( @input_files ) {
        $count++;
        if($count > 25) {
            $count = 0;
            print STDERR "+";
        }
        find_kategorie ($file)
    }
}

##########################################################################################
#
##########################################################################################
sub map_source {
    my ( $type, $source ) = @_;

    $source =~ s/(.*?filesystem)\/(.*)/$1\/.\/$2/g;
    return $source;
}

##########################################################################################
#
##########################################################################################
sub map_dest {
    my ( $type, $dest ) = @_;

    $dest =~ s/(.*?filesystem)\/rootfs\/(.*)/.\/$2/g;
    return $dest;
}


##########################################################################################
#
##########################################################################################
sub write_add_file_dirs {
    my ( $out ) = @_;
    foreach my $i (keys %dirs) {
        my $D = undef;
        my $trenner = "";
#        print "## hash=" . $i . "\n";
        print $out "[";
        foreach my $dir (@{${dirs}{$i}}) {
            print $out $trenner . $dir->{tag};
            $trenner = ",";
#            print "\tdir " . $dir->{dir} . " TAG " . $dir->{tag} . "\n";
            if ( not defined($D) ) {
                $D = $dir;
            }
        }
        my $source = map_source("dir", $D->{dir});
        my $dest   = map_dest("dir", $D->{dir});
        print $out "] D 777 " . $source . " " . $dest . "\n";
    }
}

##########################################################################################
#
##########################################################################################
sub write_add_file_files {
    my ( $out ) = @_;
    foreach my $i (keys %files) {
        my $F = undef;
        my $trenner = "";
        print $out "[";
        foreach my $file (@{${files}{$i}}) {
            print $out $trenner . $file->{tag};
            $trenner = ",";
#            print "\tfile " . $file->{file} . " TAG " . $file->{tag} . "\n";
            if ( not defined($F) ) {
                $F = $file;
            }
        }
        if(-l $F->{file}) {
            my $source = map_dest("link", $F->{file});
            my $dest   = map_dest("link", readlink $F->{file});
            print $out "] L 777 " . $source . " " . $dest . "\n";
        } else {
            my $s_handle;
            my $strip_info = "";
            my $strip_option = "--strip-debug --strip-unneeded";
            if ( open $s_handle, "file \"" . $F->{file} . "\" |" ) {
                while(<$s_handle>) {
                    if($_ =~ /$FILE_ARCH/) {
                        if($_ =~ /not stripped/) {
                            $strip_info = $F->{file};
                        }
                        if($_ =~ /shared object/) {
                            my $strip_option = $strip_option . " --";
                        }
                        if($_ =~ /executable/) {
                            my $strip_option = $strip_option . " --discard-all --";
                        }
                        
                    }
                }
                close $s_handle;
            }
            my $attr = "000";
            if ( -x $F->{file} ) {
                if ( -w $F->{file} ) {
                    $attr = "777";
                } else {
                    $attr = "555";
                }
            } else {
                if ( -w $F->{file} ) {
                    $attr = "666";
                } else {
                    $attr = "444";
                }
            }
            my $source = map_source("file", $F->{file});
            my $dest   = map_dest("file", $F->{file});
            print $out "] F " . $attr . " " . $source . " " . $dest . "\n";
            if(length($strip_info) > 1) {
                my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size, $atime,$mtime,$ctime,$blksize,$blocks) = stat($strip_info);
                my $vor = $size;
                print $out "##### File before strip: '" . $strip_info . "' " . $size . " Bytes\n";
                if( open $s_handle, "filesystem/usr/bin/" . lc($TOOL_ARCH) . "-linux-strip --verbose " . $strip_info . " " . $strip_option . " |" ) {
                    while(<$s_handle>) {
                        print $out "###" . $_;
                    }
                    close $s_handle;
                }
                ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size, $atime,$mtime,$ctime,$blksize,$blocks) = stat($strip_info);
                my $nach = $size;
                print $out "##### File after strip: '" . $strip_info . "' " . $size . " Bytes\n";
                print STDERR "[STRIP] " . $dest . " vor " . $vor . " nach " . $nach . " -" . ( 100 - ($nach * 100 / $vor)) . "%\n";
            }
        }
    }
}

my $out;

if ( defined( $ARGV[0] )) {
    print "Schreibe Datei " . $ARGV[0] . " ...\n";
    open $out, ">" . $ARGV[0] or die "FEHLER: Konnte " . $ARGV[0] . " nicht öffnen\n";
} else {
    $out = <STDOUT>;
}


print "# Read Filesystem\n";
read_files ("filesystem");
process_files ();
print "# add file dirs \n";
write_add_file_dirs($out) ;
print "# add files \n";
write_add_file_files($out) ;
print "# done\n";

