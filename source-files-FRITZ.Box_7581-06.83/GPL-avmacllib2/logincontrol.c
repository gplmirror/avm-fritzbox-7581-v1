/*
 * - User Login Control / Event Logging
 *
 * Copyright 2009 by AVM GmbH <info@avm.de>
 *
 * This software contains free software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License ("License") as published by the Free Software Foundation 
 * (version 2 of the License). This software is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
 * A PARTICULAR PURPOSE. See the copy of the License you received along with this software for more details.
 */

/* memory debugging
 * #include "config.h"
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdarg.h>

#include "logincontrol.h"

#pragma pack(1)
struct record
{
	char name[256];  // empty name -> free record
	short failed_logins_local;
	short failed_logins_extern;
	time_t blocked;      // 0: not blocked
						// else time till login is blocked in means of CLOCK_MONOTONIC
};
#pragma pack()

#define LOCK_FILE "/var/userlogin.lock"
#define DATA_FILE "/var/userlogin.data"

#define BLOCK_TIME (60*60)


#if 0
static void avmacl_log(const char *fmt, ...)
{
	FILE *fp = fopen("/dev/console", "w");
	if (fp) {
		va_list va;
		va_start(va, fmt);
		vfprintf(fp, fmt, va);
		va_end(va);
		fclose(fp);
	}
}
#endif

static int Lock(void)
{
	short tries = 0;
	int fd;

again:
	fd = open(LOCK_FILE, O_CREAT|O_WRONLY|O_TRUNC|O_EXCL, 0666);
	if (fd >= 0) {
		close(fd);
		return 1; // ok
	}
	if (errno != EEXIST) {
		return 0;
	}
	if (tries++ > 100) {
		return 0;
	}

	struct timespec ts;
	ts.tv_sec = 0;
	ts.tv_nsec = 10 * 1000 * 1000; // 10ms

	nanosleep(&ts, 0);
	goto again;
}

static void Unlock(void)
{
	unlink(LOCK_FILE);
}

static struct record *LockRecordIfExists(const char *display_name)
{
	int found = 0;
	FILE *fp = NULL;
	struct record *r = malloc(sizeof(struct record));

	if (!r) return 0;

	if (!Lock()) {
		free (r);
		r = 0;

		return 0;
	}

	fp = fopen(DATA_FILE, "r");

	if (!fp) goto end;

	do {
		if (sizeof(struct record) != fread(r, 1, sizeof(struct record), fp)) break;
		r->name[sizeof(r->name)-1] = '\0';
		if (!strcmp(display_name, r->name)) { found = 1; }
	} while(!found);
	fclose(fp);

end:
	if (!found) {
		Unlock();
		free(r);
		r = 0;
	}
	return r;
}

static struct record *LockRecordWithCreate(const char *display_name)
{
	FILE *fp = NULL;
	int found = 0;
	struct record *r = malloc(sizeof(struct record));
	if (!r) return 0;

	if (!Lock()) {
		free (r);
		r = 0;

		return 0;
	}

	fp = fopen(DATA_FILE, "r");
	if (!fp) goto end;

	do {
		if (sizeof(struct record) != fread(r, 1, sizeof(struct record), fp)) break;
		r->name[sizeof(r->name)-1] = '\0';
		if (!strcmp(display_name, r->name)) { found = 1; }
	} while(!found);
	fclose(fp);

end:
	if (!found) {
		memset(r, 0, sizeof(*r));
		strncpy(r->name, display_name, sizeof(r->name)-1);
		r->name[sizeof(r->name)-1] = '\0';
	}
	return r;
}

static void UnlockRecord(struct record *r)
{
	Unlock();
	free(r);
}

// -------------------------------------------------------

static void WriteAndUnlockRecord(struct record *r)
{
	short found = 0;

	FILE *fp = fopen(DATA_FILE, "r+");
	if (!fp) {
		int fd = open(DATA_FILE, O_CREAT|O_WRONLY|O_TRUNC, 0666);
		if (fd >= 0) close(fd);
		fp = fopen(DATA_FILE, "r+");
		if (!fp) {
			goto end;
		}
	}

	long free_offset = -1;
	do {
		struct record rec;
		if (sizeof(struct record) != fread(&rec, 1, sizeof(struct record), fp)) break;
		if (free_offset == -1 && rec.name[0] == '\0') free_offset = ftell(fp);
		rec.name[sizeof(rec.name)-1] = '\0';
		if (!strcmp(r->name, rec.name)) { found = 1; }
	} while(!found);

	if (0 == r->failed_logins_local && 0 == r->failed_logins_extern && 0 == r->blocked) {
		// delete this entry
		r->name[0] = '\0';
	}

	if (!found) {
		if (r->name[0] == '\0') goto end;
		if (free_offset != -1) {
			fseek(fp, free_offset -  sizeof(struct record), SEEK_SET);
		}
	} else {
		fseek(fp, - sizeof(struct record), SEEK_CUR);
	}
	fwrite(r, 1, sizeof(struct record), fp);
	fclose(fp);
	fp = NULL;

end:
	if (fp)
	{
		fclose (fp);
	}

	free(r);
	Unlock();
}

// ---------------------------------------------------------------------------

static int IsOEM_tcom(void)
{
	static int is = -2;

	if (-2 == is) {
		char *s = getenv("OEM");
		if (s && (!strcmp(s, "tcom") || !strcmp(s, "congstar"))) is = 1;
		else is = 0;
	}
	return is;
}

static void Event(unsigned id, const char *display_name)
{
    int status;
    pid_t pid;
	
	pid = fork();
	if (pid < 0) {
		return;
	}
	if (pid == 0) { /* child */
		char id_buf[32];
		snprintf(id_buf, sizeof(id_buf), "%u", id);
		id_buf[sizeof(id_buf)-1] = '\0';
		(void)execl("/sbin/eventadd", "eventadd", id_buf, display_name, NULL);
		exit(1);
	}
	/* parent */
	(void)waitpid( pid, &status, 0); 	
}


static int IsStillBlocked(time_t t)
{
	if (!t) return 0;
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return t > ts.tv_sec;
}

int LoginControlIsAllowed(const char *display_name)
{
	if (!IsOEM_tcom()) return 1;
	if (!display_name) return 0;

	struct record *r = LockRecordIfExists(display_name);
	if (!r) return 1;

	short modified = 0;
	int allow = 1;
	if (r->blocked) {
		if (IsStillBlocked(r->blocked)) allow = 0;
		else {
			// reset
			Event(712, display_name);
			r->blocked = 0;
			r->failed_logins_local = 0;
			r->failed_logins_extern = 0;
			modified = 1;
		}
	}
	if (modified) WriteAndUnlockRecord(r);
	else UnlockRecord(r);
	return allow;
}

void LoginControlWrongPassword(const char *display_name, int from_internet)
{
	if (!display_name || !IsOEM_tcom()) return;

	struct record *r = LockRecordWithCreate(display_name);
	if (!r) {
		return;
	}
	if (r->blocked && !IsStillBlocked(r->blocked)) {
		// reset
		r->blocked = 0;
		r->failed_logins_local = 0;
		r->failed_logins_extern = 0;
	}
	if (from_internet) r->failed_logins_extern++;
	else r->failed_logins_local++;
	if (r->failed_logins_local > 5 || r->failed_logins_extern > 5) {
		 struct timespec ts;
		 clock_gettime(CLOCK_MONOTONIC, &ts);
		r->blocked = ts.tv_sec + BLOCK_TIME; // login blocked for some time
		Event(from_internet ? 718 : 717, display_name);
	}
	WriteAndUnlockRecord(r);
}

void LoginControlPasswordOk(const char *display_name, int from_internet)
{
	if (!display_name || !IsOEM_tcom()) return;

	if (from_internet) Event(701, display_name); // EVENT_ID_TCOM_FW102
	Event(from_internet ? 715 : 713, display_name);

	struct record *r = LockRecordIfExists(display_name);
	if (!r) return;
	short modified = 0;
	if (!from_internet && r->failed_logins_local) { modified = 1; r->failed_logins_local = 0; }
	if (from_internet && r->failed_logins_extern) { modified = 1; r->failed_logins_extern = 0; }
	if (r->blocked) {
		r->blocked = 0;
		modified = 1;
	}
	if (modified) WriteAndUnlockRecord(r);
	else UnlockRecord(r);
}

void LoginControlLogout(const char *display_name, int from_internet)
{
	if (!display_name || !IsOEM_tcom()) return;

	if (from_internet) Event(702, display_name); // EVENT_ID_TCOM_FW103
	Event(from_internet ? 716 : 714, display_name);
}

void LoginControlReset(const char *display_name)
{
	if (!display_name || !IsOEM_tcom()) return;

	struct record *r = LockRecordIfExists(display_name);
	if (!r) return;
	r->failed_logins_local = 0;
	r->failed_logins_extern = 0;
	r->blocked = 0;
	WriteAndUnlockRecord(r);
}

// check all for expired blocking
// return time till next expire
time_t  LoginControlCleanup(void)
{
	if (!IsOEM_tcom()) return (time_t)-1;

	if (!Lock()) return 50;

	time_t min_blocked = (time_t)-1;
	FILE *fp = fopen(DATA_FILE, "r+");
	if (!fp) goto end;

	do {
		struct record rec;
		if (sizeof(struct record) != fread(&rec, 1, sizeof(struct record), fp)) break;
		if (rec.name[0] == '\0') continue;
		if (rec.blocked) {
			if (!IsStillBlocked(rec.blocked)) {
				rec.name[sizeof(rec.name)-1] = '\0';
				Event(712, rec.name);
				rec.name[0] = '\0';
				fseek(fp, - sizeof(struct record), SEEK_CUR);
				fwrite(&rec, 1, sizeof(struct record), fp);
				fseek(fp, 0, SEEK_CUR); // read/write switch
			} else {
				if (min_blocked == (time_t)-1 || rec.blocked < min_blocked) min_blocked = rec.blocked;
			}
		}
	} while(1);
	fclose(fp);

end:
	Unlock();
	if (min_blocked == (time_t)-1) return BLOCK_TIME;
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	if (ts.tv_sec >= min_blocked) return 1;
	return min_blocked - ts.tv_sec + 1;
}
