#ifndef _INCLUDE_LINUX_AVM_KERNEL_CONFIG_H_
#define _INCLUDE_LINUX_AVM_KERNEL_CONFIG_H_

#include <linux/err.h>
#include <uapi/linux/avm_kernel_config.h>

extern struct _avm_kernel_config **avm_kernel_config;
extern struct _kernel_modulmemory_config *kernel_modulmemory_config;
extern unsigned char *avm_kernel_config_device_tree[avm_subrev_max];
extern struct _avm_kernel_version_info *avm_kernel_version_info;

static inline int init_avm_kernel_config_ptr(void)
{
	extern unsigned int __avm_kernel_config_start __attribute__ ((weak));

	if (IS_ERR(&__avm_kernel_config_start) ||
	    (&__avm_kernel_config_start == NULL))
		return -1;

	avm_kernel_config =
	    (struct _avm_kernel_config **)&__avm_kernel_config_start;

	return 0;
}

extern void init_avm_kernel_config(void);

#endif
/* vim: set noexpandtab sw=8 ts=8 sts=0: */
