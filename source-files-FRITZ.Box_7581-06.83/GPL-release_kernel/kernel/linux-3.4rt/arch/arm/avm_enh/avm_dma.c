/*
 *
 * ########################################################################
 *
 *  This program is free software; you can distribute it and/or modify it
 *  under the terms of the GNU General Public License (Version 2) as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  59 Temple Place - Suite 330, Boston MA 02111-1307, USA.
 *
 * ########################################################################
 *
 *
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/types.h>

#if defined(CONFIG_ARCH_IPQ806X_DT) || defined(CONFIG_MACH_BCM963138)
extern void v7_flush_kern_dcache_area(unsigned long page_address, ssize_t size);
extern void v7_dma_flush_range(unsigned long start_virt_addr, unsigned long end_virt_addr);

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void dma_cache_inv(unsigned long start_virt_addr, size_t len) {
	v7_dma_flush_range(start_virt_addr, start_virt_addr + len - 1);
}
EXPORT_SYMBOL(dma_cache_inv);

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void dma_cache_wback_inv(unsigned long page_address, ssize_t size) {
    v7_flush_kern_dcache_area(page_address, size);
}
EXPORT_SYMBOL(dma_cache_wback_inv);
#endif


#include <asm/mach_avm.h>
