/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2016 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 *
 *   backtrace-helper-functions for stackdump on smp etc.
\*------------------------------------------------------------------------------------------*/

#if defined(CONFIG_AVM_FASTIRQ)

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/percpu.h>
#include <linux/init.h>
#include <linux/init_task.h>
#include <asm/thread_info.h>
#include <asm/traps.h>
#include <asm/cacheflush.h>
#include <asm/cp15.h>
#include <linux/spinlock.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/irq_on.h>
#include <linux/delay.h>

#include <mach/avm_fiq_os.h>
#include <asm/mach_avm.h>
#include <mach/avm_gic.h>
#include <mach/avm_gic_fiq.h>
#include <asm/irq_regs.h>
#include <asm/unwind.h>

#include <linux/string.h>
#include <linux/errno.h>

enum _monitor_ipi_type {
    monitor_nop                = 0x0,
    monitor_bt                 = 0x1,
};

struct _monitor_bt {
    struct pt_regs ptregs;
    unsigned long svc_sp;   /*--- fuer current ---*/
    unsigned long transbase;
    unsigned long dac;
    unsigned long ctrl;
    unsigned int all_register_valid;
};

struct _monitor_ipi {
    enum _monitor_ipi_type type;
    union __monitor_ipi {
        struct _monitor_bt bt;
    } buf;
    unsigned int init;
    unsigned int irq;
    atomic_t     ready;
    spinlock_t   lock; 
};
static struct _monitor_ipi IPI_Monitor[NR_CPUS];
static atomic_t backtrace_busy;

static const char *processor_modes[] = {
  "USER_26", "FIQ_26" , "IRQ_26" , "SVC_26" , "UK4_26" , "UK5_26" , "UK6_26" , "UK7_26" ,
  "UK8_26" , "UK9_26" , "UK10_26", "UK11_26", "UK12_26", "UK13_26", "UK14_26", "UK15_26",
  "USER_32", "FIQ_32" , "IRQ_32" , "SVC_32" , "UK4_32" , "UK5_32" , "UK6_32" , "ABT_32" ,
  "UK8_32" , "UK9_32" , "UK10_32", "UND_32" , "UK12_32", "UK13_32", "UK14_32", "SYS_32"
};
static const char *isa_modes[] = {
  "ARM" , "Thumb" , "Jazelle", "ThumbEE"
};
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static void get_cp15_register(struct _monitor_bt *pbt) {
#ifdef CONFIG_CPU_CP15
    unsigned int ctrl;
#ifdef CONFIG_CPU_CP15_MMU
    unsigned int transbase, dac;
    asm("mrc p15, 0, %0, c2, c0\n\t"
        "mrc p15, 0, %1, c3, c0\n"
                : "=r" (transbase), "=r" (dac));
    pbt->transbase = transbase;
    pbt->dac       = dac;
#endif
    asm("mrc p15, 0, %0, c1, c0\n" : "=r" (ctrl));
    pbt->ctrl = ctrl;
#endif
}
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static inline long get_cpsr_reg(void) {
        unsigned long cpsr_reg;
        asm volatile(
                     "       mrs     %0, cpsr        @ get_cpsr\n"
                     : "=r" (cpsr_reg)
                     :
                     : "memory", "cc");
   return cpsr_reg;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int __get_userinfo(char *buf, unsigned int maxbuflen, struct mm_struct *mmm, unsigned long addr) {
    struct vm_area_struct *vm;
    unsigned int i = 0;
    if(mmm == NULL) {
        return 1;
    }
    vm = mmm->mmap;
    while(vm) {
        /*--- printk(KERN_INFO"%s[%x]:%p %x - %x vm_mm %p\n", __func__, addr, vm, vm->vm_start, vm->vm_end, vm->vm_mm); ---*/
        if((addr >= vm->vm_start) && (addr < vm->vm_end)) {
            snprintf(buf, maxbuflen,"seg=%3u of=0x%08lx/0x%lx [%s]", i, addr - (unsigned long)vm->vm_start, (unsigned long)vm->vm_end - (unsigned long)vm->vm_start,
                         (vm->vm_file && vm->vm_file->f_path.dentry) ? (char *)vm->vm_file->f_path.dentry->d_name.name : "");
            /*--- printk(KERN_INFO"%s", buf); ---*/
            return 0;
        }
        vm = vm->vm_next;
        i++;
    }
    return 1;
}
/*--------------------------------------------------------------------------------*\
 *  Schwachstelle: thread/task nicht mehr valid (in dem Moment freigegeben)
\*--------------------------------------------------------------------------------*/
static char *get_user_symbol(char *txt, unsigned int maxttxlen, struct thread_info *thread, unsigned long addr) {
    struct task_struct *task = NULL;
    unsigned long flags;
    txt[0] = 0;

    firq_local_irq_save(flags);
    task = thread->task;
    if(task == NULL) {
        firq_local_irq_restore(flags);
        return txt; 
    }
    if(firq_spin_trylock(&task->alloc_lock)) {
        /*--- kritisch: mmput()/get_task_mm() darf im FIrq-Kontext nicht verwendet werden, mmput hoffentlich nur im task_lock() ---*/
        __get_userinfo(txt, maxttxlen, task->mm, addr);
        firq_spin_unlock(&task->alloc_lock);
    }
    firq_local_irq_restore(flags);
    return txt; 
}
/**--------------------------------------------------------------------------------**\
 * FAST-IRQ-Kontext
\**--------------------------------------------------------------------------------**/
static noinline void monitor_ipi_firq_context(struct _monitor_ipi *m_ipi) {
    if(atomic_read(&m_ipi->ready)) {
        return;
    }
    switch(m_ipi->type) {
            case monitor_bt:  
                copy_banked_regs_full(&m_ipi->buf.bt.ptregs, get_fiq_regs());
                m_ipi->buf.bt.svc_sp = get_svc_sp(); /*--- der current-thread-sp existiert nur im SVC-Mode ---*/
                get_cp15_register(&m_ipi->buf.bt);
                m_ipi->buf.bt.all_register_valid = 1;
                break;
            case monitor_nop:  
            default:  
                break;
    }
    atomic_set(&m_ipi->ready, 1);
}

/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static void dump_instr(const char *lvl, struct pt_regs *regs) {
	unsigned long addr = instruction_pointer(regs);
	const int thumb = thumb_mode(regs);
	const int width = thumb ? 4 : 8;
	mm_segment_t fs;
	char str[sizeof("00000000 ") * 5 + 2 + 1], *p = str;
	int i;

	/*
	 * We need to switch to kernel mode so that we can use __get_user
	 * to safely read from kernel space.  Note that we now dump the
	 * code first, just in case the backtrace kills us.
	 */
	fs = get_fs();
	set_fs(KERNEL_DS);

	for (i = -4; i < 1 + !!thumb; i++) {
		unsigned int val, bad;

		if (thumb)
			bad = __get_user(val, &((u16 *)addr)[i]);
		else
			bad = __get_user(val, &((u32 *)addr)[i]);

		if (!bad)
			p += sprintf(p, i == 0 ? "(%0*x) " : "%0*x ",
					width, val);
		else {
			p += sprintf(p, "bad PC value");
			break;
		}
	}
	printk("%sCode: %s\n", lvl, str);

	set_fs(fs);
}

/**--------------------------------------------------------------------------------**\
 * Dump memory
\**--------------------------------------------------------------------------------**/
static void dump_mem(const char *lvl, const char *str, unsigned long bottom,
		     unsigned long top)
{
	unsigned long first;
	mm_segment_t fs;
	int i;

	/*
	 * We need to switch to kernel mode so that we can use __get_user
	 * to safely read from kernel space.  Note that we now dump the
	 * code first, just in case the backtrace kills us.
	 */
	fs = get_fs();
	set_fs(KERNEL_DS);

	printk("%s%s(0x%08lx to 0x%08lx)\n", lvl, str, bottom, top);

	for (first = bottom & ~31; first < top; first += 32) {
		unsigned long p;
		char str[sizeof(" 12345678") * 8 + 1];

		memset(str, ' ', sizeof(str));
		str[sizeof(str) - 1] = '\0';

		for (p = first, i = 0; i < 8 && p < top; i++, p += 4) {
			if (p >= bottom && p < top) {
				unsigned long val;
				if (__get_user(val, (unsigned long *)p) == 0)
					sprintf(str + i * 9, " %08lx", val);
				else
					sprintf(str + i * 9, " ????????");
			}
		}
		printk("%s%04lx:%s\n", lvl, first & 0xffff, str);
	}

	set_fs(fs);
}

/**--------------------------------------------------------------------------------**\
 * wir sind auf der cpu nicht im firq: nun steht noch dir Kontext-Frage:
 *  (a) Interrupt
 *  (b) Exception
 *  (c) direkter Aufruf 
\**--------------------------------------------------------------------------------**/
static void monitor_ipi_direct_context(struct _monitor_ipi *m_ipi, struct pt_regs *exception_regs) {
    struct pt_regs *regs;
    register unsigned long current_sp asm ("sp");

    if(atomic_read(&m_ipi->ready)) {
        return;
    }
    switch(m_ipi->type) {
            /*--- kein break ---*/
        case monitor_bt:  
              if(exception_regs) {
                /*--- Exception-Kontext: Exception-Register muessen extern uebergeben werden ---*/
                regs = exception_regs;
            } else if(in_interrupt()) {
                /*--- Irq-Kontext ---*/
                regs = get_irq_regs();
            } else {
                regs = NULL;
            }
            if(regs) {
                if(regs) {
                    memcpy(&m_ipi->buf.bt.ptregs, regs, sizeof(m_ipi->buf.bt.ptregs));
                    m_ipi->buf.bt.all_register_valid = 1;
                }
            } else {
                /**--------------------------------------------------------------------------------**\
                 * Wenn aus einer (Fault-)Exception ohne exception_regs: 
                 * erstmal (unsinniger) Backtrace des Exception-Handlers, dabei  
                 * erkennt der Backtracer hoffentlich das Exception-Text-Segment
                 * und liefert dann doch nochmal den relevanten Backtrace
                \**--------------------------------------------------------------------------------**/
                memset(&m_ipi->buf.bt, 0, sizeof(m_ipi->buf.bt));
                m_ipi->buf.bt.ptregs.ARM_fp   = (unsigned long)__builtin_frame_address(0);
                m_ipi->buf.bt.ptregs.ARM_pc   = _THIS_IP_;
                m_ipi->buf.bt.ptregs.ARM_lr   = _RET_IP_;
                m_ipi->buf.bt.ptregs.ARM_sp   = current_sp;
                m_ipi->buf.bt.ptregs.ARM_cpsr = get_cpsr_reg();
                m_ipi->buf.bt.all_register_valid = 0;         /*--- Registerdump sinnfrei ---*/
            }
            m_ipi->buf.bt.svc_sp = current_sp;
            get_cp15_register(&m_ipi->buf.bt);
            break;
        case monitor_nop:  
        default:  
            break;
    }
    atomic_set(&m_ipi->ready, 1);
}
/**--------------------------------------------------------------------------------**\
 * FAST-IRQ-Kontext
\**--------------------------------------------------------------------------------**/
static irqreturn_t firq_monitor(int irq __attribute__((unused)), void *handle) {
    struct _monitor_ipi *m_ipi = (struct _monitor_ipi *)handle;
    monitor_ipi_firq_context(m_ipi);
    return IRQ_HANDLED;
}
/**--------------------------------------------------------------------------------**\
  Absetzen eines Monitor-Befehls um synchron Infos von anderen CPU's zu erhalten
  ret: 0 ok 
\**--------------------------------------------------------------------------------**/
static noinline int trigger_firq_monitor(unsigned int cpu, enum _monitor_ipi_type type, void *buf, struct pt_regs *exception_regs) {
    int ret = 0, os_context;
    unsigned long flags, retry = 1000;
    struct _monitor_ipi *m_ipi = &IPI_Monitor[cpu];
    if(cpu > ARRAY_SIZE(IPI_Monitor)) {
        return -EINVAL;
    }
    if(m_ipi->init == 0) {
        return -EACCES;
    }
    os_context = firq_is_linux_context();
    if(os_context) {
        spin_lock_irqsave(&m_ipi->lock, flags);
    }
    atomic_set(&m_ipi->ready, 0);
    m_ipi->type = type;
    if(raw_smp_processor_id() == cpu) {
        if(!os_context) {
            /*--- sind auf dieser CPU im fastirq ---*/
            monitor_ipi_firq_context(m_ipi);
        } else {
            /*--- sind auf dieser CPU (irq oder normaler kernel-kontext) ---*/
            monitor_ipi_direct_context(m_ipi, exception_regs);
        }
    } else {
        avm_gic_fiq_raise_irq(m_ipi->irq);/*--- trigger m_ipi irq ---*/
    }
    while(retry && atomic_read(&m_ipi->ready) == 0) {
        udelay(1);
        retry--;
    }
    if(atomic_read(&m_ipi->ready)) {
       switch(type) {
            case monitor_bt:  
                memcpy(buf, &m_ipi->buf.bt, sizeof(m_ipi->buf.bt));
                break;
            case monitor_nop:  
            default:
                break;
        }
    } else {
        printk(KERN_ERR"%s: for cpu%u on cpu%u%s timeout-error\n", __func__, cpu, raw_smp_processor_id(), os_context ? "" : "(fiq-context)");
        ret = -EACCES;
    }
    /*--- printk(KERN_ERR"%s: retry=%lu\n", __func__, retry); ---*/
    if(os_context) {
        spin_unlock_irqrestore(&m_ipi->lock, flags);
    }
    return ret;
}
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static void bt_show_regs(int cpu, struct _monitor_bt *pbt, struct thread_info *pthread) {
    unsigned int all_register_valid;
    struct pt_regs *regs;
    struct task_struct *curr_tsk = NULL;
	unsigned long flags, flag;
	char txtbuf[64];
	char buf[64];
    
    regs               = &pbt->ptregs;
    all_register_valid = pbt->all_register_valid;
    curr_tsk           = pthread->task;

    firq_local_irq_save(flag);
    printk(KERN_ERR"\n");
    printk(KERN_ERR"CPU: %d Pid: %d, comm: %20s\n", cpu, task_pid_nr(curr_tsk), curr_tsk->comm);
	printk(KERN_ERR"       %s  (%s %.*s)\n",        print_tainted(), init_utsname()->release, 
                                                    (int)strcspn(init_utsname()->version, " "),
                                                    init_utsname()->version);
    if(user_mode(regs)) {
        printk(KERN_ERR"PC is at %s\n", get_user_symbol(txtbuf, sizeof(txtbuf), pthread, regs->ARM_pc)); 
        printk(KERN_ERR"LC is at %s\n", get_user_symbol(txtbuf, sizeof(txtbuf), pthread, regs->ARM_lr)); 
    } else {
        print_symbol("PC is at %s\n", instruction_pointer(regs));
        print_symbol("LR is at %s\n", regs->ARM_lr);
    }
    if(all_register_valid) {
        printk(KERN_ERR"pc :[<%08lx>] lr :[<%08lx>] psr: %08lx\n"
	                   "sp :  %08lx   ip :  %08lx   fp : %08lx\n",
            regs->ARM_pc, regs->ARM_lr, regs->ARM_cpsr,
            regs->ARM_sp, regs->ARM_ip, regs->ARM_fp);
        printk(KERN_ERR"r10:  %08lx   r9 :  %08lx   r8 : %08lx\n",
                        regs->ARM_r10, regs->ARM_r9,
                        regs->ARM_r8);
        printk(KERN_ERR"r7 :  %08lx   r6 : %08lx    r5 : %08lx r4 : %08lx\n",
                        regs->ARM_r7, regs->ARM_r6,
                        regs->ARM_r5, regs->ARM_r4);
        printk(KERN_ERR"r3 :  %08lx   r2 : %08lx    r1 : %08lx r0 : %08lx\n",
                        regs->ARM_r3, regs->ARM_r2,
                        regs->ARM_r1, regs->ARM_r0);
    } else {
        printk(KERN_ERR"pc :[<%08lx>] lr :[<%08lx>] psr: %08lx\n"
                       "sp :  %08lx   fp :  %08lx\n",
            regs->ARM_pc, regs->ARM_lr, regs->ARM_cpsr,
            regs->ARM_sp, regs->ARM_fp);
    }
	flags = regs->ARM_cpsr;
	buf[0] = flags & PSR_N_BIT ? 'N' : 'n';
	buf[1] = flags & PSR_Z_BIT ? 'Z' : 'z';
	buf[2] = flags & PSR_C_BIT ? 'C' : 'c';
	buf[3] = flags & PSR_V_BIT ? 'V' : 'v';
	buf[4] = '\0';

	printk(KERN_ERR"Flags: %s  IRQs o%s  FIQs o%s  Mode %s  ISA %s Segment %s\n",
		buf, interrupts_enabled(regs) ? "n" : "ff",
		fast_interrupts_enabled(regs) ? "n" : "ff",
		processor_modes[processor_mode(regs)],
		isa_modes[isa_mode(regs)],
		pthread->addr_limit == get_ds() ? "kernel" : "user");
#ifdef CONFIG_CPU_CP15
		buf[0] = '\0';
#ifdef CONFIG_CPU_CP15_MMU
		snprintf(buf, sizeof(buf), "  Table: %08lx  DAC: %08lx", pbt->transbase, pbt->dac);
#endif
		printk("Control: %08lx%s\n", pbt->ctrl, buf);
#endif
    firq_local_irq_restore(flag);
}
/**--------------------------------------------------------------------------------**\
 * \param exeception_regs register der aktuellen CPU (kann auch NULL gesetzt werden)
\**--------------------------------------------------------------------------------**/
bool avm_trigger_all_cpu_backtrace(struct pt_regs *exception_regs) {
    struct _monitor_bt bt;
    unsigned int cpu;

    if(atomic_add_return(1, &backtrace_busy) > 1) {
        return 0;
    }
    for_each_online_cpu(cpu){
        unsigned long sp;
        struct thread_info *pthread = NULL;
        struct task_struct *curr_tsk = NULL;

        memset(&bt, 0, sizeof(bt));
        if(trigger_firq_monitor(cpu, monitor_bt, &bt, exception_regs)) {
            continue;
        }
        sp = bt.svc_sp;
        if(sp && (pthread = thread_info_by_sp(sp))) {
            curr_tsk = pthread->task;
        }
        /*--- printk(KERN_ERR"cpu=%u %s:cpsr=0x%lx pc=0x%lx lr=0x%lx sp=0x%lx\n", cpu, curr_tsk ? curr_tsk->comm : "?",bt.ptregs.ARM_cpsr, bt.ptregs.ARM_pc, bt.ptregs.ARM_lr, bt.ptregs.ARM_sp); ---*/
        if(!curr_tsk) {
            continue;
        }
        bt_show_regs(cpu, &bt, pthread);
        if(!user_mode(&bt.ptregs)) {
#ifdef CONFIG_ARM_UNWIND
            unwind_backtrace(&bt.ptregs, NULL);
#else
            show_stack(curr_tsk, (void *)bt.ptregs.ARM_sp);
#endif/*--- #ifdef CONFIG_ARM_UNWIND ---*/
            if(firq_is_linux_context()) {
                dump_instr(KERN_ERR, &bt.ptregs);
            }
	    dump_mem(KERN_EMERG, "Stack: ", bt.ptregs.ARM_sp, 
                     (bt.ptregs.ARM_sp & (~(THREAD_SIZE - 1))) + THREAD_SIZE);
	}
    }
    atomic_set(&backtrace_busy, 0);
    return 1;
}
/*--------------------------------------------------------------------------------*\
 * installiere auf jeder CPU ein monitor-ipi-linux-firq
\*--------------------------------------------------------------------------------*/
static int monitor_ipi_init(void) {
    char txt[64];
    int cpu;

    for_each_cpu(cpu, cpu_online_mask) {
        int monitor_irq = AVM_IRQ_MONITOR_START + cpu;
        snprintf(txt, sizeof(txt), "monitor/%u", cpu);
        spin_lock_init(&IPI_Monitor[cpu].lock);
        IPI_Monitor[cpu].init = 0;
        IPI_Monitor[cpu].irq  = monitor_irq;
        if(avm_request_fiq_on(cpu, monitor_irq, firq_monitor, 0, txt, &IPI_Monitor[cpu]) >= 0) {
            avm_gic_fiq_setup(monitor_irq, 1 << cpu, 0x00, 1, 0);
            IPI_Monitor[cpu].init = 1;
        } else {
            printk(KERN_ERR"%s: error on install irq=%u\n", __func__, monitor_irq);
        }
        /*--- printk(KERN_ERR"%s: cpu %d initialized\n", __func__, cpu); ---*/
    }
    atomic_set(&backtrace_busy, 0);
    return 0;
}
late_initcall(monitor_ipi_init);
#endif/*--- #if defined(CONFIG_AVM_FASTIRQ) ---*/
