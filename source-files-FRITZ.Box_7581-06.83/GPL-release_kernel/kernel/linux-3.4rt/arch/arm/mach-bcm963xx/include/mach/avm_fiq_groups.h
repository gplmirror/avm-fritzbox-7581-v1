/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2016 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/

#ifndef _AVM_FIQ_GROUPS_H_
#define _AVM_FIQ_GROUPS_H_

typedef enum ClientFiqPrio_e
{
   FIQ_PRIO_USER = 0x40,
   FIQ_PRIO_PROFILING = 0x20,
   FIQ_PRIO_WATCHDOG = 0x10,
   FIQ_PRIO_MONITOR = 0x00
} ClientFiqPrios_t;

#endif // #ifndef _AVM_FIQ_GROUPS_H_

