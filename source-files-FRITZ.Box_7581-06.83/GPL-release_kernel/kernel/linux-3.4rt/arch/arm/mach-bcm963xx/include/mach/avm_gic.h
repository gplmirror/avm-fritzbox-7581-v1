  /*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2016 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/

#ifndef _AVM_GIC_H_
#define _AVM_GIC_H_


#include <asm/hardware/gic.h>

#define OFFSET_ICDICTR GIC_DIST_CTR
#define OFFSET_ICDISER GIC_DIST_ENABLE_SET
#define OFFSET_ICDICER GIC_DIST_ENABLE_CLEAR
#define OFFSET_ICDISPR GIC_DIST_PENDING_SET
#define OFFSET_ICDICPR GIC_DIST_PENDING_CLEAR
#define OFFSET_ICDIPTR GIC_DIST_TARGET
#define OFFSET_ICDIPR  GIC_DIST_PRI
#define OFFSET_ICDICR  GIC_DIST_CONFIG
#define OFFSET_ICDISR  GIC_DIST_SEC
#define OFFSET_ICDICPR GIC_DIST_PENDING_CLEAR

#define OFFSET_ICCIAR  GIC_CPU_INTACK
#define OFFSET_ICCEOIR GIC_CPU_EOI
#define OFFSET_ICCHPIR GIC_CPU_HIGHPRI
#define OFFSET_ICCPMR  GIC_CPU_PRIMASK

#define AVM_IRQ_WD INTERRUPT_ID_WDTIMER
#define AVM_IRQ_MESSAGING_START 0xAC
#define AVM_IRQ_PROFILING_START (AVM_IRQ_MESSAGING_START + NR_CPUS)
#define AVM_IRQ_MONITOR_START   (AVM_IRQ_PROFILING_START + NR_CPUS)


#endif // #ifndef _AVM_GIC_H_
