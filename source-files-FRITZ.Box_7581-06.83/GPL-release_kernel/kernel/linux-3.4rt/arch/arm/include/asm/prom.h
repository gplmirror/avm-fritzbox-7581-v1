/*
 *  arch/arm/include/asm/prom.h
 *
 *  Copyright (C) 2009 Canonical Ltd. <jeremy.kerr@canonical.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */
#ifndef __ASMARM_PROM_H
#define __ASMARM_PROM_H

#ifdef CONFIG_OF

extern struct machine_desc *setup_machine_fdt(unsigned int dt_phys);
extern void arm_dt_memblock_reserve(void);

#else /* CONFIG_OF */

static inline struct machine_desc *setup_machine_fdt(unsigned int dt_phys __attribute__((unused)))
{
	return NULL;
}

#if 0
extern char *prom_getcmdline(void);
extern void setup_prom_printf(int tty_no);
extern void prom_init_cmdline(void);
extern void prom_meminit(void);
extern void prom_fixup_mem_map(unsigned long start_mem, unsigned long end_mem);
extern unsigned long prom_free_prom_memory (void);
extern void mips_display_message(const char *str);
extern void mips_display_word(unsigned int num);
extern int get_ethernet_addr(char *ethernet_addr);
#endif
#if defined(CONFIG_EARLY_PRINTK)
extern int prom_printf(const char *fmt, ...);
#else/*--- #if defined(CONFIG_EARLY_PRINTK) ---*/
#define prom_printf(arg...)
#endif/*--- #else ---*//*--- #if defined(CONFIG_EARLY_PRINTK) ---*/

extern char *prom_getenv(char *name);

/* Memory descriptor management. */
#define PROM_MAX_PMEMBLOCKS    32
struct prom_pmemblock {
        unsigned long base; /* Within KSEG0. */
        unsigned int size;  /* In bytes. */
        unsigned int type;  /* free or prom memory */
};

/*------------------------------------------------------------------------------------------*\
 * Header WLAN - DECT - Config
\*------------------------------------------------------------------------------------------*/
#define AVM_MAX_CONFIG_ENTRIES  8

enum wlan_dect_type {
    WLAN,           /*--- 0 ---*/
    DECT,           /*--- 1 ---*/
    WLAN2,          /*--- 2 ---*/
    ZERTIFIKATE,    /*--- 3 ---*/
    DOCSIS,         /*--- 4 ---*/
    DSL,            /*--- 5 ---*/
    PROLIFIC,       /*--- 6 ---*/
    WLAN_ZIP,       /*--- 7 ---*/
    WLAN2_ZIP,      /*--- 8 ---*/
    MAX_TYPE        /*--- 9 ---*/
};

struct __attribute__ ((packed)) wlan_dect_config {
    unsigned char           Version;        /*--- z.Z. 1 ---*/
    enum  wlan_dect_type    Type :8;        /*--- 0 - WLAN; 1 - DECT ---*/
    unsigned short          Len;            /*--- 384 - WLAN, 128 - DECT ---*/
};

extern void set_wlan_dect_config_address(unsigned int value);
extern int get_wlan_dect_config(enum wlan_dect_type Type, unsigned char *buffer, unsigned int len);
extern int prom_c55_get_base_memory(unsigned int *base, unsigned int *len);


static inline void arm_dt_memblock_reserve(void) { }

#endif /* CONFIG_OF */
#endif /* ASMARM_PROM_H */
