/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2006 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/
#include <linux/version.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <asm/fcntl.h>
#include <asm/ioctl.h>
#include <asm/errno.h>
#include <asm/uaccess.h>
#include <linux/wait.h>
#include <linux/poll.h>
#include <linux/timer.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/threads.h>  // für NR_CPUS
/*--- #include <asm/semaphore.h> ---*/


/*------------------------------------------------------------------------------------------*\
 *  2.6 Kernel H-Files
\*------------------------------------------------------------------------------------------*/
#if KERNEL_VERSION(2, 6, 0) < LINUX_VERSION_CODE 
#include <linux/cdev.h>
#endif 

/*------------------------------------------------------------------------------------------*\
 *  2.4 Kernel H-Files
\*------------------------------------------------------------------------------------------*/
#if KERNEL_VERSION(2, 6, 0) > LINUX_VERSION_CODE 
#include <linux/devfs_fs_kernel.h>
#include <asm-mips/avalanche/sangam/sangam.h>
#include <asm-mips/avalanche/sangam/sangam_clk_cntl.h>
#include <asm/avalanche/generic/led_config.h>
#include <asm/avalanche/generic/avalanche_misc.h>
#include <asm/smplock.h>
#endif 

#include <linux/ar7wdt.h>
#include <linux/avm_event.h>
#include "avm_event_intern.h"

#if defined(CONFIG_MIPS)
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,28)
#include <asm/mips-boards/prom.h>
#else/*--- #if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,28) ---*/
#include <asm/prom.h>
#endif/*--- #else ---*//*--- #if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,28) ---*/
#else
#include <asm/setup.h>
#endif /*--- #if defined(CONFIG_MIPS) ---*/
#include "avm_sammel.h"
#include "avm_event.h"
#include "avm_debug.h"
#include "ar7wdt.h"

#define MODULE_NAME     "avm"
MODULE_DESCRIPTION("AR7 Watchdog Timer + AVM Central Event distribution");
MODULE_LICENSE("GPL");


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int ar7wdt_no_reboot = 0;
module_param(ar7wdt_no_reboot, int,  0644);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
extern void avm_checkpoint_init(void);
int __init avm_sammel_init(void) {
    int ret __maybe_unused;
    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
    printk("[avm] configured: "
#ifdef CONFIG_AVM_WATCHDOG
            "watchdog "
#endif
#ifdef CONFIG_AVM_WATCHDOG_MODULE
            "watchdog (module) "
#endif
#ifdef CONFIG_AVM_EVENT
            "event "
#endif
#ifdef CONFIG_AVM_DEBUG
            "debug "
#endif
#ifdef CONFIG_AVM_EVENT_MODULE
            "event (module) "
#endif
          );
    printk("\n");
    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/

#if defined(CONFIG_AVM_WATCHDOG) || defined(CONFIG_AVM_WATCHDOG_MODULE)
    ret = ar7wdt_init();
    if(ret) {
        printk("[avm]: ar7wdt_init: failed\n");
        return ret;
    }
#endif /*--- #if (CONFIG_AVM_WATCHDOG == 1) || (CONFIG_AVM_WATCHDOG_MODULE == 1) ---*/
#if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE)
    ret = avm_event_init();
    if(ret) {
        printk("[avm]: avm_event_init: failed\n");
        return ret;
    }
#endif /*--- #if (CONFIG_AVM_EVENT == 1) || (CONFIG_AVM_EVENT_MODULE == 1) ---*/
#if defined(CONFIG_AVM_DEBUG) || defined(CONFIG_AVM_DEBUG_MODULE)
    ret = avm_debug_init();
    if(ret) {
        printk("[avm]: avm_event_init: failed\n");
        return ret;
    }
#endif /*--- #if (CONFIG_AVM_DEBUG == 1) || (CONFIG_AVM_DEBUG_MODULE == 1) ---*/
#if defined(CONFIG_AVM_EVENTNODE_PUMA6)
    avm_checkpoint_init();
#endif /*--- defined(CONFIG_AVM_EVENTNODE_PUMA6) ---*/
    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
module_init(avm_sammel_init);

#if defined(CONFIG_ARCH_IPQ806X_DT) || defined(CONFIG_ARCH_IPQ40XX)
// Dakota: Funktioniert nicht - cycle counter werden irgendwo wieder ausgeschaltet?
/*--------------------------------------------------------------------------------*\
 * actually used for get_cycles()
\*--------------------------------------------------------------------------------*/
static void initialize_perfcounter(void *dummy __attribute__((unused))) {
	unsigned int val;
	/*---  initialize c9, Performance Monitor Control Register ---*/
	asm volatile("mrc p15, 0, %0, c9, c12, 0" : "=r"(val)); /* PMNC-read */
	val |= 1;	   /* Enable all counters */
	val |= (1 << 3);   /* counts every 64th processor clock cycle */
	val &= ~(1 << 5);  /* count is enabled in regions where non-invasive debug is prohibited */
	isb();
	asm volatile("mcr p15, 0, %0, c9, c12, 0" : : "r"(val)); /* PMNC-write */

	/* Count Enable Set Register */
	/*--- asm volatile("mrc p15, 0, %0, c9, c12, 1" : "=r"(val)); ---*/
	val = 1 << 31;	   /* Enable cycle counter */
	asm volatile("mcr p15, 0, %0, c9, c12, 1" : : "r"(val)); /* write */
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int __init avm_cycle_count_enable(void) {
	on_each_cpu(initialize_perfcounter, NULL, 0);
    return 0;
}
arch_initcall(avm_cycle_count_enable);
#endif/*--- #if defined(CONFIG_ARCH_IPQ806X_DT) ---*/

/*--------------------------------------------------------------------------------*\
 * neccessary: early install the '/proc/avm' directory 
\*--------------------------------------------------------------------------------*/
struct proc_dir_entry *proc_avm __attribute__((weak));

static int __init early_avm_proc_init(void)
{
	if (!proc_avm)
		proc_avm = proc_mkdir("avm", NULL);
    return 0;
}
fs_initcall(early_avm_proc_init);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(AVM_SAMMEL_MODULE)
int avm_sammel_deinit(void) {
#if (CONFIG_AVM_WATCHDOG_MODULE == 1)
    ar7wdt_cleanup();
#endif /*--- #if (CONFIG_AVM_WATCHDOG_MODULE == 1) ---*/
#if (CONFIG_AVM_EVENT_MODULE == 1)
    avm_event_cleanup();
#endif /*--- #if (CONFIG_AVM_EVENT_MODULE == 1) ---*/
#if (CONFIG_AVM_EVENT_MODULE == 1)
    avm_event_cleanup();
#endif /*--- #if (CONFIG_AVM_EVENT_MODULE == 1) ---*/
#if (CONFIG_AVM_DEBUG_MODULE == 1)
    avm_debug_cleanup();
#endif /*--- #if (CONFIG_AVM_DEBUG_MODULE == 1) ---*/
    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
module_exit(avm_sammel_deinit);
#endif /*--- #if defined(AVM_SAMMEL_MODULE) ---*/
