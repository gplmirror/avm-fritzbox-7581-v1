#ifndef __arch_avm_reboot_status_brcma_h__
#define __arch_avm_reboot_status_brcma_h__

#define UPDATE_REBOOT_STATUS_TEXT       "(c) AVM 2015, Reboot Status is: Firmware-Update" \
                                        "(c) AVM 2015, Reboot Status is: Firmware-Update" \
                                        "(c) AVM 2015, Reboot Status is: Firmware-Update"
#define NMI_REBOOT_STATUS_TEXT               "(c) AVM 2015, Reboot Status is: NMI-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: NMI-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: NMI-Watchdog"
#define SOFTWATCHDOG_REBOOT_STATUS_TEXT "(c) AVM 2015, Reboot Status is: Software-Watchdog" \
                                        "(c) AVM 2015, Reboot Status is: Software-Watchdog" \
                                        "(c) AVM 2015, Reboot Status is: Software-Watchdog"
#define POWERON_REBOOT_STATUS_TEXT      "(c) AVM 2015, Reboot Status is: Power-On-Reboot" \
                                        "(c) AVM 2015, Reboot Status is: Power-On-Reboot" \
                                        "(c) AVM 2015, Reboot Status is: Power-On-Reboot"
#define TEMP_REBOOT_STATUS_TEXT         "(c) AVM 2015, Reboot Status is: Temperature-Reboot" \
                                        "(c) AVM 2015, Reboot Status is: Temperature-Reboot" \
                                        "(c) AVM 2015, Reboot Status is: Temperature-Reboot"
#define SOFT_REBOOT_STATUS_TEXT_PANIC   "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2015, Reboot Status is: Software-Reboot\0(PANIC)"
#define SOFT_REBOOT_STATUS_TEXT_OOM     "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2015, Reboot Status is: Software-Reboot\0(OOM)"
#define SOFT_REBOOT_STATUS_TEXT_OOPS    "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2015, Reboot Status is: Software-Reboot\0(OOPS)"
/*--- Achtung! Untermenge von obigen Eintraegen: ---*/
#define SOFT_REBOOT_STATUS_TEXT         "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                        "(c) AVM 2015, Reboot Status is: Software-Reboot"

#include <linux/avm_kernel_config.h>

extern void *avm_boot_string_virt_addr;
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static char *arch_get_mailbox(void){
    return (char *)avm_boot_string_virt_addr;
}
#if defined(CONFIG_AVM_FASTIRQ)
static int fastirq_dump_once = 0;
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static int arch_brcma_die_notifier(struct notifier_block *self __maybe_unused, unsigned long cmd, void *ptr) {
    struct die_args *args = (struct die_args *)ptr;
    struct pt_regs *regs = args->regs;
    /*--- struct thread_info *thread = (struct thread_info *)((unsigned int)regs & (unsigned int)(~(THREAD_SIZE - 1))); ---*/
    /*--- struct task_struct *tsk = thread->task; ---*/
    if(cmd == DIE_OOPS) {
        fastirq_dump_once = 1;
        avm_fiq_dump_stat();
        printk(KERN_ERR"\nBacktrace of all CPU's:");
        avm_trigger_all_cpu_backtrace(regs);
        printk(KERN_ERR"\nBacktrace of all CPU's done\n\n");
        panic("Fatal exception");
    }
    return NOTIFY_OK;
}
#define arch_die_notifier arch_brcma_die_notifier

/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static int arch_brcma_panic_notifier(struct notifier_block *notifier_block __maybe_unused, unsigned long event __maybe_unused, void *cause_string __maybe_unused) {
    if(fastirq_dump_once == 0) {
        avm_fiq_dump_stat();
    }
    return NOTIFY_OK;
}
#define arch_panic_notifier arch_brcma_panic_notifier
#endif/*--- #if defined(CONFIG_AVM_FASTIRQ) ---*/

#endif /*--- #ifndef __arch_avm_reboot_status_brcma_h__ ---*/
