#ifndef __arch_avm_reboot_status_ipq40xx_h__
#define __arch_avm_reboot_status_ipq40xx_h__

#include <linux/of_reserved_mem.h>
#include <linux/avm_hw_config.h>
#include <linux/avm_kernel_config.h>
#include <asm/mach_avm.h>
#include <asm/cacheflush.h>

#define UPDATE_REBOOT_STATUS_TEXT            "(c) AVM 2015, Reboot Status is: Firmware-Update" \
                                             "(c) AVM 2015, Reboot Status is: Firmware-Update" \
                                             "(c) AVM 2015, Reboot Status is: Firmware-Update"
#define NMI_REBOOT_STATUS_TEXT               "(c) AVM 2015, Reboot Status is: NMI-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: NMI-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: NMI-Watchdog"
#define SOFTWATCHDOG_REBOOT_STATUS_TEXT      "(c) AVM 2015, Reboot Status is: Software-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: Software-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: Software-Watchdog"
#define POWERON_REBOOT_STATUS_TEXT           "(c) AVM 2015, Reboot Status is: Power-On-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Power-On-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Power-On-Reboot"
#define TEMP_REBOOT_STATUS_TEXT              "(c) AVM 2015, Reboot Status is: Temperature-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Temperature-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Temperature-Reboot"
#define SOFT_REBOOT_STATUS_TEXT_PANIC        "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot\0(PANIC)"
#define SOFT_REBOOT_STATUS_TEXT_OOM          "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot\0(OOM)"
#define SOFT_REBOOT_STATUS_TEXT_OOPS         "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot\0(OOPS)"
/*--- Achtung! Untermenge von obigen Eintraegen: ---*/
#define SOFT_REBOOT_STATUS_TEXT              "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot"


#if defined(CONFIG_OF_AVM_DT)
#include <linux/of_fdt.h>
#endif /* defined(CONFIG_OF_AVM_DT) */

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static char *arch_get_mailbox(void){
    struct reserved_mem *rmem;
    static char *mailbox = 0;
    if(mailbox)
        return mailbox;

#if defined(CONFIG_OF_AVM_DT)
    //try to find the reserved entry for 'avm_reboot_string'
    rmem = fdt_get_reserved_mem_resource("avm_reboot_string");
    if (rmem) {
        mailbox = (char *) phys_to_virt(rmem->base);        
        /*--- mailbox = rmem->base; ---*/
        /*--- pr_info("Found avm_reboot_string, setting mailbox to 0x%08x \n",(uint32_t) mailbox); ---*/
        return mailbox;
    }
#endif /* defined(CONFIG_OF_AVM_DT) */
    mailbox = (char *)  AVM_REBOOT_STRING_LOCATION;
    pr_err("No reserved memory for avm_reboot_string found. Setting mailbox to fallback 0x%08x \n",(uint32_t)  mailbox);
    return mailbox;
}
#if defined(CONFIG_AVM_FASTIRQ)
static int fastirq_dump_once = 0;
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static int arch_dakota_die_notifier(struct notifier_block *self __maybe_unused, unsigned long cmd, void *ptr) {
    struct die_args *args = (struct die_args *)ptr;
    struct pt_regs *regs = args->regs;
    if(cmd == DIE_OOPS) {
        fastirq_dump_once = 1;
        avm_fiq_dump_stat();
        printk(KERN_ERR"\nBacktrace of all CPU's:");
        avm_trigger_all_cpu_backtrace(regs);
        printk(KERN_ERR"\nBacktrace of all CPU's done\n\n");
    }
    return NOTIFY_OK;
}
#define arch_die_notifier arch_dakota_die_notifier
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static int arch_dakota_panic_notifier(struct notifier_block *notifier_block __maybe_unused, 
                                      unsigned long event __maybe_unused, void *cause_string __maybe_unused) {
    if(fastirq_dump_once == 0) {
        avm_fiq_dump_stat();
    }
    return NOTIFY_OK;
}
#define arch_panic_notifier arch_dakota_panic_notifier

#endif/*--- #if defined(CONFIG_AVM_FASTIRQ) ---*/

#endif /*--- #ifndef __arch_avm_reboot_status_ipq40xx_h__ ---*/
