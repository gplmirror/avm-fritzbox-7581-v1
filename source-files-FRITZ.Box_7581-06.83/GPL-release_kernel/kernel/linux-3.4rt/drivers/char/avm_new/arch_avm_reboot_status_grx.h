#ifndef __arch_avm_reboot_status_grx_h__
#define __arch_avm_reboot_status_grx_h__

#define UPDATE_REBOOT_STATUS_TEXT            "(c) AVM 2015, Reboot Status is: Firmware-Update" \
                                             "(c) AVM 2015, Reboot Status is: Firmware-Update" \
                                             "(c) AVM 2015, Reboot Status is: Firmware-Update"
#define NMI_REBOOT_STATUS_TEXT               "(c) AVM 2015, Reboot Status is: NMI-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: NMI-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: NMI-Watchdog"
#define SOFTWATCHDOG_REBOOT_STATUS_TEXT      "(c) AVM 2015, Reboot Status is: Software-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: Software-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: Software-Watchdog"
#define POWERON_REBOOT_STATUS_TEXT           "(c) AVM 2015, Reboot Status is: Power-On-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Power-On-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Power-On-Reboot"
#define TEMP_REBOOT_STATUS_TEXT              "(c) AVM 2015, Reboot Status is: Temperature-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Temperature-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Temperature-Reboot"
#define SOFT_REBOOT_STATUS_TEXT_PANIC        "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot\0(PANIC)"
#define SOFT_REBOOT_STATUS_TEXT_OOM          "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot\0(OOM)"
#define SOFT_REBOOT_STATUS_TEXT_OOPS         "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot\0(OOPS)"
/*--- Achtung! Untermenge von obigen Eintraegen: ---*/
#define SOFT_REBOOT_STATUS_TEXT              "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot"

#include <linux/avm_kernel_config.h>
#include <asm/reboot.h>

#if defined(CONFIG_OF_AVM_DT)
#include <linux/of_fdt.h>
#endif /* defined(CONFIG_OF_AVM_DT) */

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static char *arch_get_mailbox(void){
    static char *mailbox = 0;
    unsigned int k;
    if(mailbox) {
        return mailbox;
    }
#if defined(CONFIG_OF_AVM_DT)
    //try to find the reserved entry for 'avm_reboot_string'
    for(k = 0; k < reserved_resources_used; k++){
        if(!strcmp(reserved_resources[k].name, "avm_reboot_string")){
            mailbox = (char *) KSEG1ADDR(reserved_resources[k].start);        
            /*--- pr_info("Found avm_reboot_string, setting mailbox to 0x%08x \n",(uint32_t) mailbox); ---*/
            return mailbox;
        }
    }
#endif /* defined(CONFIG_OF_AVM_DT) */
    mailbox = (char *)  AVM_REBOOT_STRING_LOCATION;
    pr_err("Didn't found avm_reboot_string, setting mailbox to fallback 0x%08x \n",(uint32_t)  mailbox);
    return mailbox;
}

/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static int arch_grx_die_notifier(struct notifier_block *self __maybe_unused, unsigned long cmd __maybe_unused, 
                                 void *ptr __maybe_unused) {
    if(cmd == DIE_OOPS) {
        struct die_args *args = (struct die_args *)ptr;
        struct pt_regs *regs = args->regs;
        static int die_counter;

        oops_enter();

        console_verbose();
        bust_spinlocks(1);

        printk("%s[#%d]:\n", args->str, ++die_counter);
        show_registers(regs);
        add_taint(TAINT_DIE, LOCKDEP_NOW_UNRELIABLE);
        trigger_all_cpu_backtrace();

        oops_exit();
        panic("Fatal exception %s", in_interrupt() ? "in interrupt" : "");
    }
    return NOTIFY_OK;
}
#define arch_die_notifier arch_grx_die_notifier
#endif /*--- #ifndef __arch_avm_reboot_status_grx_h__ ---*/
