#ifndef _AVM_DIST_EVENT_ENDIAN_H_
#define _AVM_DIST_EVENT_ENDIAN_H_

#include <linux/swab.h>
#include "avm_event_gen_endian_external_defines.h"

#define __bswap_16(x) __swab16(x)
#define __bswap_32(x) __swab32(x)
#define __bswap_64(x) __swab64(x)

/*------------------------------------------------------------------------------------------*\
 * Defines
\*------------------------------------------------------------------------------------------*/
#if defined(__LITTLE_ENDIAN)
#define MACHINE_IS_LITTLE_ENDIAN
#else /*--- #if __BYTE_ORDER == __LITTLE_ENDIAN ---*/
#endif /*--- #else ---*/ /*--- #if __BYTE_ORDER == __LITTLE_ENDIAN ---*/

#define __unused__      __attribute__ ((unused))

int convert_fromBigEndian_toMachine(unsigned int global_length, struct _endian_convert *C, unsigned char *in, unsigned char *out, unsigned int verison_from);
int convert_fromLittleEndian_toMachine(unsigned int global_length, struct _endian_convert *C, unsigned char *in, unsigned char *out, unsigned int verison_from);
int convert_fromMachine_toBigEndian(unsigned int global_length, struct _endian_convert *C, unsigned char *in, unsigned char *out, unsigned int version_to);
int convert_fromMachine_toLittleEndian(unsigned int global_length, struct _endian_convert *C, unsigned char *in, unsigned char *out, unsigned int version_to);
int convert_fromMachine_toMachine(unsigned int bytes_to_convert, struct _endian_convert *C, unsigned char *in, unsigned char *out, unsigned int version_from);

#endif /*--- #ifndef _AVM_DIST_EVENT_ENDIAN_H_ ---*/


