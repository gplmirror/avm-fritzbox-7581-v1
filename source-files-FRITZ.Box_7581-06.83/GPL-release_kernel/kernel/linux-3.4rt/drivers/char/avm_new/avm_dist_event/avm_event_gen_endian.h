#include "avm_event_gen_endian_external_defines.h"

#include "avm_event_gen_enum_range.h"


/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_mass_storage_mount'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_mass_storage_mount[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_mass_storage_mount",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_mass_storage_mount __attribute__ ((packed)), header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_mass_storage_mount",
		.type_name   =  "unsigned long long",
		.flags = 0,
		.name = "size",
		.size = sizeof(unsigned long long),
		.offset = __builtin_offsetof(struct _avm_event_mass_storage_mount __attribute__ ((packed)), size),
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_mass_storage_mount",
		.type_name   =  "unsigned long long",
		.flags = 0,
		.name = "free",
		.size = sizeof(unsigned long long),
		.offset = __builtin_offsetof(struct _avm_event_mass_storage_mount __attribute__ ((packed)), free),
	},
	[3] = {
		.struct_name =  "convert_message_struct__avm_event_mass_storage_mount",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | 0,
		.name = "name_length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_mass_storage_mount __attribute__ ((packed)), name_length),
	},
	[4] = {
		.struct_name =  "convert_message_struct__avm_event_mass_storage_mount",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_ARRAY_USE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "name",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_mass_storage_mount __attribute__ ((packed)), name),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_remotewatchdog'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_remotewatchdog[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_remotewatchdog",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "event_header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_remotewatchdog __attribute__ ((packed)), event_header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_remotewatchdog",
		.type_name   =  "enum _avm_remote_wdt_cmd",
		.flags = 0,
		.name = "cmd",
		.size = sizeof(enum _avm_remote_wdt_cmd),
		.offset = __builtin_offsetof(struct _avm_event_remotewatchdog __attribute__ ((packed)), cmd),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_remote_wdt_cmd_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_remote_wdt_cmd,
		.enumName = "enum_table__avm_remote_wdt_cmd"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_remotewatchdog",
		.type_name   =  "char",
		.flags = 0,
		.name = "name",
		.size = sizeof(char) * 16,
		.offset = __builtin_offsetof(struct _avm_event_remotewatchdog __attribute__ ((packed)), name),
	},
	[3] = {
		.struct_name =  "convert_message_struct__avm_event_remotewatchdog",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "param",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_remotewatchdog __attribute__ ((packed)), param),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_mass_storage_unmount'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_mass_storage_unmount[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_mass_storage_unmount",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_mass_storage_unmount __attribute__ ((packed)), header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_mass_storage_unmount",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | 0,
		.name = "name_length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_mass_storage_unmount __attribute__ ((packed)), name_length),
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_mass_storage_unmount",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_ARRAY_USE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "name",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_mass_storage_unmount __attribute__ ((packed)), name),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_remotewatchdog'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_remotewatchdog[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_remotewatchdog",
		.type_name   =  "enum _avm_remote_wdt_cmd",
		.flags = 0,
		.name = "cmd",
		.size = sizeof(enum _avm_remote_wdt_cmd),
		.offset = __builtin_offsetof(struct avm_event_remotewatchdog __attribute__ ((packed)), cmd),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_remote_wdt_cmd_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_remote_wdt_cmd,
		.enumName = "enum_table__avm_remote_wdt_cmd"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_remotewatchdog",
		.type_name   =  "char",
		.flags = 0,
		.name = "name",
		.size = sizeof(char) * 16,
		.offset = __builtin_offsetof(struct avm_event_remotewatchdog __attribute__ ((packed)), name),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_remotewatchdog",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "param",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_remotewatchdog __attribute__ ((packed)), param),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_internet_new_ip'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_internet_new_ip[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_internet_new_ip",
		.type_name   =  "enum avm_event_internet_new_ip_param_sel",
		.flags = ENDIAN_CONVERT_SELECT | 0,
		.name = "sel",
		.size = sizeof(enum avm_event_internet_new_ip_param_sel),
		.offset = __builtin_offsetof(struct avm_event_internet_new_ip __attribute__ ((packed)), sel),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_avm_event_internet_new_ip_param_sel_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_avm_event_internet_new_ip_param_sel,
		.enumName = "enum_table_avm_event_internet_new_ip_param_sel"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_internet_new_ip",
		.type_name   =  "union avm_event_internet_new_ip_param",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_SELECT_ENTRY | ENDIAN_CONVERT_IGNORE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "params",
		.substruct = convert_message_union_avm_event_internet_new_ip_param,
		.offset = __builtin_offsetof(struct avm_event_internet_new_ip __attribute__ ((packed)), params),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_cmd'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_cmd[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_cmd",
		.type_name   =  "enum __avm_event_cmd",
		.flags = 0,
		.name = "cmd",
		.size = sizeof(enum __avm_event_cmd),
		.offset = __builtin_offsetof(struct _avm_event_cmd __attribute__ ((packed)), cmd),
		.enum_debug_function = (char *(*)(unsigned int))get_enum___avm_event_cmd_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table___avm_event_cmd,
		.enumName = "enum_table___avm_event_cmd"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_cmd",
		.type_name   =  "union _avm_event_cmd_param",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_IGNORE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "param",
		.substruct = convert_message_union__avm_event_cmd_param,
		.offset = __builtin_offsetof(struct _avm_event_cmd __attribute__ ((packed)), param),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct wlan_event_def'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_wlan_event_def[] = {
	[0] = {
		.struct_name =  "convert_message_struct_wlan_event_def",
		.type_name   =  "enum wlan_event_id",
		.flags = ENDIAN_CONVERT_SELECT | 0,
		.name = "event_id",
		.size = sizeof(enum wlan_event_id),
		.offset = __builtin_offsetof(struct wlan_event_def __attribute__ ((packed)), event_id),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_wlan_event_id_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_wlan_event_id,
		.enumName = "enum_table_wlan_event_id"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_wlan_event_def",
		.type_name   =  "union wlan_event_data",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_SELECT_ENTRY | ENDIAN_CONVERT_IGNORE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "event_data",
		.substruct = convert_message_union_wlan_event_data,
		.offset = __builtin_offsetof(struct wlan_event_def __attribute__ ((packed)), event_data),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_checkpoint'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_checkpoint[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_checkpoint",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "event_header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_checkpoint __attribute__ ((packed)), event_header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_checkpoint",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "node_id",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct _avm_event_checkpoint __attribute__ ((packed)), node_id),
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_checkpoint",
		.type_name   =  "uint64_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "checkpoints",
		.size = sizeof(uint64_t),
		.offset = __builtin_offsetof(struct _avm_event_checkpoint __attribute__ ((packed)), checkpoints),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_telephony_missed_call_params'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_telephony_missed_call_params[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_telephony_missed_call_params",
		.type_name   =  "enum avm_event_telephony_param_sel",
		.flags = ENDIAN_CONVERT_SELECT | 0,
		.name = "id",
		.size = sizeof(enum avm_event_telephony_param_sel),
		.offset = __builtin_offsetof(struct _avm_event_telephony_missed_call_params __attribute__ ((packed)), id),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_avm_event_telephony_param_sel_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_avm_event_telephony_param_sel,
		.enumName = "enum_table_avm_event_telephony_param_sel"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_telephony_missed_call_params",
		.type_name   =  "union avm_event_telephony_call_params",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_SELECT_ENTRY | ENDIAN_CONVERT_IGNORE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "params",
		.substruct = convert_message_union_avm_event_telephony_call_params,
		.offset = __builtin_offsetof(struct _avm_event_telephony_missed_call_params __attribute__ ((packed)), params),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_pm_info_stat'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_pm_info_stat[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "reserved1",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), reserved1),
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_sumact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_sumact),
	},
	[3] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_sumcum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_sumcum),
	},
	[4] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_systemact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_systemact),
	},
	[5] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_systemcum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_systemcum),
	},
	[6] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "system_status",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), system_status),
	},
	[7] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_dspact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_dspact),
	},
	[8] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_dspcum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_dspcum),
	},
	[9] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_wlanact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_wlanact),
	},
	[10] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_wlancum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_wlancum),
	},
	[11] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "wlan_devices",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), wlan_devices),
	},
	[12] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "wlan_status",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), wlan_status),
	},
	[13] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_ethact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_ethact),
	},
	[14] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_ethcum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_ethcum),
	},
	[15] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned short",
		.flags = 0,
		.name = "eth_status",
		.size = sizeof(unsigned short),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), eth_status),
	},
	[16] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_abact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_abact),
	},
	[17] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_abcum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_abcum),
	},
	[18] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned short",
		.flags = 0,
		.name = "isdn_status",
		.size = sizeof(unsigned short),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), isdn_status),
	},
	[19] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_dectact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_dectact),
	},
	[20] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_dectcum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_dectcum),
	},
	[21] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_battchargeact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_battchargeact),
	},
	[22] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_battchargecum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_battchargecum),
	},
	[23] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "dect_status",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), dect_status),
	},
	[24] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_usbhostact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_usbhostact),
	},
	[25] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_usbhostcum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_usbhostcum),
	},
	[26] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "usb_status",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), usb_status),
	},
	[27] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "signed char",
		.flags = 0,
		.name = "act_temperature",
		.size = sizeof(signed char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), act_temperature),
	},
	[28] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "signed char",
		.flags = 0,
		.name = "min_temperature",
		.size = sizeof(signed char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), min_temperature),
	},
	[29] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "signed char",
		.flags = 0,
		.name = "max_temperature",
		.size = sizeof(signed char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), max_temperature),
	},
	[30] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "signed char",
		.flags = 0,
		.name = "avg_temperature",
		.size = sizeof(signed char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), avg_temperature),
	},
	[31] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_lteact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_lteact),
	},
	[32] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_ltecum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_ltecum),
	},
	[33] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_dvbcact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_dvbcact),
	},
	[34] = {
		.struct_name =  "convert_message_struct__avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "rate_dvbccum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_pm_info_stat __attribute__ ((packed)), rate_dvbccum),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_cpu_run'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_cpu_run[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_cpu_run",
		.type_name   =  "enum _cputype",
		.flags = 0,
		.name = "cputype",
		.size = sizeof(enum _cputype),
		.offset = __builtin_offsetof(struct avm_event_cpu_run __attribute__ ((packed)), cputype),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__cputype_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__cputype,
		.enumName = "enum_table__cputype"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_cpu_run",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "cpu_run",
		.size = sizeof(unsigned char) * 4,
		.offset = __builtin_offsetof(struct avm_event_cpu_run __attribute__ ((packed)), cpu_run),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_led_info'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_led_info[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_led_info",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "mode",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_led_info __attribute__ ((packed)), mode),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_led_info",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "param1",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_led_info __attribute__ ((packed)), param1),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_led_info",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "param2",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_led_info __attribute__ ((packed)), param2),
	},
	[3] = {
		.struct_name =  "convert_message_struct_avm_event_led_info",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "gpio_driver_type",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_led_info __attribute__ ((packed)), gpio_driver_type),
	},
	[4] = {
		.struct_name =  "convert_message_struct_avm_event_led_info",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "gpio",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_led_info __attribute__ ((packed)), gpio),
	},
	[5] = {
		.struct_name =  "convert_message_struct_avm_event_led_info",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "pos",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_led_info __attribute__ ((packed)), pos),
	},
	[6] = {
		.struct_name =  "convert_message_struct_avm_event_led_info",
		.type_name   =  "char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "name",
		.size = sizeof(char) * 32,
		.offset = __builtin_offsetof(struct avm_event_led_info __attribute__ ((packed)), name),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_header'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_header[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_header",
		.type_name   =  "enum _avm_event_id",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "id",
		.size = sizeof(enum _avm_event_id),
		.offset = __builtin_offsetof(struct _avm_event_header __attribute__ ((packed)), id),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_event_id_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_event_id,
		.enumName = "enum_table__avm_event_id"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_telephony_missed_call'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_telephony_missed_call[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_telephony_missed_call",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | 0,
		.name = "length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_telephony_missed_call __attribute__ ((packed)), length),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_telephony_missed_call",
		.type_name   =  "struct _avm_event_telephony_missed_call_params",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "p",
		.substruct = convert_message_struct__avm_event_telephony_missed_call_params,
		.offset = __builtin_offsetof(struct avm_event_telephony_missed_call __attribute__ ((packed)), p),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_tffs_init'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_tffs_init[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_init",
		.type_name   =  "int64_t",
		.flags = 0,
		.name = "mem_offset",
		.size = sizeof(int64_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_init __attribute__ ((packed)), mem_offset),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_init",
		.type_name   =  "uint32_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "max_seg_size",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_init __attribute__ ((packed)), max_seg_size),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_ping'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_ping[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_ping",
		.type_name   =  "uint32_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "seq",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_ping __attribute__ ((packed)), seq),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_mass_storage_mount'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_mass_storage_mount[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_mass_storage_mount",
		.type_name   =  "unsigned long long",
		.flags = 0,
		.name = "size",
		.size = sizeof(unsigned long long),
		.offset = __builtin_offsetof(struct avm_event_mass_storage_mount __attribute__ ((packed)), size),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_mass_storage_mount",
		.type_name   =  "unsigned long long",
		.flags = 0,
		.name = "free",
		.size = sizeof(unsigned long long),
		.offset = __builtin_offsetof(struct avm_event_mass_storage_mount __attribute__ ((packed)), free),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_mass_storage_mount",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | 0,
		.name = "name_length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_mass_storage_mount __attribute__ ((packed)), name_length),
	},
	[3] = {
		.struct_name =  "convert_message_struct_avm_event_mass_storage_mount",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_ARRAY_USE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "name",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_mass_storage_mount __attribute__ ((packed)), name),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_message'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_message[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_message",
		.type_name   =  "uint32_t",
		.flags = ENDIAN_CONVERT_TOTAL_LENGTH | 0,
		.name = "length",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_message __attribute__ ((packed)), length),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_message",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "magic",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_message __attribute__ ((packed)), magic),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_message",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "nonce",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_message __attribute__ ((packed)), nonce),
	},
	[3] = {
		.struct_name =  "convert_message_struct_avm_event_message",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "flags",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_message __attribute__ ((packed)), flags),
	},
	[4] = {
		.struct_name =  "convert_message_struct_avm_event_message",
		.type_name   =  "int32_t",
		.flags = 0,
		.name = "result",
		.size = sizeof(int32_t),
		.offset = __builtin_offsetof(struct avm_event_message __attribute__ ((packed)), result),
	},
	[5] = {
		.struct_name =  "convert_message_struct_avm_event_message",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "transmitter_handle",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_message __attribute__ ((packed)), transmitter_handle),
	},
	[6] = {
		.struct_name =  "convert_message_struct_avm_event_message",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "receiver_handle",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_message __attribute__ ((packed)), receiver_handle),
	},
	[7] = {
		.struct_name =  "convert_message_struct_avm_event_message",
		.type_name   =  "enum avm_event_msg_type",
		.flags = ENDIAN_CONVERT_SELECT | 0,
		.name = "type",
		.size = sizeof(enum avm_event_msg_type),
		.offset = __builtin_offsetof(struct avm_event_message __attribute__ ((packed)), type),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_avm_event_msg_type_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_avm_event_msg_type,
		.enumName = "enum_table_avm_event_msg_type"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[8] = {
		.struct_name =  "convert_message_struct_avm_event_message",
		.type_name   =  "union avm_event_message_union",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_SELECT_ENTRY | ENDIAN_CONVERT_IGNORE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "message",
		.substruct = convert_message_union_avm_event_message_union,
		.offset = __builtin_offsetof(struct avm_event_message __attribute__ ((packed)), message),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_wlan'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_wlan[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_wlan",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_wlan __attribute__ ((packed)), header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_wlan",
		.type_name   =  "char",
		.flags = 0,
		.name = "mac",
		.size = sizeof(char) * 6,
		.offset = __builtin_offsetof(struct _avm_event_wlan __attribute__ ((packed)), mac),
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_wlan",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "u1",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_wlan __attribute__ ((packed)), u1),
	},
	[3] = {
		.struct_name =  "convert_message_struct__avm_event_wlan",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "event",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_wlan __attribute__ ((packed)), event),
	},
	[4] = {
		.struct_name =  "convert_message_struct__avm_event_wlan",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "info",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_wlan __attribute__ ((packed)), info),
	},
	[5] = {
		.struct_name =  "convert_message_struct__avm_event_wlan",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "status",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_wlan __attribute__ ((packed)), status),
	},
	[6] = {
		.struct_name =  "convert_message_struct__avm_event_wlan",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "u2",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_wlan __attribute__ ((packed)), u2),
	},
	[7] = {
		.struct_name =  "convert_message_struct__avm_event_wlan",
		.type_name   =  "char",
		.flags = 0,
		.name = "if_name",
		.size = sizeof(char) * IFNAMSIZ,
		.offset = __builtin_offsetof(struct _avm_event_wlan __attribute__ ((packed)), if_name),
	},
	[8] = {
		.struct_name =  "convert_message_struct__avm_event_wlan",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "ev_initiator",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_wlan __attribute__ ((packed)), ev_initiator),
	},
	[9] = {
		.struct_name =  "convert_message_struct__avm_event_wlan",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "ev_reason",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_wlan __attribute__ ((packed)), ev_reason),
	},
	[10] = {
		.struct_name =  "convert_message_struct__avm_event_wlan",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "avm_capabilities",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_wlan __attribute__ ((packed)), avm_capabilities),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_pm_info_stat'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_pm_info_stat[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "reserved1",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), reserved1),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_sumact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_sumact),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_sumcum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_sumcum),
	},
	[3] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_systemact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_systemact),
	},
	[4] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_systemcum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_systemcum),
	},
	[5] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "system_status",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), system_status),
	},
	[6] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_dspact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_dspact),
	},
	[7] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_dspcum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_dspcum),
	},
	[8] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_wlanact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_wlanact),
	},
	[9] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_wlancum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_wlancum),
	},
	[10] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "wlan_devices",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), wlan_devices),
	},
	[11] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "wlan_status",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), wlan_status),
	},
	[12] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_ethact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_ethact),
	},
	[13] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_ethcum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_ethcum),
	},
	[14] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned short",
		.flags = 0,
		.name = "eth_status",
		.size = sizeof(unsigned short),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), eth_status),
	},
	[15] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_abact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_abact),
	},
	[16] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_abcum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_abcum),
	},
	[17] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned short",
		.flags = 0,
		.name = "isdn_status",
		.size = sizeof(unsigned short),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), isdn_status),
	},
	[18] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_dectact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_dectact),
	},
	[19] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_dectcum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_dectcum),
	},
	[20] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_battchargeact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_battchargeact),
	},
	[21] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_battchargecum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_battchargecum),
	},
	[22] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "dect_status",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), dect_status),
	},
	[23] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_usbhostact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_usbhostact),
	},
	[24] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_usbhostcum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_usbhostcum),
	},
	[25] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "usb_status",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), usb_status),
	},
	[26] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "signed char",
		.flags = 0,
		.name = "act_temperature",
		.size = sizeof(signed char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), act_temperature),
	},
	[27] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "signed char",
		.flags = 0,
		.name = "min_temperature",
		.size = sizeof(signed char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), min_temperature),
	},
	[28] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "signed char",
		.flags = 0,
		.name = "max_temperature",
		.size = sizeof(signed char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), max_temperature),
	},
	[29] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "signed char",
		.flags = 0,
		.name = "avg_temperature",
		.size = sizeof(signed char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), avg_temperature),
	},
	[30] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_lteact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_lteact),
	},
	[31] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_ltecum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_ltecum),
	},
	[32] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "rate_dvbcact",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_dvbcact),
	},
	[33] = {
		.struct_name =  "convert_message_struct_avm_event_pm_info_stat",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "rate_dvbccum",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_pm_info_stat __attribute__ ((packed)), rate_dvbccum),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_unserialised'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_unserialised[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_unserialised",
		.type_name   =  "uint64_t",
		.flags = 0,
		.name = "evnt_id",
		.size = sizeof(uint64_t),
		.offset = __builtin_offsetof(struct avm_event_unserialised __attribute__ ((packed)), evnt_id),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_unserialised",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "data_len",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_unserialised __attribute__ ((packed)), data_len),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_unserialised",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "data",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_unserialised __attribute__ ((packed)), data),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_telephony_string'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_telephony_string[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_telephony_string",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | 0,
		.name = "length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_telephony_string __attribute__ ((packed)), length),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_telephony_string",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_ARRAY_USE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "string",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_telephony_string __attribute__ ((packed)), string),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_mass_storage_unmount'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_mass_storage_unmount[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_mass_storage_unmount",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | 0,
		.name = "name_length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_mass_storage_unmount __attribute__ ((packed)), name_length),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_mass_storage_unmount",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_ARRAY_USE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "name",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_mass_storage_unmount __attribute__ ((packed)), name),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_tffs_write'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_tffs_write[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_write",
		.type_name   =  "uint64_t",
		.flags = 0,
		.name = "buff_addr",
		.size = sizeof(uint64_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_write __attribute__ ((packed)), buff_addr),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_write",
		.type_name   =  "uint64_t",
		.flags = 0,
		.name = "len",
		.size = sizeof(uint64_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_write __attribute__ ((packed)), len),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_write",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "id",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_write __attribute__ ((packed)), id),
	},
	[3] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_write",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "final",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_write __attribute__ ((packed)), final),
	},
	[4] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_write",
		.type_name   =  "int32_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "crc",
		.size = sizeof(int32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_write __attribute__ ((packed)), crc),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_tffs'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_tffs[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_tffs",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "src_id",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs __attribute__ ((packed)), src_id),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_tffs",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "dst_id",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs __attribute__ ((packed)), dst_id),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_tffs",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "seq_nr",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs __attribute__ ((packed)), seq_nr),
	},
	[3] = {
		.struct_name =  "convert_message_struct_avm_event_tffs",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "ack",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs __attribute__ ((packed)), ack),
	},
	[4] = {
		.struct_name =  "convert_message_struct_avm_event_tffs",
		.type_name   =  "uint64_t",
		.flags = 0,
		.name = "srv_handle",
		.size = sizeof(uint64_t),
		.offset = __builtin_offsetof(struct avm_event_tffs __attribute__ ((packed)), srv_handle),
	},
	[5] = {
		.struct_name =  "convert_message_struct_avm_event_tffs",
		.type_name   =  "uint64_t",
		.flags = 0,
		.name = "clt_handle",
		.size = sizeof(uint64_t),
		.offset = __builtin_offsetof(struct avm_event_tffs __attribute__ ((packed)), clt_handle),
	},
	[6] = {
		.struct_name =  "convert_message_struct_avm_event_tffs",
		.type_name   =  "int32_t",
		.flags = 0,
		.name = "result",
		.size = sizeof(int32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs __attribute__ ((packed)), result),
	},
	[7] = {
		.struct_name =  "convert_message_struct_avm_event_tffs",
		.type_name   =  "enum avm_event_tffs_call_type",
		.flags = ENDIAN_CONVERT_SELECT | 0,
		.name = "type",
		.size = sizeof(enum avm_event_tffs_call_type),
		.offset = __builtin_offsetof(struct avm_event_tffs __attribute__ ((packed)), type),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_avm_event_tffs_call_type_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_avm_event_tffs_call_type,
		.enumName = "enum_table_avm_event_tffs_call_type"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[8] = {
		.struct_name =  "convert_message_struct_avm_event_tffs",
		.type_name   =  "union avm_event_tffs_call_union",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_SELECT_ENTRY | ENDIAN_CONVERT_IGNORE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "call",
		.substruct = convert_message_union_avm_event_tffs_call_union,
		.offset = __builtin_offsetof(struct avm_event_tffs __attribute__ ((packed)), call),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_telefonprofile'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_telefonprofile[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_telefonprofile",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "event_header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_telefonprofile __attribute__ ((packed)), event_header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_telefonprofile",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "on",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_telefonprofile __attribute__ ((packed)), on),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_cmd_param_source_trigger'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_cmd_param_source_trigger[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_cmd_param_source_trigger",
		.type_name   =  "enum _avm_event_id",
		.flags = 0,
		.name = "id",
		.size = sizeof(enum _avm_event_id),
		.offset = __builtin_offsetof(struct _avm_event_cmd_param_source_trigger __attribute__ ((packed)), id),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_event_id_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_event_id,
		.enumName = "enum_table__avm_event_id"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_cmd_param_source_trigger",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "data_length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_cmd_param_source_trigger __attribute__ ((packed)), data_length),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_ambient_brightness'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_ambient_brightness[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_ambient_brightness",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "value",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_ambient_brightness __attribute__ ((packed)), value),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_ambient_brightness",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "maxvalue",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_ambient_brightness __attribute__ ((packed)), maxvalue),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_id_mask'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_id_mask[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_id_mask",
		.type_name   =  "avm_event_mask_fieldentry",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "mask",
		.offset = __builtin_offsetof(struct _avm_event_id_mask __attribute__ ((packed)), mask),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_cpu_run'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_cpu_run[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_cpu_run",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "event_header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_cpu_run __attribute__ ((packed)), event_header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_cpu_run",
		.type_name   =  "enum _cputype",
		.flags = 0,
		.name = "cputype",
		.size = sizeof(enum _cputype),
		.offset = __builtin_offsetof(struct _avm_event_cpu_run __attribute__ ((packed)), cputype),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__cputype_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__cputype,
		.enumName = "enum_table__cputype"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_cpu_run",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "cpu_run",
		.size = sizeof(unsigned char) * 4,
		.offset = __builtin_offsetof(struct _avm_event_cpu_run __attribute__ ((packed)), cpu_run),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_led_info'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_led_info[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_led_info",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_led_info __attribute__ ((packed)), header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_led_info",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "mode",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_led_info __attribute__ ((packed)), mode),
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_led_info",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "param1",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_led_info __attribute__ ((packed)), param1),
	},
	[3] = {
		.struct_name =  "convert_message_struct__avm_event_led_info",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "param2",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_led_info __attribute__ ((packed)), param2),
	},
	[4] = {
		.struct_name =  "convert_message_struct__avm_event_led_info",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "gpio_driver_type",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_led_info __attribute__ ((packed)), gpio_driver_type),
	},
	[5] = {
		.struct_name =  "convert_message_struct__avm_event_led_info",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "gpio",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_led_info __attribute__ ((packed)), gpio),
	},
	[6] = {
		.struct_name =  "convert_message_struct__avm_event_led_info",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "pos",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_led_info __attribute__ ((packed)), pos),
	},
	[7] = {
		.struct_name =  "convert_message_struct__avm_event_led_info",
		.type_name   =  "char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "name",
		.size = sizeof(char) * 32,
		.offset = __builtin_offsetof(struct _avm_event_led_info __attribute__ ((packed)), name),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_firmware_update_available'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_firmware_update_available[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_firmware_update_available",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_firmware_update_available __attribute__ ((packed)), header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_firmware_update_available",
		.type_name   =  "enum avm_event_firmware_type",
		.flags = 0,
		.name = "type",
		.size = sizeof(enum avm_event_firmware_type),
		.offset = __builtin_offsetof(struct _avm_event_firmware_update_available __attribute__ ((packed)), type),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_avm_event_firmware_type_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_avm_event_firmware_type,
		.enumName = "enum_table_avm_event_firmware_type"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_firmware_update_available",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | 0,
		.name = "version_length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_firmware_update_available __attribute__ ((packed)), version_length),
	},
	[3] = {
		.struct_name =  "convert_message_struct__avm_event_firmware_update_available",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_ARRAY_USE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "version",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_firmware_update_available __attribute__ ((packed)), version),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_log'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_log[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_log",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "event_header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_log __attribute__ ((packed)), event_header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_log",
		.type_name   =  "enum _avm_logtype",
		.flags = 0,
		.name = "logtype",
		.size = sizeof(enum _avm_logtype),
		.offset = __builtin_offsetof(struct _avm_event_log __attribute__ ((packed)), logtype),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_logtype_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_logtype,
		.enumName = "enum_table__avm_logtype"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_log",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "loglen",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_log __attribute__ ((packed)), loglen),
	},
	[3] = {
		.struct_name =  "convert_message_struct__avm_event_log",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "logpointer",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_log __attribute__ ((packed)), logpointer),
	},
	[4] = {
		.struct_name =  "convert_message_struct__avm_event_log",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "checksum",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_log __attribute__ ((packed)), checksum),
	},
	[5] = {
		.struct_name =  "convert_message_struct__avm_event_log",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "rebootflag",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_log __attribute__ ((packed)), rebootflag),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_source_notifier'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_source_notifier[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_source_notifier",
		.type_name   =  "enum _avm_event_id",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "id",
		.size = sizeof(enum _avm_event_id),
		.offset = __builtin_offsetof(struct avm_event_source_notifier __attribute__ ((packed)), id),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_event_id_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_event_id,
		.enumName = "enum_table__avm_event_id"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_rpc'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_rpc[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_rpc",
		.type_name   =  "enum _avm_rpctype",
		.flags = 0,
		.name = "type",
		.size = sizeof(enum _avm_rpctype),
		.offset = __builtin_offsetof(struct avm_event_rpc __attribute__ ((packed)), type),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_rpctype_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_rpctype,
		.enumName = "enum_table__avm_rpctype"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_rpc",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "id",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_rpc __attribute__ ((packed)), id),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_rpc",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | 0,
		.name = "length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_rpc __attribute__ ((packed)), length),
	},
	[3] = {
		.struct_name =  "convert_message_struct_avm_event_rpc",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_ARRAY_USE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "message",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_rpc __attribute__ ((packed)), message),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_wlan'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_wlan[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_wlan",
		.type_name   =  "char",
		.flags = 0,
		.name = "mac",
		.size = sizeof(char) * 6,
		.offset = __builtin_offsetof(struct avm_event_wlan __attribute__ ((packed)), mac),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_wlan",
		.type_name   =  "union avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_IGNORE | 0,
		.name = "u1",
		.substruct = convert_message_union_avm_event_wlan_client_status_u1,
		.offset = __builtin_offsetof(struct avm_event_wlan __attribute__ ((packed)), u1),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_wlan",
		.type_name   =  "wlan_event",
		.flags = 0,
		.name = "event",
		.size = sizeof(wlan_event),
		.offset = __builtin_offsetof(struct avm_event_wlan __attribute__ ((packed)), event),
	},
	[3] = {
		.struct_name =  "convert_message_struct_avm_event_wlan",
		.type_name   =  "wlan_info",
		.flags = 0,
		.name = "info",
		.size = sizeof(wlan_info),
		.offset = __builtin_offsetof(struct avm_event_wlan __attribute__ ((packed)), info),
	},
	[4] = {
		.struct_name =  "convert_message_struct_avm_event_wlan",
		.type_name   =  "enum wlan_sm_states",
		.flags = 0,
		.name = "status",
		.size = sizeof(enum wlan_sm_states),
		.offset = __builtin_offsetof(struct avm_event_wlan __attribute__ ((packed)), status),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_wlan_sm_states_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_wlan_sm_states,
		.enumName = "enum_table_wlan_sm_states"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[5] = {
		.struct_name =  "convert_message_struct_avm_event_wlan",
		.type_name   =  "union avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_IGNORE | 0,
		.name = "u2",
		.substruct = convert_message_union_avm_event_wlan_client_status_u2,
		.offset = __builtin_offsetof(struct avm_event_wlan __attribute__ ((packed)), u2),
	},
	[6] = {
		.struct_name =  "convert_message_struct_avm_event_wlan",
		.type_name   =  "char",
		.flags = 0,
		.name = "if_name",
		.size = sizeof(char) * IFNAMSIZ,
		.offset = __builtin_offsetof(struct avm_event_wlan __attribute__ ((packed)), if_name),
	},
	[7] = {
		.struct_name =  "convert_message_struct_avm_event_wlan",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "ev_initiator",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_wlan __attribute__ ((packed)), ev_initiator),
	},
	[8] = {
		.struct_name =  "convert_message_struct_avm_event_wlan",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "ev_reason",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_wlan __attribute__ ((packed)), ev_reason),
	},
	[9] = {
		.struct_name =  "convert_message_struct_avm_event_wlan",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "avm_capabilities",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_wlan __attribute__ ((packed)), avm_capabilities),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_tffs_paniclog'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_tffs_paniclog[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_paniclog",
		.type_name   =  "uint64_t",
		.flags = 0,
		.name = "buff_addr",
		.size = sizeof(uint64_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_paniclog __attribute__ ((packed)), buff_addr),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_paniclog",
		.type_name   =  "uint64_t",
		.flags = 0,
		.name = "len",
		.size = sizeof(uint64_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_paniclog __attribute__ ((packed)), len),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_paniclog",
		.type_name   =  "int32_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "crc",
		.size = sizeof(int32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_paniclog __attribute__ ((packed)), crc),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_source_register'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_source_register[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_source_register",
		.type_name   =  "struct _avm_event_id_mask",
		.flags = 0,
		.name = "id_mask",
		.substruct = convert_message_struct__avm_event_id_mask,
		.offset = __builtin_offsetof(struct avm_event_source_register __attribute__ ((packed)), id_mask),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_source_register",
		.type_name   =  "char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "name",
		.size = sizeof(char) * 32,
		.offset = __builtin_offsetof(struct avm_event_source_register __attribute__ ((packed)), name),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_tffs_reindex'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_tffs_reindex[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_reindex",
		.type_name   =  "uint32_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_reindex __attribute__ ((packed)), dummy),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _cpmac_event_struct'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__cpmac_event_struct[] = {
	[0] = {
		.struct_name =  "convert_message_struct__cpmac_event_struct",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "ports",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _cpmac_event_struct __attribute__ ((packed)), ports),
	},
	[1] = {
		.struct_name =  "convert_message_struct__cpmac_event_struct",
		.type_name   =  "struct cpmac_port",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "port",
		.substruct = convert_message_struct_cpmac_port,
		.offset = __builtin_offsetof(struct _cpmac_event_struct __attribute__ ((packed)), port),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct wlan_event_data_client_connect_info'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_wlan_event_data_client_connect_info[] = {
	[0] = {
		.struct_name =  "convert_message_struct_wlan_event_data_client_connect_info",
		.type_name   =  "struct wlan_event_data_client_common",
		.flags = 0,
		.name = "common",
		.substruct = convert_message_struct_wlan_event_data_client_common,
		.offset = __builtin_offsetof(struct wlan_event_data_client_connect_info __attribute__ ((packed)), common),
	},
	[1] = {
		.struct_name =  "convert_message_struct_wlan_event_data_client_connect_info",
		.type_name   =  "uint8_t",
		.flags = 0,
		.name = "info_context",
		.size = sizeof(uint8_t),
		.offset = __builtin_offsetof(struct wlan_event_data_client_connect_info __attribute__ ((packed)), info_context),
	},
	[2] = {
		.struct_name =  "convert_message_struct_wlan_event_data_client_connect_info",
		.type_name   =  "uint8_t",
		.flags = 0,
		.name = "reason",
		.size = sizeof(uint8_t),
		.offset = __builtin_offsetof(struct wlan_event_data_client_connect_info __attribute__ ((packed)), reason),
	},
	[3] = {
		.struct_name =  "convert_message_struct_wlan_event_data_client_connect_info",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "max_node_count",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct wlan_event_data_client_connect_info __attribute__ ((packed)), max_node_count),
	},
	[4] = {
		.struct_name =  "convert_message_struct_wlan_event_data_client_connect_info",
		.type_name   =  "uint16_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "ieee80211_code",
		.size = sizeof(unsigned short),
		.offset = __builtin_offsetof(struct wlan_event_data_client_connect_info __attribute__ ((packed)), ieee80211_code),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_fax_status'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_fax_status[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_fax_status",
		.type_name   =  "enum fax_receive_mode",
		.flags = 0,
		.name = "fax_receive_mode",
		.size = sizeof(enum fax_receive_mode),
		.offset = __builtin_offsetof(struct avm_event_fax_status __attribute__ ((packed)), fax_receive_mode),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_fax_receive_mode_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_fax_receive_mode,
		.enumName = "enum_table_fax_receive_mode"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_fax_status",
		.type_name   =  "enum fax_storage_dest",
		.flags = 0,
		.name = "fax_storage_dest",
		.size = sizeof(enum fax_storage_dest),
		.offset = __builtin_offsetof(struct avm_event_fax_status __attribute__ ((packed)), fax_storage_dest),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_fax_storage_dest_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_fax_storage_dest,
		.enumName = "enum_table_fax_storage_dest"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_fax_status",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | 0,
		.name = "dirname_length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_fax_status __attribute__ ((packed)), dirname_length),
	},
	[3] = {
		.struct_name =  "convert_message_struct_avm_event_fax_status",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_ARRAY_USE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dirname",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_fax_status __attribute__ ((packed)), dirname),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct wlan_event_data_client_state_change'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_wlan_event_data_client_state_change[] = {
	[0] = {
		.struct_name =  "convert_message_struct_wlan_event_data_client_state_change",
		.type_name   =  "struct wlan_event_data_client_common",
		.flags = 0,
		.name = "common",
		.substruct = convert_message_struct_wlan_event_data_client_common,
		.offset = __builtin_offsetof(struct wlan_event_data_client_state_change __attribute__ ((packed)), common),
	},
	[1] = {
		.struct_name =  "convert_message_struct_wlan_event_data_client_state_change",
		.type_name   =  "uint8_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "state",
		.size = sizeof(uint8_t),
		.offset = __builtin_offsetof(struct wlan_event_data_client_state_change __attribute__ ((packed)), state),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_cpu_idle'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_cpu_idle[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_cpu_idle",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "cpu_idle",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_cpu_idle __attribute__ ((packed)), cpu_idle),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_cpu_idle",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "dsl_dsp_idle",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_cpu_idle __attribute__ ((packed)), dsl_dsp_idle),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_cpu_idle",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "voice_dsp_idle",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_cpu_idle __attribute__ ((packed)), voice_dsp_idle),
	},
	[3] = {
		.struct_name =  "convert_message_struct_avm_event_cpu_idle",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "mem_strictlyused",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_cpu_idle __attribute__ ((packed)), mem_strictlyused),
	},
	[4] = {
		.struct_name =  "convert_message_struct_avm_event_cpu_idle",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "mem_cacheused",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_cpu_idle __attribute__ ((packed)), mem_cacheused),
	},
	[5] = {
		.struct_name =  "convert_message_struct_avm_event_cpu_idle",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "mem_physfree",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_cpu_idle __attribute__ ((packed)), mem_physfree),
	},
	[6] = {
		.struct_name =  "convert_message_struct_avm_event_cpu_idle",
		.type_name   =  "enum _cputype",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "cputype",
		.size = sizeof(enum _cputype),
		.offset = __builtin_offsetof(struct avm_event_cpu_idle __attribute__ ((packed)), cputype),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__cputype_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__cputype,
		.enumName = "enum_table__cputype"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_led_status'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_led_status[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_led_status",
		.type_name   =  "enum avm_event_led_id",
		.flags = 0,
		.name = "led",
		.size = sizeof(enum avm_event_led_id),
		.offset = __builtin_offsetof(struct avm_event_led_status __attribute__ ((packed)), led),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_avm_event_led_id_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_avm_event_led_id,
		.enumName = "enum_table_avm_event_led_id"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_led_status",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "state",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_led_status __attribute__ ((packed)), state),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_led_status",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "param_len",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_led_status __attribute__ ((packed)), param_len),
	},
	[3] = {
		.struct_name =  "convert_message_struct_avm_event_led_status",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "params",
		.size = sizeof(unsigned char) * 245,
		.offset = __builtin_offsetof(struct avm_event_led_status __attribute__ ((packed)), params),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_user_mode_source_notify'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_user_mode_source_notify[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_user_mode_source_notify",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_user_mode_source_notify __attribute__ ((packed)), header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_user_mode_source_notify",
		.type_name   =  "enum _avm_event_id",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "id",
		.size = sizeof(enum _avm_event_id),
		.offset = __builtin_offsetof(struct _avm_event_user_mode_source_notify __attribute__ ((packed)), id),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_event_id_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_event_id,
		.enumName = "enum_table__avm_event_id"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_fax_status'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_fax_status[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_fax_status",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_fax_status __attribute__ ((packed)), header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_fax_status",
		.type_name   =  "enum fax_receive_mode",
		.flags = 0,
		.name = "fax_receive_mode",
		.size = sizeof(enum fax_receive_mode),
		.offset = __builtin_offsetof(struct _avm_event_fax_status __attribute__ ((packed)), fax_receive_mode),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_fax_receive_mode_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_fax_receive_mode,
		.enumName = "enum_table_fax_receive_mode"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_fax_status",
		.type_name   =  "enum fax_storage_dest",
		.flags = 0,
		.name = "fax_storage_dest",
		.size = sizeof(enum fax_storage_dest),
		.offset = __builtin_offsetof(struct _avm_event_fax_status __attribute__ ((packed)), fax_storage_dest),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_fax_storage_dest_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_fax_storage_dest,
		.enumName = "enum_table_fax_storage_dest"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[3] = {
		.struct_name =  "convert_message_struct__avm_event_fax_status",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | 0,
		.name = "dirname_length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_fax_status __attribute__ ((packed)), dirname_length),
	},
	[4] = {
		.struct_name =  "convert_message_struct__avm_event_fax_status",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_ARRAY_USE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dirname",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_fax_status __attribute__ ((packed)), dirname),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_fax_file'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_fax_file[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_fax_file",
		.type_name   =  "enum fax_file_event_type",
		.flags = 0,
		.name = "action",
		.size = sizeof(enum fax_file_event_type),
		.offset = __builtin_offsetof(struct avm_event_fax_file __attribute__ ((packed)), action),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_fax_file_event_type_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_fax_file_event_type,
		.enumName = "enum_table_fax_file_event_type"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_fax_file",
		.type_name   =  "time_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "date",
		.offset = __builtin_offsetof(struct avm_event_fax_file __attribute__ ((packed)), date),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_cmd_param_register'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_cmd_param_register[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_cmd_param_register",
		.type_name   =  "struct _avm_event_id_mask",
		.flags = 0,
		.name = "mask",
		.substruct = convert_message_struct__avm_event_id_mask,
		.offset = __builtin_offsetof(struct _avm_event_cmd_param_register __attribute__ ((packed)), mask),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_cmd_param_register",
		.type_name   =  "char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "Name",
		.size = sizeof(char) * 33,
		.offset = __builtin_offsetof(struct _avm_event_cmd_param_register __attribute__ ((packed)), Name),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct wlan_event_data_scan_common'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_wlan_event_data_scan_common[] = {
	[0] = {
		.struct_name =  "convert_message_struct_wlan_event_data_scan_common",
		.type_name   =  "char",
		.flags = 0,
		.name = "iface",
		.size = sizeof(char) * IFNAMSIZ + 1,
		.offset = __builtin_offsetof(struct wlan_event_data_scan_common __attribute__ ((packed)), iface),
	},
	[1] = {
		.struct_name =  "convert_message_struct_wlan_event_data_scan_common",
		.type_name   =  "char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "initiator",
		.size = sizeof(char) * 16 + 1,
		.offset = __builtin_offsetof(struct wlan_event_data_scan_common __attribute__ ((packed)), initiator),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_cpu_idle'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_cpu_idle[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_cpu_idle",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "event_header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_cpu_idle __attribute__ ((packed)), event_header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_cpu_idle",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "cpu_idle",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_cpu_idle __attribute__ ((packed)), cpu_idle),
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_cpu_idle",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "dsl_dsp_idle",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_cpu_idle __attribute__ ((packed)), dsl_dsp_idle),
	},
	[3] = {
		.struct_name =  "convert_message_struct__avm_event_cpu_idle",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "voice_dsp_idle",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_cpu_idle __attribute__ ((packed)), voice_dsp_idle),
	},
	[4] = {
		.struct_name =  "convert_message_struct__avm_event_cpu_idle",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "mem_strictlyused",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_cpu_idle __attribute__ ((packed)), mem_strictlyused),
	},
	[5] = {
		.struct_name =  "convert_message_struct__avm_event_cpu_idle",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "mem_cacheused",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_cpu_idle __attribute__ ((packed)), mem_cacheused),
	},
	[6] = {
		.struct_name =  "convert_message_struct__avm_event_cpu_idle",
		.type_name   =  "unsigned char",
		.flags = 0,
		.name = "mem_physfree",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_cpu_idle __attribute__ ((packed)), mem_physfree),
	},
	[7] = {
		.struct_name =  "convert_message_struct__avm_event_cpu_idle",
		.type_name   =  "enum _cputype",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "cputype",
		.size = sizeof(enum _cputype),
		.offset = __builtin_offsetof(struct _avm_event_cpu_idle __attribute__ ((packed)), cputype),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__cputype_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__cputype,
		.enumName = "enum_table__cputype"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_remote_source_trigger_request'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_remote_source_trigger_request[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_remote_source_trigger_request",
		.type_name   =  "struct avm_event_data",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "data",
		.substruct = convert_message_struct_avm_event_data,
		.offset = __builtin_offsetof(struct avm_event_remote_source_trigger_request __attribute__ ((packed)), data),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct cpmac_port'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_cpmac_port[] = {
	[0] = {
		.struct_name =  "convert_message_struct_cpmac_port",
		.type_name   =  "uint8_t",
		.flags = 0,
		.name = "cable",
		.size = sizeof(uint8_t),
		.offset = __builtin_offsetof(struct cpmac_port __attribute__ ((packed)), cable),
	},
	[1] = {
		.struct_name =  "convert_message_struct_cpmac_port",
		.type_name   =  "uint8_t",
		.flags = 0,
		.name = "link",
		.size = sizeof(uint8_t),
		.offset = __builtin_offsetof(struct cpmac_port __attribute__ ((packed)), link),
	},
	[2] = {
		.struct_name =  "convert_message_struct_cpmac_port",
		.type_name   =  "uint8_t",
		.flags = 0,
		.name = "speed100",
		.size = sizeof(uint8_t),
		.offset = __builtin_offsetof(struct cpmac_port __attribute__ ((packed)), speed100),
	},
	[3] = {
		.struct_name =  "convert_message_struct_cpmac_port",
		.type_name   =  "uint8_t",
		.flags = 0,
		.name = "fullduplex",
		.size = sizeof(uint8_t),
		.offset = __builtin_offsetof(struct cpmac_port __attribute__ ((packed)), fullduplex),
	},
	[4] = {
		.struct_name =  "convert_message_struct_cpmac_port",
		.type_name   =  "enum _avm_event_ethernet_speed",
		.flags = 0,
		.name = "speed",
		.size = sizeof(enum _avm_event_ethernet_speed),
		.offset = __builtin_offsetof(struct cpmac_port __attribute__ ((packed)), speed),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_event_ethernet_speed_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_event_ethernet_speed,
		.enumName = "enum_table__avm_event_ethernet_speed"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[5] = {
		.struct_name =  "convert_message_struct_cpmac_port",
		.type_name   =  "enum _avm_event_ethernet_speed",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "maxspeed",
		.size = sizeof(enum _avm_event_ethernet_speed),
		.offset = __builtin_offsetof(struct cpmac_port __attribute__ ((packed)), maxspeed),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_event_ethernet_speed_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_event_ethernet_speed,
		.enumName = "enum_table__avm_event_ethernet_speed"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_led_status'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_led_status[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_led_status",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_led_status __attribute__ ((packed)), header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_led_status",
		.type_name   =  "enum avm_event_led_id",
		.flags = 0,
		.name = "led",
		.size = sizeof(enum avm_event_led_id),
		.offset = __builtin_offsetof(struct _avm_event_led_status __attribute__ ((packed)), led),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_avm_event_led_id_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_avm_event_led_id,
		.enumName = "enum_table_avm_event_led_id"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_led_status",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "state",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_led_status __attribute__ ((packed)), state),
	},
	[3] = {
		.struct_name =  "convert_message_struct__avm_event_led_status",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "param_len",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_led_status __attribute__ ((packed)), param_len),
	},
	[4] = {
		.struct_name =  "convert_message_struct__avm_event_led_status",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "params",
		.size = sizeof(unsigned char) * 245,
		.offset = __builtin_offsetof(struct _avm_event_led_status __attribute__ ((packed)), params),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_powermanagment_remote_ressourceinfo'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_powermanagment_remote_ressourceinfo[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_powermanagment_remote_ressourceinfo",
		.type_name   =  "enum _powermanagment_device",
		.flags = 0,
		.name = "device",
		.size = sizeof(enum _powermanagment_device),
		.offset = __builtin_offsetof(struct avm_event_powermanagment_remote_ressourceinfo __attribute__ ((packed)), device),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__powermanagment_device_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__powermanagment_device,
		.enumName = "enum_table__powermanagment_device"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_powermanagment_remote_ressourceinfo",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "power_rate",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_powermanagment_remote_ressourceinfo __attribute__ ((packed)), power_rate),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_data'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_data[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_data",
		.type_name   =  "enum _avm_event_id",
		.flags = ENDIAN_CONVERT_SELECT | 0,
		.name = "id",
		.size = sizeof(enum _avm_event_id),
		.offset = __builtin_offsetof(struct avm_event_data __attribute__ ((packed)), id),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_event_id_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_event_id,
		.enumName = "enum_table__avm_event_id"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_data",
		.type_name   =  "union avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_SELECT_ENTRY | ENDIAN_CONVERT_IGNORE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "data",
		.substruct = convert_message_union_avm_event_data_union,
		.offset = __builtin_offsetof(struct avm_event_data __attribute__ ((packed)), data),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_user_mode_source_notify'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_user_mode_source_notify[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_user_mode_source_notify",
		.type_name   =  "enum _avm_event_id",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "id",
		.size = sizeof(enum _avm_event_id),
		.offset = __builtin_offsetof(struct avm_event_user_mode_source_notify __attribute__ ((packed)), id),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_event_id_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_event_id,
		.enumName = "enum_table__avm_event_id"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_source_unregister'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_source_unregister[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_source_unregister",
		.type_name   =  "struct _avm_event_id_mask",
		.flags = 0,
		.name = "id_mask",
		.substruct = convert_message_struct__avm_event_id_mask,
		.offset = __builtin_offsetof(struct avm_event_source_unregister __attribute__ ((packed)), id_mask),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_source_unregister",
		.type_name   =  "char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "name",
		.size = sizeof(char) * 32,
		.offset = __builtin_offsetof(struct avm_event_source_unregister __attribute__ ((packed)), name),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_checkpoint'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_checkpoint[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_checkpoint",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "node_id",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_checkpoint __attribute__ ((packed)), node_id),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_checkpoint",
		.type_name   =  "uint64_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "checkpoints",
		.size = sizeof(uint64_t),
		.offset = __builtin_offsetof(struct avm_event_checkpoint __attribute__ ((packed)), checkpoints),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_telefonprofile'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_telefonprofile[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_telefonprofile",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "on",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_telefonprofile __attribute__ ((packed)), on),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_powermanagment_remote'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_powermanagment_remote[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_powermanagment_remote",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_powermanagment_remote __attribute__ ((packed)), header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_powermanagment_remote",
		.type_name   =  "enum avm_event_powermanagment_remote_action",
		.flags = ENDIAN_CONVERT_SELECT | 0,
		.name = "remote_action",
		.size = sizeof(enum avm_event_powermanagment_remote_action),
		.offset = __builtin_offsetof(struct _avm_event_powermanagment_remote __attribute__ ((packed)), remote_action),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_avm_event_powermanagment_remote_action_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_avm_event_powermanagment_remote_action,
		.enumName = "enum_table_avm_event_powermanagment_remote_action"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_powermanagment_remote",
		.type_name   =  "union avm_event_powermanagment_remote_union",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_SELECT_ENTRY | ENDIAN_CONVERT_IGNORE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "param",
		.substruct = convert_message_union_avm_event_powermanagment_remote_union,
		.offset = __builtin_offsetof(struct _avm_event_powermanagment_remote __attribute__ ((packed)), param),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_log'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_log[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_log",
		.type_name   =  "enum _avm_logtype",
		.flags = 0,
		.name = "logtype",
		.size = sizeof(enum _avm_logtype),
		.offset = __builtin_offsetof(struct avm_event_log __attribute__ ((packed)), logtype),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_logtype_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_logtype,
		.enumName = "enum_table__avm_logtype"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_log",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "loglen",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_log __attribute__ ((packed)), loglen),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_log",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "logpointer",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_log __attribute__ ((packed)), logpointer),
	},
	[3] = {
		.struct_name =  "convert_message_struct_avm_event_log",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "checksum",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_log __attribute__ ((packed)), checksum),
	},
	[4] = {
		.struct_name =  "convert_message_struct_avm_event_log",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "rebootflag",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_log __attribute__ ((packed)), rebootflag),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_powermanagment_status'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_powermanagment_status[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_powermanagment_status",
		.type_name   =  "enum _powermanagment_status_type",
		.flags = ENDIAN_CONVERT_SELECT | 0,
		.name = "substatus",
		.size = sizeof(enum _powermanagment_status_type),
		.offset = __builtin_offsetof(struct avm_event_powermanagment_status __attribute__ ((packed)), substatus),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__powermanagment_status_type_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__powermanagment_status_type,
		.enumName = "enum_table__powermanagment_status_type"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_powermanagment_status",
		.type_name   =  "union __powermanagment_status_union",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_SELECT_ENTRY | ENDIAN_CONVERT_IGNORE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "param",
		.substruct = convert_message_union___powermanagment_status_union,
		.offset = __builtin_offsetof(struct avm_event_powermanagment_status __attribute__ ((packed)), param),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_rpc'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_rpc[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_rpc",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "event_header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_rpc __attribute__ ((packed)), event_header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_rpc",
		.type_name   =  "enum _avm_rpctype",
		.flags = 0,
		.name = "type",
		.size = sizeof(enum _avm_rpctype),
		.offset = __builtin_offsetof(struct _avm_event_rpc __attribute__ ((packed)), type),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_rpctype_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_rpctype,
		.enumName = "enum_table__avm_rpctype"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_rpc",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "id",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_rpc __attribute__ ((packed)), id),
	},
	[3] = {
		.struct_name =  "convert_message_struct__avm_event_rpc",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | 0,
		.name = "length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_rpc __attribute__ ((packed)), length),
	},
	[4] = {
		.struct_name =  "convert_message_struct__avm_event_rpc",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_ARRAY_USE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "message",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_rpc __attribute__ ((packed)), message),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct wlan_event_data_scan_event_info'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_wlan_event_data_scan_event_info[] = {
	[0] = {
		.struct_name =  "convert_message_struct_wlan_event_data_scan_event_info",
		.type_name   =  "struct wlan_event_data_scan_common",
		.flags = 0,
		.name = "common",
		.substruct = convert_message_struct_wlan_event_data_scan_common,
		.offset = __builtin_offsetof(struct wlan_event_data_scan_event_info __attribute__ ((packed)), common),
	},
	[1] = {
		.struct_name =  "convert_message_struct_wlan_event_data_scan_event_info",
		.type_name   =  "enum wlan_event_scan_type",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "event_type",
		.size = sizeof(enum wlan_event_scan_type),
		.offset = __builtin_offsetof(struct wlan_event_data_scan_event_info __attribute__ ((packed)), event_type),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_wlan_event_scan_type_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_wlan_event_scan_type,
		.enumName = "enum_table_wlan_event_scan_type"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_cmd_param_release'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_cmd_param_release[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_cmd_param_release",
		.type_name   =  "char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "Name",
		.size = sizeof(char) * 33,
		.offset = __builtin_offsetof(struct _avm_event_cmd_param_release __attribute__ ((packed)), Name),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct wlan_event_data_client_common'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_wlan_event_data_client_common[] = {
	[0] = {
		.struct_name =  "convert_message_struct_wlan_event_data_client_common",
		.type_name   =  "uint8_t",
		.flags = 0,
		.name = "mac",
		.size = sizeof(uint8_t) * 6,
		.offset = __builtin_offsetof(struct wlan_event_data_client_common __attribute__ ((packed)), mac),
	},
	[1] = {
		.struct_name =  "convert_message_struct_wlan_event_data_client_common",
		.type_name   =  "char",
		.flags = 0,
		.name = "iface",
		.size = sizeof(char) * IFNAMSIZ + 1,
		.offset = __builtin_offsetof(struct wlan_event_data_client_common __attribute__ ((packed)), iface),
	},
	[2] = {
		.struct_name =  "convert_message_struct_wlan_event_data_client_common",
		.type_name   =  "char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "initiator",
		.size = sizeof(char) * 16 + 1,
		.offset = __builtin_offsetof(struct wlan_event_data_client_common __attribute__ ((packed)), initiator),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_remotepcmlink'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_remotepcmlink[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_remotepcmlink",
		.type_name   =  "enum _avm_remotepcmlinktype",
		.flags = 0,
		.name = "type",
		.size = sizeof(enum _avm_remotepcmlinktype),
		.offset = __builtin_offsetof(struct avm_event_remotepcmlink __attribute__ ((packed)), type),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_remotepcmlinktype_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_remotepcmlinktype,
		.enumName = "enum_table__avm_remotepcmlinktype"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_remotepcmlink",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "sharedlen",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_remotepcmlink __attribute__ ((packed)), sharedlen),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_remotepcmlink",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "sharedpointer",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_remotepcmlink __attribute__ ((packed)), sharedpointer),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_firmware_update_available'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_firmware_update_available[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_firmware_update_available",
		.type_name   =  "enum avm_event_firmware_type",
		.flags = 0,
		.name = "type",
		.size = sizeof(enum avm_event_firmware_type),
		.offset = __builtin_offsetof(struct avm_event_firmware_update_available __attribute__ ((packed)), type),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_avm_event_firmware_type_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_avm_event_firmware_type,
		.enumName = "enum_table_avm_event_firmware_type"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_firmware_update_available",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | 0,
		.name = "version_length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_firmware_update_available __attribute__ ((packed)), version_length),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_firmware_update_available",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_ARRAY_USE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "version",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_firmware_update_available __attribute__ ((packed)), version),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_temperature'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_temperature[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_temperature",
		.type_name   =  "int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "temperature",
		.size = sizeof(int),
		.offset = __builtin_offsetof(struct avm_event_temperature __attribute__ ((packed)), temperature),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_ambient_brightness'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_ambient_brightness[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_ambient_brightness",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "event_header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_ambient_brightness __attribute__ ((packed)), event_header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_ambient_brightness",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "value",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_ambient_brightness __attribute__ ((packed)), value),
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_ambient_brightness",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "maxvalue",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_ambient_brightness __attribute__ ((packed)), maxvalue),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_tffs_cleanup'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_tffs_cleanup[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_cleanup",
		.type_name   =  "uint32_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_cleanup __attribute__ ((packed)), dummy),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_powerline_status'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_powerline_status[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_powerline_status",
		.type_name   =  "enum ePLCState",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "status",
		.size = sizeof(enum ePLCState),
		.offset = __builtin_offsetof(struct avm_event_powerline_status __attribute__ ((packed)), status),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_ePLCState_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_ePLCState,
		.enumName = "enum_table_ePLCState"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_powermanagment_remote'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_powermanagment_remote[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_powermanagment_remote",
		.type_name   =  "enum avm_event_powermanagment_remote_action",
		.flags = ENDIAN_CONVERT_SELECT | 0,
		.name = "remote_action",
		.size = sizeof(enum avm_event_powermanagment_remote_action),
		.offset = __builtin_offsetof(struct avm_event_powermanagment_remote __attribute__ ((packed)), remote_action),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_avm_event_powermanagment_remote_action_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_avm_event_powermanagment_remote_action,
		.enumName = "enum_table_avm_event_powermanagment_remote_action"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_powermanagment_remote",
		.type_name   =  "union avm_event_powermanagment_remote_union",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_SELECT_ENTRY | ENDIAN_CONVERT_IGNORE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "param",
		.substruct = convert_message_union_avm_event_powermanagment_remote_union,
		.offset = __builtin_offsetof(struct avm_event_powermanagment_remote __attribute__ ((packed)), param),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct cpmac_event_struct'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_cpmac_event_struct[] = {
	[0] = {
		.struct_name =  "convert_message_struct_cpmac_event_struct",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "event_header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct cpmac_event_struct __attribute__ ((packed)), event_header),
	},
	[1] = {
		.struct_name =  "convert_message_struct_cpmac_event_struct",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "ports",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct cpmac_event_struct __attribute__ ((packed)), ports),
	},
	[2] = {
		.struct_name =  "convert_message_struct_cpmac_event_struct",
		.type_name   =  "struct cpmac_port",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "port",
		.substruct = convert_message_struct_cpmac_port,
		.offset = __builtin_offsetof(struct cpmac_event_struct __attribute__ ((packed)), port),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_powerline_status'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_powerline_status[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_powerline_status",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "event_header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_powerline_status __attribute__ ((packed)), event_header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_powerline_status",
		.type_name   =  "enum ePLCState",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "status",
		.size = sizeof(enum ePLCState),
		.offset = __builtin_offsetof(struct _avm_event_powerline_status __attribute__ ((packed)), status),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_ePLCState_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_ePLCState,
		.enumName = "enum_table_ePLCState"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_temperature'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_temperature[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_temperature",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "event_header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_temperature __attribute__ ((packed)), event_header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_temperature",
		.type_name   =  "int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "temperature",
		.size = sizeof(int),
		.offset = __builtin_offsetof(struct _avm_event_temperature __attribute__ ((packed)), temperature),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_smarthome_switch_status'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_smarthome_switch_status[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_smarthome_switch_status",
		.type_name   =  "enum avm_event_switch_type",
		.flags = 0,
		.name = "type",
		.size = sizeof(enum avm_event_switch_type),
		.offset = __builtin_offsetof(struct avm_event_smarthome_switch_status __attribute__ ((packed)), type),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_avm_event_switch_type_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_avm_event_switch_type,
		.enumName = "enum_table_avm_event_switch_type"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_smarthome_switch_status",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "value",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_smarthome_switch_status __attribute__ ((packed)), value),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_smarthome_switch_status",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | 0,
		.name = "ain_length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct avm_event_smarthome_switch_status __attribute__ ((packed)), ain_length),
	},
	[3] = {
		.struct_name =  "convert_message_struct_avm_event_smarthome_switch_status",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_ARRAY_USE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "ain",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct avm_event_smarthome_switch_status __attribute__ ((packed)), ain),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_tffs_notify'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_tffs_notify[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_notify",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "id",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_notify __attribute__ ((packed)), id),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_notify",
		.type_name   =  "enum avm_event_tffs_notify_event",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "event",
		.size = sizeof(enum avm_event_tffs_notify_event),
		.offset = __builtin_offsetof(struct avm_event_tffs_notify __attribute__ ((packed)), event),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_avm_event_tffs_notify_event_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_avm_event_tffs_notify_event,
		.enumName = "enum_table_avm_event_tffs_notify_event"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_tffs_close'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_tffs_close[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_close",
		.type_name   =  "uint32_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_close __attribute__ ((packed)), dummy),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_fax_file'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_fax_file[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_fax_file",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_fax_file __attribute__ ((packed)), header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_fax_file",
		.type_name   =  "enum fax_file_event_type",
		.flags = 0,
		.name = "action",
		.size = sizeof(enum fax_file_event_type),
		.offset = __builtin_offsetof(struct _avm_event_fax_file __attribute__ ((packed)), action),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_fax_file_event_type_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_fax_file_event_type,
		.enumName = "enum_table_fax_file_event_type"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_fax_file",
		.type_name   =  "time_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "date",
		.offset = __builtin_offsetof(struct _avm_event_fax_file __attribute__ ((packed)), date),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_powermanagment_remote_ressourceinfo'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_powermanagment_remote_ressourceinfo[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_powermanagment_remote_ressourceinfo",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_powermanagment_remote_ressourceinfo __attribute__ ((packed)), header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_powermanagment_remote_ressourceinfo",
		.type_name   =  "enum _powermanagment_device",
		.flags = 0,
		.name = "device",
		.size = sizeof(enum _powermanagment_device),
		.offset = __builtin_offsetof(struct _avm_event_powermanagment_remote_ressourceinfo __attribute__ ((packed)), device),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__powermanagment_device_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__powermanagment_device,
		.enumName = "enum_table__powermanagment_device"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_powermanagment_remote_ressourceinfo",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "power_rate",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_powermanagment_remote_ressourceinfo __attribute__ ((packed)), power_rate),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_tffs_read'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_tffs_read[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_read",
		.type_name   =  "uint64_t",
		.flags = 0,
		.name = "buff_addr",
		.size = sizeof(uint64_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_read __attribute__ ((packed)), buff_addr),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_read",
		.type_name   =  "uint64_t",
		.flags = 0,
		.name = "len",
		.size = sizeof(uint64_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_read __attribute__ ((packed)), len),
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_read",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "id",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_read __attribute__ ((packed)), id),
	},
	[3] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_read",
		.type_name   =  "int32_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "crc",
		.size = sizeof(int32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_read __attribute__ ((packed)), crc),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_telephony_missed_call'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_telephony_missed_call[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_telephony_missed_call",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_telephony_missed_call __attribute__ ((packed)), header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_telephony_missed_call",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | 0,
		.name = "length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_telephony_missed_call __attribute__ ((packed)), length),
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_telephony_missed_call",
		.type_name   =  "struct _avm_event_telephony_missed_call_params",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "p",
		.substruct = convert_message_struct__avm_event_telephony_missed_call_params,
		.offset = __builtin_offsetof(struct _avm_event_telephony_missed_call __attribute__ ((packed)), p),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_internet_new_ip'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_internet_new_ip[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_internet_new_ip",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_internet_new_ip __attribute__ ((packed)), header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_internet_new_ip",
		.type_name   =  "enum avm_event_internet_new_ip_param_sel",
		.flags = ENDIAN_CONVERT_SELECT | 0,
		.name = "sel",
		.size = sizeof(enum avm_event_internet_new_ip_param_sel),
		.offset = __builtin_offsetof(struct _avm_event_internet_new_ip __attribute__ ((packed)), sel),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_avm_event_internet_new_ip_param_sel_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_avm_event_internet_new_ip_param_sel,
		.enumName = "enum_table_avm_event_internet_new_ip_param_sel"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_internet_new_ip",
		.type_name   =  "union avm_event_internet_new_ip_param",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_SELECT_ENTRY | ENDIAN_CONVERT_IGNORE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "params",
		.substruct = convert_message_union_avm_event_internet_new_ip_param,
		.offset = __builtin_offsetof(struct _avm_event_internet_new_ip __attribute__ ((packed)), params),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_remotepcmlink'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_remotepcmlink[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_remotepcmlink",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "event_header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_remotepcmlink __attribute__ ((packed)), event_header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_remotepcmlink",
		.type_name   =  "enum _avm_remotepcmlinktype",
		.flags = 0,
		.name = "type",
		.size = sizeof(enum _avm_remotepcmlinktype),
		.offset = __builtin_offsetof(struct _avm_event_remotepcmlink __attribute__ ((packed)), type),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_remotepcmlinktype_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_remotepcmlinktype,
		.enumName = "enum_table__avm_remotepcmlinktype"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_remotepcmlink",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "sharedlen",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_remotepcmlink __attribute__ ((packed)), sharedlen),
	},
	[3] = {
		.struct_name =  "convert_message_struct__avm_event_remotepcmlink",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "sharedpointer",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_remotepcmlink __attribute__ ((packed)), sharedpointer),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_tffs_open'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_tffs_open[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_open",
		.type_name   =  "uint32_t",
		.flags = 0,
		.name = "id",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_open __attribute__ ((packed)), id),
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_open",
		.type_name   =  "enum avm_event_tffs_open_mode",
		.flags = 0,
		.name = "mode",
		.size = sizeof(enum avm_event_tffs_open_mode),
		.offset = __builtin_offsetof(struct avm_event_tffs_open __attribute__ ((packed)), mode),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_avm_event_tffs_open_mode_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_avm_event_tffs_open_mode,
		.enumName = "enum_table_avm_event_tffs_open_mode"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_open",
		.type_name   =  "uint32_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "max_segment_size",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_open __attribute__ ((packed)), max_segment_size),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_powermanagment_status'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_powermanagment_status[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_powermanagment_status",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "event_header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_powermanagment_status __attribute__ ((packed)), event_header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_powermanagment_status",
		.type_name   =  "enum _powermanagment_status_type",
		.flags = ENDIAN_CONVERT_SELECT | 0,
		.name = "substatus",
		.size = sizeof(enum _powermanagment_status_type),
		.offset = __builtin_offsetof(struct _avm_event_powermanagment_status __attribute__ ((packed)), substatus),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__powermanagment_status_type_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__powermanagment_status_type,
		.enumName = "enum_table__powermanagment_status_type"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_powermanagment_status",
		.type_name   =  "union __powermanagment_status_union",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_SELECT_ENTRY | ENDIAN_CONVERT_IGNORE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "param",
		.substruct = convert_message_union___powermanagment_status_union,
		.offset = __builtin_offsetof(struct _avm_event_powermanagment_status __attribute__ ((packed)), param),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_tffs_info'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_tffs_info[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_info",
		.type_name   =  "uint32_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "fill_level",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_info __attribute__ ((packed)), fill_level),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_push_button'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_push_button[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_push_button",
		.type_name   =  "enum _avm_event_push_button_key",
		.flags = 0,
		.name = "key",
		.size = sizeof(enum _avm_event_push_button_key),
		.offset = __builtin_offsetof(struct avm_event_push_button __attribute__ ((packed)), key),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_event_push_button_key_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_event_push_button_key,
		.enumName = "enum_table__avm_event_push_button_key"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct_avm_event_push_button",
		.type_name   =  "uint32_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "pressed",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_push_button __attribute__ ((packed)), pressed),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_push_button'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_push_button[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_push_button",
		.type_name   =  "enum _avm_event_id",
		.flags = 0,
		.name = "id",
		.size = sizeof(enum _avm_event_id),
		.offset = __builtin_offsetof(struct _avm_event_push_button __attribute__ ((packed)), id),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_event_id_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_event_id,
		.enumName = "enum_table__avm_event_id"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_push_button",
		.type_name   =  "enum _avm_event_push_button_key",
		.flags = 0,
		.name = "key",
		.size = sizeof(enum _avm_event_push_button_key),
		.offset = __builtin_offsetof(struct _avm_event_push_button __attribute__ ((packed)), key),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_event_push_button_key_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_event_push_button_key,
		.enumName = "enum_table__avm_event_push_button_key"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_push_button",
		.type_name   =  "uint32_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "pressed",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct _avm_event_push_button __attribute__ ((packed)), pressed),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_tffs_deinit'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_tffs_deinit[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_tffs_deinit",
		.type_name   =  "uint32_t",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy",
		.size = sizeof(uint32_t),
		.offset = __builtin_offsetof(struct avm_event_tffs_deinit __attribute__ ((packed)), dummy),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct avm_event_piglet'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct_avm_event_piglet[] = {
	[0] = {
		.struct_name =  "convert_message_struct_avm_event_piglet",
		.type_name   =  "enum _avm_piglettype",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "type",
		.size = sizeof(enum _avm_piglettype),
		.offset = __builtin_offsetof(struct avm_event_piglet __attribute__ ((packed)), type),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_piglettype_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_piglettype,
		.enumName = "enum_table__avm_piglettype"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_cmd_param_trigger'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_cmd_param_trigger[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_cmd_param_trigger",
		.type_name   =  "enum _avm_event_id",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "id",
		.size = sizeof(enum _avm_event_id),
		.offset = __builtin_offsetof(struct _avm_event_cmd_param_trigger __attribute__ ((packed)), id),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_event_id_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_event_id,
		.enumName = "enum_table__avm_event_id"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_smarthome_switch_status'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_smarthome_switch_status[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_smarthome_switch_status",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_smarthome_switch_status __attribute__ ((packed)), header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_smarthome_switch_status",
		.type_name   =  "enum avm_event_switch_type",
		.flags = 0,
		.name = "type",
		.size = sizeof(enum avm_event_switch_type),
		.offset = __builtin_offsetof(struct _avm_event_smarthome_switch_status __attribute__ ((packed)), type),
		.enum_debug_function = (char *(*)(unsigned int))get_enum_avm_event_switch_type_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table_avm_event_switch_type,
		.enumName = "enum_table_avm_event_switch_type"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
	[2] = {
		.struct_name =  "convert_message_struct__avm_event_smarthome_switch_status",
		.type_name   =  "unsigned int",
		.flags = 0,
		.name = "value",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_smarthome_switch_status __attribute__ ((packed)), value),
	},
	[3] = {
		.struct_name =  "convert_message_struct__avm_event_smarthome_switch_status",
		.type_name   =  "unsigned int",
		.flags = ENDIAN_CONVERT_ARRAY_ELEMENT_ANZAHL | 0,
		.name = "ain_length",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(struct _avm_event_smarthome_switch_status __attribute__ ((packed)), ain_length),
	},
	[4] = {
		.struct_name =  "convert_message_struct__avm_event_smarthome_switch_status",
		.type_name   =  "unsigned char",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_ARRAY_USE | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "ain",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(struct _avm_event_smarthome_switch_status __attribute__ ((packed)), ain),
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'struct _avm_event_piglet'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_struct__avm_event_piglet[] = {
	[0] = {
		.struct_name =  "convert_message_struct__avm_event_piglet",
		.type_name   =  "struct _avm_event_header",
		.flags = 0,
		.name = "event_header",
		.substruct = convert_message_struct__avm_event_header,
		.offset = __builtin_offsetof(struct _avm_event_piglet __attribute__ ((packed)), event_header),
	},
	[1] = {
		.struct_name =  "convert_message_struct__avm_event_piglet",
		.type_name   =  "enum _avm_piglettype",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "type",
		.size = sizeof(enum _avm_piglettype),
		.offset = __builtin_offsetof(struct _avm_event_piglet __attribute__ ((packed)), type),
		.enum_debug_function = (char *(*)(unsigned int))get_enum__avm_piglettype_name,
#ifdef WIRESHARK_PLUGIN
		.enumInfo = enum_table__avm_piglettype,
		.enumName = "enum_table__avm_piglettype"
#endif /*--- #ifdef WIRESHARK_PLUGIN ---*/
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'union avm_event_telephony_call_params'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_union_avm_event_telephony_call_params[] = {
	[avm_event_telephony_params_name] = {
		.struct_name =  "convert_message_union_avm_event_telephony_call_params",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "string_avm_event_telephony_params_name",
		.substruct = convert_message_struct_avm_event_telephony_string,
		.offset = __builtin_offsetof(union avm_event_telephony_call_params __attribute__ ((packed)), string)
	},
	[avm_event_telephony_params_msn_name] = {
		.struct_name =  "convert_message_union_avm_event_telephony_call_params",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "string_avm_event_telephony_params_msn_name",
		.substruct = convert_message_struct_avm_event_telephony_string,
		.offset = __builtin_offsetof(union avm_event_telephony_call_params __attribute__ ((packed)), string)
	},
	[avm_event_telephony_params_portname] = {
		.struct_name =  "convert_message_union_avm_event_telephony_call_params",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "string_avm_event_telephony_params_portname",
		.substruct = convert_message_struct_avm_event_telephony_string,
		.offset = __builtin_offsetof(union avm_event_telephony_call_params __attribute__ ((packed)), string)
	},
	[avm_event_telephony_params_tam_path] = {
		.struct_name =  "convert_message_union_avm_event_telephony_call_params",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "string_avm_event_telephony_params_tam_path",
		.substruct = convert_message_struct_avm_event_telephony_string,
		.offset = __builtin_offsetof(union avm_event_telephony_call_params __attribute__ ((packed)), string)
	},
	[avm_event_telephony_params_calling] = {
		.struct_name =  "convert_message_union_avm_event_telephony_call_params",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "number",
		.size = sizeof(unsigned char) * 32,
		.offset = __builtin_offsetof(union avm_event_telephony_call_params __attribute__ ((packed)), number)
	},
	[avm_event_telephony_params_called] = {
		.struct_name =  "convert_message_union_avm_event_telephony_call_params",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "number",
		.size = sizeof(unsigned char) * 32,
		.offset = __builtin_offsetof(union avm_event_telephony_call_params __attribute__ ((packed)), number)
	},
	[avm_event_telephony_params_duration] = {
		.struct_name =  "convert_message_union_avm_event_telephony_call_params",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "duration",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_telephony_call_params __attribute__ ((packed)), duration)
	},
	[avm_event_telephony_params_port] = {
		.struct_name =  "convert_message_union_avm_event_telephony_call_params",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "port",
		.size = sizeof(unsigned char),
		.offset = __builtin_offsetof(union avm_event_telephony_call_params __attribute__ ((packed)), port)
	},
	[avm_event_telephony_params_id] = {
		.struct_name =  "convert_message_union_avm_event_telephony_call_params",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "id",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_telephony_call_params __attribute__ ((packed)), id)
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'union __powermanagment_status_union'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_union___powermanagment_status_union[] = {
	[dsl_status] = {
		.struct_name =  "convert_message_union___powermanagment_status_union",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dsl_status",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union __powermanagment_status_union __attribute__ ((packed)), dsl_status)
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'union avm_event_powermanagment_remote_union'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_union_avm_event_powermanagment_remote_union[] = {
	[avm_event_powermanagment_ressourceinfo] = {
		.struct_name =  "convert_message_union_avm_event_powermanagment_remote_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "ressourceinfo",
		.substruct = convert_message_struct_avm_event_powermanagment_remote_ressourceinfo,
		.offset = __builtin_offsetof(union avm_event_powermanagment_remote_union __attribute__ ((packed)), ressourceinfo)
	},
	[avm_event_powermanagment_activatepowermode] = {
		.struct_name =  "convert_message_union_avm_event_powermanagment_remote_union",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "powermode",
		.size = sizeof(char) * 32,
		.offset = __builtin_offsetof(union avm_event_powermanagment_remote_union __attribute__ ((packed)), powermode)
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'union avm_event_internet_new_ip_param'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_union_avm_event_internet_new_ip_param[] = {
	[avm_event_internet_new_ip_v4] = {
		.struct_name =  "convert_message_union_avm_event_internet_new_ip_param",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "ipv4",
		.size = sizeof(unsigned char) * 4,
		.offset = __builtin_offsetof(union avm_event_internet_new_ip_param __attribute__ ((packed)), ipv4)
	},
	[avm_event_internet_new_ip_v6] = {
		.struct_name =  "convert_message_union_avm_event_internet_new_ip_param",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "ipv6",
		.size = sizeof(unsigned char) * 16,
		.offset = __builtin_offsetof(union avm_event_internet_new_ip_param __attribute__ ((packed)), ipv6)
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'union avm_event_data_union'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_union_avm_event_data_union[] = {
	[avm_event_id_push_button] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "push_button",
		.substruct = convert_message_struct_avm_event_push_button,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), push_button)
	},
	[avm_event_id_ethernet_connect_status] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "cpmac",
		.substruct = convert_message_struct__cpmac_event_struct,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), cpmac)
	},
	[avm_event_id_led_status] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "led_status",
		.substruct = convert_message_struct_avm_event_led_status,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), led_status)
	},
	[avm_event_id_led_info] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "led_info",
		.substruct = convert_message_struct_avm_event_led_info,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), led_info)
	},
	[avm_event_id_telefonprofile] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "telefonprofile",
		.substruct = convert_message_struct_avm_event_telefonprofile,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), telefonprofile)
	},
	[avm_event_id_temperature] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "temperature",
		.substruct = convert_message_struct_avm_event_temperature,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), temperature)
	},
	[avm_event_id_powermanagment_status] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "powermanagment_status",
		.substruct = convert_message_struct_avm_event_powermanagment_status,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), powermanagment_status)
	},
	[avm_event_id_cpu_idle] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "cpu_idle",
		.substruct = convert_message_struct_avm_event_cpu_idle,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), cpu_idle)
	},
	[avm_event_id_powermanagment_remote] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "powermanagment_remote",
		.substruct = convert_message_struct_avm_event_powermanagment_remote,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), powermanagment_remote)
	},
	[avm_event_id_log] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "id_log",
		.substruct = convert_message_struct_avm_event_log,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), id_log)
	},
	[avm_event_id_remotepcmlink] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "id_remotepcmlink",
		.substruct = convert_message_struct_avm_event_remotepcmlink,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), id_remotepcmlink)
	},
	[avm_event_id_remotewatchdog] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "id_remotewatchdog",
		.substruct = convert_message_struct_avm_event_remotewatchdog,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), id_remotewatchdog)
	},
	[avm_event_id_rpc] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "id_rpc",
		.substruct = convert_message_struct_avm_event_rpc,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), id_rpc)
	},
	[avm_event_id_pm_ressourceinfo_status] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "pm_info_stat",
		.substruct = convert_message_struct_avm_event_pm_info_stat,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), pm_info_stat)
	},
	[avm_event_id_wlan_client_status] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "wlan",
		.substruct = convert_message_struct_avm_event_wlan,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), wlan)
	},
	[avm_event_id_wlan_event] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "wlan_event",
		.substruct = convert_message_struct_wlan_event_def,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), wlan_event)
	},
	[avm_event_id_telephony_missed_call] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "telephony_missed_call",
		.substruct = convert_message_struct_avm_event_telephony_missed_call,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), telephony_missed_call)
	},
	[avm_event_id_telephony_tam_call] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "telephony_tam_call",
		.substruct = convert_message_struct_avm_event_telephony_missed_call,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), telephony_tam_call)
	},
	[avm_event_id_telephony_fax_received] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "telephony_fax_received",
		.substruct = convert_message_struct_avm_event_telephony_missed_call,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), telephony_fax_received)
	},
	[avm_event_id_telephony_incoming_call] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "telephony_incoming_call",
		.substruct = convert_message_struct_avm_event_telephony_missed_call,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), telephony_incoming_call)
	},
	[avm_event_id_firmware_update_available] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "firmware_update_available",
		.substruct = convert_message_struct_avm_event_firmware_update_available,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), firmware_update_available)
	},
	[avm_event_id_internet_new_ip] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "internet_new_ip",
		.substruct = convert_message_struct_avm_event_internet_new_ip,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), internet_new_ip)
	},
	[avm_event_id_smarthome_switch_status] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "smarthome_switch_status",
		.substruct = convert_message_struct_avm_event_smarthome_switch_status,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), smarthome_switch_status)
	},
	[avm_event_id_mass_storage_mount] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "mass_storage_mount",
		.substruct = convert_message_struct_avm_event_mass_storage_mount,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), mass_storage_mount)
	},
	[avm_event_id_mass_storage_unmount] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "mass_storage_unmount",
		.substruct = convert_message_struct_avm_event_mass_storage_unmount,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), mass_storage_unmount)
	},
	[avm_event_id_checkpoint] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "checkpoint",
		.substruct = convert_message_struct_avm_event_checkpoint,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), checkpoint)
	},
	[avm_event_id_cpu_run] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "cpu_run",
		.substruct = convert_message_struct_avm_event_cpu_run,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), cpu_run)
	},
	[avm_event_id_ambient_brightness] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "ambient_brightness",
		.substruct = convert_message_struct_avm_event_ambient_brightness,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), ambient_brightness)
	},
	[avm_event_id_fax_status_change] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "fax_status",
		.substruct = convert_message_struct_avm_event_fax_status,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), fax_status)
	},
	[avm_event_id_fax_file] = {
		.struct_name =  "convert_message_union_avm_event_data_union",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "fax_file",
		.substruct = convert_message_struct_avm_event_fax_file,
		.offset = __builtin_offsetof(union avm_event_data_union __attribute__ ((packed)), fax_file)
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'union wlan_event_data'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_union_wlan_event_data[] = {
	[CLIENT_STATE_CHANGE] = {
		.struct_name =  "convert_message_union_wlan_event_data",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "client_state_change",
		.substruct = convert_message_struct_wlan_event_data_client_state_change,
		.offset = __builtin_offsetof(union wlan_event_data __attribute__ ((packed)), client_state_change)
	},
	[CLIENT_CONNECT_INFO] = {
		.struct_name =  "convert_message_union_wlan_event_data",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "client_connect_info",
		.substruct = convert_message_struct_wlan_event_data_client_connect_info,
		.offset = __builtin_offsetof(union wlan_event_data __attribute__ ((packed)), client_connect_info)
	},
	[WLAN_EVENT_SCAN] = {
		.struct_name =  "convert_message_union_wlan_event_data",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "scan_event_info",
		.substruct = convert_message_struct_wlan_event_data_scan_event_info,
		.offset = __builtin_offsetof(union wlan_event_data __attribute__ ((packed)), scan_event_info)
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'union _avm_event_cmd_param'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_union__avm_event_cmd_param[] = {
	[avm_event_cmd_register] = {
		.struct_name =  "convert_message_union__avm_event_cmd_param",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "avm_event_cmd_param_register",
		.substruct = convert_message_struct__avm_event_cmd_param_register,
		.offset = __builtin_offsetof(union _avm_event_cmd_param __attribute__ ((packed)), avm_event_cmd_param_register)
	},
	[avm_event_cmd_release] = {
		.struct_name =  "convert_message_union__avm_event_cmd_param",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "avm_event_cmd_param_release",
		.substruct = convert_message_struct__avm_event_cmd_param_release,
		.offset = __builtin_offsetof(union _avm_event_cmd_param __attribute__ ((packed)), avm_event_cmd_param_release)
	},
	[avm_event_cmd_trigger] = {
		.struct_name =  "convert_message_union__avm_event_cmd_param",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "avm_event_cmd_param_trigger",
		.substruct = convert_message_struct__avm_event_cmd_param_trigger,
		.offset = __builtin_offsetof(union _avm_event_cmd_param __attribute__ ((packed)), avm_event_cmd_param_trigger)
	},
	[avm_event_cmd_source_register] = {
		.struct_name =  "convert_message_union__avm_event_cmd_param",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "avm_event_cmd_param_source_register",
		.substruct = convert_message_struct__avm_event_cmd_param_register,
		.offset = __builtin_offsetof(union _avm_event_cmd_param __attribute__ ((packed)), avm_event_cmd_param_source_register)
	},
	[avm_event_cmd_source_trigger] = {
		.struct_name =  "convert_message_union__avm_event_cmd_param",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "avm_event_cmd_param_source_trigger",
		.substruct = convert_message_struct__avm_event_cmd_param_source_trigger,
		.offset = __builtin_offsetof(union _avm_event_cmd_param __attribute__ ((packed)), avm_event_cmd_param_source_trigger)
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'union avm_event_wlan_client_status_u2'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_union_avm_event_wlan_client_status_u2[] = {
	[INPUT_RADAR] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "radar_freq",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), radar_freq)
	},
	[INPUT_AUTH_1_D] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_AUTH_1_D",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_AUTH_3_D] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_AUTH_3_D",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_DE_AUTH_STATION] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_DE_AUTH_STATION",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_ASSOC_REQ_A] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_ASSOC_REQ_A",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_ASSOC_REQ_D] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_ASSOC_REQ_D",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_RE_ASSOC_REQ_D] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_RE_ASSOC_REQ_D",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_DIS_ASSOC_STATION] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_DIS_ASSOC_STATION",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_AUTH_TIMEOUT] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_AUTH_TIMEOUT",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_DE_AUTH_MNG_UNICAST] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_DE_AUTH_MNG_UNICAST",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_DE_AUTH_MNG_BROADCAST] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_DE_AUTH_MNG_BROADCAST",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_DIS_ASSOC_MNG_UNICAST] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_DIS_ASSOC_MNG_UNICAST",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_DIS_ASSOC_MNG_BROADCAST] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_DIS_ASSOC_MNG_BROADCAST",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_MAC_AUTHORIZE] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_MAC_AUTHORIZE",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_WDS_LINK_UP] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_WDS_LINK_UP",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_WDS_LINK_DOWN] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_WDS_LINK_DOWN",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_MADWIFI_WRONG_PSK] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_MADWIFI_WRONG_PSK",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_WPS] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_WPS",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_MINI] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_MINI",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_WPS_ENROLLEE] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_WPS_ENROLLEE",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_STA] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_STA",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_GREENAP_PS] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_GREENAP_PS",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_EAP_AUTHORIZED] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_EAP_AUTHORIZED",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_MWO_INTERFERENCE] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_MWO_INTERFERENCE",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_AUTH_EXPIRED] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_AUTH_EXPIRED",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_COEXIST_SWITCH] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_COEXIST_SWITCH",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_STA_ASSOC] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_STA_ASSOC",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_STA_AUTH] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_STA_AUTH",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_STA_AUTHORIZATION] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_STA_AUTHORIZATION",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_WDS_NO_TIAGGR] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_WDS_NO_TIAGGR",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_MAX_NODE_REACHED] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_MAX_NODE_REACHED",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_RADAR_DFS_WAIT] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_RADAR_DFS_WAIT",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_AUTH_1_OS_A] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_AUTH_1_OS_A",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_AUTH_1_SK_A] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_AUTH_1_SK_A",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_ASSOC_REQ_CHECK] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_ASSOC_REQ_CHECK",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_ASSOC_REQ_SEC_D] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_ASSOC_REQ_SEC_D",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_RE_ASSOC_REQ_CHECK] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_RE_ASSOC_REQ_CHECK",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_RE_ASSOC_REQ_A] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_RE_ASSOC_REQ_A",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_RE_ASSOC_REQ_SEC_D] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_RE_ASSOC_REQ_SEC_D",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_CLASS_3] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_CLASS_3",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
	[INPUT_FRAME_TX_COMPLETE] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u2",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "wlan_mode_INPUT_FRAME_TX_COMPLETE",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u2 __attribute__ ((packed)), wlan_mode)
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'union avm_event_message_union'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_union_avm_event_message_union[] = {
	[avm_event_source_register_type] = {
		.struct_name =  "convert_message_union_avm_event_message_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "source_register",
		.substruct = convert_message_struct_avm_event_source_register,
		.offset = __builtin_offsetof(union avm_event_message_union __attribute__ ((packed)), source_register)
	},
	[avm_event_source_unregister_type] = {
		.struct_name =  "convert_message_union_avm_event_message_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "source_unregister",
		.substruct = convert_message_struct_avm_event_source_unregister,
		.offset = __builtin_offsetof(union avm_event_message_union __attribute__ ((packed)), source_unregister)
	},
	[avm_event_source_notifier_type] = {
		.struct_name =  "convert_message_union_avm_event_message_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "source_notifier",
		.substruct = convert_message_struct_avm_event_source_notifier,
		.offset = __builtin_offsetof(union avm_event_message_union __attribute__ ((packed)), source_notifier)
	},
	[avm_event_remote_source_trigger_request_type] = {
		.struct_name =  "convert_message_union_avm_event_message_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "remote_source_trigger_request",
		.substruct = convert_message_struct_avm_event_remote_source_trigger_request,
		.offset = __builtin_offsetof(union avm_event_message_union __attribute__ ((packed)), remote_source_trigger_request)
	},
	[avm_event_ping_type] = {
		.struct_name =  "convert_message_union_avm_event_message_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "ping",
		.substruct = convert_message_struct_avm_event_ping,
		.offset = __builtin_offsetof(union avm_event_message_union __attribute__ ((packed)), ping)
	},
	[avm_event_tffs_type] = {
		.struct_name =  "convert_message_union_avm_event_message_union",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "tffs",
		.substruct = convert_message_struct_avm_event_tffs,
		.offset = __builtin_offsetof(union avm_event_message_union __attribute__ ((packed)), tffs)
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'union avm_event_tffs_call_union'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_union_avm_event_tffs_call_union[] = {
	[avm_event_tffs_call_open] = {
		.struct_name =  "convert_message_union_avm_event_tffs_call_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "open",
		.substruct = convert_message_struct_avm_event_tffs_open,
		.offset = __builtin_offsetof(union avm_event_tffs_call_union __attribute__ ((packed)), open)
	},
	[avm_event_tffs_call_close] = {
		.struct_name =  "convert_message_union_avm_event_tffs_call_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "close",
		.substruct = convert_message_struct_avm_event_tffs_close,
		.offset = __builtin_offsetof(union avm_event_tffs_call_union __attribute__ ((packed)), close)
	},
	[avm_event_tffs_call_read] = {
		.struct_name =  "convert_message_union_avm_event_tffs_call_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "read",
		.substruct = convert_message_struct_avm_event_tffs_read,
		.offset = __builtin_offsetof(union avm_event_tffs_call_union __attribute__ ((packed)), read)
	},
	[avm_event_tffs_call_write] = {
		.struct_name =  "convert_message_union_avm_event_tffs_call_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "write",
		.substruct = convert_message_struct_avm_event_tffs_write,
		.offset = __builtin_offsetof(union avm_event_tffs_call_union __attribute__ ((packed)), write)
	},
	[avm_event_tffs_call_cleanup] = {
		.struct_name =  "convert_message_union_avm_event_tffs_call_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "cleanup",
		.substruct = convert_message_struct_avm_event_tffs_cleanup,
		.offset = __builtin_offsetof(union avm_event_tffs_call_union __attribute__ ((packed)), cleanup)
	},
	[avm_event_tffs_call_reindex] = {
		.struct_name =  "convert_message_union_avm_event_tffs_call_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "reindex",
		.substruct = convert_message_struct_avm_event_tffs_reindex,
		.offset = __builtin_offsetof(union avm_event_tffs_call_union __attribute__ ((packed)), reindex)
	},
	[avm_event_tffs_call_info] = {
		.struct_name =  "convert_message_union_avm_event_tffs_call_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "info",
		.substruct = convert_message_struct_avm_event_tffs_info,
		.offset = __builtin_offsetof(union avm_event_tffs_call_union __attribute__ ((packed)), info)
	},
	[avm_event_tffs_call_init] = {
		.struct_name =  "convert_message_union_avm_event_tffs_call_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "init",
		.substruct = convert_message_struct_avm_event_tffs_init,
		.offset = __builtin_offsetof(union avm_event_tffs_call_union __attribute__ ((packed)), init)
	},
	[avm_event_tffs_call_deinit] = {
		.struct_name =  "convert_message_union_avm_event_tffs_call_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "deinit",
		.substruct = convert_message_struct_avm_event_tffs_deinit,
		.offset = __builtin_offsetof(union avm_event_tffs_call_union __attribute__ ((packed)), deinit)
	},
	[avm_event_tffs_call_notify] = {
		.struct_name =  "convert_message_union_avm_event_tffs_call_union",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "notify",
		.substruct = convert_message_struct_avm_event_tffs_notify,
		.offset = __builtin_offsetof(union avm_event_tffs_call_union __attribute__ ((packed)), notify)
	},
	[avm_event_tffs_call_paniclog] = {
		.struct_name =  "convert_message_union_avm_event_tffs_call_union",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "paniclog",
		.substruct = convert_message_struct_avm_event_tffs_paniclog,
		.offset = __builtin_offsetof(union avm_event_tffs_call_union __attribute__ ((packed)), paniclog)
	},
};

/*------------------------------------------------------------------------------------------*\
 * Endian Convert-Table for: 'union avm_event_wlan_client_status_u1'
\*------------------------------------------------------------------------------------------*/
struct _endian_convert convert_message_union_avm_event_wlan_client_status_u1[] = {
	[INPUT_RADAR_DFS_WAIT] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "sub_event",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), sub_event)
	},
	[INPUT_MAC_AUTHORIZE] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "active_rate",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), active_rate)
	},
	[INPUT_EAP_AUTHORIZED] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "active_rate1",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), active_rate1)
	},
	[INPUT_MADWIFI_WRONG_PSK] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "active_rate2",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), active_rate2)
	},
	[INPUT_AUTH_EXPIRED] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "active_rate3",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), active_rate3)
	},
	[INPUT_STA] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "active_rate4",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), active_rate4)
	},
	[INPUT_WDS_LINK_UP] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "active_rate5",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), active_rate5)
	},
	[INPUT_WDS_LINK_DOWN] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "active_rate6",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), active_rate6)
	},
	[INPUT_RADAR] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "radar_chan",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), radar_chan)
	},
	[INPUT_GREENAP_PS] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "green_ap_ps_state",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), green_ap_ps_state)
	},
	[INPUT_COEXIST_SWITCH] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "coexist_ht40_state",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), coexist_ht40_state)
	},
	[INPUT_MAX_NODE_REACHED] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "max_node_count",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), max_node_count)
	},
	[INPUT_INTERFERENCE_CHAN_CHANGE] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | 0,
		.name = "channel",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), channel)
	},
	[INPUT_AUTH_1_OS_A] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_AUTH_1_OS_A",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_AUTH_1_SK_A] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_AUTH_1_SK_A",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_AUTH_1_D] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_AUTH_1_D",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_AUTH_3_A] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_AUTH_3_A",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_AUTH_3_D] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_AUTH_3_D",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_DE_AUTH_STATION] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_DE_AUTH_STATION",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_ASSOC_REQ_CHECK] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_ASSOC_REQ_CHECK",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_ASSOC_REQ_A] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_ASSOC_REQ_A",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_ASSOC_REQ_D] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_ASSOC_REQ_D",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_ASSOC_REQ_SEC_D] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_ASSOC_REQ_SEC_D",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_RE_ASSOC_REQ_CHECK] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_RE_ASSOC_REQ_CHECK",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_RE_ASSOC_REQ_A] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_RE_ASSOC_REQ_A",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_RE_ASSOC_REQ_D] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_RE_ASSOC_REQ_D",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_RE_ASSOC_REQ_SEC_D] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_RE_ASSOC_REQ_SEC_D",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_DIS_ASSOC_STATION] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_DIS_ASSOC_STATION",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_CLASS_3] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_CLASS_3",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_AUTH_TIMEOUT] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_AUTH_TIMEOUT",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_DE_AUTH_MNG_UNICAST] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_DE_AUTH_MNG_UNICAST",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_DE_AUTH_MNG_BROADCAST] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_DE_AUTH_MNG_BROADCAST",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_DIS_ASSOC_MNG_UNICAST] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_DIS_ASSOC_MNG_UNICAST",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_DIS_ASSOC_MNG_BROADCAST] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_DIS_ASSOC_MNG_BROADCAST",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_FRAME_TX_COMPLETE] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_FRAME_TX_COMPLETE",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_STA_ASSOC] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_STA_ASSOC",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_STA_AUTH] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_STA_AUTH",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_STA_AUTHORIZATION] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_STA_AUTHORIZATION",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
	[INPUT_WDS_NO_TIAGGR] = {
		.struct_name =  "convert_message_union_avm_event_wlan_client_status_u1",
		.flags = ENDIAN_CONVERT_LAST | ENDIAN_CONVERT_LAST_ARRAY_ITEM | 0,
		.name = "dummy0_INPUT_WDS_NO_TIAGGR",
		.size = sizeof(unsigned int),
		.offset = __builtin_offsetof(union avm_event_wlan_client_status_u1 __attribute__ ((packed)), dummy0)
	},
};
