#include "avm_event_gen_endian_external_defines.h"
#include "avm_event_gen_enum_range.h"

void init_enum_check(void) {
	convert_message_struct_avm_event_internet_new_ip[0].enum_check_function = (unsigned int (*)(unsigned int))avm_event_check_enum_range_avm_event_internet_new_ip_param_sel;
	convert_message_struct_wlan_event_def[0].enum_check_function = (unsigned int (*)(unsigned int))avm_event_check_enum_range_wlan_event_id;
	convert_message_struct__avm_event_telephony_missed_call_params[0].enum_check_function = (unsigned int (*)(unsigned int))avm_event_check_enum_range_avm_event_telephony_param_sel;
	convert_message_struct_avm_event_message[7].enum_check_function = (unsigned int (*)(unsigned int))avm_event_check_enum_range_avm_event_msg_type;
	convert_message_struct_avm_event_tffs[7].enum_check_function = (unsigned int (*)(unsigned int))avm_event_check_enum_range_avm_event_tffs_call_type;
	convert_message_struct_avm_event_data[0].enum_check_function = (unsigned int (*)(unsigned int))avm_event_check_enum_range__avm_event_id;
	convert_message_struct__avm_event_powermanagment_remote[1].enum_check_function = (unsigned int (*)(unsigned int))avm_event_check_enum_range_avm_event_powermanagment_remote_action;
	convert_message_struct_avm_event_powermanagment_status[0].enum_check_function = (unsigned int (*)(unsigned int))avm_event_check_enum_range__powermanagment_status_type;
	convert_message_struct_avm_event_powermanagment_remote[0].enum_check_function = (unsigned int (*)(unsigned int))avm_event_check_enum_range_avm_event_powermanagment_remote_action;
	convert_message_struct__avm_event_internet_new_ip[1].enum_check_function = (unsigned int (*)(unsigned int))avm_event_check_enum_range_avm_event_internet_new_ip_param_sel;
	convert_message_struct__avm_event_powermanagment_status[1].enum_check_function = (unsigned int (*)(unsigned int))avm_event_check_enum_range__powermanagment_status_type;
}
