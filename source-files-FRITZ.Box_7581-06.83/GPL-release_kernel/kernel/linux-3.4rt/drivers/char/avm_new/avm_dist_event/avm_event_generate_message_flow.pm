#! /usr/bin/perl -w
package avm_event_generate_message_flow;

use strict;
use warnings;
use avm_event_generate_struct;

use Exporter ();
our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
$VERSION     = 1.00;

@ISA = qw(Exporter);
@EXPORT = qw(&message_flow_erzeugen);
@EXPORT_OK = qw();

our %struct;

my %module;
my %messages;
my %groups;
my %printed;

my @drivers = ( "dectule", "network", "prolific", "button", "led" );
my @function = ( "power_meter", "relay", "statistic" );
my @config = ( "socket", "config" );

sub get_module_group {
    my ( $name ) = @_;

    for my $d ( @drivers ) {
        if($d eq $name) {
            return "drivers";
        }
    }
    for my $f ( @function ) {
        if($f eq $name) {
            return "function";
        }
    }
    for my $c ( @config ) {
        if($c eq $name) {
            return "config";
        }
    }
    return "other";
}

sub message_flow_erzeugen {
    my ( $conn_group ) = $@;
    my $handle;

    if(defined($conn_group)) {
        open $handle,  ">message_flow_" . $conn_group . ".dot";
    } else {
        open $handle,  ">message_flow_all.dot";
    }

    foreach my $i ( keys %struct ) {
        if(not defined($struct{$i}->{message})) {
            next;
        }
        foreach my $ii ( @{$struct{$i}->{message}->{connections}} ) {
            if(defined($conn_group) and defined($ii->{group})) {
                if($ii->{group} ne $conn_group) {
                    next;
                }
            }
            my $group_from = get_module_group($ii->{from});
            if(not defined($module{$ii->{from}})) {
                $module{$ii->{from}} = { "name" => $ii->{from}, "group" => $group_from };
                if(not defined($groups{$group_from})) {
                    $groups{$group_from} = [];
                }
                push @{$groups{$group_from}}, $ii->{from};
            }
            my $group_to = get_module_group($ii->{to});
            if(not defined($module{$ii->{to}})) {
                $module{$ii->{to}} = { "name" => $ii->{to}, "group" => $group_to };
                if(not defined($groups{$group_to})) {
                    $groups{$group_to} = [];
                }
                push @{$groups{$group_to}}, $ii->{to};
            }
        }
    }
    print $handle "digraph structs {\n";
    print $handle "\tcompound=true;\n";
#	print $handle "\trankdir=TB;\n";
    print $handle "\trankdir=LR;\n";
#	print $handle "\tsamehead=true;\n";
#	print $handle "\tsametail=true;\n";
#	print $handle "\tconcentrate=true;\n";
#	print $handle "\tnode [shape=record];\n";
    print "Module: ";
    foreach my $g ( keys %groups ) {
        print $handle "\tsubgraph cluster_" . $g . " {\n";
        print $handle "\t\tfontcolor=\"red\";\n";
        print $handle "\t\tlabel=\"Modulgruppe " . $g . "\";\n";
        foreach my $m ( @{$groups{$g}} ) {
            my $name = $module{$m}->{name};
            print $handle "\t\t" . $name . " [label=\"" . $name . "\"];\n";

            foreach my $i ( keys %struct ) {
                if(not defined($struct{$i}->{message})) {
                    next;
                }
                foreach my $ii ( @{$struct{$i}->{message}->{connections}} ) {
                    my $group_to = get_module_group($ii->{to});
                    my $group_from = get_module_group($ii->{from});
                    if(($group_to eq $group_from) and ($group_from eq $g)) {
                        if(not defined($printed{$ii->{from} . "_" . $ii->{to}})) {
                            $printed{$ii->{from} . "_" . $ii->{to}} = "yes";
                            print $handle "\t\t/*--- group_to: " . $group_to . " group_from: " . $group_from . " ---*/\n";
                            print $handle "\t\t" . $ii->{from} . " -> " . $ii->{to} . "";
                            if(defined($ii->{description})) {
                                print $handle " [";
                                print $handle "fontcolor=\"green\",fontsize=10,label=\"" . $ii->{description} . "\"";
                                print $handle "]";
                            }
                            print $handle ";\n";
                        }
                    }
                }
            }
        }
        print $handle "\t};\n";
    }
    print "\n";

    foreach my $i ( keys %struct ) {
        if(not defined($struct{$i}->{message})) {
            next;
        }
        foreach my $ii ( @{$struct{$i}->{message}->{connections}} ) {
            if(defined($printed{$ii->{from} . "_" . $ii->{to}})) {
                next;
            }
            $printed{$ii->{from} . "_" . $ii->{to}} = "yes";
            print $handle "\t" . $ii->{from} . " -> " . $ii->{to};

            if(defined($ii->{description})) {
                print $handle " [";
                print $handle "fontcolor=\"green\",fontsize=10,label=\"" . $ii->{description} . "\"";
                print $handle "]";
            }
            print $handle ";\n";
        }
    }
    print $handle "}\n";
}
	
	


1;
