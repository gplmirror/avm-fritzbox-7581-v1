/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
#ifndef __arch_profile_cortexa9_h__
#define __arch_profile_cortexa9_h__

#include <clocksource/qcom-qtimer.h>
#include <mach/avm_fiq.h>
#include <mach/avm_gic_smc.h>
#include <mach/avm_gic_fiq.h>
#include <soc/qcom/scm.h>
#include <asm/performance.h>

#if defined(CONFIG_AVM_FASTIRQ)
#define PROFILING_IN_FIQ
#endif
#define PROFILING_USEC_TO_TIMER(usec) ((usec) * 48)

#define PROFILING_MAX_PERF_REGISTER       (4 + 1)                             /* we treat the cycle counter like a performance counter */ 
#define PROFILING_PERF_REGISTERMASK       ((1 << 4) - 1)                      /* alle "echten" Performance-Register */ 
#define PROFILING_CYCLE_REGISTER          (PROFILING_MAX_PERF_REGISTER - 1) 


#define PROFILING_TRIGGER_SHIFT      10
#define PROFILING_TRIGGER_MASK       ((1 << PROFILING_TRIGGER_SHIFT) - 1)     /*--- Range: 1024 us ---*/

#include <mach/avm_fiq_os.h>
#include <mach/avm_gic_fiq.h>
#include <mach/avm_gic.h>

/*--------------------------------------------------------------------------------*\
 * Liste mit Round-Robin-Performance-Counter pro CPU
\*--------------------------------------------------------------------------------*/
struct _roundrobin_perf_ctrlstat {
    unsigned int perf_ctrl[PROFILING_MAX_PERF_REGISTER];
    unsigned long long sum_perf_time[PROFILING_MAX_PERF_REGISTER];
    unsigned long long sum_perf_count[PROFILING_MAX_PERF_REGISTER];
    const char *prefix;
    struct _roundrobin_perf_ctrlstat *next;
};

extern const struct _roundrobin_perf_ctrlstat roundrobin_performance[];
unsigned int array_size_roundrobin_performance(void);

enum _norm_factor { NORMED_BY_FREQUENCY = 0, NORMED_BY_CYCLE, NORMED_BY_INSTRUCTION, NORMED_MAX };

struct _perfcount_options {
    char *name;
    enum _norm_factor norm_factor;
};

extern const struct _perfcount_options performance_counter_options[PM_EVENT_LAST];

#define INITIAL_EVENT_CNT_SETUP()                                                             \
        arm_performance_counter_action("set 0 19");  /*--- PM_EVENT_MEM_ACCESS ---*/    \
        arm_performance_counter_action("set 1 20");  /*--- PM_EVENT_L1I_CACHE ---*/     \
        arm_performance_counter_action("set 2 201"); /*--- PM_EVENT_DATA_WRITE_STALL ---*/  \
        arm_performance_counter_action("set 3 8");   /*--- PM_EVENT_INST_RETIRED ---*/

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static const struct _cpucore_profile arm_cpu_config[4] = {
    { cpu_nr_offset: 0, vpe_nr: 1, next_core: &arm_cpu_config[1] },
    { cpu_nr_offset: 1, vpe_nr: 1, next_core: &arm_cpu_config[2] },
    { cpu_nr_offset: 2, vpe_nr: 1, next_core: &arm_cpu_config[3] },
    { cpu_nr_offset: 3, vpe_nr: 1, next_core: NULL },
};

#define PROFILING_PERFORMANCE_COUNTER_SUPPORT
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline void arch_profile_perfcnt_on(unsigned int on) {
    union __performance_monitor_control C;

     if(on){
        C.Register = read_p15_performance_monitor_control();
        C.Bits.CycleCounterDivider = 0; 
        C.Bits.EnableBit           = 1;
        write_p15_performance_monitor_control(C.Register);
        write_p15_performance_count_enable(0x8000000F);
        p15_reset_performance_counter(0);
        p15_reset_performance_counter(1);
        p15_reset_performance_counter(2);
        p15_reset_performance_counter(3);
        write_p15_cycle_counter(0);
    } else {
        C.Register = read_p15_performance_monitor_control();
        C.Bits.EnableBit      = 0;
        write_p15_performance_monitor_control(C.Register);
        write_p15_performance_count_enable(0);
    }
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline unsigned int arch_profile_perfcnt1(void) {
    return read_p15_performance_counter(0);
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline unsigned int arch_profile_perfcnt2(void) {
    return read_p15_performance_counter(1);
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline int arch_is_linux_cpu(unsigned int core __maybe_unused, unsigned int tc __maybe_unused) {
    return 1;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static struct _qcom_timer_firq {
    const qtimer_ipq40xx qtim;
    const int            fiq;
    const char          *fiq_name;
    irqreturn_t          (*firqfunc)(int irq, void *handle);
    void *handle;
    int enable_id;
} qcom_timer_firq[NR_CPUS] =  {
    [0] = { qtim: qtim3_v1, fiq: 43, fiq_name: "qtim3", firqfunc: NULL, handle: NULL, enable_id: -1},
    [1] = { qtim: qtim4_v1, fiq: 44, fiq_name: "qtim4", firqfunc: NULL, handle: NULL, enable_id: -1},
    [2] = { qtim: qtim5_v1, fiq: 45, fiq_name: "qtim5", firqfunc: NULL, handle: NULL, enable_id: -1},
    [3] = { qtim: qtim6_v1, fiq: 46, fiq_name: "qtim6", firqfunc: NULL, handle: NULL, enable_id: -1},
};

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline int arch_setup_timer_firq(int cpu, 
                                        irqreturn_t (*lfirqfunc)(int irq, void *handle),
                                        void *handle) {
    struct _qcom_timer_firq *pfirq = &qcom_timer_firq[cpu];
    /*--- printk(KERN_INFO"%s: %u %pS %p\n", __func__, cpu, lfirqfunc, handle); ---*/
    if(pfirq->enable_id < 0) {
       pfirq->firqfunc  = lfirqfunc;
       pfirq->handle    = handle;
       if(!avm_request_fiq_on(cpu, pfirq->fiq, pfirq->firqfunc, FIQ_EDGE, pfirq->fiq_name, pfirq->handle)){
           /*--- printk(KERN_INFO"%s: %u %pS %p success\n", __func__, cpu, lfirqfunc, handle); ---*/
           pfirq->enable_id = cpu;
           qtimer_ipq40xx_enable(pfirq->qtim, 1, 0);
       }
    }
    return pfirq->enable_id;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline void arch_free_timer_firq(int id, void *handle) {
    struct _qcom_timer_firq *pfirq = &qcom_timer_firq[id];
    int cpu;

    if((id < 0) || (id >= NR_CPUS) || (pfirq->enable_id != id)) {
        printk(KERN_ERR"%s: false id=%u %p\n", __func__, id, handle);
        return;
    }
    cpu              = pfirq->enable_id;
    pfirq->enable_id = -1;
    qtimer_ipq40xx_enable(pfirq->qtim, 0, 0);
    avm_free_fiq_on(cpu, pfirq->fiq, handle);
    /*--- printk(KERN_INFO"%s: %u %p done\n", __func__, id, handle); ---*/
}
/*--------------------------------------------------------------------------------*\
 * timer in usec
\*--------------------------------------------------------------------------------*/
static inline void arch_set_next_trigger(unsigned int next_us, int id) {
    struct _qcom_timer_firq *pfirq = &qcom_timer_firq[id];

    if((id < 0) || (id >= NR_CPUS) || (pfirq->enable_id != id)) {
        /*--- printk(KERN_ERR"%s: false id=%u\n", __func__, id); ---*/
        return;
    }
    qtimer_ipq40xx_set_next_event(pfirq->qtim, PROFILING_USEC_TO_TIMER(next_us));
}
#endif/*--- #ifndef __arch_profile_cortexa_h__ ---*/
