/*-
 * Copyright (c) 1992, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)extern.h	8.2 (Berkeley) 4/4/94
 */

#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/socket.h>


#ifndef NO_FTPS_SUPPORT
#include <openssl/ssl.h>
#include <openssl/err.h>
#endif

extern void cwd            __P ((const char *));
extern int  checkuser      __P ((const char *filename, const char *name));
extern void delete         __P ((const char *));
extern int  display_file   __P ((const char *name, int code));
extern void dologout       __P ((int));
extern void fatal          __P ((const char *));
extern int  ftpd_pclose    __P ((FILE *));
extern char *ftpd_popen_escape    __P ((const char *));
extern FILE *ftpd_popen    __P ((char *, const char *));
#if !defined(HAVE_DECL_GETUSERSHELL) || HAVE_DECL_GETUSERSHELL == 0
extern char *getusershell  __P ((void));
#endif
/* @see logwtmpko.c in libinetutils */
extern void logwtmp_keep_open __P ((char *, char *, char *));
extern void logwtmp        __P ((const char *, const char *, const char *));
extern void lreply         __P ((int, const char *, ...));
extern void makedir        __P ((const char *));
extern void nack           __P ((const char *));
extern void pass           __P ((const char *));
extern void passive        __P ((void));
extern void perror_reply   __P ((int, const char *));
extern void pwd            __P ((void));
extern void removedir      __P ((const char *));
extern void renamecmd      __P ((const char *, const char *));
extern char *renamefrom    __P ((const char *));
extern void reply          __P ((int, const char *, ...));
extern void retrieve       __P ((const char *, const char *));
extern void send_file_list __P ((const char *));
extern void setproctitle   __P ((const char *, ...));
extern void statcmd        __P ((void));
extern void statfilecmd    __P ((const char *));
extern void store          __P ((const char *, const char *, int));
extern void toolong        __P ((int));
extern char *get_controlconnection_line  __P ((char *, int));
extern void upper          __P ((char *));
extern void user           __P ((const char *));
extern char *sgetsave      __P ((const char *));
extern void	sizecmd        __P ((const char *));

#ifdef PUMA6_ATOM
extern int avmclient __P ((const char *));
#endif

extern void	quota        __P ((void));

/* Exported from ftpd.c.  */
jmp_buf  errcatch;
extern struct sockaddr_storage data_dest;
extern struct sockaddr_storage his_addr;
extern int g_extended_passive_all;
extern int logging;
extern int type;
extern int form;
extern int debug;
extern int timeout;
extern int maxtimeout;
extern int pdata;
extern char *hostname;
// extern char *remotehost;
extern char proctitle[];
extern int usedefault;
extern char tmpline[];
#if 1 /* FRITZBOX */
extern int max_clients;
#endif
#ifndef NO_RFC2640_SUPPORT
extern int g_utf8;
#endif

#ifndef NO_FTPS_SUPPORT
extern int g_PBSZ_Received;
#endif

extern unsigned short sockaddr_port __P ((struct sockaddr_storage *ss));
extern char *sockaddr_addr2string __P ((struct sockaddr_storage *ss));
extern struct sockaddr_storage *numericaladdr2sockaddr_storage __P ((const int family, const char *addr));

#if defined(PUMA6_ARM) || defined(PUMA6_ATOM)
extern int string_to_sockaddr_storage __P ((const char *str, struct sockaddr_storage *addr));
#endif

/* AVM */
extern int is_same_addr      __P ((struct sockaddr_storage *ss1, struct sockaddr_storage *ss2));
extern void extended_port    __P ((char *param));
extern void extended_passive __P ((unsigned net_prot));
extern void passive_ex       __P ((int extended, int family));
#ifndef NO_FTPS_SUPPORT
extern void clear_ssl_errors __P ((void));
extern void Log_SSL_Errors   __P ((void));
#endif

/* Exported from ftpcmd.y.  */
extern off_t restart_point;
extern int yyparse           __P ((void));

/* Exported from server_mode.c.  */
extern int server_mode __P ((const char *pidfile,
			     struct sockaddr_storage *phis_addr));

/* Credential for the request.  */
struct credentials
{
  char *display_name;
  char *name;
  char *homedir;
  char *rootdir;
  char *shell;
  char *remotehost;
  char *passwd;
  char *pass;
  char *message; /* Sending back custom messages.  */
  uid_t uid;
  gid_t gid;
  int guest;
  int dochroot;
  int logged_in;
#define AUTH_TYPE_PASSWD    0
#define AUTH_TYPE_PAM       1
#define AUTH_TYPE_KERBEROS  2
#define AUTH_TYPE_KERBEROS5 3
#define AUTH_TYPE_OPIE      4
  int auth_type;
#if 1 /* FRITZBOX */
  short access_from_internet; // client access from the internet or local lan
  short is_anonymous;       // anyone is mapped to the friendly username "ftpuser"
  char remote_addr_string[39+1];
#endif
};

extern struct credentials cred;
extern int  sgetcred       __P ((const char *, const char *, struct credentials *));
extern int  auth_user      __P ((const char *, const char *, struct credentials *));
extern int  auth_pass      __P ((const char *, struct credentials *));

extern int IsPrePERF12CompatibilityMode(void);
extern int IsSkipAuthenticationFromHomenetwork(char **pskip_username);

/* Exported from pam.c */
#ifdef WITH_PAM
extern int  pam_user       __P ((const char *, struct credentials *));
extern int  pam_pass       __P ((const char *, struct credentials *));
#endif

#ifndef NO_FTPS_SUPPORT
extern void SetControlConnectionSecurity __P ((void));
extern void SetDataConnectionSecurity __P ((void));

struct SecuredConnection {
	SSL_CTX *ctx;
	SSL *ssl;
};

extern struct SecuredConnection *g_pControlConnectionSecurity;

extern int g_bInternetFTPSOnly;

extern int g_bSecureDataConnection;
extern struct SecuredConnection *g_pDataConnectionSecurity;

extern unsigned short g_OpenedIPv4Port;

#endif // NO_FTPS_SUPPORT

#ifdef USE_IPV6
extern unsigned short g_OpenedIPv6Port;
#endif

void control_putc(char c);
void control_putchar(char c);
int control_vprintf(char *fmt, va_list ap);
int control_printf(char *fmt, ...);
void control_flush(void);
int control_ferror(void);


void data_putc(char c);
ssize_t data_write(const void *buf, size_t count);
ssize_t data_read(void *buf, size_t count);
void data_flush(void);
void data_fclose(void);
int data_get_fd(void);
int data_ferror(void);
int data_getc(void);
int data_printf(char *fmt, ...);

int pattern_too_complex(const char *pattern);

void passive_firewall_close(void);


// #define FTPD_DEBUG

#ifdef FTPD_DEBUG
#define Log(x) _Log x
extern void _Log(char *fmt, ...);
#define hex_dump(a,b,c) _hex_dump(a,b,c)
extern void _hex_dump(const char *hint, unsigned char *data, size_t siz);
#else
#define Log(x)
#define hex_dump(a,b,c)
#endif
