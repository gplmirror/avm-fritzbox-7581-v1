#ifdef PUMA6_ATOM

#include <stdio.h>
#include <stdlib.h>

#include <netinet/in.h>

#include "extern.h"
#include "dataconn_proxy.h"


static unsigned g_atom_port = 0;


int dataconn_proxy_create(struct sockaddr_storage *atom_addr, unsigned *parm_port)
{
	int ret = -1;
	
	dataconn_proxy_destroy();
	
	char cmd[128];
	
	snprintf(cmd, sizeof(cmd), "/bin/tcpproxy_ctl create %u %s", sockaddr_port(atom_addr),
			sockaddr_addr2string(atom_addr));
	cmd[sizeof(cmd)-1] = '\0';
	FILE *fp = popen(cmd, "r");
	if (!fp) {
Log(("%s popen '%s' failed", __FUNCTION__, cmd));	
		return -1;
	} else {
Log(("%s popen '%s' ok", __FUNCTION__, cmd));	
	}
	while(cmd == fgets(cmd, sizeof(cmd), fp)) {
		cmd[sizeof(cmd)-1] = '\0';
Log(("%s got line %s", __FUNCTION__, cmd));	
		if (1 == sscanf(cmd, "created %u", parm_port) && *parm_port != 0) {
			ret = 0;
		}
	}
	
	int status = pclose(fp);
Log(("%s pclose status is %d 0x%x", __FUNCTION__, status, status));	

	return ret;
}

void dataconn_proxy_destroy(void)
{
	if (0 != g_atom_port) {
		char cmd[128];
		
		snprintf(cmd, sizeof(cmd), "/bin/tcpproxy_ctl destroy %u", g_atom_port);
		cmd[sizeof(cmd)-1] = '\0';
		FILE *fp = popen(cmd, "r");
		if (!fp) Log(("%s popen %s failed", __FUNCTION__, cmd));	
		
		if (fp) pclose(fp);
		g_atom_port = 0;
	}
}

#endif // PUMA6_ATOM
