/*
 * Copyright (c) 2010
 * by AVM GmbH Berlin, Germany
 *
 * Licence: Free, use with no restriction.
 */


#ifndef __CHARENCODING_H__
#define __CHARENCODING_H__

size_t ConvertSingleUnicodeCodepointTo_UTF8_WithFallback(unsigned long codepoint, unsigned char *buf, size_t max_buf, unsigned long fallback_codepoint);

unsigned long ConvertSingle_UTF8_ToUnicodeCodepoint(unsigned char *buf, size_t max_buf, /*out */size_t *pBytesConverted);

char *ConvertStringFromISO8859_1ToUTF8_WithAlloc(char *iso);

int ConvertStringFromUTF8ToISO8859_1_With_Fallback(char *utf8, char fallback_iso_char);


#endif // __CHARENCODING_H__
